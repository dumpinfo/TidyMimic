#include "SceneKinChar.h"

cSceneKinChar::cSceneKinChar()
{
}

cSceneKinChar::~cSceneKinChar()
{
}

void cSceneKinChar::Init()
{
	bool succ = BuildCharacters();
}

 

void cSceneKinChar::Update(double time_elapsed)
{
	UpdateCharacters(time_elapsed);
}

const std::shared_ptr<cKinCharacter>& cSceneKinChar::GetCharacter() const
{
	return mChar;
}

tVector cSceneKinChar::GetCharPos() const
{
	return GetCharacter()->GetRootPos();
}

 

std::string cSceneKinChar::GetName() const
{
	return "Kinematic Char";
}

 

bool cSceneKinChar::BuildCharacters()
{
	mChar.reset();

	auto& params = mCharParams;
	params.mID = 0;
	params.mLoadDrawShapes = true;

	bool succ = BuildCharacter(params, mChar);

	return succ;
}

bool cSceneKinChar::BuildCharacter(const cKinCharacter::tParams& params, std::shared_ptr<cKinCharacter>& out_char) const
{
	out_char = std::shared_ptr<cKinCharacter>(new cKinCharacter());
	bool succ = out_char->Init(params);
	return succ;
}

void cSceneKinChar::ResetCharacters()
{
	mChar->Reset();
}

void cSceneKinChar::UpdateCharacters(double timestep)
{
	mChar->Update(timestep);
}

