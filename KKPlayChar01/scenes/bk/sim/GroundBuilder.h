#pragma once

#include "sim/Ground.h"

class cGroundBuilder
{
public:

	static bool ParseParamsJson(const std::string& param_file, cGround::tParams& out_params);
	 
protected:

	static bool ParseParamsJason(cGround::eType type, const Json::Value& json, Eigen::VectorXd& out_params);
	 
};