#pragma once

#include <string>

#include "util/MathUtil.h"
#include "util/ArgParser.h"
#include "util/Timer.h"
#include "scenes/ActionSpace.h"
#include "anim/KinCharacter.h"

class cSceneKinChar// : virtual public cScene
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	cSceneKinChar();
	virtual ~cSceneKinChar();

	virtual void Init();
	 

	virtual void Update(double time_elapsed);

	virtual const std::shared_ptr<cKinCharacter>& GetCharacter() const;
	virtual tVector GetCharPos() const;
	 

	virtual std::string GetName() const;
//protected:
	cKinCharacter::tParams mCharParams;
	std::shared_ptr<cKinCharacter> mChar;
	
	 
	virtual bool BuildCharacters();
	virtual bool BuildCharacter(const cKinCharacter::tParams& params, std::shared_ptr<cKinCharacter>& out_char) const;
	virtual void ResetCharacters();
	virtual void UpdateCharacters(double timestep);
	std::shared_ptr<cArgParser> mArgParser;
	cTimer::tParams mTimerParams;
	cTimer mTimer;
	virtual void Reset() {};
};