#include "DeepMimicCore.h"

#include "render/DrawUtil.h"
//#include "scenes/SceneBuilder.h"
#include "scenes/DrawSceneKinChar.h"

cDeepMimicCore::cDeepMimicCore(bool enable_draw)
{
	mArgParser = std::shared_ptr<cArgParser>(new cArgParser());
	cDrawUtil::EnableDraw(enable_draw);

	mNumUpdateSubsteps = 1;
	mPlaybackSpeed = 1;
	mUpdatesPerSec = 0;
}

cDeepMimicCore::~cDeepMimicCore()
{
}

void cDeepMimicCore::SeedRand(int seed)
{
	cMathUtil::SeedRand(seed);
}

 void cDeepMimicCore::SetKinCharFile(char* file_name)
{
	// bool succ = mArgParser->LoadFile(file_name);
	 mArgParser->ParseInt("num_update_substeps", mNumUpdateSubsteps);
}
//void cDeepMimicCore::ParseArgs(const std::vector<std::string>& args)
//{
//	 std::string arg_file = "args/bvh_walker.txt";
//	 bool succ = mArgParser->LoadFile(arg_file);
//		if (!succ)
//		{
//			printf("Failed to load args from: %s\n", arg_file.c_str());
//			assert(false);
//		}
//	 
//
//	mArgParser->ParseInt("num_update_substeps", mNumUpdateSubsteps);
//}

void cDeepMimicCore::Init(char*motionfle)
{
	
	cDrawUtil::InitDrawUtil();
	SetupScene(motionfle);
}

void cDeepMimicCore::Update(double timestep)
{
	//printf("Update TIME----->%.3lf, (%.3lf)\n", timestep, mScene->GetTime());
	mScene->Update(timestep);
}

void cDeepMimicCore::Reset()
{
        printf("WARNING! WARNING! WARNING! WARNING!_____Reset Reset Reset!!!!\n");
	mScene->Reset();
	mUpdatesPerSec = 0;
}

double cDeepMimicCore::GetTime() const
{ 
	/*double time1 = mScene->GetTime();
	printf("TIME----->%.3lf\n", time1);
	return mScene->GetTime();*/
	return 0;
}

std::string cDeepMimicCore::GetName() const
{
	return mScene->GetName();
}

bool cDeepMimicCore::EnableDraw() const
{
	return cDrawUtil::EnableDraw();
}

void cDeepMimicCore::Draw()
{
	 mScene->Draw();
	 
}

void cDeepMimicCore::Keyboard(int key, int x, int y)
{
	char c = static_cast<char>(key);
	double device_x = 0;
	double device_y = 0;
	CalcDeviceCoord(x, y, device_x, device_y);
	//KKM mScene->Keyboard(c, device_x, device_y);
}

void cDeepMimicCore::MouseClick(int button, int state, int x, int y)
{
	double device_x = 0;
	double device_y = 0;
	CalcDeviceCoord(x, y, device_x, device_y);
	mScene->MouseClick(button, state, device_x, device_y);
}

void cDeepMimicCore::MouseMove(int x, int y)
{
	double device_x = 0;
	double device_y = 0;
	CalcDeviceCoord(x, y, device_x, device_y);
	mScene->MouseMove(device_x, device_y);
}

void cDeepMimicCore::Reshape(int w, int h)
{
	//KKM mScene->Reshape(w, h);
	//mDefaultFrameBuffer->Reshape(w, h);
	glViewport(0, 0, w, h);
	glutPostRedisplay();
}

void cDeepMimicCore::Shutdown()
{
	//KKM mScene->Shutdown();
}

bool cDeepMimicCore::IsDone() const
{
	 return false; //mScene->IsDone();
}

//cDrawScene* cDeepMimicCore::GetDrawScene() const
//{
//	return dynamic_cast<cDrawScene*>(mScene.get());
//}
//
//void cDeepMimicCore::SetPlaybackSpeed(double speed)
//{
//	mPlaybackSpeed = speed;
//}

void cDeepMimicCore::SetUpdatesPerSec(double updates_per_sec)
{
	mUpdatesPerSec = updates_per_sec;
}
 
int cDeepMimicCore::GetWinWidth() const
{
	return 900;// mDefaultFrameBuffer->GetWidth();
}

int cDeepMimicCore::GetWinHeight() const
{
	return 600;// mDefaultFrameBuffer->GetHeight();
}

int cDeepMimicCore::GetNumUpdateSubsteps() const
{
	return mNumUpdateSubsteps;
}

 
 
void cDeepMimicCore::SetupScene(char*motionfile)
{
	ClearScene();
	std::string scene_name = "";
	mArgParser->ParseString("scene", scene_name);

	mScene = nullptr;
 
		 
	mScene = std::shared_ptr<cDrawSceneKinChar>(new cDrawSceneKinChar());
 
    mScene->ParseDec(mArgParser);
	mScene->Init(motionfile);
	printf("Loaded scene: %s\n", mScene->GetName().c_str());
	printf("++++++++++++++++++++++\n");
 
	printf("NOThello #%s#", mScene->GetName().c_str());
//	mRLScene=NULL;
	
	
}

void cDeepMimicCore::ClearScene()
{
	mScene = nullptr;
}

//int cDeepMimicCore::GetCurrTime() const
//{
//	return glutGet(GLUT_ELAPSED_TIME);
//}

 

void cDeepMimicCore::CalcDeviceCoord(int pixel_x, int pixel_y, double& out_device_x, double& out_device_y) const
{
	double w = GetWinWidth();
	double h = GetWinHeight();

	out_device_x = static_cast<double>(pixel_x) / w;
	out_device_y = static_cast<double>(pixel_y) / h;
	out_device_x = (out_device_x - 0.5f) * 2.f;
	out_device_y = (out_device_y - 0.5f) * -2.f;
}

 
