#include <iostream>

#include "DeepMimicCore.h"

#include "util/FileUtil.h"

#include "render/DrawUtil.h"
//#include "render/TextureDesc.h"

// Dimensions of the window we are drawing into.
int gWinWidth = 800;
int gWinHeight = static_cast<int>(gWinWidth * 9.0 / 16.0);
//int gWinWidth = 1055;
//int gWinHeight = 450;
bool gReshaping = false;

// intermediate frame buffers
//std::unique_ptr<cTextureDesc> gDefaultFrameBuffer;

// anim
const double gFPS = 60.0;
const double gAnimStep = 1.0 / gFPS;
const int gDisplayAnimTime = static_cast<int>(1000 * gAnimStep);
bool gAnimating = true;

int gSampleCount = 0;

double gPlaybackSpeed = 1;
const double gPlaybackDelta = 0.05;

// FPS counter
int gPrevTime = 0;
double gUpdatesPerSec = 0;

//std::vector<std::string> gArgs;
std::unique_ptr<cDeepMimicCore> gCore;

void SetupDeepMimicCore(char* kingcharfile)
{
	bool enable_draw = true;
	gCore = std::unique_ptr<cDeepMimicCore>(new cDeepMimicCore(enable_draw));
	gCore->SetKinCharFile(kingcharfile);
	gCore->Init(kingcharfile);

}

void FormatArgs(int argc, char** argv, std::vector<std::string>& out_args)
{
	out_args.resize(argc);
	for (int i = 0; i < argc; ++i)
	{
		out_args[i] = std::string(argv[i]);
	}
}

void UpdateFrameBuffer()
{
	if (!gReshaping)
	{
		if (gWinWidth != gCore->GetWinWidth() || gWinHeight != gCore->GetWinHeight())
		{
			gCore->Reshape(gWinWidth, gWinHeight);
		}
	}
}

void Update(double time_elapsed)
{
	int num_substeps = gCore->GetNumUpdateSubsteps();
	double timestep = time_elapsed / num_substeps;
	num_substeps = (time_elapsed == 0) ? 1 : num_substeps;

	for (int i = 0; i < num_substeps; ++i)
	{
		 
		gCore->Update(timestep);

	}
}

void Draw(void)
{
	UpdateFrameBuffer();
	gCore->Draw();
	
	glutSwapBuffers();
	gReshaping = false;
}

void Reshape(int w, int h)
{
	gReshaping = true;

	gWinWidth = w;
	gWinHeight = h;
	
	//gDefaultFrameBuffer->Reshape(w, h);
	glViewport(0, 0, gWinWidth, gWinHeight);
	glutPostRedisplay();
}

void StepAnim(double time_step)
{
	Update(time_step);
	gAnimating = false;
	glutPostRedisplay();
}

 
void Reset()
{
	gCore->Reset();
}

int GetNumTimeSteps()
{
	int num_steps = static_cast<int>(gPlaybackSpeed);
	if (num_steps == 0)
	{
		num_steps = 1;
	}
	num_steps = std::abs(num_steps);
	return num_steps;
}

int CalcDisplayAnimTime(int num_timesteps)
{
	int anim_time = static_cast<int>(gDisplayAnimTime * num_timesteps / gPlaybackSpeed);
	anim_time = std::abs(anim_time);
	return anim_time;
}

void Shutdown()
{
	gCore->Shutdown();
	exit(0);
}

int GetCurrTime()
{
	return glutGet(GLUT_ELAPSED_TIME);
}

void InitTime()
{
	gPrevTime = GetCurrTime();
	gUpdatesPerSec = 0;
}

void Animate(int callback_val)
{
	const double counter_decay = 0;

	if (gAnimating)
	{
		int num_steps = GetNumTimeSteps();
		int curr_time = GetCurrTime();
		int time_elapsed = curr_time - gPrevTime;
		gPrevTime = curr_time;

		double timestep = (gPlaybackSpeed < 0) ? -gAnimStep : gAnimStep;
		for (int i = 0; i < num_steps; ++i)
		{
			Update(timestep);
		}
		
		// FPS counting
		double update_count = num_steps / (0.001 * time_elapsed);
		if (std::isfinite(update_count))
		{
			gUpdatesPerSec = counter_decay * gUpdatesPerSec + (1 - counter_decay) * update_count;
			gCore->SetUpdatesPerSec(gUpdatesPerSec);
		}

		int timer_step = CalcDisplayAnimTime(num_steps);
		int update_dur = GetCurrTime() - curr_time;
		timer_step -= update_dur;
		timer_step = std::max(timer_step, 0);
		
		glutTimerFunc(timer_step, Animate, 0);
		glutPostRedisplay();
	}

	if (gCore->IsDone())
	{
		Shutdown();
	}
}

//void ToggleAnimate()
//{
//	gAnimating = !gAnimating;
//	if (gAnimating)
//	{
//		glutTimerFunc(gDisplayAnimTime, Animate, 0);
//	}
//}
//
//void ChangePlaybackSpeed(double delta)
//{
//	double prev_playback = gPlaybackSpeed;
//	gPlaybackSpeed += delta;
//	gCore->SetPlaybackSpeed(gPlaybackSpeed);
//
//	if (std::abs(prev_playback) < 0.0001 && std::abs(gPlaybackSpeed) > 0.0001)
//	{
//		glutTimerFunc(gDisplayAnimTime, Animate, 0);
//	}
//}

void Keyboard(unsigned char key, int x, int y) 
{
	gCore->Keyboard(key, x, y);

	switch (key) {
	case 27: // escape
		Shutdown();
		break;
	case '>':
		StepAnim(gAnimStep);
		break;
	case '<':
		StepAnim(-gAnimStep);
		break;
	case 'r':
		Reset();
		break;
	default:
		break;
	}

	glutPostRedisplay();
}

void MouseClick(int button, int state, int x, int y)
{
	gCore->MouseClick(button, state, x, y);
	glutPostRedisplay();
}

void MouseMove(int x, int y)
{
	gCore->MouseMove(x, y);
	glutPostRedisplay();
}

 

void InitDraw(int argc, char** argv)
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(gWinWidth, gWinHeight);
	glutCreateWindow("DeepMimic");
}

void SetupDraw()
{
	glutDisplayFunc(Draw);
	glutReshapeFunc(Reshape);
	glutKeyboardFunc(Keyboard);
	glutMouseFunc(MouseClick);
	glutMotionFunc(MouseMove);
	glutTimerFunc(gDisplayAnimTime, Animate, 0);

	Reshape(gWinWidth, gWinHeight);
	gCore->Reshape(gWinWidth, gWinHeight);
}

void DrawMainLoop()
{
	InitTime();
	glutMainLoop();
}

int main(int argc, char** argv)
{
	//gArgs.resize(2);
	//gArgs[1]="args/bvh_walker.txt";
	InitDraw(argc, argv);
	SetupDeepMimicCore(argv[1]);
	SetupDraw();

	DrawMainLoop();

	return EXIT_SUCCESS;
}

