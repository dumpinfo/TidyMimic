#include "KinCharacter.h"
#include <assert.h>
#include <functional>
#include "util/FileUtil.h"

const double gDiffTimeStep = 1 / 600.0;

cKinCharacter::tParams::tParams()
{
	mID = gInvalidIdx;
	mCharFile = "";
	mMotionFile = "";
	mOrigin.setZero();
	mLoadDrawShapes = true;
}

cKinCharacter::cKinCharacter()
{
	mOrigin.setZero();
	mOriginRot.setIdentity();
	mCycleRootDelta.setZero();
}

cKinCharacter::~cKinCharacter()
{
}

bool cKinCharacter::Init(const tParams& params)
{
	mID = params.mID;
	bool succ = cCharacter::Init(params.mCharFile, params.mLoadDrawShapes);
	if (succ)
	{
		if (params.mMotionFile != "")
		{
			LoadMotion(params.mMotionFile);
		}

		SetOriginPos(params.mOrigin);
	}
	 
	return succ;
}

void cKinCharacter::Clear()
{
	cCharacter::Clear();
	mMotion.Clear();
}

void cKinCharacter::Update(double time_step)
{
	cCharacter::Update(time_step);
	mTime += time_step;
	Pose(mTime);
}

void cKinCharacter::Reset()
{
	cCharacter::Reset();
}

bool cKinCharacter::LoadMotion(const std::string& motion_file)
{
	cMotion::tParams motion_params;
	motion_params.mMotionFile = motion_file;
	motion_params.mBlendFunc = std::bind(&cKinCharacter::BlendFrames, this,
											std::placeholders::_1, std::placeholders::_2, std::placeholders::_3,
											std::placeholders::_4);
	//motion_params.mMirrorFunc = std::bind(&cKinCharacter::MirrorFrame, this,
	//										std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
	motion_params.mVelFunc = std::bind(&cKinCharacter::CalcFrameVel, this,
											std::placeholders::_1, std::placeholders::_2, std::placeholders::_3,
											std::placeholders::_4);
	motion_params.mPostProcessFunc = std::bind(&cKinCharacter::PostProcessFrame, this, std::placeholders::_1);
	bool succ = mMotion.Load(motion_params);

 

	if (succ)
	{
		mCycleRootDelta = CalcCycleRootDelta();
		Pose(mTime);
		mPose0 = GetPose();
		mVel0 = GetVel();
	}

	if (!succ)
	{
		printf("Failed to load motion from: %s\n", motion_file.c_str());
	}
	return succ;
}

 

void cKinCharacter::Pose(double time)
{
	CalcPose(time, mPose);
	SetPose(mPose);

	CalcVel(time, mVel);
	SetVel(mVel);
}

 

bool cKinCharacter::HasMotion() const
{
	return mMotion.IsValid();
}

 

void cKinCharacter::SetOriginPos(const tVector& origin)
{
	tVector delta = origin - mOrigin;
	MoveOrigin(delta);
	mOrigin = origin; // this is needed in canse of NaNs
}

void cKinCharacter::MoveOrigin(const tVector& delta)
{
	mOrigin += delta;

	tVector root0 = cKinTree::GetRootPos(mJointMat, mPose0);
	root0 += delta;
	cKinTree::SetRootPos(mJointMat, root0, mPose0);

	tVector root = cKinTree::GetRootPos(mJointMat, mPose);
	root += delta;
	cKinTree::SetRootPos(mJointMat, root, mPose);
}

 
void cKinCharacter::ResetParams()
{ 
	cCharacter::ResetParams();
	mTime = 0;
}

 

tVector cKinCharacter::CalcCycleRootDelta() const
{
	int num_frames = mMotion.GetNumFrames();
	Eigen::VectorXd frame_beg = mMotion.GetFrame(0);
	Eigen::VectorXd  frame_end = mMotion.GetFrame(num_frames - 1);

	tVector root_pos_beg = cKinTree::GetRootPos(mJointMat, frame_beg);
	tVector root_pos_end = cKinTree::GetRootPos(mJointMat, frame_end);

	tVector delta = root_pos_end - root_pos_beg;
	return delta;
}

void cKinCharacter::CalcPose(double time, Eigen::VectorXd& out_pose) const
{
	tVector root_delta = tVector::Zero();
	tQuaternion root_delta_rot = tQuaternion::Identity();

	if (HasMotion())
	{
		mMotion.CalcFrame(time, out_pose);
		if (mMotion.EnableLoop())
		{
			int cycle_count = mMotion.CalcCycleCount(time);
			root_delta = cycle_count * mCycleRootDelta;
		}
	}
	else
	{
		out_pose = mPose0;
	}

	tVector root_pos = cKinTree::GetRootPos(mJointMat, out_pose);
	tQuaternion root_rot = cKinTree::GetRootRot(mJointMat, out_pose);

	root_delta_rot = mOriginRot * root_delta_rot;
	root_rot = root_delta_rot * root_rot;
	root_pos += root_delta;
	root_pos = cMathUtil::QuatRotVec(root_delta_rot, root_pos);
	root_pos += mOrigin;

	cKinTree::SetRootPos(mJointMat, root_pos, out_pose);
	cKinTree::SetRootRot(mJointMat, root_rot, out_pose);
}

void cKinCharacter::CalcVel(double time, Eigen::VectorXd& out_vel) const
{
	if (HasMotion())
	{
		mMotion.CalcFrameVel(time, out_vel);
	}
	else
	{
		out_vel = Eigen::VectorXd::Zero(GetNumDof());
	}
}

 

void cKinCharacter::BlendFrames(const cMotion::tFrame* a, const cMotion::tFrame* b, double lerp, cMotion::tFrame* out_frame) const
{
	cKinTree::LerpPoses(mJointMat, *a, *b, lerp, *out_frame);
}

 

void cKinCharacter::CalcFrameVel(const cMotion::tFrame* a, const cMotion::tFrame* b, double timestep, cMotion::tFrame* out_vel) const
{
	const auto& joint_mat = GetJointMat();
	cKinTree::CalcVel(joint_mat, *a, *b, timestep, *out_vel);
}

void cKinCharacter::PostProcessFrame(cMotion::tFrame* out_frame) const
{
	const auto& joint_mat = GetJointMat();
	cKinTree::PostProcessPose(joint_mat, *out_frame);
}