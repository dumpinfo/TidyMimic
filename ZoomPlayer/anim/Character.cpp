#include "Character.h"
#include <assert.h>
#include "util/json/json.h"

#include "util/FileUtil.h"
#include "util/JsonUtil.h"
//#include "render/MeshUtil.h"
#include "render/DrawUtil.h"

// Json keys
const std::string cCharacter::gSkeletonKey = "Skeleton";
const std::string gPoseKey = "Pose";
const std::string gVelKey = "Vel";
const std::string gMeshesKey = "Meshes";

cCharacter::cCharacter()
{
	mID = gInvalidIdx;
	ResetParams();
}

cCharacter::~cCharacter()
{
}

void cCharacter::SetID(int id)
{
	mID = id;
}

int cCharacter::GetID() const
{
	return mID;
}

bool cCharacter::Init(const std::string& char_file, bool load_draw_shapes)
{
	Clear();

	bool succ = true;
	if (char_file != "")
	{
		std::ifstream f_stream(char_file);
		Json::Reader reader;
		Json::Value root;
		succ = reader.parse(f_stream, root);
		f_stream.close();

		if (succ)
		{
			if (root[gSkeletonKey].isNull())
			{
				succ = false;
			}
			else
			{
				succ = LoadSkeleton(root[gSkeletonKey]);
			}
		}
	}

	if (succ)
	{
		InitDefaultState();

		if (cDrawUtil::EnableDraw() && load_draw_shapes)
		{
			succ &= LoadMeshes(char_file, mMeshes);
			succ &= LoadDrawShapeDefs(char_file, mDrawShapeDefs);
		}
	}

	if (!succ)
	{
		printf("Failed to parse character from file %s.\n", char_file.c_str());
	}

	return succ;
}

void cCharacter::Clear()
{
	ResetParams();
	mPose.resize(0);
	mVel.resize(0);
	mPose0.resize(0);
	mVel0.resize(0);

	mDrawShapeDefs.resize(0, 0);
	mMeshes.clear();
}

void cCharacter::Update(double time_step)
{
}

void cCharacter::Reset()
{
	ResetParams();

	const Eigen::VectorXd& pose0 = GetPose0();
	const Eigen::VectorXd& vel0 = GetVel0();
	
	SetPose(pose0);
	SetVel(vel0);
}

int cCharacter::GetNumDof() const
{
	int dofs = cKinTree::GetNumDof(mJointMat);
	return dofs;
}

const Eigen::MatrixXd& cCharacter::GetJointMat() const
{
	return mJointMat;
}

int cCharacter::GetNumJoints() const
{
	return cKinTree::GetNumJoints(mJointMat);
}

const Eigen::VectorXd& cCharacter::GetPose() const
{
	return mPose;
}

void cCharacter::SetPose(const Eigen::VectorXd& pose)
{
	assert(pose.size() == GetNumDof());
	mPose = pose;
}

const Eigen::VectorXd& cCharacter::GetPose0() const
{
	return mPose0;
}

void cCharacter::SetPose0(const Eigen::VectorXd& pose)
{
	mPose0 = pose;
}

const Eigen::VectorXd& cCharacter::GetVel() const
{
	return mVel;
}

void cCharacter::SetVel(const Eigen::VectorXd& vel)
{
	assert(vel.size() == GetNumDof());
	mVel = vel;
}

const Eigen::VectorXd& cCharacter::GetVel0() const
{
	return mVel0;
}

void cCharacter::SetVel0(const Eigen::VectorXd& vel)
{
	mVel0 = vel;
}

int cCharacter::GetRootID() const
{
	int root_id = cKinTree::GetRoot(mJointMat);
	return root_id;
}

tVector cCharacter::GetRootPos() const
{
	tVector pos = cKinTree::GetRootPos(mJointMat, mPose);
	return pos;
}



tMatrix cCharacter::BuildJointWorldTrans(int joint_id) const
{
	return cKinTree::JointWorldTrans(mJointMat, mPose, joint_id);
}



bool cCharacter::LoadSkeleton(const Json::Value& root)
{
	return cKinTree::Load(root, mJointMat);
}

void cCharacter::InitDefaultState()
{
	int state_size = GetNumDof();
	cKinTree::BuildDefaultPose(mJointMat, mPose0);
	cKinTree::BuildDefaultVel(mJointMat, mVel0);
	mPose = mPose0;
	mVel = mVel0;
}

bool cCharacter::HasDrawShapes() const
{
	return mDrawShapeDefs.size() > 0;
}

const Eigen::MatrixXd& cCharacter::GetDrawShapeDefs() const
{
	return mDrawShapeDefs;
}

const std::shared_ptr<cDrawMesh>& cCharacter::GetMesh(int i) const
{
	assert(i >= 0 && i < GetNumMeshes());
	return mMeshes[i];
}

int cCharacter::GetNumMeshes() const
{
	return static_cast<int>(mMeshes.size());
}

void cCharacter::ResetParams()
{
}

bool cCharacter::ParseState(const Json::Value& root, Eigen::VectorXd& out_state) const
{
	bool succ = cJsonUtil::ReadVectorJson(root, out_state);
	int num_dof = GetNumDof();
	assert(out_state.size() == num_dof);
	return succ;
}

std::string cCharacter::BuildStateJson(const Eigen::VectorXd& pose, const Eigen::VectorXd& vel) const
{
	std::string json = "";

	std::string pose_json = cJsonUtil::BuildVectorJson(pose);
	std::string vel_json = cJsonUtil::BuildVectorJson(vel);

	json = "{\n\"Pose\":" + pose_json + ",\n\"Vel\":" + vel_json + "\n}";
	return json;
}


bool cCharacter::LoadDrawShapeDefs(const std::string& char_file, Eigen::MatrixXd& out_draw_defs) const
{
	bool succ = cKinTree::LoadDrawShapeDefs(char_file, out_draw_defs);
	return succ;
}

bool cCharacter::LoadMeshes(const std::string& char_file, std::vector<std::shared_ptr<cDrawMesh>>& out_meshes) const
{
	bool succ = true;
	 
	return succ;
}
