#include <iostream>
#include "util/ArgParser.h"
#include "util/MathUtil.h"
#include "util/ArgParser.h"
#include "util/Timer.h"
#include "scenes/ActionSpace.h"
#include "scenes/DrawSceneKinChar.h"

struct cTextureDesc
{
	int testAA;
};

class cDeepMimicCore
{
public:
	cDeepMimicCore(bool enable_draw);
	virtual ~cDeepMimicCore();

	virtual void SeedRand(int seed);
	//virtual void ParseArgs(const std::vector<std::string>& args);
	virtual void SetKinCharFile(char* file_name);
	virtual void Init();
	virtual void Update(double timestep);
	virtual void Reset();

	virtual double GetTime() const;
	virtual std::string GetName() const;
	virtual bool EnableDraw() const;

	virtual void Draw();
	virtual void Keyboard(int key, int x, int y);
	virtual void MouseClick(int button, int state, int x, int y);
	virtual void MouseMove(int x, int y);
	virtual void Reshape(int w, int h);

	virtual void Shutdown();
	virtual bool IsDone() const;
	//virtual cDrawScene* GetDrawScene() const;

	//virtual void SetPlaybackSpeed(double speed);
	virtual void SetUpdatesPerSec(double updates_per_sec);

	virtual int GetWinWidth() const;
	virtual int GetWinHeight() const;

	virtual int GetNumUpdateSubsteps() const;

 

protected:
	unsigned long int mRandSeed;

	std::shared_ptr<cArgParser> mArgParser;
	std::shared_ptr<cDrawSceneKinChar> mScene;
	 

	int mNumUpdateSubsteps;

	double mPlaybackSpeed;
	double mUpdatesPerSec; // FPS counter
	
	virtual void SetupScene();
	virtual void ClearScene();

	virtual void CalcDeviceCoord(int pixel_x, int pixel_y, double& out_device_x, double& out_device_y) const;

};