#ifndef __ZMATH__
#define __ZMATH__
#include <string.h>
#include <math.h>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>


#define RADIANTODEGREE(x) x *180.0/3.14159265358979

using namespace std;
/* Matrix & Vertex */
#define M_PI 3.141592653  

#define X v[0]
#define Y v[1]
#define Z v[2]
#define W v[3]

typedef struct {
	 float v[3];
} V3;

typedef struct {
	 float v[4];
} V4;
	
void gl_MoveV3(V3 *a,V3 *b);
//void gl_MulM3V3(V3 *a,M4 *b,V3 *c);
int  gl_V3_Norm(V3 *a);
V3 gl_V3_New(float x,float y,float z);
V4 gl_V4_New(float x,float y,float z,float w);


class M4
{

public:

M4(){};
float m[4][4];

static inline void gl_M4_Id(M4 *a);
static inline int gl_M4_IsId(M4 *a);
static inline void gl_M4_Move(M4 *a,M4 *b);
static inline void gl_MulM4V3(V3 *a,M4 *b,V3 *c);
static inline void gl_M4_MulV4(V4 * a,M4 *b,V4 * c);
static inline void gl_M4_InvOrtho(M4 *a,M4 b);
static inline void gl_M4_Inv(M4 *a,M4 *b);
static inline void gl_M4_Mul(M4 *c,M4 *a,M4 *b);
static inline void gl_M4_MulLeft(M4 *c,M4 *a);
static inline void gl_M4_Transpose(M4 *a,M4 *b);
static inline void gl_M4_Rotate(M4 *c,float t,int u);
void inline printMat();
//M4	operator*(  M4& y);
//void gl_MulM4V3(V3 *a,M4 *b,V3 *c);
//void gl_M4_MulV4(V4 *a,M4 *b,V4 *c);
} ;

/*M4	M4::operator*(  M4& y)
{
 
		M4 ret = (*this);
		M4::gl_M4_MulLeft(&ret, &y);
		return ret;
	
}*/

void inline M4::printMat()
{
	printf("%.3f, %.3f, %.3f, %.3f\n" , m[0][0], m[0][1], m[0][2], m[0][3] );
	printf("%.3f, %.3f, %.3f, %.3f\n" , m[1][0], m[1][1], m[1][2], m[1][3] );
	printf("%.3f, %.3f, %.3f, %.3f\n" , m[2][0], m[2][1], m[2][2], m[2][3] );
	printf("%.3f, %.3f, %.3f, %.3f\n" , m[3][0], m[3][1], m[3][2], m[3][3] );
}

void inline gl_MoveV3(V3 *a,V3 *b)
{
	memcpy(a,b,sizeof(V3));
}

int inline gl_V3_Norm(V3 *a)
{
	float n;
	n=sqrt(a->X*a->X+a->Y*a->Y+a->Z*a->Z);
	if (n==0) return 1;
	a->X/=n;
	a->Y/=n;
	a->Z/=n;
	return 0;
}

V3  inline gl_V3_New(float x,float y,float z)
{
	 V3 a;
	 a.X=x;
	 a.Y=y;
	 a.Z=z;
	 return a;
}

V4 inline gl_V4_New(float x,float y,float z,float w)
{
  V4 a;
  a.X=x;
  a.Y=y;
  a.Z=z;
  a.W=w;
  return a;
}

//int gl_Matrix_Inv(float *r,float *m,int n);


/* ******* Gestion des matrices 4x4 ****** */

void M4::gl_M4_Id(M4 *a)
{
	int i,j;
	for(i=0;i<4;i++)
	for(j=0;j<4;j++) 
	if (i==j) a->m[i][j]=1.0; else a->m[i][j]=0.0;
}

int M4::gl_M4_IsId(M4 *a)
{
	int i,j;
	for(i=0;i<4;i++)
    for(j=0;j<4;j++) {
      if (i==j) { 
        if (a->m[i][j] != 1.0) return 0;
      } else if (a->m[i][j] != 0.0) return 0;
    }
  return 1;
}

void M4::gl_M4_Mul(M4 *c,M4 *a,M4 *b)
{
  int i,j,k;
  float s;
  for(i=0;i<4;i++)
    for(j=0;j<4;j++) {
      s=0.0;
      for(k=0;k<4;k++) s+=a->m[i][k]*b->m[k][j];
      c->m[i][j]=s;
    }
}

/* c= a X C */
void M4::gl_M4_MulLeft(M4 *c,M4 *b)
{
  int i,j,k;
  float s;
  M4 a;

  /*memcpy(&a, c, 16*sizeof(float));
  */
  a=*c;

  for(i=0;i<4;i++)
    for(j=0;j<4;j++) {
      s=0.0;
      for(k=0;k<4;k++) s+=a.m[i][k]*b->m[k][j];
      c->m[i][j]=s;
    }
}

void M4::gl_M4_Move(M4 *a,M4 *b)
{
	memcpy(a,b,sizeof(M4));
}


void M4::gl_MulM4V3(V3 *a,M4 *b,V3 *c)
{
	 a->X=b->m[0][0]*c->X+b->m[0][1]*c->Y+b->m[0][2]*c->Z+b->m[0][3];
	 a->Y=b->m[1][0]*c->X+b->m[1][1]*c->Y+b->m[1][2]*c->Z+b->m[1][3];
	 a->Z=b->m[2][0]*c->X+b->m[2][1]*c->Y+b->m[2][2]*c->Z+b->m[2][3];
}

 

void M4::gl_M4_MulV4(V4 *a,M4 *b,V4 *c)
{
	 a->X=b->m[0][0]*c->X+b->m[0][1]*c->Y+b->m[0][2]*c->Z+b->m[0][3]*c->W;
	 a->Y=b->m[1][0]*c->X+b->m[1][1]*c->Y+b->m[1][2]*c->Z+b->m[1][3]*c->W;
	 a->Z=b->m[2][0]*c->X+b->m[2][1]*c->Y+b->m[2][2]*c->Z+b->m[2][3]*c->W;
	 a->W=b->m[3][0]*c->X+b->m[3][1]*c->Y+b->m[3][2]*c->Z+b->m[3][3]*c->W;
}
	
/* transposition of a 4x4 matrix */
void M4::gl_M4_Transpose(M4 *a,M4 *b)
{
  a->m[0][0]=b->m[0][0]; 
  a->m[0][1]=b->m[1][0]; 
  a->m[0][2]=b->m[2][0]; 
  a->m[0][3]=b->m[3][0]; 

  a->m[1][0]=b->m[0][1]; 
  a->m[1][1]=b->m[1][1]; 
  a->m[1][2]=b->m[2][1]; 
  a->m[1][3]=b->m[3][1]; 

  a->m[2][0]=b->m[0][2]; 
  a->m[2][1]=b->m[1][2]; 
  a->m[2][2]=b->m[2][2]; 
  a->m[2][3]=b->m[3][2]; 

  a->m[3][0]=b->m[0][3]; 
  a->m[3][1]=b->m[1][3]; 
  a->m[3][2]=b->m[2][3]; 
  a->m[3][3]=b->m[3][3]; 
}

/* inversion of an orthogonal matrix of type Y=M.X+P */ 
void M4::gl_M4_InvOrtho(M4 *a,M4 b)
{
	int i,j;
	float s;
	for(i=0;i<3;i++)
	for(j=0;j<3;j++) a->m[i][j]=b.m[j][i];
	a->m[3][0]=0.0; a->m[3][1]=0.0; a->m[3][2]=0.0; a->m[3][3]=1.0;
	for(i=0;i<3;i++) {
		s=0;
		for(j=0;j<3;j++) s-=b.m[j][i]*b.m[j][3];
		a->m[i][3]=s;
	}
}

/* Inversion of a general nxn matrix.
   Note : m is destroyed */

int inline Matrix_Inv(float *r,float *m,int n)
{
	 int i,j,k,l;
	 float max,tmp,t;

	 /* identit�e dans r */
	 for(i=0;i<n*n;i++) r[i]=0;
	 for(i=0;i<n;i++) r[i*n+i]=1;
	 
	 for(j=0;j<n;j++) {
			
			/* recherche du nombre de plus grand module sur la colonne j */
			max=m[j*n+j];
			k=j;
			for(i=j+1;i<n;i++)
				if (fabs(m[i*n+j])>fabs(max)) {
					 k=i;
					 max=m[i*n+j];
				}

      /* non intersible matrix */
      if (max==0) return 1;

			
			/* permutation des lignes j et k */
			if (k!=j) {
				 for(i=0;i<n;i++) {
						tmp=m[j*n+i];
						m[j*n+i]=m[k*n+i];
						m[k*n+i]=tmp;
						
						tmp=r[j*n+i];
						r[j*n+i]=r[k*n+i];
						r[k*n+i]=tmp;
				 }
			}
			
			/* multiplication de la ligne j par 1/max */
			max=1/max;
			for(i=0;i<n;i++) {
				 m[j*n+i]*=max;
				 r[j*n+i]*=max;
			}
			
			
			for(l=0;l<n;l++) if (l!=j) {
				 t=m[l*n+j];
				 for(i=0;i<n;i++) {
						m[l*n+i]-=m[j*n+i]*t;
						r[l*n+i]-=r[j*n+i]*t;
				 }
			}
	 }

	 return 0;
}


/* inversion of a 4x4 matrix */
//<=:   ( a = inv(b) );
void M4::gl_M4_Inv(M4 *a,M4 *b)
{
  M4 tmp;
  memcpy(&tmp, b, 16*sizeof(float));
  /*tmp=*b;*/
  Matrix_Inv(&a->m[0][0],&tmp.m[0][0],4);
}

void M4::gl_M4_Rotate(M4 *a,float t,int u)
{
	 float s,c;
	 int v,w;
   if ((v=u+1)>2) v=0;
	 if ((w=v+1)>2) w=0;
	 s=sin(t);
	 c=cos(t);
	 gl_M4_Id(a);
	 a->m[v][v]=c;	a->m[v][w]=-s;
	 a->m[w][v]=s;	a->m[w][w]=c;
}
	
																										
/* vector arithmetic */
//=======================================================
//=======================================================
inline float SIGN(float x) {
	return (x >= 0.0f) ? +1.0f : -1.0f;
}

inline float NORM(float a, float b, float c, float d) {
	return sqrt(a * a + b * b + c * c + d * d);
}

// quaternion = [w, x, y, z]
V4 inline mRot2Quat(const M4& Mat)
{
	float r11 = Mat.m[0][0];
	float r12 = Mat.m[0][1];
	float r13 = Mat.m[0][2];
	float r21 = Mat.m[1][0];
	float r22 = Mat.m[1][1];
	float r23 = Mat.m[1][2];
	float r31 = Mat.m[2][0];
	float r32 = Mat.m[2][1];
	float r33 = Mat.m[2][2];
	float q0 = (r11 + r22 + r33 + 1.0f) / 4.0f;
	float q1 = (r11 - r22 - r33 + 1.0f) / 4.0f;
	float q2 = (-r11 + r22 - r33 + 1.0f) / 4.0f;
	float q3 = (-r11 - r22 + r33 + 1.0f) / 4.0f;
	if (q0 < 0.0f) {
		q0 = 0.0f;
	}
	if (q1 < 0.0f) {
		q1 = 0.0f;
	}
	if (q2 < 0.0f) {
		q2 = 0.0f;
	}
	if (q3 < 0.0f) {
		q3 = 0.0f;
	}
	q0 = sqrt(q0);
	q1 = sqrt(q1);
	q2 = sqrt(q2);
	q3 = sqrt(q3);
	if (q0 >= q1 && q0 >= q2 && q0 >= q3) {
		q0 *= +1.0f;
		q1 *= SIGN(r32 - r23);
		q2 *= SIGN(r13 - r31);
		q3 *= SIGN(r21 - r12);
	}
	else if (q1 >= q0 && q1 >= q2 && q1 >= q3) {
		q0 *= SIGN(r32 - r23);
		q1 *= +1.0f;
		q2 *= SIGN(r21 + r12);
		q3 *= SIGN(r13 + r31);
	}
	else if (q2 >= q0 && q2 >= q1 && q2 >= q3) {
		q0 *= SIGN(r13 - r31);
		q1 *= SIGN(r21 + r12);
		q2 *= +1.0f;
		q3 *= SIGN(r32 + r23);
	}
	else if (q3 >= q0 && q3 >= q1 && q3 >= q2) {
		q0 *= SIGN(r21 - r12);
		q1 *= SIGN(r31 + r13);
		q2 *= SIGN(r32 + r23);
		q3 *= +1.0f;
	}
	else {
		printf("coding error\n");
	}
	float r = NORM(q0, q1, q2, q3);
	q0 /= r;
	q1 /= r;
	q2 /= r;
	q3 /= r;

	V4 res; 
	res.v[0] = q0;
	res.v[1] = q1;
	res.v[2] = q2;
	res.v[3] = q3;
	
	return res;
}


M4 inline GetRotateMat(double angleX, double angleY, double angleZ)
{
	angleX = angleX * M_PI / 180.0;
	angleY = angleY * M_PI / 180.0;
	angleZ = angleZ * M_PI / 180.0;

	M4 identityMat, rotZ, rotY, rotX;
	M4::gl_M4_Id(&identityMat);
	M4 retM = identityMat;
	M4::gl_M4_Rotate(&rotZ, angleZ, 2);
	M4::gl_M4_Rotate(&rotY, angleY, 1);
	M4::gl_M4_Rotate(&rotX, angleX, 0);
	M4::gl_M4_MulLeft(&retM, &rotZ);
	M4::gl_M4_MulLeft(&retM, &rotY);
	M4::gl_M4_MulLeft(&retM, &rotX);

	return retM;
}

M4 inline GetRotateMatZYX(double angleX, double angleY, double angleZ)
{
	return GetRotateMat(angleZ, angleY, angleX);
}


M4 Quaternion2RotMat(float qw, float qx, float qy, float qz)
{
	//First you have to normalize the quaternion :
	const float n = 1.0f / sqrt(qx*qx + qy*qy + qz*qz + qw*qw);
	qx *= n;
	qy *= n;
	qz *= n;
	qw *= n;
	//Then you can create your matrix :
	M4 retM;
	float array[16]={
		1.0f - 2.0f*qy*qy - 2.0f*qz*qz, 2.0f*qx*qy - 2.0f*qz*qw, 2.0f*qx*qz + 2.0f*qy*qw, 0.0f,
			2.0f*qx*qy + 2.0f*qz*qw, 1.0f - 2.0f*qx*qx - 2.0f*qz*qz, 2.0f*qy*qz - 2.0f*qx*qw, 0.0f,
			2.0f*qx*qz - 2.0f*qy*qw, 2.0f*qy*qz + 2.0f*qx*qw, 1.0f - 2.0f*qx*qx - 2.0f*qy*qy, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f};
	memcpy(&retM.m[0][0], array, sizeof(float)* 16);
	 
	return retM;	
}

/*M4 InvMat(cont M4 &b)
{
	M4 invMat; 
	M4::gl_M4_Inv(&retM, &invMat);
}*/

void DebugEuler2Quant(double angleX, double angleY, double angleZ)
{
	M4 retM = GetRotateMat( angleX,  angleY,  angleZ);
	V4 quant = mRot2Quat(retM);
	printf("%6.2f, %6.2f, %6.2f, %6.2f\n", quant.v[0], quant.v[1], quant.v[2], quant.v[3]);
	M4 invMat; 
	M4::gl_M4_Inv(&retM, &invMat);
	quant = mRot2Quat(retM);
	//printf("inv: %6.2f, %6.2f, %6.2f, %6.2f\n", quant.v[0], quant.v[1], quant.v[2], quant.v[3]);
}

void DebugMat2Quant(M4 retM)
{
	V4 quant = mRot2Quat(retM);
	printf("%6.3f, %6.3f, %6.3f, %6.3f );\n", quant.v[0], quant.v[1], quant.v[2], quant.v[3]);
}


M4 operator *(M4 a, M4 b)
{
	M4 ret = a;
	M4::gl_M4_MulLeft(&ret, &b);
	return ret;
}

M4 InvM4(M4&a)
{
//	a.printMat();
	M4 ret;
	M4 copya = a;
	M4::gl_M4_Inv(&ret, &copya);
//	a.printMat();
	return ret;
}
void DebugEuler2Quant(double angleX, double angleY, double angleZ, M4 baseMT)
{
	M4 retM = GetRotateMat(angleX, angleY, angleZ);
	M4::gl_M4_MulLeft(&baseMT, &retM);
	V4 quant = mRot2Quat(baseMT);
	printf("%6.2f, %6.2f, %6.2f, %6.2f\n", quant.v[0], quant.v[1], quant.v[2], quant.v[3]);
}


void DebugEuler2Quant(double angleX, double angleY, double angleZ,
	double base_angleX, double base_angleY, double base_angleZ)
	//M4 baseMT)
{
	M4  retM     = GetRotateMat(angleX, angleY, angleZ);
	M4 baseMT = GetRotateMat(base_angleX,   base_angleY,  base_angleZ);
	M4::gl_M4_MulLeft(&baseMT, &retM);
	V4 quant = mRot2Quat(baseMT);
	printf("%6.2f, %6.2f, %6.2f, %6.2f\n", quant.v[0], quant.v[1], quant.v[2], quant.v[3]);
}

 

void inline printout(FILE*file, M4&rotMat)
{
	V4 quant = mRot2Quat(rotMat);
	fprintf(file, "%6.2f, %6.2f, %6.2f, %6.2f, \n", quant.v[0], quant.v[1], quant.v[2], quant.v[3]);
}


void inline CorrectML(M4& inputMat)
{
	M4  ZeroPose = GetRotateMat(00,  180, 0.0);
	M4::gl_M4_MulLeft(&ZeroPose, &inputMat);
	inputMat = ZeroPose;
}

void inline CorrectMR(M4& inputMat)
{
	M4  ZeroPose = GetRotateMat(0.0, 180, 0);
	M4::gl_M4_MulLeft(&ZeroPose, &inputMat);
	inputMat = ZeroPose;
}

 

#endif  __ZMATH__
