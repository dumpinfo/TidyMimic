#pragma once

 
#include "Controller.h"
#include "anim/Motion.h"
#include "sim/ActionSpace.h"
#include "util/CircularBuffer.h"
#include "sim/ExpPDController.h"
//#include "sim/CtCtrlUtil.h"

class cDeepMimicCharController //: public cController
{
public:
	enum eMode
	{
		eModeActive,
		eModeInactive,
		eModePassive,
		eModeMax
	};
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
		cDeepMimicCharController();
	virtual ~cDeepMimicCharController();
	const static int gNormGroupNone;
	const static int gNormGroupSingle;
	virtual const std::shared_ptr<cDeepMimicCharController>& GetChild(int i) const;
	virtual int GetGoalSize() const;
	virtual int GetNumActions() const;
	virtual int NumChildren() const;
	virtual void BuildGoalNormGroups(Eigen::VectorXi& out_groups) const;
	virtual void BuildGoalOffsetScale(Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const;
	virtual void HandlePoseReset();
	virtual void HandleVelReset();
	virtual void PostUpdate(double time_step);
	virtual void Update(double time_step);
	//############################################################################
	virtual bool LoadParams(const std::string& param_file);
	virtual void Init(cSimCharacter* character, const std::string& param_file);
	virtual void Reset();
	virtual void Clear();
	virtual void UpdateCalcTau(double timestep, Eigen::VectorXd& out_tau);
	virtual void UpdateApplyTau(const Eigen::VectorXd& tau);

	virtual bool NeedNewAction() const;
	virtual void ApplyAction(const Eigen::VectorXd& action);
	virtual void RecordState(Eigen::VectorXd& out_state);
	virtual void RecordGoal(Eigen::VectorXd& out_goal) const;
	virtual void RecordAction(Eigen::VectorXd& out_action) const;

	virtual eActionSpace GetActionSpace() const;
	virtual int GetStateSize() const;

	virtual double GetRewardMin() const;
	virtual double GetRewardMax() const;

	virtual void SetViewDistMin(double dist);
	virtual void SetViewDistMax(double dist);
	virtual double GetViewDistMin() const;
	virtual double GetViewDistMax() const;

	virtual void GetViewBound(tVector& out_min, tVector& out_max) const;

	virtual double GetPrevActionTime() const;
	virtual const tVector& GetPrevActionCOM() const;
	virtual double GetTime() const;

	virtual const Eigen::VectorXd& GetTau() const;
	virtual const cCircularBuffer<double>& GetValLog() const;
	virtual void LogVal(double val);

protected:
	double mTime;
	bool mNeedNewAction;
	Eigen::VectorXd mAction;
	Eigen::VectorXd mTau;

	double mViewDistMin;
	double mViewDistMax;
	int mPosDim;

	double mPrevActionTime;
	tVector mPrevActionCOM;

	// for recording prediction from the value function, mainly for visualization
	cCircularBuffer<double> mValLog;
	virtual bool ParseParams(const Json::Value& json);

	virtual void ResetParams();
	virtual void InitResources();
	virtual void InitAction();
	virtual void InitTau();

	virtual int GetPosDim() const;

	virtual bool CheckNeedNewAction(double timestep) const;
	virtual void NewActionUpdate();
	virtual void HandleNewAction();
	virtual void PostProcessAction(Eigen::VectorXd& out_action) const;

	virtual void BuildStatePose(Eigen::VectorXd& out_pose) const;
	virtual void BuildStateVel(Eigen::VectorXd& out_vel) const;
	virtual int GetStatePoseOffset() const;
	virtual int GetStateVelOffset() const;
	virtual int GetStatePoseSize() const;
	virtual int GetStateVelSize() const;
public:
	virtual void SetGravity(const tVector& gravity);
	virtual std::string GetName() const;
	//===================================================================
	virtual void SetUpdateRate(double rate);
	virtual double GetUpdateRate() const;
	virtual double GetCyclePeriod() const;
	virtual void SetCyclePeriod(double period);
	virtual void SetInitTime(double time);
	virtual double GetPhase() const;
	virtual int GetActionSize() const;
	virtual void BuildStateOffsetScale(Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const;
	virtual void BuildActionBounds(Eigen::VectorXd& out_min, Eigen::VectorXd& out_max) const;
	virtual void BuildActionOffsetScale(Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const;
	virtual void BuildStateNormGroups(Eigen::VectorXi& out_groups) const;
protected:
	virtual void UpdateBuildTau(double time_step, Eigen::VectorXd& out_tau);
	virtual void SetupPDControllers(const Json::Value& json, const tVector& gravity);
	virtual void UpdatePDCtrls(double time_step, Eigen::VectorXd& out_tau);
	virtual void ConvertActionToTargetPose(int joint_id, Eigen::VectorXd& out_theta) const;
	virtual cKinTree::eJointType GetJointType(int joint_id) const;
	virtual void SetPDTargets(const Eigen::VectorXd& targets);
protected:
	cExpPDController mPDCtrl;
	tVector mGravity;
	double mUpdateRate;
	double mCyclePeriod;
	bool mEnablePhaseInput;
	bool mEnablePhaseAction;
	bool mRecordWorldRootPos;
	bool mRecordWorldRootRot;
	double mPhaseOffset;
	double mInitTimeOffset;
	Eigen::VectorXd mActionBoundMin;
	Eigen::VectorXd mActionBoundMax;
	virtual int GetPosFeatureDim() const;
	virtual int GetRotFeatureDim() const;
	virtual int GetStatePhaseSize() const;
	virtual int GetStatePhaseOffset() const;
	virtual int GetActionPhaseOffset() const;
	virtual int GetActionPhaseSize() const;
	virtual int GetActionCtrlOffset() const;
	virtual int GetActionCtrlSize() const;
	virtual void BuildStatePhaseOffsetScale(Eigen::VectorXd& phase_offset, Eigen::VectorXd& phase_scale) const;
	virtual void BuildStatePhase(Eigen::VectorXd& out_phase) const;
	virtual void BuildJointActionBounds(int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max) const;
	virtual void BuildJointActionOffsetScale(int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const;
	virtual bool FlipStance() const;
	virtual int RetargetJointID(int joint_id) const;
	virtual double GetPhaseRate() const;
	public:
	int GetNumOptParams() const;
	const cSimCharacter* GetChar() const;
	void BuildOptParams(Eigen::VectorXd& out_params) const;
	void SetOptParams(const Eigen::VectorXd& params);
	void SetOptParams(const Eigen::VectorXd& params, Eigen::VectorXd& out_params) const;
	void FetchOptParamScale(Eigen::VectorXd& out_scale) const;
	void OutputOptParams(const std::string& file, const Eigen::VectorXd& params) const;
	void OutputOptParams(FILE* f, const Eigen::VectorXd& params) const;

	void SetActive(bool active);
	bool IsActive() const;
	void SetMode(eMode mode);
	//----------------------------------
	cSimCharacter* mChar;
	eMode mMode;
	bool mValid;
};
