#include "Controller.h"
#include "sim/SimCharacter.h"
#include "util/FileUtil.h"

cController::cController()
{
	mChar = nullptr;
}

cController::~cController()
{

}


void cController::Init(cSimCharacter* character)
{
	assert(character != nullptr);
	Clear();
	mChar = character;
}

void cController::Reset()
{
	mMode = eModeActive;
}


void cController::Clear()
{
	mValid = false;
	mChar = nullptr;
	mMode = eModeActive;
}

//void cController::Update(double time_step)
//{
//	// *whistle whistle*....nothing to see here
//}

bool cController::IsValid() const
{
	return mValid;
}


 