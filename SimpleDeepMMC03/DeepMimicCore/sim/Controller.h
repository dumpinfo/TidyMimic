#pragma once

#include <memory>
#include "util/MathUtil.h"
#include "util/json/json.h"
#include "sim/Ground.h"
class cSimCharacter;

enum eCharCtrl
{
	eCharCtrlNone,
	eCharCtrlCt,
	eCharCtrlCtPD,
	eCharCtrlCtVel,
	eCharCtrlMax
};

struct tCtrlParams
{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

		eCharCtrl mCharCtrl;
	std::string mCtrlFile;

	std::shared_ptr<cSimCharacter> mChar;
	std::shared_ptr<cGround> mGround;
	tVector mGravity;
	//tCtrlParams();
};

class cController //: public std::enable_shared_from_this<cController>
{
public:
	enum eMode
	{
		eModeActive,
		eModeInactive,
		eModePassive,
		eModeMax
	};

	virtual ~cController();

	void Init(cSimCharacter* character);
	void Reset();
	void Clear();
	//void Update(double time_step);
	bool IsValid() const;

	bool  IsActive() const
   {
	return mMode != eModeInactive;
   }

	const cSimCharacter*  GetChar() const
   {
	return mChar;
   }

//protected:
public:
	cSimCharacter* mChar;
	eMode mMode;
	bool mValid;

	cController();
	//virtual bool ParseParams(const Json::Value& json);
};