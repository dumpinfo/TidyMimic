#include "FbxAnimationUpdate.h"
#include "FbxCommonTool.h"
//#include "FbxDisplayTool.h"
#include "zmath.h"
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#define RADIANTODEGREE(x) x *180.0/3.14159265358979

using namespace std;

struct CharSkeleton
{
//public:
   // M4 & rotMat;
	vector<FbxAMatrix> fbxNodes;
	vector<tgFbxNode> fbxLinks;
	int sequence_id;
	float time;
	MimicMotionInfo mimicInfo;
};

FbxAnimationUpdate::FbxAnimationUpdate()
{
}


FbxAnimationUpdate::~FbxAnimationUpdate()
{
}

void FbxAnimationUpdate::FbxAnimationUpdateWithPb(const FbxString& fbxFile, const FbxString& AnimationPbFile)
{
	FbxManager* lFbxSdkManager = NULL;
	FbxScene* lFbxScene = NULL;

	FbxCommonTool::GetInstance()->InitializeSdkObjects(lFbxSdkManager, lFbxScene);

	bool lResult = true;
	if (fbxFile.IsEmpty() || AnimationPbFile.IsEmpty())
	{
		lResult = false;
		FBXSDK_printf("\n\nUsage: ImportScene <FBX file name>\n\n");
	}
	else
	{
		lResult = FbxCommonTool::GetInstance()->LoadScene(lFbxSdkManager, lFbxScene, fbxFile.Buffer());

		FbxAnimationCover(lFbxScene, AnimationPbFile);

		FbxString OutputFbxFile = fbxFile.Left(fbxFile.GetLen() - 4) + FbxString("_Cover") + FbxString(".fbx");
		lResult = FbxCommonTool::GetInstance()->SaveScene(lFbxSdkManager, lFbxScene, OutputFbxFile);
	}

	FbxCommonTool::GetInstance()->DestroySdkObjects(lFbxSdkManager, lResult);
}

void FbxAnimationUpdate::FbxAnimationCover(FbxScene* lFbxScene, const FbxString& AnimationPbFile)
{
	 if (lFbxScene == NULL)
		return;

	/*msg::FbxAnimationStack fbxAnimationStackData;
	std::fstream input(AnimationPbFile.Buffer(), std::ios::in | std::ios::binary);
	if (!input) {
		std::cout << AnimationPbFile << ": File not found.  Creating a new file." << std::endl;
	}
	else if (!fbxAnimationStackData.ParseFromIstream(&input)) {
		std::cerr << "Failed to parse pb file" << std::endl;
		return;
	}*/

	//递归遍历存储骨骼点到数组中
	FbxArray<FbxNode *> FbxSkeletonArray;
	FbxCommonTool::GetInstance()->NodeExtractWithDepth(lFbxScene, FbxSkeletonArray);
	printf("array size %i\n", FbxSkeletonArray.Size());

	for (int i = 0; i < lFbxScene->GetSrcObjectCount<FbxAnimStack>(); i++)
	{
		FbxAnimStack* pAnimStack = lFbxScene->GetSrcObject<FbxAnimStack>(i);

		if (pAnimStack->GetMemberCount<FbxAnimLayer>() > 0)
		{
			FbxAnimLayer* pAnimLayer = pAnimStack->GetMember<FbxAnimLayer>(0); //暂时只考虑单层layer的情况，多层混合比较复杂，暂不考虑
		//	const msg::FbxAnimationData& fbxAnimationData = fbxAnimationStackData.animations(0);

		//	FbxAnimationCover(pAnimLayer, FbxSkeletonArray, fbxAnimationData);
			FbxAnimationCoverAA( pAnimLayer, FbxSkeletonArray);
		}
	}
}

FbxAMatrix MatArray[400][100];


void FbxAnimationUpdate::FbxAnimationCoverAA(FbxAnimLayer* pAnimLayer, FbxArray<FbxNode *> &FbxSkeletonArray )
{
	if (pAnimLayer == NULL || FbxSkeletonArray.Size() == 0)
		return;
	FbxAnimationData fbxAnimationData;
	
	vector<string> nodenameVec;
	FbxArray<FbxTime> KeyTimes;
	map< string, int > fbxNodeNameMap;
    fbxNodeNameMap.clear();
     int i,  lKeyCount(0);
	 int KeyFrameCount(0);
	for (i = 0; i < FbxSkeletonArray.Size(); ++i)
	{
		FbxNode *pFbxNode = FbxSkeletonArray[i];
		FbxAnimCurve* lAnimCurve = pFbxNode->LclTranslation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_X);
		
		string node_name = pFbxNode->GetName();
		printf("%02i, %s\n", i, (char*)node_name.c_str());
		nodenameVec.push_back(node_name); 
		
		if (lAnimCurve == NULL)
			continue;
		
		lKeyCount = lAnimCurve->KeyGetCount();

		KeyTimes.Clear();
		for (int lCount = 0; lCount < lKeyCount; lCount++)
		{
			FbxTime lKeyTime = lAnimCurve->KeyGetTime(lCount);
			printf("lKeyTime %02i: %lf\n",lCount, lKeyTime.GetSecondDouble());
			KeyTimes.Add(lKeyTime);
		}

		
		vector<FbxAMatrix> transmat;
		transmat.clear();
		for (int lCount = 0; lCount < lKeyCount; lCount++)
		{
			FbxTime lKeyTime = lAnimCurve->KeyGetTime(lCount);
			//KeyTimes.Add(lKeyTime);
			//transmat.push_back(pFbxNode->EvaluateGlobalTransform(lKeyTime));
			MatArray[lCount][i] = pFbxNode->EvaluateLocalTransform(lKeyTime);
		}
 
	}
	KeyFrameCount = lKeyCount;
	//---------------------------------------------------------------------------
	for ( i = 0; i < KeyFrameCount; i++)
	{
		//CharSkeleton *frame;
		//frame = &motionFrames[i];
		printf("===================Raw Info %02i===========================\n", i);
		for (int j = 0; j < nodenameVec.size(); j++)
		{
			//FbxNode *pFbxNode = FbxSkeletonArray[i];
			//FbxAnimCurve* lAnimCurve = pFbxNode->LclTranslation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_X);
			//FbxTime lKeyTime = lAnimCurve->KeyGetTime(i);
			printf("%s,%.2lf, %.2lf,%.2lf\n", (char*)nodenameVec[j].c_str(), 
			MatArray[i][j].GetR()[0], 
			MatArray[i][j].GetR()[1],
			MatArray[i][j].GetR()[2]);	
		}/**/
	}
	return; 
	//---------------------------------------------------------------------------
	for(i=0;i< nodenameVec.size();i++)
		fbxNodeNameMap[nodenameVec[i]] = i;
	
	vector<CharSkeleton> motionFrames;
	motionFrames.resize(lKeyCount);
	//==================================================================================================================
	//##################################################################################################################
	//return;
	
	for(i=0; i< lKeyCount; i++) 
	{
	 motionFrames[i].fbxNodes.resize(FbxSkeletonArray.Size());
     motionFrames[i].fbxLinks.resize(FbxSkeletonArray.Size());
	}
	
	for ( i = 0; i < FbxSkeletonArray.Size(); ++i)
	{
		FbxNode *pFbxNode = FbxSkeletonArray[i];
		FbxAnimCurve* lAnimCurve = pFbxNode->LclTranslation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_X);
		if (lAnimCurve == NULL)
			continue;
		int lKeyCount = lAnimCurve->KeyGetCount();

        if(lKeyCount!=0)
			KeyFrameCount = lKeyCount;
		
		KeyTimes.Clear();

		for (int lCount = 0; lCount < lKeyCount; lCount++)
		{
			FbxTime lKeyTime = lAnimCurve->KeyGetTime(lCount);
			KeyTimes.Add( lKeyTime.GetSecondDouble());
			
			motionFrames[lCount].fbxLinks[i].nodeRot[0] = MatArray[lCount][i].GetR()[0];
		    motionFrames[lCount].fbxLinks[i].nodeRot[1] = MatArray[lCount][i].GetR()[1];
		    motionFrames[lCount].fbxLinks[i].nodeRot[2] = MatArray[lCount][i].GetR()[2];
		}

		string node_name = pFbxNode->GetName();
		printf("%02i, %s\n", i, (char*)node_name.c_str());
		
		
			//MatArray[i][j].GetR()[0], 
			//MatArray[i][j].GetR()[1],
			//MatArray[i][j].GetR()[2]

	}
	/**/

	//-----------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------
	for ( i = 0; i < KeyFrameCount; i++)
	{
		CharSkeleton *frame;
		frame = &motionFrames[i];
		//printf("==============================================\n");
		/*for (int j = 0; j < nodenameVec.size(); j++)
		{
			//FbxNode *pFbxNode = FbxSkeletonArray[i];
			//FbxAnimCurve* lAnimCurve = pFbxNode->LclTranslation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_X);
			//FbxTime lKeyTime = lAnimCurve->KeyGetTime(i);
			printf("%s,%.2lf, %.2lf,%.2lf\n", (char*)nodenameVec[j].c_str(), 
			MatArray[i][j].GetR()[0], 
			MatArray[i][j].GetR()[1],
			MatArray[i][j].GetR()[2]);	
		}*/
		//printf("==============================================\n");
		
		//jnameGlobalTrsMap["left_shoulder"] 
		M4 dbg;
		dbg = frame->mimicInfo.jnameLocalTrsMap["right_clavicle"];
		//dbg.printMat();

		frame->mimicInfo.init(); //mimicInfo();
		int ss = frame->mimicInfo.jnameGlobalTrsMap.size();
		dbg = frame->mimicInfo.jnameGlobalTrsMap["left_shoulder"];

                    
		int Idx = frame->mimicInfo.jnameMapIdx["left_shoulder"];
		M4 Rot = GetRotateMat(frame->fbxLinks[Idx].nodeRot[0], frame->fbxLinks[Idx].nodeRot[1], frame->fbxLinks[Idx].nodeRot[2]);
		M4 Rot22 = Rot * Rot * Rot;
                frame->mimicInfo.left_shoulder = frame->mimicInfo.jnameGlobalTrsMap["left_shoulder"] * Rot
			*(InvM4(frame->mimicInfo.jnameLocalTrsMap["left_shoulder"]))*
			(InvM4(frame->mimicInfo.jnameGlobalTrsMap["left_shoulder"]));


		//------------------------------------------------------------------------------------------------
		//jnameGlobalTrsMap["neck"]
		Idx = frame->mimicInfo.jnameMapIdx["neck"];
		Rot = GetRotateMat(frame->fbxLinks[Idx].nodeRot[0], frame->fbxLinks[Idx].nodeRot[1], frame->fbxLinks[Idx].nodeRot[2]);
		frame->mimicInfo.neck = frame->mimicInfo.jnameGlobalTrsMap["neck"] * Rot
			*(InvM4(frame->mimicInfo.jnameLocalTrsMap["neck"]))*
			(InvM4(frame->mimicInfo.jnameGlobalTrsMap["neck"]));
		//------------------------------------------------------------------------------------------------
			

		Idx = frame->mimicInfo.jnameMapIdx["right_shoulder"];
		Rot = GetRotateMat(frame->fbxLinks[Idx].nodeRot[0], frame->fbxLinks[Idx].nodeRot[1], frame->fbxLinks[Idx].nodeRot[2]);
		frame->mimicInfo.right_shoulder = frame->mimicInfo.jnameGlobalTrsMap["right_shoulder"] * Rot
			*(InvM4(frame->mimicInfo.jnameLocalTrsMap["right_shoulder"]))*
			(InvM4(frame->mimicInfo.jnameGlobalTrsMap["right_shoulder"]));

		Idx = frame->mimicInfo.jnameMapIdx["left_hip"];
		Rot = GetRotateMat(frame->fbxLinks[Idx].nodeRot[0], frame->fbxLinks[Idx].nodeRot[1], frame->fbxLinks[Idx].nodeRot[2]);
		frame->mimicInfo.left_hip = frame->mimicInfo.jnameGlobalTrsMap["left_hip"] * Rot
			*(InvM4(frame->mimicInfo.jnameLocalTrsMap["left_hip"]))*
			(InvM4(frame->mimicInfo.jnameGlobalTrsMap["left_hip"]));

		Idx = frame->mimicInfo.jnameMapIdx["right_hip"];
		Rot = GetRotateMat(frame->fbxLinks[Idx].nodeRot[0], frame->fbxLinks[Idx].nodeRot[1], frame->fbxLinks[Idx].nodeRot[2]);
		frame->mimicInfo.right_hip = frame->mimicInfo.jnameGlobalTrsMap["right_hip"] * Rot
			*(InvM4(frame->mimicInfo.jnameLocalTrsMap["right_hip"]))*
			(InvM4(frame->mimicInfo.jnameGlobalTrsMap["right_hip"]));

		Idx = frame->mimicInfo.jnameMapIdx["right_clavicle"];
		Rot = GetRotateMat(frame->fbxLinks[Idx].nodeRot[0], frame->fbxLinks[Idx].nodeRot[1], frame->fbxLinks[Idx].nodeRot[2]);
		dbg = frame->mimicInfo.jnameLocalTrsMap["right_clavicle"];
		//dbg.printMat();

		frame->mimicInfo.right_clavicle = frame->mimicInfo.jnameGlobalTrsMap["right_clavicle"] * Rot
			*(InvM4(frame->mimicInfo.jnameLocalTrsMap["right_clavicle"]))*
			(InvM4(frame->mimicInfo.jnameGlobalTrsMap["right_clavicle"]));


		frame->mimicInfo.left_knee = frame->fbxLinks[17].nodeRot[2];
		frame->mimicInfo.right_knee = frame->fbxLinks[21].nodeRot[2];


		//=======================================================
		M4 mb0, mb1, mb2, b_Spine, b_Spine1, b_Spine2, retM;
		mb0 = GetRotateMat(-90.00, 90.00, 0.00);
		mb1 = GetRotateMat(frame->fbxLinks[1].nodeRot[0], frame->fbxLinks[1].nodeRot[1], frame->fbxLinks[1].nodeRot[2]);
		mb2 = GetRotateMat(frame->fbxLinks[2].nodeRot[0], frame->fbxLinks[2].nodeRot[1], frame->fbxLinks[2].nodeRot[2]);
		b_Spine = GetRotateMat(frame->fbxLinks[3].nodeRot[0], frame->fbxLinks[3].nodeRot[1], frame->fbxLinks[3].nodeRot[2]);
		b_Spine1 = GetRotateMat(frame->fbxLinks[4].nodeRot[0], frame->fbxLinks[4].nodeRot[1], frame->fbxLinks[4].nodeRot[2]);
		b_Spine2 = GetRotateMat(frame->fbxLinks[5].nodeRot[0], frame->fbxLinks[5].nodeRot[1], frame->fbxLinks[5].nodeRot[2]);
		retM = mb0 * mb1 *mb2* b_Spine * b_Spine1 * b_Spine2;

		M4 Minit;
		float mval[16] = { 0, 1, 0, 0,/**/1, 0, 0, 0,/**/0, 0, -1, 0,/**/0, 0, 0, 1 };

		memcpy(Minit.m, mval, 16 * 4);
		frame->mimicInfo.root = mb0 * mb1 * mb2 * InvM4(Minit);
		frame->mimicInfo.chest = Minit*(b_Spine * b_Spine1 * b_Spine2)* InvM4(Minit);// Minit * *InvM4(Minit);
		//frame->mimicInfo.right_clavicle.printMat();
		//====================================================
		M4  RM10, RMtb, R0A, Mpos;
		 

		Idx = frame->mimicInfo.jnameMapIdx["left_clavicle"];
		Rot = GetRotateMat(frame->fbxLinks[Idx].nodeRot[0], frame->fbxLinks[Idx].nodeRot[1], frame->fbxLinks[Idx].nodeRot[2]);
		frame->mimicInfo.left_clavicle = frame->mimicInfo.jnameGlobalTrsMap["left_clavicle"] * Rot
			*(InvM4(frame->mimicInfo.jnameLocalTrsMap["left_clavicle"]))*
			(InvM4(frame->mimicInfo.jnameGlobalTrsMap["left_clavicle"]));


		frame->mimicInfo.left_elbow = frame->fbxLinks[10].nodeRot[2];
		frame->mimicInfo.right_elbow = frame->fbxLinks[14].nodeRot[2];
		float v = frame->fbxLinks[1].nodePos[2] ;
		frame->mimicInfo.root_height = frame->fbxLinks[1].nodePos[2];
		
		 
	}

     FILE* file = fopen("transact.txt","wt+");
    fprintf(file, "{\n");
	fprintf(file, "	\"Loop\": \"wrap\",\n");
	fprintf(file, "		\"Frames\" :\n");
	fprintf(file, "		[\n");
        for (int fcount = 0; fcount < KeyFrameCount;fcount++)
	 {
		 motionFrames[fcount].mimicInfo.PrintCharFrame(file);
	 }

	 fprintf(file,"]\n}\n\n");
        fclose(file);
	 printf("################FINISH:%i##################\n",KeyFrameCount);   
	 
	 file = fopen("transact.dat","wb+");
	 for (int fcount = 0; fcount < KeyFrameCount;fcount++)
	 {
		 motionFrames[fcount].mimicInfo.dump2file(file);
	 }
	 fclose(file);

}
/*void FbxAnimationUpdate::FbxAnimationCover(FbxAnimLayer* pAnimLayer, FbxArray<FbxNode *> FbxSkeletonArray, const msg::FbxAnimationData& fbxAnimationData)
{
	if (pAnimLayer == NULL || FbxSkeletonArray.Size() == 0)
		return;

	for (int i = 0; i < FbxSkeletonArray.Size(); ++i)
	{
		FbxNode *pFbxNode = FbxSkeletonArray[i];
		FbxAnimCurve* lAnimCurve = pFbxNode->LclTranslation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_X);
		if (lAnimCurve == NULL)
			continue;
		int lKeyCount = lAnimCurve->KeyGetCount();

		FbxArray<FbxTime> KeyTimes;
		for (int lCount = 0; lCount < lKeyCount; lCount++)
		{
			FbxTime lKeyTime = lAnimCurve->KeyGetTime(lCount);
			KeyTimes.Add(lKeyTime);
		}

		for (int k = 0; k < fbxAnimationData.skeleton_nodes_size(); ++k)
		{
			const msg::FbxSkeletonNode &fbxSkeletonNode = fbxAnimationData.skeleton_nodes(k);
			if (fbxSkeletonNode.name() == std::string(pFbxNode->GetName()))
			{
				FbxAnimCurve* lAnimCurveTX = pFbxNode->LclTranslation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_X);
				FbxAnimCurve* lAnimCurveTY = pFbxNode->LclTranslation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Y);
				FbxAnimCurve* lAnimCurveTZ = pFbxNode->LclTranslation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Z);
				FbxAnimCurve* lAnimCurveRX = pFbxNode->LclRotation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_X);
				FbxAnimCurve* lAnimCurveRY = pFbxNode->LclRotation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Y);
				FbxAnimCurve* lAnimCurveRZ = pFbxNode->LclRotation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Z);

				lAnimCurveTX->KeyModifyBegin();
				lAnimCurveTY->KeyModifyBegin();
				lAnimCurveTZ->KeyModifyBegin();
				lAnimCurveRX->KeyModifyBegin();
				lAnimCurveRY->KeyModifyBegin();
				lAnimCurveRZ->KeyModifyBegin();

				int lKeyIndexTX = 0;
				int lKeyIndexTY = 0;
				int lKeyIndexTZ = 0;
				int lKeyIndexRX = 0;
				int lKeyIndexRY = 0;
				int lKeyIndexRZ = 0;

				lAnimCurveTX->KeyClear();
				lAnimCurveTY->KeyClear();
				lAnimCurveTZ->KeyClear();
				lAnimCurveRX->KeyClear();
				lAnimCurveRY->KeyClear();
				lAnimCurveRZ->KeyClear();

				for (int lCount = 0; lCount < lKeyCount; lCount++)
				{
					const msg::FbxTransMatrix& LocalTransMatrix = fbxSkeletonNode.local_transition_matrix(lCount);

					lKeyIndexTX = lAnimCurveTX->KeyAdd(KeyTimes[lCount]);
					lAnimCurveTX->KeySet(lKeyIndexTX, KeyTimes[lCount], LocalTransMatrix.translation()[0]);

					lKeyIndexTY = lAnimCurveTY->KeyAdd(KeyTimes[lCount]);
					lAnimCurveTY->KeySet(lKeyIndexTY, KeyTimes[lCount], LocalTransMatrix.translation()[1]);

					lKeyIndexTZ = lAnimCurveTZ->KeyAdd(KeyTimes[lCount]);
					lAnimCurveTZ->KeySet(lKeyIndexTZ, KeyTimes[lCount], LocalTransMatrix.translation()[2]);

					lKeyIndexRX = lAnimCurveRX->KeyAdd(KeyTimes[lCount]);
					lAnimCurveRX->KeySet(lKeyIndexRX, KeyTimes[lCount], RADIANTODEGREE(LocalTransMatrix.rotation()[0]));

					lKeyIndexRY = lAnimCurveRY->KeyAdd(KeyTimes[lCount]);
					lAnimCurveRY->KeySet(lKeyIndexRY, KeyTimes[lCount], RADIANTODEGREE(LocalTransMatrix.rotation()[1]));

					lKeyIndexRZ = lAnimCurveRZ->KeyAdd(KeyTimes[lCount]);
					lAnimCurveRZ->KeySet(lKeyIndexRZ, KeyTimes[lCount], RADIANTODEGREE(LocalTransMatrix.rotation()[2]));
				}

				lAnimCurveTX->KeyModifyEnd();
				lAnimCurveTY->KeyModifyEnd();
				lAnimCurveTZ->KeyModifyEnd();
				lAnimCurveRX->KeyModifyEnd();
				lAnimCurveRY->KeyModifyEnd();
				lAnimCurveRZ->KeyModifyEnd();

				break;
			}
		}
	}
}*/


/*
			{
				FbxAnimCurve* lAnimCurveTX = pFbxNode->LclTranslation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_X);
				FbxAnimCurve* lAnimCurveTY = pFbxNode->LclTranslation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Y);
				FbxAnimCurve* lAnimCurveTZ = pFbxNode->LclTranslation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Z);
				FbxAnimCurve* lAnimCurveRX = pFbxNode->LclRotation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_X);
				FbxAnimCurve* lAnimCurveRY = pFbxNode->LclRotation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Y);
				FbxAnimCurve* lAnimCurveRZ = pFbxNode->LclRotation.GetCurve(pAnimLayer, FBXSDK_CURVENODE_COMPONENT_Z);

				lAnimCurveTX->KeyModifyBegin();
				lAnimCurveTY->KeyModifyBegin();
				lAnimCurveTZ->KeyModifyBegin();
				lAnimCurveRX->KeyModifyBegin();
				lAnimCurveRY->KeyModifyBegin();
				lAnimCurveRZ->KeyModifyBegin();

				int lKeyIndexTX = 0;
				int lKeyIndexTY = 0;
				int lKeyIndexTZ = 0;
				int lKeyIndexRX = 0;
				int lKeyIndexRY = 0;
				int lKeyIndexRZ = 0;

				lAnimCurveTX->KeyClear();
				lAnimCurveTY->KeyClear();
				lAnimCurveTZ->KeyClear();
				lAnimCurveRX->KeyClear();
				lAnimCurveRY->KeyClear();
				lAnimCurveRZ->KeyClear();

				for (int lCount = 0; lCount < lKeyCount; lCount++)
				{
					FbxAMatrix  LocalTransMatrix = MatArray[lCount][i]; // fbxSkeletonNode.local_transition_matrix(lCount); //lKeyCount - 1 - 

					lKeyIndexTX = lAnimCurveTX->KeyAdd(KeyTimes[lCount]);
					lAnimCurveTX->KeySet(lKeyIndexTX, KeyTimes[lCount], LocalTransMatrix.GetT()[0]);
					lAnimCurveTX->KeySetInterpolation(lKeyIndexTX, FbxAnimCurveDef::eInterpolationLinear);  

					lKeyIndexTY = lAnimCurveTY->KeyAdd(KeyTimes[lCount]);
					lAnimCurveTY->KeySet(lKeyIndexTY, KeyTimes[lCount], LocalTransMatrix.GetT()[1]);
                    lAnimCurveTY->KeySetInterpolation(lKeyIndexTY, FbxAnimCurveDef::eInterpolationLinear);  

					lKeyIndexTZ = lAnimCurveTZ->KeyAdd(KeyTimes[lCount]);
					lAnimCurveTZ->KeySet(lKeyIndexTZ, KeyTimes[lCount], LocalTransMatrix.GetT()[2]);
					lAnimCurveTZ->KeySetInterpolation(lKeyIndexTZ, FbxAnimCurveDef::eInterpolationLinear);

					lKeyIndexRX = lAnimCurveRX->KeyAdd(KeyTimes[lCount]);
					lAnimCurveRX->KeySet(lKeyIndexRX, KeyTimes[lCount], LocalTransMatrix.GetR()[0]);
                    lAnimCurveRX->KeySetInterpolation(lKeyIndexRX, FbxAnimCurveDef::eInterpolationLinear);  

					lKeyIndexRY = lAnimCurveRY->KeyAdd(KeyTimes[lCount]);
					lAnimCurveRY->KeySet(lKeyIndexRY, KeyTimes[lCount], LocalTransMatrix.GetR()[1]);
                    lAnimCurveRY->KeySetInterpolation(lKeyIndexRY, FbxAnimCurveDef::eInterpolationLinear);

					lKeyIndexRZ = lAnimCurveRZ->KeyAdd(KeyTimes[lCount]);
					lAnimCurveRZ->KeySet(lKeyIndexRZ, KeyTimes[lCount], LocalTransMatrix.GetR()[2]);
					lAnimCurveRZ->KeySetInterpolation(lKeyIndexRZ, FbxAnimCurveDef::eInterpolationLinear);

					printf("node %i,time %i, rotation %.2lf,  %.2lf,  %.2lf \n", i, lCount,
						LocalTransMatrix.GetR()[0],
						LocalTransMatrix.GetR()[1],
						LocalTransMatrix.GetR()[2]);
				}

				lAnimCurveTX->KeyModifyEnd();
				lAnimCurveTY->KeyModifyEnd();
				lAnimCurveTZ->KeyModifyEnd();
				lAnimCurveRX->KeyModifyEnd();
				lAnimCurveRY->KeyModifyEnd();
				lAnimCurveRZ->KeyModifyEnd();
 
			}
*/
