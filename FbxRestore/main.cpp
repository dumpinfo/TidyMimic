//#include "ModelReconstruct.h"
#include <string>
#include "zmath.h"
#include "FbxCommonTool.h"
#include "FbxAnimationUpdate.h"
#include <iostream> 
using namespace std;

//FbxParser *parser;
bool gSupportVBO;
//struct 
int main(int argc, char **argv)
{
map<int, string> mmicNameIdx;
mmicNameIdx[ 0 ] ="root"           ;
mmicNameIdx[ 1 ] ="chest"          ;
mmicNameIdx[ 2 ] ="neck"           ;
mmicNameIdx[ 3 ] ="right_hip"      ;
mmicNameIdx[ 4 ] ="right_knee"     ;
mmicNameIdx[ 5 ] ="right_ankle"    ;
mmicNameIdx[ 6 ] ="right_clavicle" ;
mmicNameIdx[ 7 ] ="right_shoulder" ;
mmicNameIdx[ 8 ] ="right_elbow"    ;
mmicNameIdx[ 9 ] ="right_wrist"    ;
mmicNameIdx[ 10] ="left_hip"       ;
mmicNameIdx[ 11] ="left_knee"      ;
mmicNameIdx[ 12] ="left_ankle"     ;
mmicNameIdx[ 13] ="left_clavicle"  ;
mmicNameIdx[ 14] ="left_shoulder"  ;
mmicNameIdx[ 15] ="left_elbow"     ;
mmicNameIdx[ 16] ="left_wrist"     ;

 map<string, int> jnameMapIdx;
 jnameMapIdx["chest"]           = 5;  
 jnameMapIdx["neck"]            = 6;  
 jnameMapIdx["right_hip"]       = 20; 
 jnameMapIdx["right_ankle"]     = 22; 
 jnameMapIdx["right_clavicle"]  = 12; 
 jnameMapIdx["right_shoulder"]  = 13; 
 jnameMapIdx["left_hip"]        = 16; 
 jnameMapIdx["left_ankle"]      = 18; 
 jnameMapIdx["left_clavicle"]   = 8;  
 jnameMapIdx["left_shoulder"]   = 9;  
 jnameMapIdx["left_elbow"]      = 10; 
 jnameMapIdx["right_elbow"]     = 14; 

	vector<MimicMotionInfo> frame_actions;
	int framenum = fileSize("transact.dat")/720; //file2data()
	printf("framenum: %i\n", framenum);
	frame_actions.resize(framenum);
	
	/*int i,j;
	for(j =0; j<framenum; j++)
	for ( i = 0; i < 17; i++)
	{
		RotSeq rotSeq;
		rotSeq = zyx;
		Quaternion q;
		M4 mb0, mb1;
		mb1 = MMcInfo.jointMat[i];
		V4 quant = mRot2Quat(mb1);
		q.w = quant.v[0]; q.x = quant.v[1]; q.y = quant.v[2]; q.z = quant.v[3];
		double res[4];
		quaternion2Euler(q, res, rotSeq);
		printf("( %6.3f, %6.3f, %6.3f  );\n",
			res[0] * 180.0 / 3.1415826,
			res[1] * 180.0 / 3.1415826,
			res[2] * 180.0 / 3.1415826);
		
	}*/
	int i,j;
	FILE*file = fopen("transact.dat", "rb");
	
	for( i=0; i< framenum; i++)
	{
		printf("======================================================\n");
		frame_actions[i].file2data(file);
		frame_actions[i].JointTranformBack(frame_actions[i].jointMat);
		for(j=0;j<24;j++) memset(&frame_actions[i].jointPos[j],0,sizeof(frame_actions[i].jointPos[j]));
		
		for(j=0;j<17;j++)
		{
			RotSeq rotSeq;
		    rotSeq = zyx;
		    Quaternion q;
			V4 quant = mRot2Quat(frame_actions[i].jointMat[j]);
		    q.w = quant.v[0]; q.x = quant.v[1]; q.y = quant.v[2]; q.z = quant.v[3];
		    double res[4];
		    quaternion2Euler(q, res, rotSeq);
		    printf("( %6.3f, %6.3f, %6.3f  );\n",
			   res[0] * 180.0 / 3.1415826,
			   res[1] * 180.0 / 3.1415826,
			   res[2] * 180.0 / 3.1415826);
			string nodename = mmicNameIdx[i+1];
			if(nodename=="")continue;
			int FbxIdx = jnameMapIdx[nodename];
			if(FbxIdx == 0)continue;
			frame_actions[i].jointPos[FbxIdx].Rot[0] = res[0] * 180.0 / 3.1415826;
			frame_actions[i].jointPos[FbxIdx].Rot[1] = res[1] * 180.0 / 3.1415826;
			frame_actions[i].jointPos[FbxIdx].Rot[2] = res[2] * 180.0 / 3.1415826;
		}
		
		//frame->mimicInfo.left_knee = frame->fbxLinks[17].nodeRot[2];
		//frame->mimicInfo.right_knee = frame->fbxLinks[21].nodeRot[2];
		//frame->mimicInfo.left_elbow = frame->fbxLinks[10].nodeRot[2];
		//frame->mimicInfo.right_elbow = frame->fbxLinks[14].nodeRot[2];
		frame_actions[i].jointPos[17].Rot[2] = frame_actions[i].left_knee;
		frame_actions[i].jointPos[21].Rot[2] = frame_actions[i].right_knee;
		frame_actions[i].jointPos[10].Rot[2] = frame_actions[i].left_elbow;
		frame_actions[i].jointPos[14].Rot[2] = frame_actions[i].right_elbow;
		 
		
	}
	
	fclose(file);
	
	FbxCommonTool FbxTool;
	FbxManager* lFbxSdkManager = NULL;
	FbxScene* lFbxScene = NULL;

	
	FbxTool.InitializeSdkObjects(lFbxSdkManager, lFbxScene);
	FbxTool.LoadScene(lFbxSdkManager, lFbxScene, argv[1]);
	 

	FbxAnimationUpdate fnn;
	 
	fnn.FbxAnimationCover(lFbxScene, "");
	FbxTool.SaveScene(lFbxSdkManager, lFbxScene, "aa2.fbx");

	 
}
