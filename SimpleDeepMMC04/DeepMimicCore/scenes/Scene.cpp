#include "Scene.h"

cScene::cScene()
{
	mRand.Seed(cMathUtil::RandUint());
	mRandSeed = 0;
	mHasRandSeed = false;
	
	mMode = eModeTrain;
	mSampleCount = 0;
}

cScene::~cScene()
{
}

void cScene::ParseArgs(const std::shared_ptr<cArgParser>& parser)
{
	mArgParser = parser;

	std::string timer_type_str = "";
	mArgParser->ParseString("timer_type", timer_type_str);
	mTimerParams.mType = cTimer::ParseTypeStr(timer_type_str);
	mArgParser->ParseDouble("time_lim_min", mTimerParams.mTimeMin);
	mArgParser->ParseDouble("time_lim_max", mTimerParams.mTimeMax);
	mArgParser->ParseDouble("time_lim_exp", mTimerParams.mTimeExp);
}

void cScene::Init()
{
	if (HasRandSeed())
	{
		SetRandSeed(mRandSeed);
	}

	InitTimers();
	ResetParams();
	mSampleCount = 0;
}

void cScene::Clear()
{
	ResetParams();
	mSampleCount = 0;
}
//--------------------------------------------------------------------------
double cScene::GetRewardFail(int agent_id)
{
	return GetRewardMin(agent_id);
}

double cScene::GetRewardSucc(int agent_id)
{
	return GetRewardMax(agent_id);
}

bool cScene::IsEpisodeEnd() const
{
	bool is_end = mTimer.IsEnd();//cScene::IsEpisodeEnd();
	eTerminate termin = eTerminateNull;
	for (int i = 0; i < GetNumAgents(); ++i)
	{
		termin = CheckTerminate(i);
		if (termin != eTerminateNull)
		{
			is_end = true;
			break;
		}
	}
	return is_end;
}

cScene::eTerminate cScene::CheckTerminate(int agent_id) const
{
	return eTerminateNull;
}

void cScene::SetSampleCount(int count)
{
	mSampleCount = count;
}

void cScene::SetMode(eMode mode)
{
	mMode = mode;
}
//--------------------------------------------------------------------------
void cScene::Reset()
{
	ResetScene();
}

void cScene::Update(double timestep)
{
	UpdateTimers(timestep);
}

void cScene::Draw()
{
}

void cScene::Keyboard(unsigned char key, double device_x, double device_y)
{
}

void cScene::MouseClick(int button, int state, double device_x, double device_y)
{
}

void cScene::MouseMove(double device_x, double device_y)
{
}


void cScene::Reshape(int w, int h)
{
}

void cScene::Shutdown()
{
}

bool cScene::IsDone() const
{
	return false;
}

double cScene::GetTime() const
{
	return mTimer.GetTime();
}

bool cScene::HasRandSeed() const
{
	return mHasRandSeed;
}

void cScene::SetRandSeed(unsigned long seed)
{
	mHasRandSeed = true;
	mRandSeed = seed;
	mRand.Seed(seed);
}

unsigned long cScene::GetRandSeed() const
{
	return mRandSeed;
}

/*bool cScene::IsEpisodeEnd() const
{
	return ;
}*/

bool cScene::CheckValidEpisode() const
{
	return true;
}

void cScene::ResetParams()
{
	ResetTimers();
}

void cScene::ResetScene()
{
	ResetParams();
}

void cScene::InitTimers()
{
	mTimer.Init(mTimerParams);
}

void cScene::ResetTimers()
{
	mTimer.Reset();
}

void cScene::UpdateTimers(double timestep)
{
	mTimer.Update(timestep);
}