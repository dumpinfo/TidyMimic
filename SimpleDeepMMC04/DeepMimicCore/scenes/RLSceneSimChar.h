#pragma once

#include "Scene.h"
//#include "SceneSimChar.h"
#include "sim/AgentRegistry.h"
#include "util/Annealer.h"
//#include "scenes/RLSceneSimChar.h"
#include "anim/KinCharacter.h"
#include "scenes/DrawScene.h"
#include "sim/World.h"
//#include "sim/SimCharBuilder.h"
#include "sim/SimCharacter.h"
#include "sim/Ground.h"
#include "util/IndexBuffer.h"

class cRLSceneSimChar : virtual public cScene
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	struct tObjEntry
	{
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW

		std::shared_ptr<cSimRigidBody> mObj;
		double mEndTime;
		tVector mColor;
		bool mPersist;

		tObjEntry();
		bool IsValid() const;
	};
bool mEnableContactFall;
bool mEnableRandCharPlacement;
cGround::tParams mGroundParams;
cIndexBuffer<tObjEntry, Eigen::aligned_allocator<tObjEntry>> mObjs;
cWorld::tParams mWorldParams;
std::shared_ptr<cGround> mGround;
std::shared_ptr<cWorld> mWorld;
std::vector<cSimCharacter::tParams> mCharParams;
std::vector<int> mFallContactBodies;
std::vector<std::shared_ptr<cSimCharacter>> mChars;
std::vector< tCtrlParams> mCtrlParams;
 
virtual bool HasFallen(const cSimCharacter& sim_char) const;
virtual bool LoadControlParams(const std::string& param_file, const std::shared_ptr<cSimCharacter>& out_char);
virtual bool ParseCharCtrlParams(const std::shared_ptr<cArgParser>& parser, std::vector< tCtrlParams>& out_params) const;
virtual bool ParseCharParams(const std::shared_ptr<cArgParser>& parser, std::vector<cSimCharacter::tParams>& out_params) const;
virtual const std::shared_ptr<cGround>& GetGround() const;
virtual const std::shared_ptr<cSimCharacter>& GetCharacter() const;
virtual const std::shared_ptr<cSimCharacter>& GetCharacter(int char_id) const;
virtual const std::shared_ptr<cWorld>& GetWorld() const;
virtual const tVector& GetGravity() const;
virtual int GetNumChars() const;
virtual int GetNumParamSets() const;
 
virtual tVector GetCharPos() const;
virtual void BuildGround();
virtual void BuildWorld();
virtual void CalcCharRandPlacement(const std::shared_ptr<cSimCharacter>& out_char, tVector& out_pos, tQuaternion& out_rot);
 
virtual void GetViewBound(tVector& out_min, tVector& out_max) const;
 
virtual void InitCharacterPos();
virtual void InitCharacterPos(const std::shared_ptr<cSimCharacter>& out_char);
virtual void InitCharacterPosFixed(const std::shared_ptr<cSimCharacter>& out_char);
virtual void OutputCharState(const std::string& out_file) const;
virtual void OutputGround(const std::string& out_file) const;
virtual void ParseGroundParams(const std::shared_ptr<cArgParser>& parser, cGround::tParams& out_params) const;
virtual void PostUpdateCharacters(double time_step);
virtual void PostUpdate(double timestep);
virtual void RayTest(const tVector& beg, const tVector& end, cWorld::tRayTestResult& out_result) const;
virtual void ResetGround();
virtual void ResetWorld();
virtual void SetCharRandPlacement(const std::shared_ptr<cSimCharacter>& out_char);
virtual void SetFallContacts(const std::vector<int>& fall_bodies, cSimCharacter& out_char) const;
virtual void SetGroundParamBlend(double lerp);
virtual void SetRandSeed(unsigned long seed);
virtual void Update(double time_elapsed);
virtual void UpdateGround(double time_elapsed);
virtual void UpdateWorld(double time_step);

//###################################################################
	cRLSceneSimChar();
	virtual ~cRLSceneSimChar();

	virtual void ParseArgs(const std::shared_ptr<cArgParser>& parser);
	virtual void Init();
	virtual void Clear();

	virtual int GetNumAgents() const;
	virtual bool NeedNewAction(int agent_id) const;
	virtual void RecordState(int agent_id, Eigen::VectorXd& out_state) const;
	virtual void RecordGoal(int agent_id, Eigen::VectorXd& out_goal) const;
	virtual void SetAction(int agent_id, const Eigen::VectorXd& action);

	virtual eActionSpace GetActionSpace(int agent_id) const;
	virtual int GetStateSize(int agent_id) const;
	virtual int GetGoalSize(int agent_id) const;
	virtual int GetActionSize(int agent_id) const;
	virtual int GetNumActions(int agent_id) const;

	virtual void BuildStateOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const;
	virtual void BuildGoalOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const;
	virtual void BuildActionOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const;
	virtual void BuildActionBounds(int agent_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max) const;

	virtual void BuildStateNormGroups(int agent_id, Eigen::VectorXi& out_groups) const;
	virtual void BuildGoalNormGroups(int agent_id, Eigen::VectorXi& out_groups) const;

	virtual double GetRewardMin(int agent_id) const;
	virtual double GetRewardMax(int agent_id) const;

	virtual eTerminate CheckTerminate(int agent_id) const;
	virtual bool CheckValidEpisode() const;
	virtual void LogVal(int agent_id, double val);
	virtual void SetSampleCount(int count);

	virtual std::string GetName() const;
	
	virtual const std::shared_ptr<cKinCharacter>& GetKinChar() const;
	virtual void EnableRandRotReset(bool enable);
	virtual bool EnabledRandRotReset() const;

	virtual double CalcReward(int agent_id) const;


protected:

	std::string mMotionFile;
	std::shared_ptr<cKinCharacter> mKinChar;

	Eigen::VectorXd mJointWeights;
	bool mEnableRandRotReset;
	bool mSyncCharRootPos;
	bool mSyncCharRootRot;
	bool mEnableRootRotFail;
	double mHoldEndFrame;

	virtual bool BuildCharacters();

	virtual void CalcJointWeights(const std::shared_ptr<cSimCharacter>& character, Eigen::VectorXd& out_weights) const;
	virtual bool BuildController(const  tCtrlParams& ctrl_params, std::shared_ptr<cDeepMimicCharController>& out_ctrl);
	virtual void BuildKinChar();
	virtual bool BuildKinCharacter(int id, std::shared_ptr<cKinCharacter>& out_char) const;
	virtual void UpdateCharacters(double timestep);
	virtual void UpdateKinChar(double timestep);

	virtual void ResetCharacters();
	virtual void ResetKinChar();
	virtual void SyncCharacters();
	virtual bool EnableSyncChar() const;

	virtual void InitJointWeights();
	virtual void ResolveCharGroundIntersect();
	virtual void ResolveCharGroundIntersect(const std::shared_ptr<cSimCharacter>& out_char) const;
	virtual void SyncKinCharRoot();
	virtual void SyncKinCharNewCycle(const cSimCharacter& sim_char, cKinCharacter& out_kin_char) const;

	virtual double GetKinTime() const;
	virtual bool CheckKinNewCycle(double timestep) const;
	virtual bool CheckRootRotFail(const cSimCharacter& sim_char) const;
	virtual bool CheckRootRotFail(const cSimCharacter& sim_char, const cKinCharacter& kin_char) const;
	
	virtual double CalcRandKinResetTime();
	virtual double CalcRewardImitate(const cSimCharacter& sim_char, const cKinCharacter& ref_char) const;
protected:

	bool mEnableFallEnd;
	cAgentRegistry mAgentReg;
	cTimer::tParams mTimerParamsEnd;
	cAnnealer mTimerAnnealer;
	int mAnnealSamples;

	virtual void ResetParams();
	virtual void ResetScene();
	virtual const std::shared_ptr<cDeepMimicCharController>& GetController() const;
	virtual const std::shared_ptr<cDeepMimicCharController>& GetController(int agent_id) const;
	virtual const cSimCharacter* GetAgentChar(int agent_id) const;

	virtual void PreUpdate(double timestep);
	virtual void ResetTimers();

	virtual void NewActionUpdate(int agent_id);
	virtual bool EnableFallEnd() const;

	virtual void RegisterAgents();
	virtual void RegisterAgent(const std::shared_ptr<cDeepMimicCharController>& ctrl, const std::shared_ptr<cSimCharacter>& character);
	virtual void RegisterAgent(const std::shared_ptr<cDeepMimicCharController>& ctrl, const std::shared_ptr<cSimCharacter>& character,
								std::vector<int>& out_ids);

	virtual void SetupTimerAnnealer(cAnnealer& out_annealer) const;
	virtual void UpdateTimerParams();
};
