#include "RLSceneSimChar.h"
#include "sim/DeepMimicCharController.h"
//#include "SceneImitate.h"
#include "sim/RBDUtil.h"
#include "util/FileUtil.h"
#include "util/JsonUtil.h"

#include <memory>
#include <ctime>
//#include "sim/SimBox.h"
#include "sim/GroundPlane.h"
#include "sim/GroundBuilder.h"
#include "sim/DeepMimicCharController.h"
//#include "sim/CtPDController.h"
#include "util/FileUtil.h"

const int gDefaultCharID = 0;
const double gCharViewDistPad = 1;
 
const size_t gInitGroundUpdateCount = std::numeric_limits<size_t>::max();

cRLSceneSimChar::tObjEntry::tObjEntry()
{
	mObj = nullptr;
	mEndTime = std::numeric_limits<double>::infinity();
	mColor = tVector(0.5, 0.5, 0.5, 1);
	mPersist = false;
}

bool cRLSceneSimChar::tObjEntry::IsValid() const
{
	return mObj != nullptr;
}

 


void cRLSceneSimChar::Clear()
{
	//cRLScene::Clear();
	//cSceneSimChar::Clear(); 
	cScene::Clear();
	mChars.clear();
	mGround.reset();
	mFallContactBodies.clear();
	//-----------------------------------
	mAgentReg.Clear();
}

 

void cRLSceneSimChar::Update(double time_elapsed)
{
	cScene::Update(time_elapsed);

	if (time_elapsed < 0)
	{
		return;
	}

 
	UpdateCharacters(time_elapsed);
	UpdateWorld(time_elapsed);
	UpdateGround(time_elapsed);
 

	PostUpdateCharacters(time_elapsed);
	PostUpdate(time_elapsed);
}

int cRLSceneSimChar::GetNumChars() const
{
	return static_cast<int>(mChars.size());
}

const std::shared_ptr<cSimCharacter>& cRLSceneSimChar::GetCharacter()  const
{
	return GetCharacter(gDefaultCharID);
}

const std::shared_ptr<cSimCharacter>& cRLSceneSimChar::GetCharacter(int char_id) const
{
	return mChars[char_id];
}

const std::shared_ptr<cWorld>& cRLSceneSimChar::GetWorld() const
{
	return mWorld;
}

tVector cRLSceneSimChar::GetCharPos() const
{
	return GetCharacter()->GetRootPos();
}

const std::shared_ptr<cGround>& cRLSceneSimChar::GetGround() const
{
	return mGround;
}

const tVector& cRLSceneSimChar::GetGravity() const
{
	return mWorldParams.mGravity;
}

bool cRLSceneSimChar::LoadControlParams(const std::string& param_file, const std::shared_ptr<cSimCharacter>& out_char)
{
	const auto& ctrl = out_char->GetController();
	bool succ = ctrl->LoadParams(param_file);
	return succ;
}



void cRLSceneSimChar::RayTest(const tVector& beg, const tVector& end, cWorld::tRayTestResult& out_result) const
{
	cWorld::tRayTestResults results;
	mWorld->RayTest(beg, end, results);

	out_result.mObj = nullptr;
	if (results.size() > 0)
	{
		out_result = results[0];
	}
}

void cRLSceneSimChar::SetGroundParamBlend(double lerp)
{
	mGround->SetParamBlend(lerp);
}

int cRLSceneSimChar::GetNumParamSets() const
{
	return static_cast<int>(mGroundParams.mParamArr.rows());
}

void cRLSceneSimChar::OutputCharState(const std::string& out_file) const
{
	const auto& char0 = GetCharacter();
	tVector root_pos = char0->GetRootPos();
	double ground_h = mGround->SampleHeight(root_pos);
	tMatrix trans = char0->BuildOriginTrans();
	trans(1, 3) -= ground_h;

	char0->WriteState(out_file, trans);
}

void cRLSceneSimChar::OutputGround(const std::string& out_file) const
{
	mGround->Output(out_file);
}

 

bool cRLSceneSimChar::BuildCharacters()
{
	bool succ = true;
	mChars.clear();

	int num_chars = static_cast<int>(mCharParams.size());
	for (int i = 0; i < num_chars; ++i)
	{
		const cSimCharacter::tParams& curr_params = mCharParams[i];
		std::shared_ptr<cSimCharacter> curr_char;

		curr_char = std::shared_ptr<cSimCharacter>(new cSimCharacter());

		succ &= curr_char->Init(mWorld, curr_params);
		if (succ)
		{
			SetFallContacts(mFallContactBodies, *curr_char);
			curr_char->RegisterContacts(cWorld::eContactFlagCharacter, cWorld::eContactFlagEnvironment);

			InitCharacterPos(curr_char);

			if (i < mCtrlParams.size())
			{
				auto ctrl_params = mCtrlParams[i];
				ctrl_params.mChar = curr_char;
				ctrl_params.mGravity = GetGravity();
				ctrl_params.mGround = mGround;

				std::shared_ptr<cDeepMimicCharController> ctrl;
				succ = BuildController(ctrl_params, ctrl);
				if (succ && ctrl != nullptr)
				{
					curr_char->SetController(ctrl);
				}
			}

			mChars.push_back(curr_char);
		}
	}
    
	if (EnableSyncChar())
	{
		SyncCharacters();
	}
	return succ;
}

 

bool cRLSceneSimChar::ParseCharParams(const std::shared_ptr<cArgParser>& parser, std::vector<cSimCharacter::tParams>& out_params) const
{
	bool succ = true;

	std::vector<std::string> char_files;
	succ = parser->ParseStrings("character_files", char_files);

	std::vector<std::string> state_files;
	parser->ParseStrings("state_files", state_files);

	std::vector<double> init_pos_xs;
	parser->ParseDoubles("char_init_pos_xs", init_pos_xs);
	
	int num_files = static_cast<int>(char_files.size());
	out_params.resize(num_files);
	for (int i = 0; i < num_files; ++i)
	{
		cSimCharacter::tParams& params = out_params[i];
		params.mID = i;
		params.mCharFile = char_files[i];
		
		params.mEnableContactFall = mEnableContactFall;

		if (state_files.size() > i)
		{
			params.mStateFile = state_files[i];
		}

		if (init_pos_xs.size() > i)
		{
			params.mInitPos[0] = init_pos_xs[i];
		}
	}

	if (!succ)
	{
		printf("No valid character file specified.\n");
	}

	return succ;
}


bool cRLSceneSimChar::ParseCharCtrlParams(const std::shared_ptr<cArgParser>& parser, std::vector< tCtrlParams>& out_params) const
{
	bool succ = true;

	std::vector<std::string> ctrl_files;
	parser->ParseStrings("char_ctrl_files", ctrl_files);

	int num_ctrls = static_cast<int>(ctrl_files.size());

	std::vector<std::string> char_ctrl_strs;
	parser->ParseStrings("char_ctrls", char_ctrl_strs);

	out_params.resize(num_ctrls);
	for (int i = 0; i < num_ctrls; ++i)
	{
		auto& ctrl_params = out_params[i];
		const std::string& type_str = char_ctrl_strs[i];
		//cCtrlBuilder::ParseCharCtrl(type_str, ctrl_params.mCharCtrl);
		ctrl_params.mCharCtrl = eCharCtrlCtPD;
		ctrl_params.mCtrlFile = ctrl_files[i];
	}

	return succ;
}

void cRLSceneSimChar::BuildWorld()
{
	mWorld = std::shared_ptr<cWorld>(new cWorld());
	mWorld->Init(mWorldParams);
}

void cRLSceneSimChar::BuildGround()
{
	mGroundParams.mHasRandSeed = mHasRandSeed;
	mGroundParams.mRandSeed = mRandSeed;
	cGroundBuilder::BuildGround(mWorld, mGroundParams, mGround);
}

 

void cRLSceneSimChar::SetFallContacts(const std::vector<int>& fall_bodies, cSimCharacter& out_char) const
{
	int num_fall_bodies = static_cast<int>(fall_bodies.size());
	if (num_fall_bodies > 0)
	{
		for (int i = 0; i < out_char.GetNumBodyParts(); ++i)
		{
			out_char.SetBodyPartFallContact(i, false);
		}

		for (int i = 0; i < num_fall_bodies; ++i)
		{
			int b = fall_bodies[i];
			out_char.SetBodyPartFallContact(b, true);
		}
	}
}

void cRLSceneSimChar::InitCharacterPos()
{
	int num_chars = GetNumChars();
	for (int i = 0; i < num_chars; ++i)
	{
		InitCharacterPos(mChars[i]);
	}
}

void cRLSceneSimChar::InitCharacterPos(const std::shared_ptr<cSimCharacter>& out_char)
{
	if (mEnableRandCharPlacement)
	{
		SetCharRandPlacement(out_char);
	}
	else
	{
		InitCharacterPosFixed(out_char);
	}
}

void cRLSceneSimChar::InitCharacterPosFixed(const std::shared_ptr<cSimCharacter>& out_char)
{
	tVector root_pos = out_char->GetRootPos();
	int char_id = out_char->GetID();
	root_pos[0] = mCharParams[char_id].mInitPos[0];

	double h = mGround->SampleHeight(root_pos);
	root_pos[1] += h;

	out_char->SetRootPos(root_pos);
}

void cRLSceneSimChar::SetCharRandPlacement(const std::shared_ptr<cSimCharacter>& out_char)
{
	tVector rand_pos = tVector::Zero();
	tQuaternion rand_rot = tQuaternion::Identity();
	CalcCharRandPlacement(out_char, rand_pos, rand_rot);
	out_char->SetRootTransform(rand_pos, rand_rot);
}

void cRLSceneSimChar::CalcCharRandPlacement(const std::shared_ptr<cSimCharacter>& out_char, tVector& out_pos, tQuaternion& out_rot)
{
	tVector char_pos = out_char->GetRootPos();
	tQuaternion char_rot = out_char->GetRootRotation();

	tVector rand_pos;
	tQuaternion rand_rot;
	mGround->SamplePlacement(tVector::Zero(), rand_pos, rand_rot);

	out_pos = rand_pos;
	out_pos[1] += char_pos[1];
	out_rot = rand_rot * char_rot;
}





void cRLSceneSimChar::UpdateWorld(double time_step)
{
	mWorld->Update(time_step);
}

 

void cRLSceneSimChar::PostUpdateCharacters(double time_step)
{
	int num_chars = GetNumChars();
	for (int i = 0; i < num_chars; ++i)
	{
		const auto& curr_char = GetCharacter(i);
		curr_char->PostUpdate(time_step);
	}
}

void cRLSceneSimChar::UpdateGround(double time_elapsed)
{
	tVector view_min;
	tVector view_max;
	GetViewBound(view_min, view_max);
	mGround->Update(time_elapsed, view_min, view_max);
}

 




void cRLSceneSimChar::ResetWorld()
{
	mWorld->Reset();
}

void cRLSceneSimChar::ResetGround()
{
	mGround->Clear();

	tVector view_min;
	tVector view_max;
	GetViewBound(view_min, view_max);

	tVector view_size = view_max - view_min;
	view_min = -view_size;
	view_max = view_size;

 

	mGround->Update(0, view_min, view_max);
}

 

void cRLSceneSimChar::PostUpdate(double timestep)
{
}

void cRLSceneSimChar::GetViewBound(tVector& out_min, tVector& out_max) const
{
	const std::shared_ptr<cSimCharacter>& character = GetCharacter();
	const cDeepMimicCharController* ctrl = reinterpret_cast<cDeepMimicCharController*>(character->GetController().get());

	out_min.setZero();
	out_max.setZero();
	if (ctrl != nullptr)
	{
		ctrl->GetViewBound(out_min, out_max);
	}
	else
	{
		character->CalcAABB(out_min, out_max);
	}

	out_min += tVector(-gCharViewDistPad, 0, -gCharViewDistPad, 0);
	out_max += tVector(gCharViewDistPad, 0, gCharViewDistPad, 0);
}

void cRLSceneSimChar::ParseGroundParams(const std::shared_ptr<cArgParser>& parser, cGround::tParams& out_params) const
{
	std::string terrain_file = "";
	parser->ParseString("terrain_file", terrain_file);
	parser->ParseDouble("terrain_blend", out_params.mBlend);

	if (terrain_file != "")
	{
		bool succ = cGroundBuilder::ParseParamsJson(terrain_file, out_params);
		if (!succ)
		{
			printf("Failed to parse terrain params from %s\n", terrain_file.c_str());
			assert(false);
		}
	}
}


 

bool cRLSceneSimChar::HasFallen(const cSimCharacter& sim_char) const
{
	bool fallen = sim_char.HasFallen();

	tVector root_pos = sim_char.GetRootPos();
	tVector ground_aabb_min;
	tVector ground_aabb_max;
	mGround->CalcAABB(ground_aabb_min, ground_aabb_max);
	ground_aabb_min[1] = -std::numeric_limits<double>::infinity();
	ground_aabb_max[1] = std::numeric_limits<double>::infinity();
	bool in_aabb = cMathUtil::ContainsAABB(root_pos, ground_aabb_min, ground_aabb_max);
	fallen |= !in_aabb;
	if (mEnableRootRotFail)
	{
		fallen |= CheckRootRotFail(sim_char);
	}

	return fallen;
}
  

void cRLSceneSimChar::SetRandSeed(unsigned long seed)
{
	cScene::SetRandSeed(seed);
	if (mGround != nullptr)
	{
		mGround->SeedRand(seed);
	}
}

 
//################################################################
//################################################################
//cSceneImitate::cSceneImitate()
cRLSceneSimChar::cRLSceneSimChar()
{
	mEnableContactFall = true;
	mEnableRandCharPlacement = true;

	mWorldParams.mNumSubsteps = 1;
	mWorldParams.mScale = 1;
	mWorldParams.mGravity = gGravity;
	//-------------------------------------
	mEnableFallEnd = true;
	mAnnealSamples = gInvalidIdx;
	//==========================
	mEnableRandRotReset = false;
	mSyncCharRootPos = true;
	mSyncCharRootRot = false;
	mMotionFile = "";
	mEnableRootRotFail = false;
	mHoldEndFrame = 0;
}

double cRLSceneSimChar::CalcRewardImitate(const cSimCharacter& sim_char, const cKinCharacter& kin_char) const
{
	double pose_w = 0.5;
	double vel_w = 0.05;
	double end_eff_w = 0.15;
	double root_w = 0.2;
	double com_w = 0.1;

	double total_w = pose_w + vel_w + end_eff_w + root_w + com_w;
	pose_w /= total_w;
	vel_w /= total_w;
	end_eff_w /= total_w;
	root_w /= total_w;
	com_w /= total_w;

	const double pose_scale = 2;
	const double vel_scale = 0.1;
	const double end_eff_scale = 40;
	const double root_scale = 5;
	const double com_scale = 10;
	const double err_scale = 1;

	const auto& joint_mat = sim_char.GetJointMat();
	const auto& body_defs = sim_char.GetBodyDefs();
	double reward = 0;

	const Eigen::VectorXd& pose0 = sim_char.GetPose();
	const Eigen::VectorXd& vel0 = sim_char.GetVel();
	const Eigen::VectorXd& pose1 = kin_char.GetPose();
	const Eigen::VectorXd& vel1 = kin_char.GetVel();
	tMatrix origin_trans = sim_char.BuildOriginTrans();
	tMatrix kin_origin_trans = kin_char.BuildOriginTrans();

	tVector com0_world = sim_char.CalcCOM();
	tVector com_vel0_world = sim_char.CalcCOMVel();
	tVector com1_world;
	tVector com_vel1_world;
	cRBDUtil::CalcCoM(joint_mat, body_defs, pose1, vel1, com1_world, com_vel1_world);

	int root_id = sim_char.GetRootID();
	tVector root_pos0 = cKinTree::GetRootPos(joint_mat, pose0);
	tVector root_pos1 = cKinTree::GetRootPos(joint_mat, pose1);
	tQuaternion root_rot0 = cKinTree::GetRootRot(joint_mat, pose0);
	tQuaternion root_rot1 = cKinTree::GetRootRot(joint_mat, pose1);
	tVector root_vel0 = cKinTree::GetRootVel(joint_mat, vel0);
	tVector root_vel1 = cKinTree::GetRootVel(joint_mat, vel1);
	tVector root_ang_vel0 = cKinTree::GetRootAngVel(joint_mat, vel0);
	tVector root_ang_vel1 = cKinTree::GetRootAngVel(joint_mat, vel1);

	double pose_err = 0;
	double vel_err = 0;
	double end_eff_err = 0;
	double root_err = 0;
	double com_err = 0;
	double heading_err = 0;

	int num_end_effs = 0;
	int num_joints = sim_char.GetNumJoints();
	assert(num_joints == mJointWeights.size());

	double root_rot_w = mJointWeights[root_id];
	pose_err += root_rot_w * cKinTree::CalcRootRotErr(joint_mat, pose0, pose1);
	vel_err += root_rot_w * cKinTree::CalcRootAngVelErr(joint_mat, vel0, vel1);

	for (int j = root_id + 1; j < num_joints; ++j)
	{
		double w = mJointWeights[j];
		double curr_pose_err = cKinTree::CalcPoseErr(joint_mat, j, pose0, pose1);
		double curr_vel_err = cKinTree::CalcVelErr(joint_mat, j, vel0, vel1);
		pose_err += w * curr_pose_err;
		vel_err += w * curr_vel_err;

		bool is_end_eff = sim_char.IsEndEffector(j);
		if (is_end_eff)
		{
			tVector pos0 = sim_char.CalcJointPos(j);
			tVector pos1 = cKinTree::CalcJointWorldPos(joint_mat, pose1, j);
			double ground_h0 = mGround->SampleHeight(pos0);
			double ground_h1 = kin_char.GetOriginPos()[1];

			tVector pos_rel0 = pos0 - root_pos0;
			tVector pos_rel1 = pos1 - root_pos1;
			pos_rel0[1] = pos0[1] - ground_h0;
			pos_rel1[1] = pos1[1] - ground_h1;

			pos_rel0 = origin_trans * pos_rel0;
			pos_rel1 = kin_origin_trans * pos_rel1;

			double curr_end_err = (pos_rel1 - pos_rel0).squaredNorm();
			end_eff_err += curr_end_err;
			++num_end_effs;
		}
	}

	if (num_end_effs > 0)
	{
		end_eff_err /= num_end_effs;
	}

	double root_ground_h0 = mGround->SampleHeight(sim_char.GetRootPos());
	double root_ground_h1 = kin_char.GetOriginPos()[1];
	root_pos0[1] -= root_ground_h0;
	root_pos1[1] -= root_ground_h1;
	double root_pos_err = (root_pos0 - root_pos1).squaredNorm();

	double root_rot_err = cMathUtil::QuatDiffTheta(root_rot0, root_rot1);
	root_rot_err *= root_rot_err;

	double root_vel_err = (root_vel1 - root_vel0).squaredNorm();
	double root_ang_vel_err = (root_ang_vel1 - root_ang_vel0).squaredNorm();

	root_err = root_pos_err
		+ 0.1 * root_rot_err
		+ 0.01 * root_vel_err
		+ 0.001 * root_ang_vel_err;
	com_err = 0.1 * (com_vel1_world - com_vel0_world).squaredNorm();

	double pose_reward = exp(-err_scale * pose_scale * pose_err);
	double vel_reward = exp(-err_scale * vel_scale * vel_err);
	double end_eff_reward = exp(-err_scale * end_eff_scale * end_eff_err);
	double root_reward = exp(-err_scale * root_scale * root_err);
	double com_reward = exp(-err_scale * com_scale * com_err);

	reward = pose_w * pose_reward + vel_w * vel_reward + end_eff_w * end_eff_reward
		+ root_w * root_reward + com_w * com_reward;
	//printf("imitate reward %.3lf\n", reward);
	return reward;
}





void cRLSceneSimChar::ParseArgs(const std::shared_ptr<cArgParser>& parser)
{
	//cRLSceneSimChar::ParseArgs(parser);
	//cRLScene::ParseArgs(parser);
 
	cScene::ParseArgs(parser);

	bool succ = true;

	parser->ParseBool("enable_char_contact_fall", mEnableContactFall);
	parser->ParseBool("enable_rand_char_placement", mEnableRandCharPlacement);
	
	succ &= ParseCharParams(parser, mCharParams);
	succ &= ParseCharCtrlParams(parser, mCtrlParams);
	if (mCharParams.size() != mCtrlParams.size())
	{
		printf("Char and ctrl file mismatch, %zi vs %zi\n", mCharParams.size(), mCtrlParams.size());
		assert(false);
	}

	std::string sim_mode_str = "";
	parser->ParseInt("num_sim_substeps", mWorldParams.mNumSubsteps);
	parser->ParseDouble("world_scale", mWorldParams.mScale);
	parser->ParseVector("gravity", mWorldParams.mGravity);

	 

	parser->ParseInts("fall_contact_bodies", mFallContactBodies);

	ParseGroundParams(parser, mGroundParams);
//---------------------------------------------------------------------
	parser->ParseBool("enable_fall_end", mEnableFallEnd);
	parser->ParseInt("anneal_samples", mAnnealSamples);

	mTimerParamsEnd = mTimerParams;
	mArgParser->ParseDouble("time_end_lim_min", mTimerParamsEnd.mTimeMin);
	mArgParser->ParseDouble("time_end_lim_max", mTimerParamsEnd.mTimeMax);
	mArgParser->ParseDouble("time_end_lim_exp", mTimerParamsEnd.mTimeExp);
	//====================================================================
	parser->ParseString("motion_file", mMotionFile);
	parser->ParseBool("enable_rand_rot_reset", mEnableRandRotReset);
	parser->ParseBool("sync_char_root_pos", mSyncCharRootPos);
	parser->ParseBool("sync_char_root_rot", mSyncCharRootRot);
	parser->ParseBool("enable_root_rot_fail", mEnableRootRotFail);
	parser->ParseDouble("hold_end_frame", mHoldEndFrame);
}

void cRLSceneSimChar::Init()
{
	mKinChar.reset();
	BuildKinChar();
	//cScene::Init();
	//--------------------------------------
 
	cScene::Init();
	BuildWorld();
	BuildGround();
	BuildCharacters();
	InitCharacterPos();
	ResolveCharGroundIntersect();
    //--------------------------------------
	mAgentReg.Clear();
	RegisterAgents();
	mAgentReg.PrintInfo();
	SetupTimerAnnealer(mTimerAnnealer);
	//============================================
	InitJointWeights();
}

double cRLSceneSimChar::CalcReward(int agent_id) const
{
	const cSimCharacter* sim_char = GetAgentChar(agent_id);
	bool fallen = HasFallen(*sim_char);

	double r = 0;
	int max_id = 0;
	if (!fallen)
	{
		r = CalcRewardImitate(*sim_char, *mKinChar);
	}
	return r;
}

const std::shared_ptr<cKinCharacter>& cRLSceneSimChar::GetKinChar() const
{
	return mKinChar;
}

void cRLSceneSimChar::EnableRandRotReset(bool enable)
{
	mEnableRandRotReset = enable;
}

bool cRLSceneSimChar::EnabledRandRotReset() const
{
	bool enable = mEnableRandRotReset;
	return enable;
}

cRLSceneSimChar::eTerminate cRLSceneSimChar::CheckTerminate(int agent_id) const
{
	//eTerminate terminated = cRLSceneSimChar::CheckTerminate(agent_id);
	bool fail = false;
	if (EnableFallEnd())
	{
		const auto& character = GetAgentChar(agent_id);
		fail |= HasFallen(*character);
	}
	eTerminate terminated = (fail) ? eTerminateFail : eTerminateNull;
	if (terminated == eTerminateNull)
	{
		bool end_motion = false;
		const auto& kin_char = GetKinChar();
		const cMotion& motion = kin_char->GetMotion();

		if (motion.GetLoop() == cMotion::eLoopNone)
		{
			double dur = motion.GetDuration();
			double kin_time = kin_char->GetTime();
			end_motion = kin_time > dur + mHoldEndFrame;
		}
		else
		{
			end_motion = kin_char->IsMotionOver();
		}

		terminated = (end_motion) ? eTerminateFail : terminated;
	}
	return terminated;
}

std::string cRLSceneSimChar::GetName() const
{
	return "Imitate";
}



void cRLSceneSimChar::CalcJointWeights(const std::shared_ptr<cSimCharacter>& character, Eigen::VectorXd& out_weights) const
{
	int num_joints = character->GetNumJoints();
	out_weights = Eigen::VectorXd::Ones(num_joints);
	for (int j = 0; j < num_joints; ++j)
	{
		double curr_w = character->GetJointDiffWeight(j);
		out_weights[j] = curr_w;
	}

	double sum = out_weights.lpNorm<1>();
	out_weights /= sum;
}

bool cRLSceneSimChar::BuildController(const  tCtrlParams& ctrl_params, 
std::shared_ptr<cDeepMimicCharController>& out_ctrl)
{
	 
	bool succ = true;
	std::shared_ptr<cDeepMimicCharController> ctrl = std::shared_ptr<cDeepMimicCharController>(new cDeepMimicCharController());
	ctrl->SetGravity(ctrl_params.mGravity);
	ctrl->Init(ctrl_params.mChar.get(), ctrl_params.mCtrlFile);
	out_ctrl = ctrl;
	
	if (succ)
	{
		auto ct_ctrl = dynamic_cast<cDeepMimicCharController*>(out_ctrl.get());
		if (ct_ctrl != nullptr)
		{
			const auto& kin_char = GetKinChar();
			double cycle_dur = kin_char->GetMotionDuration();
			ct_ctrl->SetCyclePeriod(cycle_dur);
		}
	}
	return succ;
}

void cRLSceneSimChar::BuildKinChar()
{
	bool succ = BuildKinCharacter(0, mKinChar);
	if (!succ)
	{
		printf("Failed to build kin character\n");
		assert(false);
	}
}

bool cRLSceneSimChar::BuildKinCharacter(int id, std::shared_ptr<cKinCharacter>& out_char) const
{
	auto kin_char = std::shared_ptr<cKinCharacter>(new cKinCharacter());
	const cSimCharacter::tParams& sim_char_params = mCharParams[0];
	cKinCharacter::tParams kin_char_params;

	kin_char_params.mID = id;
	kin_char_params.mCharFile = sim_char_params.mCharFile;
	kin_char_params.mOrigin = sim_char_params.mInitPos;
	kin_char_params.mLoadDrawShapes = false;
	kin_char_params.mMotionFile = mMotionFile;

	bool succ = kin_char->Init(kin_char_params);
	if (succ)
	{
		out_char = kin_char;
	}
	return succ;
}

void cRLSceneSimChar::UpdateCharacters(double time_step)
{
	UpdateKinChar(time_step);
	 
	int num_chars = GetNumChars();
	for (int i = 0; i < num_chars; ++i)
	{const auto& curr_char = GetCharacter(i);
     curr_char->Update(time_step);
	}
}

void cRLSceneSimChar::UpdateKinChar(double timestep)
{
	const auto& kin_char = GetKinChar();
	double prev_phase = kin_char->GetPhase();
	kin_char->Update(timestep);
	double curr_phase = kin_char->GetPhase();

	if (curr_phase < prev_phase)
	{
		const auto& sim_char = GetCharacter();
		SyncKinCharNewCycle(*sim_char, *kin_char);
	}
}

void cRLSceneSimChar::ResetCharacters()
{
 
	int num_chars = GetNumChars();
	for (int i = 0; i < num_chars; ++i)
	{
		const auto& curr_char = GetCharacter(i);
		curr_char->Reset();
	}
 


	ResetKinChar();
	if (EnableSyncChar())
	{
		SyncCharacters();
	}
}

void cRLSceneSimChar::ResetKinChar()
{
	double rand_time = CalcRandKinResetTime();

	const cSimCharacter::tParams& char_params = mCharParams[0];
	const auto& kin_char = GetKinChar();

	kin_char->Reset();
	kin_char->SetOriginRot(tQuaternion::Identity());
	kin_char->SetOriginPos(char_params.mInitPos); // reset origin
	kin_char->SetTime(rand_time);
	kin_char->Pose(rand_time);

	if (EnabledRandRotReset())
	{
		double rand_theta = mRand.RandDouble(-M_PI, M_PI);
		kin_char->RotateOrigin(cMathUtil::EulerToQuaternion(tVector(0, rand_theta, 0, 0)));
	}
}

void cRLSceneSimChar::SyncCharacters()
{
	const auto& kin_char = GetKinChar();
	const Eigen::VectorXd& pose = kin_char->GetPose();
	const Eigen::VectorXd& vel = kin_char->GetVel();

	const auto& sim_char = GetCharacter();
	sim_char->SetPose(pose);
	sim_char->SetVel(vel);

	const auto& ctrl = sim_char->GetController();
	auto ct_ctrl = dynamic_cast<cDeepMimicCharController*>(ctrl.get());
	if (ct_ctrl != nullptr)
	{
		double kin_time = GetKinTime();
		ct_ctrl->SetInitTime(kin_time);
	}
}

bool cRLSceneSimChar::EnableSyncChar() const
{
	const auto& kin_char = GetKinChar();
	return kin_char->HasMotion();
}


void cRLSceneSimChar::InitJointWeights()
{
	CalcJointWeights(GetCharacter(), mJointWeights);
}

void cRLSceneSimChar::ResolveCharGroundIntersect()
{
 

	int num_chars = GetNumChars();
	for (int i = 0; i < num_chars; ++i)
	{
		ResolveCharGroundIntersect(mChars[i]);
	}

	if (EnableSyncChar())
	{
		SyncKinCharRoot();
	}
}

void cRLSceneSimChar::ResolveCharGroundIntersect(const std::shared_ptr<cSimCharacter>& out_char) const
{
	const double pad = 0.001;

	int num_parts = out_char->GetNumBodyParts();
	double min_violation = 0;
	for (int b = 0; b < num_parts; ++b)
	{
		if (out_char->IsValidBodyPart(b))
		{
			tVector aabb_min;
			tVector aabb_max;
			const auto& part = out_char->GetBodyPart(b);
			part->CalcAABB(aabb_min, aabb_max);

			tVector mid = 0.5 * (aabb_min + aabb_max);
			tVector sw = tVector(aabb_min[0], 0, aabb_min[2], 0);
			tVector nw = tVector(aabb_min[0], 0, aabb_max[2], 0);
			tVector ne = tVector(aabb_max[0], 0, aabb_max[2], 0);
			tVector se = tVector(aabb_max[0], 0, aabb_min[2], 0);

			double max_ground_height = 0;
			max_ground_height = mGround->SampleHeight(aabb_min);
			max_ground_height = std::max(max_ground_height, mGround->SampleHeight(mid));
			max_ground_height = std::max(max_ground_height, mGround->SampleHeight(sw));
			max_ground_height = std::max(max_ground_height, mGround->SampleHeight(nw));
			max_ground_height = std::max(max_ground_height, mGround->SampleHeight(ne));
			max_ground_height = std::max(max_ground_height, mGround->SampleHeight(se));
			max_ground_height += pad;

			double min_height = aabb_min[1];
			min_violation = std::min(min_violation, min_height - max_ground_height);
		}
	}

	if (min_violation < 0)
	{
		tVector root_pos = out_char->GetRootPos();
		root_pos[1] += -min_violation;
		out_char->SetRootPos(root_pos);
	}
}

void cRLSceneSimChar::SyncKinCharRoot()
{
	const auto& sim_char = GetCharacter();
	tVector sim_root_pos = sim_char->GetRootPos();
	double sim_heading = sim_char->CalcHeading();

	const auto& kin_char = GetKinChar();
	double kin_heading = kin_char->CalcHeading();

	tQuaternion drot = tQuaternion::Identity();
	if (mSyncCharRootRot)
	{
		drot = cMathUtil::AxisAngleToQuaternion(tVector(0, 1, 0, 0), sim_heading - kin_heading);
	}

	kin_char->RotateRoot(drot);
	kin_char->SetRootPos(sim_root_pos);
}

void cRLSceneSimChar::SyncKinCharNewCycle(const cSimCharacter& sim_char, cKinCharacter& out_kin_char) const
{
	if (mSyncCharRootRot)
	{
		double sim_heading = sim_char.CalcHeading();
		double kin_heading = out_kin_char.CalcHeading();
		tQuaternion drot = cMathUtil::AxisAngleToQuaternion(tVector(0, 1, 0, 0), sim_heading - kin_heading);
		out_kin_char.RotateRoot(drot);
	}

	if (mSyncCharRootPos)
	{
		tVector sim_root_pos = sim_char.GetRootPos();
		tVector kin_root_pos = out_kin_char.GetRootPos();
		kin_root_pos[0] = sim_root_pos[0];
		kin_root_pos[2] = sim_root_pos[2];

		tVector origin = out_kin_char.GetOriginPos();
		double dh = kin_root_pos[1] - origin[1];
		double ground_h = mGround->SampleHeight(kin_root_pos);
		kin_root_pos[1] = ground_h + dh;

		out_kin_char.SetRootPos(kin_root_pos);
	}
}

double cRLSceneSimChar::GetKinTime() const
{
	const auto& kin_char = GetKinChar();
	return kin_char->GetTime();
}

bool cRLSceneSimChar::CheckKinNewCycle(double timestep) const
{
	bool new_cycle = false;
	const auto& kin_char = GetKinChar();
	if (kin_char->GetMotion().EnableLoop())
	{
		double cycle_dur = kin_char->GetMotionDuration();
		double time = GetKinTime();
		new_cycle = cMathUtil::CheckNextInterval(timestep, time, cycle_dur);
	}
	return new_cycle;
}




bool cRLSceneSimChar::CheckRootRotFail(const cSimCharacter& sim_char) const
{
	const auto& kin_char = GetKinChar();
	bool fail = CheckRootRotFail(sim_char, *kin_char);
	return fail;
}

bool cRLSceneSimChar::CheckRootRotFail(const cSimCharacter& sim_char, const cKinCharacter& kin_char) const
{
	const double threshold = 0.5 * M_PI;

	tQuaternion sim_rot = sim_char.GetRootRotation();
	tQuaternion kin_rot = kin_char.GetRootRotation();
	double rot_diff = cMathUtil::QuatDiffTheta(sim_rot, kin_rot);
	return rot_diff > threshold;
}

double cRLSceneSimChar::CalcRandKinResetTime()
{
	const auto& kin_char = GetKinChar();
	double dur = kin_char->GetMotionDuration();
	double rand_time = cMathUtil::RandDouble(0, dur);
	return rand_time;
}
//==========================================================================
//==========================================================================
cRLSceneSimChar::~cRLSceneSimChar()
{
	Clear();
}

 



int cRLSceneSimChar::GetNumAgents() const
{
	return mAgentReg.GetNumAgents();
}

bool cRLSceneSimChar::NeedNewAction(int agent_id) const
{
	const auto& ctrl = GetController(agent_id);
	return ctrl->NeedNewAction();
}

void cRLSceneSimChar::RecordState(int agent_id, Eigen::VectorXd& out_state) const
{
	const auto& ctrl = GetController(agent_id);
	ctrl->RecordState(out_state);
}

void cRLSceneSimChar::RecordGoal(int agent_id, Eigen::VectorXd& out_goal) const
{
	const auto& ctrl = GetController(agent_id);
	ctrl->RecordGoal(out_goal);
}

void cRLSceneSimChar::SetAction(int agent_id, const Eigen::VectorXd& action)
{
	const auto& ctrl = GetController(agent_id);
	ctrl->ApplyAction(action);
}

eActionSpace cRLSceneSimChar::GetActionSpace(int agent_id) const
{
	const auto& ctrl = GetController(agent_id);
	return ctrl->GetActionSpace();
}

int cRLSceneSimChar::GetStateSize(int agent_id) const
{
	const auto& ctrl = GetController(agent_id);
	return ctrl->GetStateSize();
}

int cRLSceneSimChar::GetGoalSize(int agent_id) const
{
	const auto& ctrl = GetController(agent_id);
	return ctrl->GetGoalSize();
}

int cRLSceneSimChar::GetActionSize(int agent_id) const
{
	const auto& ctrl = GetController(agent_id);
	return ctrl->GetActionSize();
}

int cRLSceneSimChar::GetNumActions(int agent_id) const
{
	const auto& ctrl = GetController(agent_id);
	return ctrl->GetNumActions();
}

void cRLSceneSimChar::BuildStateOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const
{
	const auto& ctrl = GetController(agent_id);
	ctrl->BuildStateOffsetScale(out_offset, out_scale);
}

void cRLSceneSimChar::BuildGoalOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const
{
	const auto& ctrl = GetController(agent_id);
	ctrl->BuildGoalOffsetScale(out_offset, out_scale);
}

void cRLSceneSimChar::BuildActionOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const
{
	const auto& ctrl = GetController(agent_id);
	ctrl->BuildActionOffsetScale(out_offset, out_scale);
}

void cRLSceneSimChar::BuildActionBounds(int agent_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max) const
{
	const auto& ctrl = GetController(agent_id);
	ctrl->BuildActionBounds(out_min, out_max);
}

void cRLSceneSimChar::BuildStateNormGroups(int agent_id, Eigen::VectorXi& out_groups) const
{
	const auto& ctrl = GetController(agent_id);
	ctrl->BuildStateNormGroups(out_groups);
}

void cRLSceneSimChar::BuildGoalNormGroups(int agent_id, Eigen::VectorXi& out_groups) const
{
	const auto& ctrl = GetController(agent_id);
	ctrl->BuildGoalNormGroups(out_groups);
}

double cRLSceneSimChar::GetRewardMin(int agent_id) const
{
	return mAgentReg.GetAgent(agent_id)->GetRewardMin();
}

double cRLSceneSimChar::GetRewardMax(int agent_id) const
{
	return mAgentReg.GetAgent(agent_id)->GetRewardMax();
}

 

bool cRLSceneSimChar::CheckValidEpisode() const
{
	for (int i = 0; i < GetNumChars(); ++i)
	{
		const auto& sim_char = GetCharacter(i);
		bool exp = sim_char->HasVelExploded();
		if (exp)
		{
			return false;
		}
	}
	return true;
}

void cRLSceneSimChar::LogVal(int agent_id, double val)
{
	const auto& ctrl = GetController(agent_id);
	cDeepMimicCharController* trl_ctrl = dynamic_cast<cDeepMimicCharController*>(ctrl.get());
	if (trl_ctrl != nullptr)
	{
		trl_ctrl->LogVal(val);
	}
}

void cRLSceneSimChar::SetSampleCount(int count)
{
	cScene::SetSampleCount(count);
	UpdateTimerParams();
}

 
void cRLSceneSimChar::ResetParams()
{
	//cScene::ResetParams();
	//cSceneSimChar::ResetParams();
	cScene::ResetParams();
}

void cRLSceneSimChar::ResetScene()
{
	//cScene::ResetScene();
	cScene::ResetScene();
	 
	ResetWorld();
	ResetCharacters();
	ResetGround();
	 
	InitCharacterPos();
	ResolveCharGroundIntersect();
}

const std::shared_ptr<cDeepMimicCharController>& cRLSceneSimChar::GetController() const
{
	const auto& character = GetCharacter();
	return character->GetController();
}

const std::shared_ptr<cDeepMimicCharController>& cRLSceneSimChar::GetController(int agent_id) const
{
	return mAgentReg.GetAgent(agent_id);
}

const cSimCharacter* cRLSceneSimChar::GetAgentChar(int agent_id) const
{
	return mAgentReg.GetChar(agent_id);
}

void cRLSceneSimChar::PreUpdate(double timestep)
{
	//cSceneSimChar::PreUpdate(timestep);
	for (int a = 0; a < GetNumAgents(); ++a)
	{
		const auto& ctrl = mAgentReg.GetAgent(a);
		bool new_action = ctrl->NeedNewAction();
		if (new_action)
		{
			NewActionUpdate(a);
		}
	}
}

void cRLSceneSimChar::ResetTimers()
{
	//cSceneSimChar::ResetTimers();
	cScene::ResetTimers();
	if (mMode == eModeTest)
	{
		mTimer.SetMaxTime(mTimerParamsEnd.mTimeMax);
	}
}

void cRLSceneSimChar::NewActionUpdate(int agent_id)
{
}

bool cRLSceneSimChar::EnableFallEnd() const
{
	return mEnableFallEnd;
}

void cRLSceneSimChar::RegisterAgents()
{
	int num_chars = GetNumChars();
	for (int i = 0; i < num_chars; ++i)
	{
		const auto& character = GetCharacter(i);
		const auto& ctrl = character->GetController();
		RegisterAgent(ctrl, character);
	}
}

void cRLSceneSimChar::RegisterAgent(const std::shared_ptr<cDeepMimicCharController>& ctrl, const std::shared_ptr<cSimCharacter>& character)
{
	std::vector<int> ids;
	RegisterAgent(ctrl, character, ids);
}

void cRLSceneSimChar::RegisterAgent(const std::shared_ptr<cDeepMimicCharController>& ctrl,
									const std::shared_ptr<cSimCharacter>& character,
									std::vector<int>& out_ids)
{
	if (ctrl != nullptr)
	{
		int id = mAgentReg.AddAgent(ctrl, character.get());
		out_ids.push_back(id);

		int num_ctrls = ctrl->NumChildren();
		for (int i = 0; i < num_ctrls; ++i)
		{
			const auto& child_ctrl = ctrl->GetChild(i);
			RegisterAgent(child_ctrl, character, out_ids);
		}
	}
}

void cRLSceneSimChar::SetupTimerAnnealer(cAnnealer& out_annealer) const
{
	cAnnealer::tParams params;
	params.mType = cAnnealer::eTypePow;
	params.mPow = 4.0;
	out_annealer.Init(params);
}

void cRLSceneSimChar::UpdateTimerParams()
{
	if (mAnnealSamples > 0)
	{
		double t = static_cast<double>(mSampleCount) / mAnnealSamples;
		double lerp = mTimerAnnealer.Eval(t);
		cTimer::tParams blend_params = mTimerParams.Blend(mTimerParamsEnd, lerp);
		mTimer.SetParams(blend_params);
	}
}