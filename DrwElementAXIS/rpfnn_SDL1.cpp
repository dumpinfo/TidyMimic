//#include <GL/glew.h>

#include <GL/glut.h>
#include <GL/freeglut.h>
#include <SDL/SDL.h>

#include <eigen3/Eigen/Dense>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>

#include <sstream>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <stdarg.h>
#include <time.h>
#include "rpfnn.h"
#include "graphdep.h"
//#include "pfshader.h"
#include "worlder.h"

using namespace std;
static GLboolean should_rotate = GL_TRUE;
using namespace Eigen;


static Options* options = NULL;
/* Phase-Functioned Neural Network */
static PFNN* pfnn = NULL;
/* Joystick */
static SDL_Joystick* stick = NULL;
/* Camera */
static CameraOrbit* camera = NULL;
static Character* character = NULL;

/* Trajectory */
static Trajectory* trajectory = NULL;
/* IK */
static IK* ik = NULL;
/* Areas */
/* Rendering */
static LightDirectional* light = NULL;
/* Heightmap */
/* Shader */

//_ static Shader* shader_terrain = NULL;
//_ static Shader* shader_terrain_shadow = NULL;
//_ static Shader* shader_character = NULL;
//_ static Shader* shader_character_shadow = NULL;
/* Character */

#include "procaction.h"
#define HeightIdx(X, Y, MAP_SIZE_X)  (X + (Y * MAP_SIZE_X))

void SurfSetColor(float rr,float gg, float bb)
{
    /*RobotMat->SetValue(DIFFUSE, sat_r, sat_g, sat_b, 1.0);
    RobotMat->SetValue(AMBIENT, sat_r, sat_g, sat_b, 1.0);
    RobotMat->SetValue(SPECULAR, 1.0, 1.0, 1.0, 1.0);
    RobotMat->SetValue(SHININESS, 100.0);*/
   
    float  Ambient[4];
	float  Diffuse[4];
    float Specular[4] ={0, 0, 0, 0.0};
    float Emission[4];
    float Shininess =70;
    rr *=1.3;
	gg *=1.3;
	bb *=1.3;
    Ambient[0] = rr; Ambient[1] = gg; Ambient[2] = bb; Ambient[3] = 1;  
	Diffuse[0] = rr; Diffuse[1] = gg; Diffuse[2] = bb; Diffuse[3] = 1;  
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, Ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, Diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, Specular);
    //glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, Emission);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, Shininess);

}

void SurfSetColorRGB(float rr,float gg, float bb)
{
    rr /=255.0;
	gg /=255.0;
	bb /=255.0;
   
    float  Ambient[4];
	float  Diffuse[4];
    float Specular[4] ={0.05, 0.05, 0.05, 1.0};
    float Emission[4];
    float Shininess =70;

    Ambient[0] = rr; Ambient[1] = gg; Ambient[2] = bb; Ambient[3] = 1;  
	Diffuse[0] = rr; Diffuse[1] = gg; Diffuse[2] = bb; Diffuse[3] = 1;  
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, Ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, Diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, Specular);
    //glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, Emission);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, Shininess);

}

void AddLight1(glm::vec3 lightPos) 
{
   GLfloat mat_specular[] = { 1.0, 1000.0, 1.0, 1.0 };
   GLfloat mat_shininess[] = { 00.0 };
   //GLfloat light_position[] = { lightPos[0], lightPos[1]*40, lightPos[2], 0.0 };
   GLfloat light_position[] = { lightPos[0], lightPos[1], lightPos[2], 0.0 };
   //glClearColor (0.0, 0.0, 0.0, 0.0);
   glShadeModel (GL_SMOOTH);

   glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
   glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
   glLightfv(GL_LIGHT1, GL_POSITION, light_position);

   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT1);
   glEnable(GL_DEPTH_TEST);
   
   glMatrixMode(GL_MODELVIEW);
   glPushMatrix();
   glTranslatef(lightPos[0], lightPos[1], lightPos[2]);
   glutWireSphere(50,8,8);
   glPopMatrix();
   
   float LPos[4];
   glGetLightfv(GL_LIGHT0, GL_POSITION, LPos);
   
   glMatrixMode(GL_MODELVIEW);
   glPushMatrix();
   glTranslatef(LPos[0], LPos[1], LPos[2]);
   glutWireSphere(50,8,8);
   glPopMatrix();
}/**/

glm::vec3 normalize(const glm::vec3 &v) {
  float length_of_v = sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
  return glm::vec3(v.x / length_of_v, v.y / length_of_v, v.z / length_of_v);
}

float angleBetween(
 glm::vec3 a,
 glm::vec3 b//,glm::vec3 origin 
){
 glm::vec3 da=glm::normalize(a);//glm::vec3 da=glm::normalize(a-origin);
 glm::vec3 db=glm::normalize(b);//glm::vec3 db=glm::normalize(b-origin);
 return  acos(glm::dot(da, db));
}

void DrawPointLink(glm::vec3 vec1, glm::vec3 vec2)
{
	glm::vec3 v = vec1 - vec2;
	float length_of_v = sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
	
	glm::vec3 yaxis(0, 0,1);
	
    glm::vec3 res = cross(v, yaxis);
	float angle = angleBetween( v, yaxis);
	
	angle = 180 - angle*180.0/3.1415;
	
	glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
	glTranslatef(vec1[0], vec1[1], vec1[2]);
	glRotatef(angle, res[0],res[1],res[2]);
	
	SurfSetColorRGB(153,160, 173);
	glutSolidCylinder(5.5,length_of_v,8,8); 
	
	SurfSetColor(0.0,0, 0);
	glutWireCylinder(5.6,length_of_v,8,2); 
    //glTranslatef(LPos[0], LPos[1], LPos[2]);
    //glutSolidSphere(radius,10,10);
    glPopMatrix();
}

void DrawSphere(glm::vec3 LPos, double radius=6)
{
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(LPos[0], LPos[1], LPos[2]);
    glutSolidSphere(radius,10,10);
    glPopMatrix();
}

void DrawSphereWire(glm::vec3 LPos, double radius=6)
{
	SurfSetColor(0.0,0.0, 0.0);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(LPos[0], LPos[1], LPos[2]);
    glutWireSphere(radius+0.4, 8, 6);
    glPopMatrix();
}


void DrawSphereWireT(glm::vec3 LPos, double radius=6)
{
	SurfSetColor(0.0,0.0, 0.0);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(LPos[0], LPos[1], LPos[2]);
	glDisable(GL_DEPTH_TEST);
    glutWireSphere(radius, 8, 8);
	glEnable(GL_DEPTH_TEST);
    glPopMatrix();
}

void myinit();
void render() {
  
  /* Render Shadows */
  glm::mat4 light_view = glm::lookAt(camera->target + light->position, camera->target, glm::vec3(0,1,0)); 
  glm::mat4 light_proj = glm::ortho(-500.0f, 500.0f, -500.0f, 500.0f, 10.0f, 10000.0f);
  light_view[1]+=2000;

  //glBindFramebuffer(GL_FRAMEBUFFER, light->fbo);
 
  glViewport(0, 0, 1024, 1024);
 // glClearDepth(1.0f);  
  glClear(GL_DEPTH_BUFFER_BIT);
  
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glCullFace(GL_FRONT);
  

  glCullFace(GL_BACK);
   
  
  glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
  //_ glBindFramebuffer(GL_FRAMEBUFFER, 0);
  
  /* Render Terrain */
 
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  
  glClearDepth(1.0);
  glClearColor(0.67, 0.85, 0.90, 1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  glm::vec3 light_direction = glm::normalize(light->target - light->position);
  
  
  
 
  //==================================================================
  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixf(glm::value_ptr(camera->view_matrix()));
  
  glMatrixMode(GL_PROJECTION);
  glLoadMatrixf(glm::value_ptr(camera->proj_matrix()));
  bool bSwitchSides = false;
  
  int STEP_SIZE  = 2;
  int MAP_SIZE_X = heightmap->w -1;
  int MAP_SIZE_Y = heightmap->h -1;
  int X,Y;
  int round_f = MAP_SIZE_Y%STEP_SIZE;
  
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  //std::cout<< light->position[0] <<"," << light->position[1] <<"," << light->position[2] <<"," <<"\n";
  //light->position[1]+=2000;
  AddLight1(light->position) ;
  SurfSetColorRGB(197, 196, 182);
  
  
  //glutWireSphere(200,20,20);
  glBegin( GL_TRIANGLE_STRIP );
 
   for ( X = 0; X <= (MAP_SIZE_X-1); X += STEP_SIZE )
	{ float x,y,z,Idx;
		// 判断渲染的面
		if(bSwitchSides)
		{	
			//  遍历所有列
			for ( Y = (MAP_SIZE_Y - round_f); Y >= 0; Y -= STEP_SIZE )
			{
				// 获得高程值		
				Idx = HeightIdx(X, Y , heightmap->w); 
				x = heightmap->posns[Idx][0];							
				y = heightmap->posns[Idx][1];	
				z = heightmap->posns[Idx][2];	
                glNormal3f(heightmap->norms[Idx][0],heightmap->norms[Idx][1],heightmap->norms[Idx][2]);				
				glVertex3f(x, y, z);		

				Idx = HeightIdx( (X + STEP_SIZE), Y , heightmap->w); 
				// 获得高程值		
				x = heightmap->posns[Idx][0]; 
				y = heightmap->posns[Idx][1]; 
				z = heightmap->posns[Idx][2];
				glNormal3f(heightmap->norms[Idx][0],heightmap->norms[Idx][1],heightmap->norms[Idx][2]);
				glVertex3f(x, y, z);			
			}
		}
		else
		{	
			//  遍历所有的行
			for ( Y = 0; Y <= (MAP_SIZE_Y); Y += STEP_SIZE )
			{
				// 获得高程值		
				Idx = HeightIdx( (X + STEP_SIZE), Y , heightmap->w); 
				x = heightmap->posns[Idx][0]; 
				y = heightmap->posns[Idx][1]; 
				z = heightmap->posns[Idx][2];
				glNormal3f(heightmap->norms[Idx][0],heightmap->norms[Idx][1],heightmap->norms[Idx][2]);
				glVertex3f(x, y, z); 

				// 获得高程值
				Idx = HeightIdx(X, Y , heightmap->w); 
				x = heightmap->posns[Idx][0]; 
				y = heightmap->posns[Idx][1]; 
				z = heightmap->posns[Idx][2];
				glNormal3f(heightmap->norms[Idx][0],heightmap->norms[Idx][1],heightmap->norms[Idx][2]);
				glVertex3f(x, y, z);		
			}
		}

		bSwitchSides = !bSwitchSides;
	}
  glEnd();/**/
  
    //------------AXIS-------------------------
	glEnable(GL_COLOR_MATERIAL); 
    glDisable(GL_DEPTH_TEST);
    glLineWidth(4);
	
    glBegin(GL_LINES);
	
	glColor3f(1, 0.0, 0);
    glVertex3f(0,  0, 0);
    glVertex3f(500, 0, 0);
	
	glColor3f(0, 0.0, 1);
    glVertex3f(0,  0, 0);
    glVertex3f(0,  0, 500);
    
    glColor3f(0, 1.0, 0);
    glVertex3f(0,  0, 0);
    glVertex3f(0,  500, 0);
	
    glEnd();
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_COLOR_MATERIAL); 
    /*
	*/
    
	//glEnable(GL_DEPTH_TEST);
  //========================================================================
  /*STEP_SIZE = 12;
  //glPolygonMode(GL_FRONT, GL_LINE);
  round_f = MAP_SIZE_Y%STEP_SIZE;
  SurfSetColor(0.0, 0.0, 0.0);
   
  
  //glutWireSphere(200,20,20);
  //glBegin( GL_LINE_STRIP );
  // glLineWidth(4.0);
   glDisable(GL_DEPTH_TEST);
   glBegin( GL_LINES  );
   
   for ( X = 0; X < (MAP_SIZE_X - STEP_SIZE*2); X += STEP_SIZE )
  //for ( X = 0; X <   STEP_SIZE*5; X += STEP_SIZE )
	{ float x,y,z,Idx;
     //  遍历所有的行
			for ( Y = 0; Y < (MAP_SIZE_Y- STEP_SIZE*2); Y += STEP_SIZE )
			//for ( Y = 0; Y < STEP_SIZE*5; Y += STEP_SIZE )	
			{
				// 获得高程值		
				Idx = HeightIdx( (X + STEP_SIZE), Y , heightmap->w); 
				x = heightmap->posns[Idx][0]; 
				y = heightmap->posns[Idx][1] ; 
				z = heightmap->posns[Idx][2];
				glVertex3f(x, y, z); 

				// 获得高程值
				Idx = HeightIdx(X, Y , heightmap->w); 
				x = heightmap->posns[Idx][0]; 
				y = heightmap->posns[Idx][1] ; 
				z = heightmap->posns[Idx][2];
				glVertex3f(x, y, z); 		
				
				Idx = HeightIdx(X , (Y) , heightmap->w); 
				x = heightmap->posns[Idx][0]; 
				y = heightmap->posns[Idx][1] ; 
				z = heightmap->posns[Idx][2];
				glVertex3f(x, y, z);
				
				Idx = HeightIdx(X, (Y  + STEP_SIZE), heightmap->w); 
				x = heightmap->posns[Idx][0]; 
				y = heightmap->posns[Idx][1] ; 
				z = heightmap->posns[Idx][2];
				glVertex3f(x, y, z); 
				
				Idx = HeightIdx(X , Y , heightmap->w); 
				x = heightmap->posns[Idx][0]; 
				y = heightmap->posns[Idx][1] ; 
				z = heightmap->posns[Idx][2];
				glVertex3f(x, y, z);
				
				Idx = HeightIdx((X+STEP_SIZE), (Y  + STEP_SIZE), heightmap->w); 
				x = heightmap->posns[Idx][0]; 
				y = heightmap->posns[Idx][1] ; 
				z = heightmap->posns[Idx][2];
				glVertex3f(x, y, z);
				
			}
		 
	}
  glEnd();/**/
  glEnable(GL_DEPTH_TEST);
  //#######################################################################
  //#######################################################################
  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixf(glm::value_ptr(camera->view_matrix()));
  
  glMatrixMode(GL_PROJECTION);
  glLoadMatrixf(glm::value_ptr(camera->proj_matrix()));
  
  SurfSetColorRGB(38, 76, 35);
  /* Render Crouch Area */

  if (options->display_debug) {
    for (int i = 0; i < areas->num_crouches(); i++) {

      glm::vec3 c0 = areas->crouch_pos[i] + glm::vec3( areas->crouch_size[i].x/2, 0,  areas->crouch_size[i].y/2);
      glm::vec3 c1 = areas->crouch_pos[i] + glm::vec3(-areas->crouch_size[i].x/2, 0,  areas->crouch_size[i].y/2);
      glm::vec3 c2 = areas->crouch_pos[i] + glm::vec3( areas->crouch_size[i].x/2, 0, -areas->crouch_size[i].y/2);
      glm::vec3 c3 = areas->crouch_pos[i] + glm::vec3(-areas->crouch_size[i].x/2, 0, -areas->crouch_size[i].y/2);

      glColor3f(0.0, 0.0, 1.0);
      glLineWidth(options->display_scale * 2.0);
      glBegin(GL_LINES);
      glVertex3f(c0.x, c0.y, c0.z); glVertex3f(c1.x, c1.y, c1.z);
      glVertex3f(c0.x, c0.y, c0.z); glVertex3f(c2.x, c2.y, c2.z);
      glVertex3f(c3.x, c3.y, c3.z); glVertex3f(c1.x, c1.y, c1.z);
      glVertex3f(c3.x, c3.y, c3.z); glVertex3f(c2.x, c2.y, c2.z);
      
      for (float j = 0; j < 1.0; j+=0.05) {
        glm::vec3 cm_a = glm::mix(c0, c1, j     );
        glm::vec3 cm_b = glm::mix(c0, c1, j+0.05);
        glm::vec3 cm_c = glm::mix(c3, c2, j     );
        glm::vec3 cm_d = glm::mix(c3, c2, j+0.05);
        float cmh_a = ((sinf(cm_a.x/Areas::CROUCH_WAVE)+1.0)/2.0) * 50 + 130;        
        float cmh_b = ((sinf(cm_b.x/Areas::CROUCH_WAVE)+1.0)/2.0) * 50 + 130;
        float cmh_c = ((sinf(cm_c.x/Areas::CROUCH_WAVE)+1.0)/2.0) * 50 + 130;        
        float cmh_d = ((sinf(cm_d.x/Areas::CROUCH_WAVE)+1.0)/2.0) * 50 + 130;
        glVertex3f(cm_a.x, cmh_a, cm_a.z);
        glVertex3f(cm_b.x, cmh_b, cm_b.z);
        glVertex3f(cm_c.x, cmh_c, cm_c.z);
        glVertex3f(cm_d.x, cmh_d, cm_d.z);
        glVertex3f(cm_a.x, cmh_a, cm_a.z);
        glVertex3f(cm_a.x, cmh_a,   c2.z);
        if (j + 0.05 >= 1.0) {
          glVertex3f(cm_b.x, cmh_b, cm_b.z);
          glVertex3f(cm_b.x, cmh_b,   c2.z);
        }
      }
      
      float c0h = ((sinf(c0.x/Areas::CROUCH_WAVE)+1.0)/2.0) * 50 + 130;
      float c1h = ((sinf(c1.x/Areas::CROUCH_WAVE)+1.0)/2.0) * 50 + 130;
      float c2h = ((sinf(c2.x/Areas::CROUCH_WAVE)+1.0)/2.0) * 50 + 130;
      float c3h = ((sinf(c3.x/Areas::CROUCH_WAVE)+1.0)/2.0) * 50 + 130;
      
      glVertex3f(c0.x, c0.y + c0h, c0.z); glVertex3f(c0.x, c0.y, c0.z);
      glVertex3f(c1.x, c1.y + c1h, c1.z); glVertex3f(c1.x, c1.y, c1.z);
      glVertex3f(c2.x, c2.y + c2h, c2.z); glVertex3f(c2.x, c2.y, c2.z);
      glVertex3f(c3.x, c3.y + c3h, c3.z); glVertex3f(c3.x, c3.y, c3.z);
      
      glEnd();
      glLineWidth(1.0);
      glColor3f(1.0, 1.0, 1.0);
    }
  }

  /* Render Jump Areas */

  if (options->display_debug && options->display_areas_jump) {
    for (int i = 0; i < areas->num_jumps(); i++) {
      glColor3f(1.0, 0.0, 0.0);
      glLineWidth(options->display_scale * 2.0);
      glBegin(GL_LINES);
      for (float r = 0; r < 1.0; r+=0.01) {
        glVertex3f(areas->jump_pos[i].x + areas->jump_size[i] * sin((r+0.00)*2*M_PI), areas->jump_pos[i].y, areas->jump_pos[i].z + areas->jump_size[i] * cos((r+0.00)*2*M_PI));
        glVertex3f(areas->jump_pos[i].x + areas->jump_size[i] * sin((r+0.01)*2*M_PI), areas->jump_pos[i].y, areas->jump_pos[i].z + areas->jump_size[i] * cos((r+0.01)*2*M_PI));
      }
      glEnd();
      glLineWidth(1.0);
      glColor3f(1.0, 1.0, 1.0);
    }
  }

  /* Render Walls */
  
  if (options->display_debug && options->display_areas_walls) {
    for (int i = 0; i < areas->num_walls(); i++) {
      glColor3f(0.0, 1.0, 0.0);
      glLineWidth(options->display_scale * 2.0);
      glBegin(GL_LINES);
      for (float r = 0; r < 1.0; r+=0.1) {
        glm::vec2 p0 = glm::mix(areas->wall_start[i], areas->wall_stop[i], r    );
        glm::vec2 p1 = glm::mix(areas->wall_start[i], areas->wall_stop[i], r+0.1);
        glVertex3f(p0.x, heightmap->sample(p0) + 5, p0.y);
        glVertex3f(p1.x, heightmap->sample(p1) + 5, p1.y);
      }
      glEnd();
      glLineWidth(1.0);
      glColor3f(1.0, 1.0, 1.0);
    }

  }
  
  /* Render Trajectory */
  
  if (options->display_debug) {
    glPointSize(1.0 * options->display_scale);
    glBegin(GL_POINTS);
    for (int i = 0; i < Trajectory::LENGTH-10; i++) {
      glm::vec3 position_c = trajectory->positions[i];
      glColor3f(trajectory->gait_jump[i], trajectory->gait_bump[i], trajectory->gait_crouch[i]);
      glVertex3f(position_c.x, position_c.y + 2.0, position_c.z);
    }
    glEnd();
    glColor3f(1.0, 1.0, 1.0);
    glPointSize(1.0);

    
    glPointSize(4.0 * options->display_scale);
    glBegin(GL_POINTS);
    for (int i = 0; i < Trajectory::LENGTH; i+=10) {
      glm::vec3 position_c = trajectory->positions[i];
      glColor3f(trajectory->gait_jump[i], trajectory->gait_bump[i], trajectory->gait_crouch[i]);
      glVertex3f(position_c.x, position_c.y + 2.0, position_c.z);
    }
    glEnd();
    glColor3f(1.0, 1.0, 1.0);
    glPointSize(1.0);

    if (options->display_debug_heights) {
      glPointSize(2.0 * options->display_scale);
      glBegin(GL_POINTS);
      for (int i = 0; i < Trajectory::LENGTH; i+=10) {
        glm::vec3 position_r = trajectory->positions[i] + (trajectory->rotations[i] * glm::vec3( trajectory->width, 0, 0));
        glm::vec3 position_l = trajectory->positions[i] + (trajectory->rotations[i] * glm::vec3(-trajectory->width, 0, 0));
        glColor3f(trajectory->gait_jump[i], trajectory->gait_bump[i], trajectory->gait_crouch[i]);
        glVertex3f(position_r.x, heightmap->sample(glm::vec2(position_r.x, position_r.z)) + 2.0, position_r.z);
        glVertex3f(position_l.x, heightmap->sample(glm::vec2(position_l.x, position_l.z)) + 2.0, position_l.z);
      }
      glEnd();
      glColor3f(1.0, 1.0, 1.0);
      glPointSize(1.0);
    }
    
    glLineWidth(1.0 * options->display_scale);
    glBegin(GL_LINES);
    for (int i = 0; i < Trajectory::LENGTH; i+=10) {
      glm::vec3 base = trajectory->positions[i] + glm::vec3(0.0, 2.0, 0.0);
      glm::vec3 side = glm::normalize(glm::cross(trajectory->directions[i], glm::vec3(0.0, 1.0, 0.0)));
      glm::vec3 fwrd = base + 15.0f * trajectory->directions[i];
      fwrd.y = heightmap->sample(glm::vec2(fwrd.x, fwrd.z)) + 2.0;
      glm::vec3 arw0 = fwrd +  4.0f * side + 4.0f * -trajectory->directions[i];
      glm::vec3 arw1 = fwrd -  4.0f * side + 4.0f * -trajectory->directions[i];
      glColor3f(trajectory->gait_jump[i], trajectory->gait_bump[i], trajectory->gait_crouch[i]);
      glVertex3f(base.x, base.y, base.z);
      glVertex3f(fwrd.x, fwrd.y, fwrd.z);
      glVertex3f(fwrd.x, fwrd.y, fwrd.z);
      glVertex3f(arw0.x, fwrd.y, arw0.z);
      glVertex3f(fwrd.x, fwrd.y, fwrd.z);
      glVertex3f(arw1.x, fwrd.y, arw1.z);
    }
    glEnd();
    glLineWidth(1.0);
    glColor3f(1.0, 1.0, 1.0);
  }
  
  /* Render Joints */
   if (1)
 // if (options->display_debug && options->display_debug_joints)
	  {
    glDisable(GL_DEPTH_TEST);
    glPointSize(3.0 * options->display_scale);
    glColor3f(0.6, 0.3, 0.4);      
    glBegin(GL_POINTS);
    for (int i = 0; i < Character::JOINT_NUM; i++) {
      glm::vec3 pos = character->joint_positions[i];
      glVertex3f(pos.x, pos.y, pos.z);
    }
    glEnd();
    glPointSize(1.0);

    /*glLineWidth(1.0 * options->display_scale);
    glColor3f(0.6, 0.3, 0.4);      
    glBegin(GL_LINES);
    for (int i = 0; i < Character::JOINT_NUM; i++) {
      glm::vec3 pos = character->joint_positions[i];
      glm::vec3 vel = pos - 5.0f * character->joint_velocities[i];
      glVertex3f(pos.x, pos.y, pos.z);
      glVertex3f(vel.x, vel.y, vel.z);
    }
    glEnd();
    glLineWidth(1.0);
    */
	
	//####################################################
	//####################################################
	
	glLineWidth(2.0);
	glBegin(GL_LINES);
    for (int i = 0; i < Character::JOINT_NUM; i++) {
	if(character->joint_parents[i]==-1) continue;
	
      glm::vec3 pos1 = character->joint_positions[i];
      glm::vec3 pos2 = character->joint_positions[character->joint_parents[i]];
	  
      glVertex3f(pos1.x, pos1.y, pos1.z);
      glVertex3f(pos2.x, pos2.y, pos2.z);
    }	  
	glEnd();
	glLineWidth(1.0);
	glEnable(GL_DEPTH_TEST);
	
	//=====================================================
	for (int i = 0; i < Character::JOINT_NUM; i++)
	{ if(i==1)   
	 {
		 
		 
		glm::vec3 offset,offset1,offset2;
		
		offset = (character->joint_positions[1] -
		                                             character->joint_positions[0]);
		offset1[0] = offset[0]*0.5;
		offset1[1] = offset[1]*0.5;
		offset1[2] = offset[2]*0.5;	
		SurfSetColorRGB(119,134, 167);
		DrawSphere(character->joint_positions[i]  - offset1, 12);	
		DrawSphereWire(character->joint_positions[i]  - offset1, 12);
		
		
		offset = (character->joint_positions[2] -
		                                             character->joint_positions[1]);
		offset2[0] = offset[0]*0.4;
		offset2[1] = offset[1]*0.4;
		offset2[2] = offset[2]*0.4;	
		SurfSetColorRGB(119,134, 167);
		DrawSphere(character->joint_positions[i]+offset2,16);
		DrawSphereWire(character->joint_positions[i]+offset2,16);
	 }
	  if(i==2)   
	  {
		//DrawSphere(character->joint_positions[i] + (character->joint_positions[2] -
		 //                                           character->joint_positions[1])/4.0, 13);
		 glm::vec3 offset = (character->joint_positions[2] -
		                                             character->joint_positions[1]);
		offset[0] = offset[0]/1.5;
		offset[1] = offset[1]/1.5;
		offset[2] = offset[2]/1.5;
		SurfSetColorRGB(119,134, 167);;
		DrawSphere(character->joint_positions[i]  + offset, 12);	
		DrawSphere(character->joint_positions[i]   , 8);
		
		DrawSphereWire(character->joint_positions[i]  + offset, 12);	
		DrawSphereWire(character->joint_positions[i]   , 8);
	  }
	  else
	  {
		  SurfSetColorRGB(119,134, 167);
		  DrawSphere(character->joint_positions[i]);
	  }
    }
	
    SurfSetColor(0.5,0.55, 0.8);
    for (int i = 0; i < Character::JOINT_NUM; i++) {
	if(character->joint_parents[i]==-1) continue;
	  if(character->joint_parents[i]==1)
	  {
	  glm::vec3 offset = (character->joint_positions[2] -
		                                             character->joint_positions[1]);
		offset[0] = offset[0]/1.2;
		offset[1] = offset[1]/1.2;
		offset[2] = offset[2]/1.2;
	   glm::vec3 pos1 = character->joint_positions[i];
       glm::vec3 pos2 = character->joint_positions[character->joint_parents[i]]+offset;	  
       DrawPointLink(pos1, pos2);  
	  }
	  else
      {glm::vec3 pos1 = character->joint_positions[i];
      glm::vec3 pos2 = character->joint_positions[character->joint_parents[i]];	  
      DrawPointLink(pos1, pos2);
	  }
    }	
	//======================================================
  }
  
  /* UI Elements */

  glm::mat4 ui_view = glm::mat4(1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1);
  glm::mat4 ui_proj = glm::ortho(0.0f, (float)WINDOW_WIDTH, (float)WINDOW_HEIGHT, 0.0f, 0.0f, 1.0f);  
  
  glMatrixMode(GL_PROJECTION);
  glLoadMatrixf(glm::value_ptr(ui_proj));

  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixf(glm::value_ptr(ui_view));
  
  /* PFNN Visual */
  
  if (options->display_debug && options->display_debug_pfnn) {
    
    glColor3f(0.0, 0.0, 0.0);  

    glLineWidth(5);
    glBegin(GL_LINES);
    for (float i = 0; i < 2*M_PI; i+=0.01) {
      glVertex3f(WINDOW_WIDTH-125+50*cos(i     ),100+50*sin(i     ),0);    
      glVertex3f(WINDOW_WIDTH-125+50*cos(i+0.01),100+50*sin(i+0.01),0);
    }
    glEnd();
    glLineWidth(1);
    
    glPointSize(20);
    glBegin(GL_POINTS);
    glVertex3f(WINDOW_WIDTH-125+50*cos(character->phase), 100+50*sin(character->phase), 0);
    glEnd();
    glPointSize(1);
    
    glColor3f(1.0, 1.0, 1.0); 

    int pindex_1 = (int)((character->phase / (2*M_PI)) * 50);
    MatrixXf W0p = pfnn->W0[pindex_1];

    glPointSize(1);
    glBegin(GL_POINTS);
    
    for (int x = 0; x < W0p.rows(); x++)
    for (int y = 0; y < W0p.cols(); y++) {
      float v = (W0p(x, y)+0.5)/2.0;
      glm::vec3 col = v > 0.5 ? glm::mix(glm::vec3(1,0,0), glm::vec3(0,0,1), v-0.5) : glm::mix(glm::vec3(0,1,0), glm::vec3(0,0,1), v*2.0);
      glColor3f(col.x, col.y, col.z); 
      glVertex3f(WINDOW_WIDTH-W0p.cols()+y-25, x+175, 0);
    }
    
    glEnd();
    glPointSize(1);
    
  }
  
  /* Display UI */
  
  if (options->display_debug && options->display_hud_options) {
    glLineWidth(3);
    glBegin(GL_LINES);
    glColor3f(0.0, 0.0, 0.0); 
    glVertex3f(125+20,20,0); glVertex3f(125+20,40,0); /* I */
    glVertex3f(125+25,20,0); glVertex3f(125+25,40,0); /* K */
    glVertex3f(125+25,30,0); glVertex3f(125+30,40,0);
    glVertex3f(125+25,30,0); glVertex3f(125+30,20,0);
    glEnd();
    glLineWidth(1);
    
    if (options->enable_ik) { glColor3f(0.0, 1.0, 0.0); } else { glColor3f(1.0, 0.0, 0.0); }
    glPointSize(10);
    glBegin(GL_POINTS);
    glVertex3f(125+60, 30, 0);
    glEnd();
    glPointSize(1); 
    glColor3f(1.0, 1.0, 1.0); 
  }
  
  if (options->display_debug && options->display_hud_options) {
    glLineWidth(3);
    glBegin(GL_LINES);
    glColor3f(0.0, 0.0, 0.0); 
    glVertex3f(125+20,50,0); glVertex3f(125+20,70,0); /* D */
    glVertex3f(125+20,50,0); glVertex3f(125+25,55,0);
    glVertex3f(125+20,70,0); glVertex3f(125+25,65,0);
    glVertex3f(125+25,65,0); glVertex3f(125+25,55,0);
    glVertex3f(125+30,50,0); glVertex3f(125+30,70,0); /* I */
    glVertex3f(125+35,50,0); glVertex3f(125+35,70,0); /* R */
    glVertex3f(125+35,60,0); glVertex3f(125+40,70,0);
    glVertex3f(125+35,50,0); glVertex3f(125+40,55,0);
    glVertex3f(125+35,60,0); glVertex3f(125+40,55,0);
    glEnd();
    glLineWidth(1);
    
    glColor3f(1.0-character->strafe_amount, character->strafe_amount, 0.0);
    glPointSize(10);
    glBegin(GL_POINTS);
    glVertex3f(125+60, 60, 0);
    glEnd();
    glPointSize(1); 
    glColor3f(1.0, 1.0, 1.0); 
  }
  
  if (options->display_debug && options->display_hud_options) {
    glLineWidth(3);
    glBegin(GL_LINES);
    glColor3f(0.0, 0.0, 0.0); 
    glVertex3f(125+20,80,0); glVertex3f(125+20,100,0); /* R */
    glVertex3f(125+20,90,0); glVertex3f(125+25,100,0);
    glVertex3f(125+20,80,0); glVertex3f(125+25,85,0);
    glVertex3f(125+20,90,0); glVertex3f(125+25,85,0);
    glVertex3f(125+30,80,0); glVertex3f(125+30,100,0); /* E */
    glVertex3f(125+30,80,0); glVertex3f(125+35,80,0); 
    glVertex3f(125+30,90,0); glVertex3f(125+35,90,0); 
    glVertex3f(125+30,100,0); glVertex3f(125+35,100,0); 
    glVertex3f(125+40,80,0); glVertex3f(125+40,90,0); /* S */
    glVertex3f(125+45,90,0); glVertex3f(125+45,100,0);
    glVertex3f(125+40,80,0); glVertex3f(125+45,80,0); 
    glVertex3f(125+40,90,0); glVertex3f(125+45,90,0); 
    glVertex3f(125+40,100,0); glVertex3f(125+45,100,0); 
    glEnd();
    glLineWidth(1);
    
    glColor3f(1.0-character->responsive, character->responsive, 0.0);
    glPointSize(10);
    glBegin(GL_POINTS);
    glVertex3f(125+60, 90, 0);
    glEnd();
    glPointSize(1); 
    glColor3f(1.0, 1.0, 1.0); 
  }
  

  if (options->display_debug && options->display_hud_stick) {
    glColor3f(0.0, 0.0, 0.0); 
    glPointSize(1 * options->display_scale);
    glBegin(GL_POINTS);
    for (float i = 0; i < 1.0; i+=0.025) {
      glVertex3f(60+40*cos(2*M_PI*(i     )),60+40*sin(2*M_PI*(i     )), 0);    
    }
    glEnd();

    int x_vel = -SDL_JoystickGetAxis(stick, GAMEPAD_STICK_L_HORIZONTAL);
    int y_vel = -SDL_JoystickGetAxis(stick, GAMEPAD_STICK_L_VERTICAL); 
    if (abs(x_vel) + abs(y_vel) < 10000) { x_vel = 0; y_vel = 0; };  
    
    glm::vec2 direction = glm::vec2((-x_vel) / 32768.0f, (-y_vel) / 32768.0f);
    glLineWidth(1 * options->display_scale);    
    glBegin(GL_LINES);
    glVertex3f(60, 60, 0);
    glVertex3f(60+direction.x*40, 60+direction.y*40, 0);
    glEnd();
    glLineWidth(1.0);    

    glPointSize(3 * options->display_scale);
    glBegin(GL_POINTS);
    glVertex3f(60, 60, 0);
    glVertex3f(60+direction.x*40, 60+direction.y*40, 0);
    glEnd();
    glPointSize(1);
    
    float speed0 = ((SDL_JoystickGetAxis(stick, 5) / 32768.0) + 1.0) / 2.0;

    glPointSize(1 * options->display_scale);
    glBegin(GL_POINTS);
    for (float i = 0; i < 1.0; i+=0.09) {
      glVertex3f(120, 100 - i * 80, 0);    
    }
    glEnd();
    
    glLineWidth(1 * options->display_scale);    
    glBegin(GL_LINES);
    glVertex3f(120, 100, 0);
    glVertex3f(120, 100 - speed0 * 80, 0);
    glEnd();
    glLineWidth(1.0); 
    
    glPointSize(3 * options->display_scale);
    glBegin(GL_POINTS);
    glVertex3f(120, 100, 0);
    glVertex3f(120, 100 - speed0 * 80, 0);
    glEnd();
    glPointSize(1);
    
    glColor3f(1.0, 1.0, 1.0); 
  }
  
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_CULL_FACE);
   
  //glDisable(GL_DEPTH_TEST);
  //glDisable(GL_CULL_FACE);

   //---------------------------------------
  SDL_GL_SwapBuffers( );//<============
}

//############################################################
static void setup_opengl( int width, int height )
{
    float ratio = (float) width / (float) height;

    /* Our shading model--Gouraud (smooth). */
    glShadeModel( GL_SMOOTH );

    /* Culling. */
    glCullFace( GL_BACK );
    glFrontFace( GL_CCW );
    glEnable( GL_CULL_FACE );

    /* Set the clear color. */
    glClearColor( 0, 0, 0, 0 );

    /* Setup our viewport. */
    glViewport( 0, 0, width, height );

    /*
     * Change to the projection matrix and set
     * our viewing volume.
     */
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity( );
    /*
     * EXERCISE:
     * Replace this with a call to glFrustum.
     */
    gluPerspective( 60.0, ratio, 1.0, 1024.0 );
}

//######################################################### 
//#########################################################
int main( int argc, char* argv[] )
{
    glutInit(&argc, argv);
    // Information about the current video settings. 
    const SDL_VideoInfo* info = NULL;
    // Dimensions of our window. 
    int width = 0;
    int height = 0;
    // Color depth in bits of our window. 
    int bpp = 0;
    // Flags we will pass into SDL_SetVideoMode. 
    int flags = 0;

    // First, initialize SDL's video subsystem. 
    if( SDL_Init( SDL_INIT_VIDEO|SDL_INIT_JOYSTICK ) < 0 ) {
        // Failed, exit. 
        fprintf( stderr, "Video initialization failed: %s\n",
             SDL_GetError( ) );
    }

    // Let's get some video information. 
    info = SDL_GetVideoInfo( );

    if( !info ) {
        // This should probably never happen. 
        fprintf( stderr, "Video query failed: %s\n",
             SDL_GetError( ) );
    }

 
    width = WINDOW_WIDTH;
    height = WINDOW_HEIGHT;
    bpp = info->vfmt->BitsPerPixel;
	printf("bpp:%i\n", bpp);

    //SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );
    //SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 5 );
    //SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );
    //SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

     
    flags = SDL_OPENGL ;//| SDL_FULLSCREEN;

     
    // Set the video mode
     
    if( SDL_SetVideoMode( width, height, bpp, flags ) == 0 ) {
         
        fprintf( stderr, "Video mode set failed: %s\n",
             SDL_GetError( ) );
        // quit_tutorial( 1 );
    }
     
     //At this point, we should have a properly setup
     // double-buffered window for use with OpenGL.
     
    //setup_opengl( width, height );
	myinit();
    stick = SDL_JoystickOpen(0);
	//GLenum err = glewInit();
	//========================================================================
	// Resources 
  
  options = new Options();
  camera = new CameraOrbit();
  light = new LightDirectional();
  
  character = new Character();
  character->load(
    "./network/character_vertices.bin", 
    "./network/character_triangles.bin", 
    "./network/character_parents.bin", 
    "./network/character_xforms.bin");
  
  trajectory = new Trajectory();
  ik = new IK();
  
 // shader_terrain = new Shader();
  //shader_terrain_shadow = new Shader();
  //shader_character = new Shader();
  //shader_character_shadow = new Shader();
 
  //shader_terrain->load("./shaders/terrain.vs", "./shaders/terrain_low.fs");
  //shader_terrain_shadow->load("./shaders/terrain_shadow.vs", "./shaders/terrain_shadow.fs");
  //shader_character->load("./shaders/character.vs", "./shaders/character_low.fs");
  //shader_character_shadow->load("./shaders/character_shadow.vs", "./shaders/character_shadow.fs");
 

  heightmap = new Heightmap();
  areas = new Areas();
  
  pfnn = new PFNN(PFNN::MODE_CONSTANT);
  //pfnn = new PFNN(PFNN::MODE_CUBIC);
  //pfnn = new PFNN(PFNN::MODE_LINEAR);
  pfnn->load();
  
  load_world3();
  
  // Game Loop 
	
	static bool running = true;
  
  while (running) {
    
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      
      if (event.type == SDL_QUIT) { running = false; break; }
      
      if (event.type == SDL_JOYBUTTONDOWN) {
        if (event.jbutton.button == GAMEPAD_B) {
          character->crouched_target = character->crouched_target ? 0.0 : 1.0;
        }
        if (event.jbutton.button == GAMEPAD_BACK) {
          character->responsive = !character->responsive;
        }
        if (event.jbutton.button == GAMEPAD_X) {
          options->display_debug = !options->display_debug;
        }
        if (event.jbutton.button == GAMEPAD_START) {
          options->enable_ik = !options->enable_ik;
        }
      }
      
      if (event.type == SDL_KEYDOWN) {
        switch (event.key.keysym.sym) {
          case SDLK_ESCAPE: running = false; break;
          case SDLK_1: load_world0(); break;
          case SDLK_2: load_world1(); break;
          case SDLK_3: load_world2(); break;
          case SDLK_4: load_world3(); break;
          case SDLK_5: load_world4(); break;
          case SDLK_6: load_world5(); break;
        }
        
      }
    }
    
    pre_render();
    render();
    post_render();
    
   //《-- glFlush();
   //《== glFinish();
    
    //《==SDL_GL_SwapWindow(window);
  }
	
	 

    // Never reached. 
    return 0;
}

void myinit()
{
    GLfloat mat_specular[]={0.2, 0.2, 0.2, 1.0};
    GLfloat mat_diffuse[]={1.0, 1.0, 1.0, 1.0};
    GLfloat mat_ambient[]={1.0, 1.0, 1.0, 1.0};
    GLfloat mat_shininess={100.0};
    GLfloat light_ambient[]={0.0, 0.0, 0.0, 1.0};
    GLfloat light_diffuse[]={1.0, 1.0, 1.0, 1.0};
    GLfloat light_specular[]={1.0, 1.0, 1.0, 1.0};
    GLfloat light_position[]={10.0, 10.0, 10.0, 0.0};

    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);

    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
    glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);

    glShadeModel(GL_SMOOTH);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    glEnable(GL_DEPTH_TEST);
    //glClearColor (0.0, 0.0, 0.0, 1.0);
    //glColor3f (1.0, 1.0, 1.0);
    glEnable(GL_NORMALIZE); /* automatic normaization of normals */
    glEnable(GL_CULL_FACE); /* eliminate backfacing polygons */
    glCullFace(GL_BACK); 
	
	//===============================================
	//glPointSize(6);
	//glLineWidth(6);

	// The following commands should induce OpenGL to create round points and 
	//	antialias points and lines.  (This is implementation dependent unfortunately, and
	//  may slow down rendering considerably.)
	//  You may comment these out if you wish.
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);	// Make round points, not square points
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);		// Antialias the lines
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

}


