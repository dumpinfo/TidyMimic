#include "KinTree.h"

#include <iostream>

#include "util/FileUtil.h"
//#include "sim/RBDUtil.h"

const int cKinTree::gPosDim = 3;
const int cKinTree::gRotDim = 4;
const int cKinTree::gRootDim = gPosDim + gRotDim;
const int cKinTree::gInvalidJointID = -1;

// Json keys
const std::string gJointTypeNames[cKinTree::eJointTypeMax] =
{
	"revolute",
	"planar",
	"prismatic",
	"fixed",
	"spherical",
	"none"
};

const std::string gJointsKey = "Joints";
const std::string gJointDescKeys[cKinTree::eJointDescMax] = 
{
	"Type",
	"Parent",
	"AttachX",
	"AttachY",
	"AttachZ",
	"AttachThetaX",
	"AttachThetaY",
	"AttachThetaZ",
	"LimLow0",
	"LimLow1",
	"LimLow2",
	"LimHigh0",
	"LimHigh1",
	"LimHigh2",
	"TorqueLim",
	"ForceLim",
	"IsEndEffector",
	"DiffWeight",
	"Offset"
};

const std::string gBodyDefsKey = "BodyDefs";
const std::string gBodyDescKeys[cKinTree::eBodyParamMax] =
{
	"Shape",
	"Mass",
	"ColGroup",
	"EnableFallContact",
	"AttachX",
	"AttachY",
	"AttachZ",
	"AttachThetaX",
	"AttachThetaY",
	"AttachThetaZ",
	"Param0",
	"Param1",
	"Param2",
	"ColorR",
	"ColorG",
	"ColorB",
	"ColorA"
};

const std::string gDrawShapeDefsKey = "DrawShapeDefs";
const std::string gDrawShapeDescKeys[cKinTree::eDrawShapeParamMax] =
{
	"Shape",
	"ParentJoint",
	"AttachX",
	"AttachY",
	"AttachZ",
	"AttachThetaX",
	"AttachThetaY",
	"AttachThetaZ",
	"Param0",
	"Param1",
	"Param2",
	"ColorR",
	"ColorG",
	"ColorB",
	"ColorA",
	"MeshID"
};

int cKinTree::GetRoot(const Eigen::MatrixXd& joint_desc)
{
	// this should always be true right?
	return 0;
}

 

bool cKinTree::LoadDrawShapeDefs(const std::string& char_file, Eigen::MatrixXd& out_draw_defs)
{
	bool succ = true;
	std::string str;

	std::ifstream f_stream(char_file.c_str());
	Json::Value root;
	Json::Reader reader;
	succ = reader.parse(f_stream, root);
	f_stream.close();

	if (succ)
	{
		if (!root[gDrawShapeDefsKey].isNull())
		{
			Json::Value shape_defs = root.get(gDrawShapeDefsKey, 0);
			int num_shapes = shape_defs.size();

			succ = true;
			out_draw_defs.resize(num_shapes, eDrawShapeParamMax);
			for (int b = 0; b < num_shapes; ++b)
			{
				tDrawShapeDef curr_def = BuildDrawShapeDef();
				Json::Value shape_json = shape_defs.get(b, 0);
				bool succ_def = ParseDrawShapeDef(shape_json, curr_def);

				if (succ)
				{
					out_draw_defs.row(b) = curr_def;
				}
				else
				{
					succ = false;
					break;
				}
			}
		}
	}

	if (!succ)
	{
		printf("Failed to load draw shape definition from %s\n", char_file.c_str());
		assert(false);
	}

	return succ;
}

bool cKinTree::ParseDrawShapeDef(const Json::Value& root, tDrawShapeDef& out_def)
{
	std::string shape_str = root.get(gDrawShapeDescKeys[eDrawShapeShape], "").asString();
	cShape::eShape shape;
	bool succ = cShape::ParseShape(shape_str, shape);
	if (succ)
	{
		out_def(eDrawShapeShape) = static_cast<double>(static_cast<int>(shape));
	}

	for (int i = 0; i < eDrawShapeParamMax; ++i)
	{
		const std::string& curr_key = gDrawShapeDescKeys[i];
		if (!root[curr_key].isNull()
			&& root[curr_key].isNumeric())
		{
			Json::Value json_val = root[curr_key];
			double val = json_val.asDouble();
			out_def(i) = val;
		}
	}

	return succ;
}

 

int cKinTree::GetDrawShapeParentJoint(const tDrawShapeDef& shape)
{
	return static_cast<int>(shape[eDrawShapeParentJoint]);
}

tVector cKinTree::GetDrawShapeAttachPt(const tDrawShapeDef& shape)
{
	return tVector(shape[eDrawShapeAttachX], shape[eDrawShapeAttachY], shape[cKinTree::eDrawShapeAttachZ], 0);
}

tVector cKinTree::GetDrawShapeAttachTheta(const tDrawShapeDef& shape)
{
	return tVector(shape[eDrawShapeAttachThetaX], shape[eDrawShapeAttachThetaY], shape[eDrawShapeAttachThetaZ], 0);
}

void cKinTree::GetDrawShapeRotation(const tDrawShapeDef& shape, tVector& out_axis, double& out_theta)
{
	tVector theta = GetDrawShapeAttachTheta(shape);
	cMathUtil::EulerToAxisAngle(theta, out_axis, out_theta);
}

tVector cKinTree::GetDrawShapeColor(const tDrawShapeDef& shape)
{
	return tVector(shape[eDrawShapeColorR], shape[eDrawShapeColorG], shape[eDrawShapeColorB], shape[cKinTree::eDrawShapeColorA]);
}

 

bool cKinTree::Load(const Json::Value& root, Eigen::MatrixXd& out_joint_mat)
{
	bool succ = false;

	if (!root[gJointsKey].isNull())
	{
		Json::Value joints = root[gJointsKey];
		int num_joints = joints.size();

		out_joint_mat.resize(num_joints, eJointDescMax);
		
		for (int j = 0; j < num_joints; ++j)
		{
			tJointDesc curr_joint_desc = tJointDesc::Zero();

			Json::Value joint_json = joints.get(j, 0);
			succ = ParseJoint(joint_json, curr_joint_desc);
			if (succ)
			{
				out_joint_mat.row(j) = curr_joint_desc;
			}
			else
			{
				printf("Failed to parse joint %i\n", j);
				return false;
			}
		}

		for (int j = 0; j < num_joints; ++j)
		{
			const auto& curr_desc = out_joint_mat.row(j);
			int parent_id = static_cast<int>(curr_desc(eJointDescParent));
			if (parent_id >= j)
			{
				printf("Parent id must be < child id, parent id: %i, child id: %i\n", parent_id, j);
				out_joint_mat.resize(0, 0);
				assert(false);

				return false;
			}

			out_joint_mat.row(j) = curr_desc;
		}

		PostProcessJointMat(out_joint_mat);
	}

	return succ;
}

 

tVector cKinTree::GetRootPos(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state)
{
	int root_id = GetRoot(joint_mat);
	tVector pos = tVector::Zero();
	int param_offset = GetParamOffset(joint_mat, root_id);
	pos.segment(0, gPosDim) = state.segment(param_offset, gPosDim);
	return pos;
}

void cKinTree::SetRootPos(const Eigen::MatrixXd& joint_mat, const tVector& pos, Eigen::VectorXd& out_state)
{
	int root_id = GetRoot(joint_mat);
	int param_offset = GetParamOffset(joint_mat, root_id);
	out_state.segment(param_offset, gPosDim) = pos.segment(0, gPosDim);
}

tQuaternion cKinTree::GetRootRot(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state)
{
	int root_id = GetRoot(joint_mat);
	int param_offset = GetParamOffset(joint_mat, root_id);
	tQuaternion rot = cMathUtil::VecToQuat(state.segment(param_offset + gPosDim, gRotDim));
	return rot;
}

void cKinTree::SetRootRot(const Eigen::MatrixXd& joint_mat, const tQuaternion& rot, Eigen::VectorXd& out_state)
{
	int root_id = GetRoot(joint_mat);
	int param_offset = GetParamOffset(joint_mat, root_id);
	out_state.segment(param_offset + gPosDim, gRotDim) = cMathUtil::QuatToVec(rot).segment(0, gRotDim);
}

 

void cKinTree::SetRootVel(const Eigen::MatrixXd& joint_mat, const tVector& vel, Eigen::VectorXd& out_vel)
{
	int root_id = GetRoot(joint_mat);
	int param_offset = GetParamOffset(joint_mat, root_id);
	out_vel.segment(param_offset, gPosDim) = vel.segment(0, gPosDim);
}

 

void cKinTree::SetRootAngVel(const Eigen::MatrixXd& joint_mat, const tVector& ang_vel, Eigen::VectorXd& out_vel)
{
	int root_id = GetRoot(joint_mat);
	int param_offset = GetParamOffset(joint_mat, root_id);
	out_vel.segment(param_offset + gPosDim, gRotDim) = ang_vel.segment(0, gRotDim);
}

 

int cKinTree::GetNumJoints(const Eigen::MatrixXd& joint_mat)
{
	return static_cast<int>(joint_mat.rows());
}

int cKinTree::GetNumDof(const Eigen::MatrixXd& joint_mat)
{
	int num_joints = GetNumJoints(joint_mat);
	int num_dof = cKinTree::GetParamOffset(joint_mat, num_joints - 1) + cKinTree::GetParamSize(joint_mat, num_joints - 1);
	return num_dof;
}

 

int cKinTree::GetParamOffset(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	int offset = static_cast<int>(joint_mat(joint_id, eJointDescParamOffset));
	return offset;
}

int cKinTree::GetParamSize(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	bool is_root = cKinTree::IsRoot(joint_mat, joint_id);
	int size = (is_root) ? gRootDim : GetJointParamSize(joint_type);
	return size;
}

int cKinTree::GetJointParamSize(eJointType joint_type)
{
	int size = 0;
	switch (joint_type)
	{
	case eJointTypeRevolute:
		size = 1;
		break;
	case eJointTypePrismatic:
		size = 1;
		break;
	case eJointTypePlanar:
		size = 3;
		break;
	case eJointTypeFixed:
		size = 0;
		break;
	case eJointTypeSpherical:
		size = 4;
		break;
	default:
		assert(false); // unsupported joint type
		break;
	}

	return size;
}

 

void cKinTree::SetJointParams(const Eigen::MatrixXd& joint_mat, int j, const Eigen::VectorXd& params, Eigen::VectorXd& out_state)
{
	int offset = GetParamOffset(joint_mat, j);
	int dim = GetParamSize(joint_mat, j);
	assert(dim == params.size());
	out_state.block(offset, 0, dim, 1) = params;
}

cKinTree::eJointType cKinTree::GetJointType(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	eJointType type = static_cast<eJointType>(static_cast<int>(joint_mat(joint_id, cKinTree::eJointDescType)));
	return type;
}

int cKinTree::GetParent(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	int parent = static_cast<int>(joint_mat(joint_id, cKinTree::eJointDescParent));
	assert(parent < joint_id); // joints should always be ordered as such
								// since some algorithms will assume this ordering
	return parent;
}

bool cKinTree::HasParent(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	int parent = GetParent(joint_mat, joint_id);
	return parent != gInvalidJointID;
}

bool cKinTree::IsRoot(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	return !HasParent(joint_mat, joint_id);
}

 

tVector cKinTree::GetAttachPt(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	tVector attach_pt = tVector(joint_mat(joint_id, cKinTree::eJointDescAttachX),
								joint_mat(joint_id, cKinTree::eJointDescAttachY),
								joint_mat(joint_id, cKinTree::eJointDescAttachZ), 0);
	return attach_pt;
}

tVector cKinTree::GetAttachTheta(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	tVector attach_theta = tVector(joint_mat(joint_id, cKinTree::eJointDescAttachThetaX),
									joint_mat(joint_id, cKinTree::eJointDescAttachThetaY),
									joint_mat(joint_id, cKinTree::eJointDescAttachThetaZ), 0);
	return attach_theta;
}

 

bool cKinTree::ParseJoint(const Json::Value& root, tJointDesc& out_joint_desc)
{
	out_joint_desc = BuildJointDesc();
	eJointType joint_type = eJointTypeNone;
	const Json::Value& type_json = root[gJointDescKeys[eJointDescType]];
	if (type_json.isNull())
	{
		printf("No joint type specified\n");
	}
	else
	{
		std::string type_str = type_json.asString();
		ParseJointType(type_str, joint_type);
		out_joint_desc[eJointDescType] = static_cast<double>(static_cast<int>(joint_type));
	}

	for (int i = 0; i < eJointDescMax; ++i)
	{
		if (i != eJointDescType)
		{
			const std::string& key = gJointDescKeys[i];
			if (!root[key].isNull())
			{
				out_joint_desc[i] = root[key].asDouble();
			}
		}
	}
	return true;
}

bool cKinTree::ParseJointType(const std::string& type_str, eJointType& out_joint_type)
{
	for (int i = 0; i < eJointTypeMax; ++i)
	{
		const std::string& name = gJointTypeNames[i];
		if (type_str == name)
		{
			out_joint_type = static_cast<eJointType>(i);
			return true;
		}
	}
	printf("Unsupported joint type: %s\n", type_str.c_str());
	assert(false); // unsupported joint type
	return false;
}

void cKinTree::PostProcessJointMat(Eigen::MatrixXd& out_joint_mat)
{
	int num_joints = GetNumJoints(out_joint_mat);
	int offset = 0;
	for (int j = 0; j < num_joints; ++j)
	{
		int curr_size = GetParamSize(out_joint_mat, j);
		out_joint_mat(j, eJointDescParamOffset) = offset;
		offset += curr_size;
	}
	int root_id = GetRoot(out_joint_mat);

	out_joint_mat(root_id, eJointDescAttachX) = 0;
	out_joint_mat(root_id, eJointDescAttachY) = 0;
	out_joint_mat(root_id, eJointDescAttachZ) = 0;
}

tMatrix cKinTree::BuildAttachTrans(const Eigen::MatrixXd& joint_mat, int joint_id)
{
	// child to parent
	tVector attach_pt = GetAttachPt(joint_mat, joint_id);
	tVector attach_theta = GetAttachTheta(joint_mat, joint_id);
	tMatrix mat = cMathUtil::RotateMat(attach_theta);
	mat(0, 3) = attach_pt[0];
	mat(1, 3) = attach_pt[1];
	mat(2, 3) = attach_pt[2];
	return mat;
}

tMatrix cKinTree::ChildParentTrans(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id)
{
	tMatrix mat;
	eJointType j_type = GetJointType(joint_mat, joint_id);
	bool is_root = IsRoot(joint_mat, joint_id);

	if (is_root)
	{
		mat = ChildParentTransRoot(joint_mat, state, joint_id);
	}
	else
	{
		switch (j_type)
		{
		case eJointTypeRevolute:
			mat = ChildParentTransRevolute(joint_mat, state, joint_id);
			break;
		case eJointTypePrismatic:
			//<--- mat = ChildParentTransPrismatic(joint_mat, state, joint_id);
			break;
		case eJointTypePlanar:
			//<--- mat = ChildParentTransPlanar(joint_mat, state, joint_id);
			break;
		case eJointTypeFixed:
			mat = ChildParentTransFixed(joint_mat, state, joint_id);
			break;
		case eJointTypeSpherical:
			mat = ChildParentTransSpherical(joint_mat, state, joint_id);
			break;
		default:
			break;
		}
	}
	
	return mat;
}

 

tMatrix cKinTree::JointWorldTrans(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id)
{
	tMatrix m = tMatrix::Identity();
	int curr_id = joint_id;
	assert(joint_id != gInvalidIdx); // invalid joint
	while (curr_id != gInvalidJointID)
	{
		tMatrix child_parent_mat = ChildParentTrans(joint_mat, state, curr_id);
		m = child_parent_mat * m;
		curr_id = GetParent(joint_mat, curr_id);
	}

	return m;
}

 

cKinTree::tJointDesc cKinTree::BuildJointDesc()
{
	tJointDesc desc;
	desc(eJointDescType) = static_cast<double>(eJointTypeRevolute);
	desc(eJointDescParent) = gInvalidIdx;
	desc(eJointDescAttachX) = 0;
	desc(eJointDescAttachY) = 0;
	desc(eJointDescAttachZ) = 0;
	desc(eJointDescAttachThetaX) = 0;
	desc(eJointDescAttachThetaY) = 0;
	desc(eJointDescAttachThetaZ) = 0;
	desc(eJointDescLimLow0) = 1;
	desc(eJointDescLimLow1) = 1;
	desc(eJointDescLimLow2) = 1;
	desc(eJointDescLimHigh0) = 0;
	desc(eJointDescLimHigh1) = 0;
	desc(eJointDescLimHigh2) = 0;
	desc(eJointDescIsEndEffector) = 0;
	desc(eJointDescTorqueLim) = std::numeric_limits<double>::infinity();
	desc(eJointDescForceLim) = std::numeric_limits<double>::infinity();
	desc(eJointDescDiffWeight) = 1;
	desc(eJointDescParamOffset) = 0;

	return desc;
}

 

cKinTree::tDrawShapeDef cKinTree::BuildDrawShapeDef()
{
	tDrawShapeDef def;
	def(eDrawShapeShape) = static_cast<double>(cShape::eShapeNull);
	def(eDrawShapeParentJoint) = gInvalidIdx;
	def(eDrawShapeAttachX) = 0;
	def(eDrawShapeAttachY) = 0;
	def(eDrawShapeAttachZ) = 0;
	def(eDrawShapeAttachThetaX) = 0;
	def(eDrawShapeAttachThetaY) = 0;
	def(eDrawShapeAttachThetaZ) = 0;
	def(eDrawShapeParam0) = 0;
	def(eDrawShapeParam1) = 0;
	def(eDrawShapeParam2) = 0;
	def(eDrawShapeColorR) = 0;
	def(eDrawShapeColorG) = 0;
	def(eDrawShapeColorB) = 0;
	def(eDrawShapeColorA) = 1;
	def(eDrawShapeMeshID) = gInvalidIdx;
	return def;
}

void cKinTree::BuildDefaultPose(const Eigen::MatrixXd& joint_mat, Eigen::VectorXd& out_pose)
{
	int num_dof = GetNumDof(joint_mat);
	out_pose = Eigen::VectorXd::Zero(num_dof);

	int num_joints = GetNumJoints(joint_mat);

	int root_id = GetRoot(joint_mat);
	Eigen::VectorXd root_pose;
	BuildDefaultPoseRoot(joint_mat, root_pose);
	SetJointParams(joint_mat, root_id, root_pose, out_pose);

	for (int j = 1; j < num_joints; ++j)
	{
		eJointType joint_type = GetJointType(joint_mat, j);
		Eigen::VectorXd joint_pose;
		switch (joint_type)
		{
		case eJointTypeRevolute:
			BuildDefaultPoseRevolute(joint_pose);
			break;
		case eJointTypePrismatic:
			//<--- BuildDefaultPosePrismatic(joint_pose);
			break;
		case eJointTypePlanar:
			//<--- BuildDefaultPosePlanar(joint_pose);
			break;
		case eJointTypeFixed:
			BuildDefaultPoseFixed(joint_pose);
			break;
		case eJointTypeSpherical:
			BuildDefaultPoseSpherical(joint_pose);
			break;
		default:
			assert(false); // unsupported joint type
			break;
		}

		SetJointParams(joint_mat, j, joint_pose, out_pose);
	}
}

void cKinTree::BuildDefaultVel(const Eigen::MatrixXd& joint_mat, Eigen::VectorXd& out_vel)
{
	int num_dof = GetNumDof(joint_mat);
	out_vel = Eigen::VectorXd::Zero(num_dof);

	int num_joints = GetNumJoints(joint_mat);

	int root_id = GetRoot(joint_mat);
	Eigen::VectorXd root_pose;
	BuildDefaultVelRoot(joint_mat, root_pose);
	SetJointParams(joint_mat, root_id, root_pose, out_vel);

	for (int j = 1; j < num_joints; ++j)
	{
		eJointType joint_type = GetJointType(joint_mat, j);
		Eigen::VectorXd joint_pose;
		switch (joint_type)
		{
		case eJointTypeRevolute:
			BuildDefaultVelRevolute(joint_pose);
			break;
		case eJointTypePrismatic:
			//<--- BuildDefaultVelPrismatic(joint_pose);
			break;
		case eJointTypePlanar:
			//<--- BuildDefaultVelPlanar(joint_pose);
			break;
		case eJointTypeFixed:
			BuildDefaultVelFixed(joint_pose);
			break;
		case eJointTypeSpherical:
			BuildDefaultVelSpherical(joint_pose);
			break;
		default:
			assert(false); // unsupported joint type
			break;
		}

		SetJointParams(joint_mat, j, joint_pose, out_vel);
	}
}

 
void cKinTree::CalcVel(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& pose0, const Eigen::VectorXd& pose1, double dt, Eigen::VectorXd& out_vel)
{
	assert(pose0.size() == pose1.size());
	out_vel.resize(pose0.size());

	int num_joints = GetNumJoints(joint_mat);
	tVector root_pos0 = GetRootPos(joint_mat, pose0);
	tVector root_pos1 = GetRootPos(joint_mat, pose1);
	tVector root_vel = (root_pos1 - root_pos0) / dt;

	tQuaternion root_rot0 = GetRootRot(joint_mat, pose0);
	tQuaternion root_rot1 = GetRootRot(joint_mat, pose1);
	tVector root_rot_vel = cMathUtil::CalcQuaternionVel(root_rot0, root_rot1, dt);

	cKinTree::SetRootVel(joint_mat, root_vel, out_vel);
	cKinTree::SetRootAngVel(joint_mat, root_rot_vel, out_vel);

	for (int j = 1; j < num_joints; ++j)
	{
		int param_offset = GetParamOffset(joint_mat, j);
		int param_size = GetParamSize(joint_mat, j);
		eJointType joint_type = GetJointType(joint_mat, j);

		switch (joint_type)
		{
		case eJointTypeSpherical:
		{
			tQuaternion q0 = cMathUtil::VecToQuat(pose0.segment(param_offset, param_size));
			tQuaternion q1 = cMathUtil::VecToQuat(pose1.segment(param_offset, param_size));
			tVector rot_vel = cMathUtil::CalcQuaternionVelRel(q0, q1, dt);
			out_vel.segment(param_offset, param_size) = rot_vel.segment(0, param_size);
			break;
		}
		default:
			out_vel.segment(param_offset, param_size) = (pose1.segment(param_offset, param_size) - pose0.segment(param_offset, param_size)) / dt;
			break;
		}
	}
}

void cKinTree::PostProcessPose(const Eigen::MatrixXd& joint_mat, Eigen::VectorXd& out_pose)
{
	// mostly to normalize quaternions
	int num_joints = GetNumJoints(joint_mat);
	int root_id = GetRoot(joint_mat);
	int root_offset = GetParamOffset(joint_mat, root_id);
	out_pose.segment(root_offset + gPosDim, gRotDim).normalize();

	for (int j = 1; j < num_joints; ++j)
	{
		eJointType joint_type = GetJointType(joint_mat, j);
		if (joint_type == eJointTypeSpherical)
		{
			int offset = GetParamOffset(joint_mat, j);
			out_pose.segment(offset, gRotDim).normalize();
		}
	}
}

void cKinTree::LerpPoses(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& pose0, const Eigen::VectorXd& pose1, double lerp, Eigen::VectorXd& out_pose)
{
	int num_joints = GetNumJoints(joint_mat);
	int root_id = GetRoot(joint_mat);
	int root_offset = GetParamOffset(joint_mat, root_id);

	out_pose.resize(pose0.size());
	assert(pose0.size() == pose1.size());

	tVector root_pos0 = GetRootPos(joint_mat, pose0);
	tVector root_pos1 = GetRootPos(joint_mat, pose1);
	tVector root_pos_lerp = (1 - lerp) * root_pos0 + lerp * root_pos1;

	tQuaternion root_rot0 = GetRootRot(joint_mat, pose0);
	tQuaternion root_rot1 = GetRootRot(joint_mat, pose1);
	assert(std::abs(root_rot0.norm() - 1) < 0.000001);
	assert(std::abs(root_rot1.norm() - 1) < 0.000001);

	tQuaternion root_rot_lerp = root_rot0.slerp(lerp, root_rot1);
	root_rot_lerp.normalize();

	cKinTree::SetRootPos(joint_mat, root_pos_lerp, out_pose);
	cKinTree::SetRootRot(joint_mat, root_rot_lerp, out_pose);

	for (int j = 1; j < num_joints; ++j)
	{
		eJointType joint_type = GetJointType(joint_mat, j);
		int offset = GetParamOffset(joint_mat, j);
		int size = GetParamSize(joint_mat, j);
		if (joint_type == eJointTypeSpherical)
		{
			tQuaternion rot0 = cMathUtil::VecToQuat(pose0.segment(offset, size));
			tQuaternion rot1 = cMathUtil::VecToQuat(pose1.segment(offset, size));
			assert(std::abs(rot0.norm() - 1) < 0.000001);
			assert(std::abs(rot1.norm() - 1) < 0.000001);
			tQuaternion rot_lerp = rot0.slerp(lerp, rot1);
			out_pose.segment(offset, size) = cMathUtil::QuatToVec(rot_lerp).segment(0, size);
		}
		else
		{
			out_pose.segment(offset, size) = (1 - lerp) * pose0.segment(offset, size) + lerp * pose1.segment(offset, size);
		}
	}
}

 

tMatrix cKinTree::ChildParentTransRoot(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id)
{
	tVector offset = GetRootPos(joint_mat, state);
	tQuaternion rot = GetRootRot(joint_mat, state);

	tMatrix A = BuildAttachTrans(joint_mat, joint_id);
	tMatrix R = cMathUtil::RotateMat(rot);
	tMatrix T = cMathUtil::TranslateMat(offset);

	tMatrix mat = A * T * R;
	return mat;
}

tMatrix cKinTree::ChildParentTransRevolute(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id)
{
	int param_offset = cKinTree::GetParamOffset(joint_mat, joint_id);
	double theta = state(param_offset);

	tMatrix A = BuildAttachTrans(joint_mat, joint_id);
	tMatrix R = cMathUtil::RotateMat(tVector(0, 0, 1, 0), theta);
	
	tMatrix mat = A * R;
	return mat;
}

 

tMatrix cKinTree::ChildParentTransFixed(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id)
{
	tMatrix A = BuildAttachTrans(joint_mat, joint_id);
	tMatrix mat = A;
	return mat;
}

 

tMatrix cKinTree::ChildParentTransSpherical(const Eigen::MatrixXd& joint_mat, const Eigen::VectorXd& state, int joint_id)
{
	int param_offset = cKinTree::GetParamOffset(joint_mat, joint_id);
	int param_size = cKinTree::GetParamSize(joint_mat, joint_id);
	tQuaternion q = cMathUtil::VecToQuat(state.segment(param_offset, param_size));

	tMatrix A = BuildAttachTrans(joint_mat, joint_id);
	tMatrix R = cMathUtil::RotateMat(q);

	tMatrix mat = A * R;
	return mat;
}



void cKinTree::BuildDefaultPoseRoot(const Eigen::MatrixXd& joint_mat, Eigen::VectorXd& out_pose)
{
	int dim = gRootDim;
	out_pose = Eigen::VectorXd::Zero(dim);
	out_pose(gPosDim) = 1;
}

void cKinTree::BuildDefaultPoseRevolute(Eigen::VectorXd& out_pose)
{
	int dim = GetJointParamSize(eJointTypeRevolute);
	out_pose = Eigen::VectorXd::Zero(dim);
}

 
void cKinTree::BuildDefaultPoseFixed(Eigen::VectorXd& out_pose)
{
	int dim = GetJointParamSize(eJointTypeFixed);
	out_pose = Eigen::VectorXd::Zero(dim);
}

void cKinTree::BuildDefaultPoseSpherical(Eigen::VectorXd& out_pose)
{
	int dim = GetJointParamSize(eJointTypeSpherical);
	out_pose = Eigen::VectorXd::Zero(dim);
	out_pose(0) = 1;
}


void cKinTree::BuildDefaultVelRoot(const Eigen::MatrixXd& joint_mat, Eigen::VectorXd& out_pose)
{
	int dim = gRootDim;
	out_pose = Eigen::VectorXd::Zero(dim);
}

void cKinTree::BuildDefaultVelRevolute(Eigen::VectorXd& out_pose)
{
	int dim = GetJointParamSize(eJointTypeRevolute);
	out_pose = Eigen::VectorXd::Zero(dim);
}

 

void cKinTree::BuildDefaultVelFixed(Eigen::VectorXd& out_pose)
{
	int dim = GetJointParamSize(eJointTypeFixed);
	out_pose = Eigen::VectorXd::Zero(dim);
}

void cKinTree::BuildDefaultVelSpherical(Eigen::VectorXd& out_pose)
{
	int dim = GetJointParamSize(eJointTypeSpherical);
	out_pose = Eigen::VectorXd::Zero(dim);
}
