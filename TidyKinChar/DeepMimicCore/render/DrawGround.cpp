#include "DrawGround.h"
#include "DrawUtil.h"
#include "sim/GroundPlane.h"

void cDrawGround::BuildMesh(const cGround* ground, cDrawMesh* out_mesh)
{
	cGround::eClass ground_class = ground->GetGroundClass();
	switch (ground_class)
	{
	case cGround::eClassPlane:
		BuildMeshPlane(ground, out_mesh);
		break;
	default:
		assert(false); // unsupported ground type
		break;
	}
}

void cDrawGround::BuildMeshPlane(const cGround* ground, cDrawMesh* out_mesh)
{
	 
}
