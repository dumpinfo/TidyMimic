#include "SceneBuilder.h"

#include <assert.h>

#include "DrawSceneKinChar.h"
 

void cSceneBuilder::BuildScene(const std::string& scene_name, std::shared_ptr<cScene>& out_scene)
{
 
		out_scene = std::shared_ptr<cSceneKinChar>(new cSceneKinChar());
	 
}

void cSceneBuilder::BuildDrawScene(const std::string& scene_name, std::shared_ptr<cScene>& out_scene)
{ 
		out_scene = std::shared_ptr<cDrawSceneKinChar>(new cDrawSceneKinChar());
	 
}
