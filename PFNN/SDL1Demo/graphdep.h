#ifndef LT_GRAPH_HEAD
#define LT_GRAPH_HEAD


struct LightDirectional {
  
  glm::vec3 target;
  glm::vec3 position;
  
  GLuint fbo;
  GLuint buf;
  GLuint tex;
  
  LightDirectional()
    : target(glm::vec3(0))
    , position(glm::vec3(3000, 3700, 1500))
    , fbo(0)
    , buf(0)
    , tex(0) {
      
    //glGenFramebuffers(1, &fbo);
    //glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    //glDrawBuffer(GL_NONE);
    //glReadBuffer(GL_NONE);
    
    //glGenRenderbuffers(1, &buf);
    //glBindRenderbuffer(GL_RENDERBUFFER, buf);
    //glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, 1024, 1024);
    //glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, buf);  
    
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
 
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
    //glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tex, 0);

    //_ glBindTexture(GL_TEXTURE_2D, 0);
    //_ glBindRenderbuffer(GL_RENDERBUFFER, 0);
    //_ glBindFramebuffer(GL_FRAMEBUFFER, 0);
      
  }
  
  ~LightDirectional() {
    //_ glDeleteBuffers(1, &fbo);
    //_ glDeleteBuffers(1, &buf);
    glDeleteTextures(1, &tex);
  }
  
};

struct Character {
  
  enum { JOINT_NUM = 31 };
  
  GLuint vbo, tbo;
  int ntri, nvtx;
  float phase;
  float strafe_amount;
  float strafe_target;
  float crouched_amount;
  float crouched_target;
  float responsive;
  
  glm::vec3 joint_positions[JOINT_NUM];
  glm::vec3 joint_velocities[JOINT_NUM];
  glm::mat3 joint_rotations[JOINT_NUM];
  
  glm::mat4 joint_anim_xform[JOINT_NUM];
  glm::mat4 joint_rest_xform[JOINT_NUM];
  glm::mat4 joint_mesh_xform[JOINT_NUM];
  glm::mat4 joint_global_rest_xform[JOINT_NUM];
  glm::mat4 joint_global_anim_xform[JOINT_NUM];

  int joint_parents[JOINT_NUM];
  
  enum {
    JOINT_ROOT_L = 1,
    JOINT_HIP_L  = 2,
    JOINT_KNEE_L = 3,  
    JOINT_HEEL_L = 4,
    JOINT_TOE_L  = 5,  
      
    JOINT_ROOT_R = 6,  
    JOINT_HIP_R  = 7,  
    JOINT_KNEE_R = 8,  
    JOINT_HEEL_R = 9,
    JOINT_TOE_R  = 10  
  };
  
  Character()
    : vbo(0)
    , tbo(0)
    , ntri(66918)
    , nvtx(11200)
    , phase(0)
    , strafe_amount(0)
    , strafe_target(0)
    , crouched_amount(0) 
    , crouched_target(0) 
    , responsive(0) {}
    
  ~Character() {
    if (vbo != 0) {  vbo = 0; }
    if (tbo != 0) {  tbo = 0; }
  }
    
  void load(const char* filename_v, const char* filename_t, const char* filename_p, const char* filename_r) {
    
    printf("Read Character '%s %s'\n", filename_v, filename_t);
    
    if (vbo != 0) { vbo = 0; }
    if (tbo != 0) { tbo = 0; }
    
    //_ glGenBuffers(1, &vbo);
    //_ glGenBuffers(1, &tbo);
    
    FILE *f;
    
    f = fopen(filename_v, "rb");
    float *vbo_data = (float*)malloc(sizeof(float) * 15 * nvtx);
    fread(vbo_data, sizeof(float) * 15 * nvtx, 1, f);
    fclose(f);
    
    f = fopen(filename_t, "rb");
    uint32_t *tbo_data = (uint32_t*)malloc(sizeof(uint32_t) * ntri);  
    fread(tbo_data, sizeof(uint32_t) * ntri, 1, f);
    fclose(f);
    
    //glBindBuffer(GL_ARRAY_BUFFER, vbo);
    //glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 15 * nvtx, vbo_data, GL_STATIC_DRAW);
    
    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tbo);
    //glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * ntri, tbo_data, GL_STATIC_DRAW);
    
    free(vbo_data);
    free(tbo_data);
    
    f = fopen(filename_p, "rb");
    float fparents[JOINT_NUM];
    fread(fparents, sizeof(float) * JOINT_NUM, 1, f);
    for (int i = 0; i < JOINT_NUM; i++)
		{ joint_parents[i] = (int)fparents[i]; 
	      printf("the (%2i) th joint parent is (%2i)\n", i, fparents[i]);
	     }
    fclose(f);
	
	
    
    f = fopen(filename_r, "rb");
    fread(glm::value_ptr(joint_rest_xform[0]), sizeof(float) * JOINT_NUM * 4 * 4, 1, f);
    for (int i = 0; i < JOINT_NUM; i++) { joint_rest_xform[i] = glm::transpose(joint_rest_xform[i]); }
    fclose(f);
    
  }
  
  void forward_kinematics() {

    for (int i = 0; i < JOINT_NUM; i++) {
      joint_global_anim_xform[i] = joint_anim_xform[i];
      joint_global_rest_xform[i] = joint_rest_xform[i];
      int j = joint_parents[i];
      while (j != -1) {
        joint_global_anim_xform[i] = joint_anim_xform[j] * joint_global_anim_xform[i];
        joint_global_rest_xform[i] = joint_rest_xform[j] * joint_global_rest_xform[i];
        j = joint_parents[j];
      }
      joint_mesh_xform[i] = joint_global_anim_xform[i] * glm::inverse(joint_global_rest_xform[i]);
    }
    
  }
  
};


struct Heightmap {
  
  float hscale;
  float vscale;
  float offset;
  std::vector<std::vector<float>> data;
  std::vector<glm::vec3> posns;
  std::vector<glm::vec3> norms;
  std::vector<float> aos;
  GLuint vbo;
  GLuint tbo;
  int w,h;
  Heightmap()
    : hscale(3.937007874)
    //, vscale(3.937007874)
    , vscale(3.0)
    , offset(0.0)
    , vbo(0)
    , tbo(0) {}
  
  ~Heightmap() {
    if (vbo != 0) {  vbo = 0; }
    if (tbo != 0) {  tbo = 0; } 
  }
  
  void load(const char* filename, float multiplier) {
    
    vscale = multiplier * vscale;
    
    if (vbo != 0) {  vbo = 0; }
    if (tbo != 0) {  tbo = 0; }
    
    //_ glGenBuffers(1, &vbo);
    //_ glGenBuffers(1, &tbo);
    
    data.clear();
    
    std::ifstream file(filename);
    
    std::string line;
    while (std::getline(file, line)) {
      std::vector<float> row;
      std::istringstream iss(line);
      while (iss) {
        float f;
        iss >> f;
        row.push_back(f);
      }
      data.push_back(row);
    }
    
    w = data.size();
    h = data[0].size();
    
    offset = 0.0;
    for (int x = 0; x < w; x++)
    for (int y = 0; y < h; y++) {
      offset += data[x][y];
    }
    offset /= w * h;
    
    printf("Loaded Heightmap '%s' (%i %i)\n", filename, (int)w, (int)h);
    
    //glm::vec3* posns = (glm::vec3*)malloc(sizeof(glm::vec3) * w * h);
    //glm::vec3* norms = (glm::vec3*)malloc(sizeof(glm::vec3) * w * h);
	posns.resize(w * h);
	norms.resize(w * h);
	aos.resize(w * h); //float* aos   = (float*)malloc(sizeof(float) * w * h);
    
    for (int x = 0; x < w; x++)
    for (int y = 0; y < h; y++) {
      float cx = hscale * x, cy = hscale * y, cw = hscale * w, ch = hscale * h;
      posns[x+y*w] = glm::vec3(cx - cw/2, sample(glm::vec2(cx-cw/2, cy-ch/2)), cy - ch/2);
    }
    
    for (int x = 0; x < w; x++)
    for (int y = 0; y < h; y++) {
      norms[x+y*w] = (x > 0 && x < w-1 && y > 0 && y < h-1) ?
        glm::normalize(glm::mix(
          glm::cross(
            posns[(x+0)+(y+1)*w] - posns[x+y*w],
            posns[(x+1)+(y+0)*w] - posns[x+y*w]),
          glm::cross(
            posns[(x+0)+(y-1)*w] - posns[x+y*w],
            posns[(x-1)+(y+0)*w] - posns[x+y*w]), 0.5)) : glm::vec3(0,1,0);
    }


    char ao_filename[512];
    memcpy(ao_filename, filename, strlen(filename)-4);
    ao_filename[strlen(filename)-4] = '\0';
    strcat(ao_filename, "_ao.txt");
    
    srand(0);

    FILE* ao_file = fopen(ao_filename, "r");
    bool ao_generate = false;
    if (ao_file == NULL || ao_generate) {
      ao_file = fopen(ao_filename, "w");
      //ao_generate = true;
    }
   
    for (int x = 0; x < w; x++)
    for (int y = 0; y < h; y++) {     
        fscanf(ao_file, y == h-1 ? "%f\n" : "%f ", &aos[x+y*w]);
    }
    
    fclose(ao_file);
    
  }
  
  float sample(glm::vec2 pos) {
  
    int w = data.size();
    int h = data[0].size();
    
    pos.x = (pos.x/hscale) + w/2;
    pos.y = (pos.y/hscale) + h/2;
    
    float a0 = fmod(pos.x, 1.0);
    float a1 = fmod(pos.y, 1.0);
    
    int x0 = (int)std::floor(pos.x), x1 = (int)std::ceil(pos.x);
    int y0 = (int)std::floor(pos.y), y1 = (int)std::ceil(pos.y);
    
    x0 = x0 < 0 ? 0 : x0; x0 = x0 >= w ? w-1 : x0;
    x1 = x1 < 0 ? 0 : x1; x1 = x1 >= w ? w-1 : x1;
    y0 = y0 < 0 ? 0 : y0; y0 = y0 >= h ? h-1 : y0;
    y1 = y1 < 0 ? 0 : y1; y1 = y1 >= h ? h-1 : y1;
    
    float s0 = vscale * (data[x0][y0] - offset);
    float s1 = vscale * (data[x1][y0] - offset);
    float s2 = vscale * (data[x0][y1] - offset);
    float s3 = vscale * (data[x1][y1] - offset);
    
    return (s0 * (1-a0) + s1 * a0) * (1-a1) + (s2 * (1-a0) + s3 * a0) * a1;
  
  }
  
};

#endif