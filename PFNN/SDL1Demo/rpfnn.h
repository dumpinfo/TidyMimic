#ifndef RPFNN_HEAD
#define RPFNN_HEAD

//#include <GL/glew.h>
#include <GL/glut.h>
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>

#include <eigen3/Eigen/Dense>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>

#include <sstream>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <stdarg.h>
#include <time.h>

using namespace Eigen;

/* Options */
enum { WINDOW_WIDTH  = 1280, WINDOW_HEIGHT = 1024 };

enum {
  GAMEPAD_BACK = 5,
  GAMEPAD_START = 4,
  GAMEPAD_A = 10,
  GAMEPAD_B = 11,
  GAMEPAD_X = 12,
  GAMEPAD_Y = 13,
  GAMEPAD_TRIGGER_L  = 4,
  GAMEPAD_TRIGGER_R  = 5,
  GAMEPAD_SHOULDER_L = 8,
  GAMEPAD_SHOULDER_R = 9,
  GAMEPAD_STICK_L_HORIZONTAL = 0,
  GAMEPAD_STICK_L_VERTICAL   = 1,
  GAMEPAD_STICK_R_HORIZONTAL = 2,
  GAMEPAD_STICK_R_VERTICAL   = 3
};

struct Options {
  
  bool invert_y;
  
  bool enable_ik;
  
  bool display_debug;
  bool display_debug_heights;
  bool display_debug_joints;
  bool display_debug_pfnn;
  bool display_hud_options;
  bool display_hud_stick;
  bool display_hud_speed;
  
  bool display_areas_jump;
  bool display_areas_walls;
  
  float display_scale;
  
  float extra_direction_smooth;
  float extra_velocity_smooth;
  float extra_strafe_smooth;
  float extra_crouched_smooth;
  float extra_gait_smooth;
  float extra_joint_smooth;
  
  Options()
    : invert_y(false)
    , enable_ik(true)
    , display_debug(true)
    , display_debug_heights(true)
    , display_debug_joints(false)
    , display_debug_pfnn(false)
    , display_hud_options(true)
    , display_hud_stick(true)
    , display_hud_speed(true)
    , display_areas_jump(false)
    , display_areas_walls(false)
    , display_scale(2.0)
    , extra_direction_smooth(0.9)
    , extra_velocity_smooth(0.9)
    , extra_strafe_smooth(0.9)
    , extra_crouched_smooth(0.9)
    , extra_gait_smooth(0.1)
    , extra_joint_smooth(0.5)
    {}
};


struct PFNN {
  
  enum { XDIM = 342, YDIM = 311, HDIM = 512 };
  enum { MODE_CONSTANT, MODE_LINEAR, MODE_CUBIC };

  int mode;
  
  ArrayXf Xmean, Xstd;
  ArrayXf Ymean, Ystd;
  
  std::vector<ArrayXXf> W0, W1, W2;
  std::vector<ArrayXf>  b0, b1, b2;
  
  ArrayXf  Xp, Yp;
  ArrayXf  H0,  H1;
  ArrayXXf W0p, W1p, W2p;
  ArrayXf  b0p, b1p, b2p;
   
  PFNN(int pfnnmode)
    : mode(pfnnmode) { 
    
    Xp = ArrayXf((int)XDIM);
    Yp = ArrayXf((int)YDIM);
    
    H0 = ArrayXf((int)HDIM);
    H1 = ArrayXf((int)HDIM);
    
    W0p = ArrayXXf((int)HDIM, (int)XDIM);
    W1p = ArrayXXf((int)HDIM, (int)HDIM);
    W2p = ArrayXXf((int)YDIM, (int)HDIM);
    
    b0p = ArrayXf((int)HDIM);
    b1p = ArrayXf((int)HDIM);
    b2p = ArrayXf((int)YDIM);
  }
  
  static void load_weights(ArrayXXf &A, int rows, int cols, const char* fmt, ...) {
    va_list valist;
    va_start(valist, fmt);
    char filename[512];
    vsprintf(filename, fmt, valist);
    va_end(valist);

    FILE *f = fopen(filename, "rb");
    if (f == NULL) { fprintf(stderr, "Couldn't load file %s\n", filename); exit(1); }

    A = ArrayXXf(rows, cols);
    for (int x = 0; x < rows; x++)
    for (int y = 0; y < cols; y++) {
      float item = 0.0;
      fread(&item, sizeof(float), 1, f);
      A(x, y) = item;
    }
    fclose(f); 
  }

  static void load_weights(ArrayXf &V, int items, const char* fmt, ...) {
    va_list valist;
    va_start(valist, fmt);
    char filename[512];
    vsprintf(filename, fmt, valist);
    va_end(valist);
    
    FILE *f = fopen(filename, "rb"); 
    if (f == NULL) { fprintf(stderr, "Couldn't load file %s\n", filename); exit(1); }
    
    V = ArrayXf(items);
    for (int i = 0; i < items; i++) {
      float item = 0.0;
      fread(&item, sizeof(float), 1, f);
      V(i) = item;
    }
    fclose(f); 
  }  
  
  void load() {
    
    load_weights(Xmean, XDIM, "./network/pfnn/Xmean.bin");
    load_weights(Xstd,  XDIM, "./network/pfnn/Xstd.bin");
    load_weights(Ymean, YDIM, "./network/pfnn/Ymean.bin");
    load_weights(Ystd,  YDIM, "./network/pfnn/Ystd.bin");
    
    switch (mode) {
      
      case MODE_CONSTANT:
        
        W0.resize(50); W1.resize(50); W2.resize(50);
        b0.resize(50); b1.resize(50); b2.resize(50);
      
        for (int i = 0; i < 50; i++) {            
          load_weights(W0[i], HDIM, XDIM, "./network/pfnn/W0_%03i.bin", i);
          load_weights(W1[i], HDIM, HDIM, "./network/pfnn/W1_%03i.bin", i);
          load_weights(W2[i], YDIM, HDIM, "./network/pfnn/W2_%03i.bin", i);
          load_weights(b0[i], HDIM, "./network/pfnn/b0_%03i.bin", i);
          load_weights(b1[i], HDIM, "./network/pfnn/b1_%03i.bin", i);
          load_weights(b2[i], YDIM, "./network/pfnn/b2_%03i.bin", i);            
        }
        
      break;
      
      case MODE_LINEAR:
      
        W0.resize(10); W1.resize(10); W2.resize(10);
        b0.resize(10); b1.resize(10); b2.resize(10);
      
        for (int i = 0; i < 10; i++) {
          load_weights(W0[i], HDIM, XDIM, "./network/pfnn/W0_%03i.bin", i * 5);
          load_weights(W1[i], HDIM, HDIM, "./network/pfnn/W1_%03i.bin", i * 5);
          load_weights(W2[i], YDIM, HDIM, "./network/pfnn/W2_%03i.bin", i * 5);
          load_weights(b0[i], HDIM, "./network/pfnn/b0_%03i.bin", i * 5);
          load_weights(b1[i], HDIM, "./network/pfnn/b1_%03i.bin", i * 5);
          load_weights(b2[i], YDIM, "./network/pfnn/b2_%03i.bin", i * 5);  
        }
      
      break;
      
      case MODE_CUBIC:
      
        W0.resize(4); W1.resize(4); W2.resize(4);
        b0.resize(4); b1.resize(4); b2.resize(4);
      
        for (int i = 0; i < 4; i++) {
          load_weights(W0[i], HDIM, XDIM, "./network/pfnn/W0_%03i.bin", (int)(i * 12.5));
          load_weights(W1[i], HDIM, HDIM, "./network/pfnn/W1_%03i.bin", (int)(i * 12.5));
          load_weights(W2[i], YDIM, HDIM, "./network/pfnn/W2_%03i.bin", (int)(i * 12.5));
          load_weights(b0[i], HDIM, "./network/pfnn/b0_%03i.bin", (int)(i * 12.5));
          load_weights(b1[i], HDIM, "./network/pfnn/b1_%03i.bin", (int)(i * 12.5));
          load_weights(b2[i], YDIM, "./network/pfnn/b2_%03i.bin", (int)(i * 12.5));  
        }
        
      break;
    }
    
  }
  
  static void ELU(ArrayXf &x) { x = x.max(0) + x.min(0).exp() - 1; }

  static void linear(ArrayXf  &o, const ArrayXf  &y0, const ArrayXf  &y1, float mu) { o = (1.0f-mu) * y0 + (mu) * y1; }
  static void linear(ArrayXXf &o, const ArrayXXf &y0, const ArrayXXf &y1, float mu) { o = (1.0f-mu) * y0 + (mu) * y1; }
  
  static void cubic(ArrayXf  &o, const ArrayXf &y0, const ArrayXf &y1, const ArrayXf &y2, const ArrayXf &y3, float mu) {
    o = (
      (-0.5*y0+1.5*y1-1.5*y2+0.5*y3)*mu*mu*mu + 
      (y0-2.5*y1+2.0*y2-0.5*y3)*mu*mu + 
      (-0.5*y0+0.5*y2)*mu + 
      (y1));
  }
  
  static void cubic(ArrayXXf &o, const ArrayXXf &y0, const ArrayXXf &y1, const ArrayXXf &y2, const ArrayXXf &y3, float mu) {
    o = (
      (-0.5*y0+1.5*y1-1.5*y2+0.5*y3)*mu*mu*mu + 
      (y0-2.5*y1+2.0*y2-0.5*y3)*mu*mu + 
      (-0.5*y0+0.5*y2)*mu + 
      (y1));
  }

  void predict(float P) {
    
    float pamount;
    int pindex_0, pindex_1, pindex_2, pindex_3;
    
    Xp = (Xp - Xmean) / Xstd;
    
    switch (mode) {
      
      case MODE_CONSTANT:
        pindex_1 = (int)((P / (2*M_PI)) * 50);
        H0 = (W0[pindex_1].matrix() * Xp.matrix()).array() + b0[pindex_1]; ELU(H0);
        H1 = (W1[pindex_1].matrix() * H0.matrix()).array() + b1[pindex_1]; ELU(H1);
        Yp = (W2[pindex_1].matrix() * H1.matrix()).array() + b2[pindex_1];
      break;
      
      case MODE_LINEAR:
        pamount = fmod((P / (2*M_PI)) * 10, 1.0);
        pindex_1 = (int)((P / (2*M_PI)) * 10);
        pindex_2 = ((pindex_1+1) % 10);
        linear(W0p, W0[pindex_1], W0[pindex_2], pamount);
        linear(W1p, W1[pindex_1], W1[pindex_2], pamount);
        linear(W2p, W2[pindex_1], W2[pindex_2], pamount);
        linear(b0p, b0[pindex_1], b0[pindex_2], pamount);
        linear(b1p, b1[pindex_1], b1[pindex_2], pamount);
        linear(b2p, b2[pindex_1], b2[pindex_2], pamount);
        H0 = (W0p.matrix() * Xp.matrix()).array() + b0p; ELU(H0);
        H1 = (W1p.matrix() * H0.matrix()).array() + b1p; ELU(H1);
        Yp = (W2p.matrix() * H1.matrix()).array() + b2p;
      break;
      
      case MODE_CUBIC:
        pamount = fmod((P / (2*M_PI)) * 4, 1.0);
        pindex_1 = (int)((P / (2*M_PI)) * 4);
        pindex_0 = ((pindex_1+3) % 4);
        pindex_2 = ((pindex_1+1) % 4);
        pindex_3 = ((pindex_1+2) % 4);
        cubic(W0p, W0[pindex_0], W0[pindex_1], W0[pindex_2], W0[pindex_3], pamount);
        cubic(W1p, W1[pindex_0], W1[pindex_1], W1[pindex_2], W1[pindex_3], pamount);
        cubic(W2p, W2[pindex_0], W2[pindex_1], W2[pindex_2], W2[pindex_3], pamount);
        cubic(b0p, b0[pindex_0], b0[pindex_1], b0[pindex_2], b0[pindex_3], pamount);
        cubic(b1p, b1[pindex_0], b1[pindex_1], b1[pindex_2], b1[pindex_3], pamount);
        cubic(b2p, b2[pindex_0], b2[pindex_1], b2[pindex_2], b2[pindex_3], pamount);
        H0 = (W0p.matrix() * Xp.matrix()).array() + b0p; ELU(H0);
        H1 = (W1p.matrix() * H0.matrix()).array() + b1p; ELU(H1);
        Yp = (W2p.matrix() * H1.matrix()).array() + b2p;
      break;
      
      default:
      break;
    }
    
    Yp = (Yp * Ystd) + Ymean;

  }
  
};

struct CameraOrbit {
  
  glm::vec3 target;
  float pitch, yaw;
  float distance;
  
  CameraOrbit()
    : target(glm::vec3(0))
    , pitch(M_PI/6)
    , yaw(0)
    , distance(300) {}
   
  glm::vec3 position() {
    glm::vec3 posn = glm::mat3(glm::rotate(yaw, glm::vec3(0,1,0))) * glm::vec3(distance, 0, 0);
    glm::vec3 axis = glm::normalize(glm::cross(posn, glm::vec3(0,1,0)));
    return glm::mat3(glm::rotate(pitch, axis)) * posn + target;
  }
 
  glm::vec3 direction() {
    return glm::normalize(target - position());
  }
  
  glm::mat4 view_matrix() {
    return glm::lookAt(position(), target, glm::vec3(0,1,0));
  }
  
  glm::mat4 proj_matrix() {
    return glm::perspective(45.0f, (float)WINDOW_WIDTH/(float)WINDOW_HEIGHT, 10.0f, 10000.0f);
  }
    
};

struct Trajectory {
  
  enum { LENGTH = 120 };
  
  float width;

  glm::vec3 positions[LENGTH];
  glm::vec3 directions[LENGTH];
  glm::mat3 rotations[LENGTH];
  float heights[LENGTH];
  
  float gait_stand[LENGTH];
  float gait_walk[LENGTH];
  float gait_jog[LENGTH];
  float gait_crouch[LENGTH];
  float gait_jump[LENGTH];
  float gait_bump[LENGTH];
  
  glm::vec3 target_dir, target_vel;
  
  Trajectory()
    : width(25)
    , target_dir(glm::vec3(0,0,1))
    , target_vel(glm::vec3(0)) {}
  
};

struct IK {
  
  enum { HL = 0, HR = 1, TL = 2, TR = 3 };
  
  float lock[4];
  glm::vec3 position[4]; 
  float height[4];
  float fade;
  float threshold;
  float smoothness;
  float heel_height;
  float toe_height;
  
  IK()
    : fade(0.075)
    , threshold(0.8)
    , smoothness(0.5)
    , heel_height(5.0)
    , toe_height(4.0) {
    memset(lock, 4, sizeof(float));
    memset(position, 4, sizeof(glm::vec3));
    memset(height, 4, sizeof(float));
  }
  
  void two_joint(
    glm::vec3 a, glm::vec3 b, 
    glm::vec3 c, glm::vec3 t, float eps, 
    glm::mat4& a_pR, glm::mat4& b_pR,
    glm::mat4& a_gR, glm::mat4& b_gR,
    glm::mat4& a_lR, glm::mat4& b_lR) {
    
    float lc = glm::length(b - a);
    float la = glm::length(b - c);
    float lt = glm::clamp(glm::length(t - a), eps, lc + la - eps);
    
    if (glm::length(c - t) < eps) { return; }

    float ac_ab_0 = acosf(glm::clamp(glm::dot(glm::normalize(c - a), glm::normalize(b - a)), -1.0f, 1.0f));
    float ba_bc_0 = acosf(glm::clamp(glm::dot(glm::normalize(a - b), glm::normalize(c - b)), -1.0f, 1.0f));
    float ac_at_0 = acosf(glm::clamp(glm::dot(glm::normalize(c - a), glm::normalize(t - a)), -1.0f, 1.0f));
    
    float ac_ab_1 = acosf(glm::clamp((la*la - lc*lc - lt*lt) / (-2*lc*lt), -1.0f, 1.0f));
    float ba_bc_1 = acosf(glm::clamp((lt*lt - lc*lc - la*la) / (-2*lc*la), -1.0f, 1.0f));
    
    glm::vec3 a0 = glm::normalize(glm::cross(b - a, c - a));
    glm::vec3 a1 = glm::normalize(glm::cross(t - a, c - a));
    
    glm::mat3 r0 = glm::mat3(glm::rotate(ac_ab_1 - ac_ab_0, -a0));
    glm::mat3 r1 = glm::mat3(glm::rotate(ba_bc_1 - ba_bc_0, -a0));
    glm::mat3 r2 = glm::mat3(glm::rotate(ac_at_0,           -a1));
    
    glm::mat3 a_lRR = glm::inverse(glm::mat3(a_pR)) * (r2 * r0 * glm::mat3(a_gR)); 
    glm::mat3 b_lRR = glm::inverse(glm::mat3(b_pR)) * (r1 * glm::mat3(b_gR)); 
    
    for (int x = 0; x < 3; x++)
    for (int y = 0; y < 3; y++) {
      a_lR[x][y] = a_lRR[x][y];
      b_lR[x][y] = b_lRR[x][y];
    }
    
  }
  
};

struct Areas {
  
  std::vector<glm::vec3> crouch_pos;
  std::vector<glm::vec2> crouch_size;
  static constexpr float CROUCH_WAVE = 50;
  
  std::vector<glm::vec3> jump_pos;
  std::vector<float> jump_size;
  std::vector<float> jump_falloff;
  
  std::vector<glm::vec2> wall_start;
  std::vector<glm::vec2> wall_stop;
  std::vector<float> wall_width;
  
  void clear() {
    crouch_pos.clear();
    crouch_size.clear();
    jump_pos.clear();
    jump_size.clear();
    jump_falloff.clear();
    wall_start.clear();
    wall_stop.clear();
    wall_width.clear();
  }
  
  void add_wall(glm::vec2 start, glm::vec2 stop, float width) {
    wall_start.push_back(start);
    wall_stop.push_back(stop);
    wall_width.push_back(width);
  }
  
  void add_crouch(glm::vec3 pos, glm::vec2 size) {
    crouch_pos.push_back(pos);
    crouch_size.push_back(size);
  }
  
  void add_jump(glm::vec3 pos, float size, float falloff) {
    jump_pos.push_back(pos);
    jump_size.push_back(size);
    jump_falloff.push_back(falloff);
  }
  
  int num_walls() { return wall_start.size(); }
  int num_crouches() { return crouch_pos.size(); }
  int num_jumps() { return jump_pos.size(); }
  
};

/* Helper Functions */

static glm::vec3 mix_directions(glm::vec3 x, glm::vec3 y, float a) {
  glm::quat x_q = glm::angleAxis(atan2f(x.x, x.z), glm::vec3(0,1,0));
  glm::quat y_q = glm::angleAxis(atan2f(y.x, y.z), glm::vec3(0,1,0));
  glm::quat z_q = glm::slerp(x_q, y_q, a);
  return z_q * glm::vec3(0,0,1);
}

static glm::mat4 mix_transforms(glm::mat4 x, glm::mat4 y, float a) {
  glm::mat4 out = glm::mat4(glm::slerp(glm::quat(x), glm::quat(y), a));
  out[3] = mix(x[3], y[3], a);
  return out;
}

static glm::quat quat_exp(glm::vec3 l) {
  float w = glm::length(l);
  glm::quat q = w < 0.01 ? glm::quat(1,0,0,0) : glm::quat(
    cosf(w),
    l.x * (sinf(w) / w),
    l.y * (sinf(w) / w),
    l.z * (sinf(w) / w));
  return q / sqrtf(q.w*q.w + q.x*q.x + q.y*q.y + q.z*q.z); 
}

static glm::vec2 segment_nearest(glm::vec2 v, glm::vec2 w, glm::vec2 p) {
  float l2 = glm::dot(v - w, v - w);
  if (l2 == 0.0) return v;
  float t = glm::clamp(glm::dot(p - v, w - v) / l2, 0.0f, 1.0f);
  return v + t * (w - v);
}

#endif