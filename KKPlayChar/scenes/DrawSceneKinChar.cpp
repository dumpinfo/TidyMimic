#include "DrawSceneKinChar.h"
#include "render/DrawUtil.h"
#include "render/DrawCharacter.h"
#include "util/FileUtil.h"

const double gLinkWidth = 0.025f;
const tVector gLineColor = tVector(0, 0, 0, 1);
const tVector gFilLColor = tVector(0.6f, 0.65f, 0.675f, 1);
const tVector gCamFocus0 = tVector(0, 0.75, 0, 0);
const double gGroundHeight = 0;

//=============================================================
// camera attributes
const tVector gCameraPosition = tVector(0, 0, 30, 0);
const tVector gCameraFocus = tVector(gCameraPosition[0], gCameraPosition[1], 0.0, 0.0);
const tVector gCameraUp = tVector(0, 1, 0, 0);

const double gViewWidth = 4.5;
const double gViewHeight = gViewWidth * 9.0 / 16.0;
const double gViewNearZ = 2;
const double gViewFarZ = 500;

//const tVector gLineColor = tVector(0, 0, 0, 1);
const tVector gVisOffset = tVector(0, 0, 1, 0); // offset for visualization elements

cDrawSceneKinChar::cDrawSceneKinChar() //: cDrawScene()
{
	mCamTrackMode = eCamTrackModeXZ;
	mDrawInfo = true;
}

cDrawSceneKinChar::~cDrawSceneKinChar()
{

}

void cDrawSceneKinChar::Draw()
{
	SetupView();
	ClearFrame();
	DrawScene();
	RestoreView();
}

void cDrawSceneKinChar::SetupView()
{
	cDrawUtil::PushMatrixProj();
	mCamera.SetupGLProj();

	cDrawUtil::PushMatrixView();
	mCamera.SetupGLView();
}


void cDrawSceneKinChar::ClearFrame()
{
	double depth = 1;
	tVector col = tVector(0.97, 0.97, 1, 0); //GetBkgColor();
	cDrawUtil::ClearDepth(depth);
	cDrawUtil::ClearColor(col);
}

void cDrawSceneKinChar::RestoreView()
{
	cDrawUtil::PopMatrixProj();
	cDrawUtil::PopMatrixView();
}

void cDrawSceneKinChar::DrawObjsMainScene()
{
	const double roughness = 0.4;
	const double enable_tex = 0;
	//KHAdd mShaderMesh->SetUniform4(mMeshMaterialDataHandle, tVector(roughness, enable_tex, 0, 0));
	//KKNULL DrawObjs();
}

void cDrawSceneKinChar::DrawMiscMainScene()
{
	const double roughness = 0.4;
	const double enable_tex = 0;
	//KHAdd mShaderMesh->SetUniform4(mMeshMaterialDataHandle, tVector(roughness, enable_tex, 0, 0));
	//KKNULL DrawMisc();
}

void cDrawSceneKinChar::MouseClick(int button, int state, double x, double y)
{
	//cScene::MouseClick(button, state, x, y);
	mCamera.MouseClick(button, state, x, y);
}

void cDrawSceneKinChar::MouseMove(double x, double y)
{
	//cScene::MouseMove(x, y);
	mCamera.MouseMove(x, y);
}




void cDrawSceneKinChar::DrawScene()
{
	 
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	//DrawGroundMainScene();
	DrawCharacterMainScene();
	DrawObjsMainScene();
	DrawMiscMainScene();

	//KHAdd mShaderMesh->Unbind();

	if (mDrawInfo)
	{
		// info is drawn in screen space
		cDrawUtil::PushMatrixProj();
		cDrawUtil::LoadIdentityProj();

		cDrawUtil::PushMatrixView();
		cDrawUtil::LoadIdentityView();

	 	cDrawUtil::PopMatrixProj();
		cDrawUtil::PopMatrixView();
	}
}

void cDrawSceneKinChar::DrawCharacterMainScene()
{
	const double roughness = 0.4;
	const double enable_tex = 0;
	//KHAdd  mShaderMesh->SetUniform4(mMeshMaterialDataHandle, tVector(roughness, enable_tex, 0, 0));
	DrawCharacters();
}

void cDrawSceneKinChar::Init(char*motionfile)//)//;
{
	 
	mScene = std::shared_ptr<cSceneKinChar>(new cSceneKinChar());
	mScene->mCharParams.mCharFile   = "data/characters/humanoid3d.txt";
	mScene->mCharParams.mMotionFile = "data/motions/TestAA.json";
	mScene->mCharParams.mStateFile = "";// state_file;
	mScene->mCharParams.mOrigin[0] = 0;// init_pos_xs;
	
	SetupScene(mScene);
	//cDrawScene::Init();
	//InitTimers();
	mTimer.Init(mTimerParams);
	//ResetParams();
	mTimer.Reset();
	mSampleCount = 0;
	//cScene::Init();
	//InitCamera();
	//-------------------------------------------------
	cCamera::eProj proj = cCamera::eProjPerspective;
	mCamera = cCamera(proj, gCameraPosition, gCameraFocus, gCameraUp,
		gViewWidth, gViewHeight, gViewNearZ, gViewFarZ);
	//-------------------------------------------------
	//ResetCamera();
	tVector target_pos = GetDefaultCamFocus();

	eCamTrackMode mode = eCamTrackModeXZ; //GetCamTrackMode();
	if (mode == eCamTrackModeXZ
		|| mode == eCamTrackModeY)
	{
		target_pos = GetCamTrackPos();
	}
	 

	tVector cam_pos = GetDefaultCamFocus();
	cam_pos[0] = target_pos[0];
	cam_pos[1] = target_pos[1];

	mCamera.TranslateFocus(cam_pos);
	//-------------------------------------------------
	//InitRenderResources();
	float shadow_size = 40.f;
	float shadow_near_z = 1.f;
	float shadow_far_z = 60.f;
	int shadow_res = 2048;
}

void cDrawSceneKinChar::Reset()
{
	//cDrawScene::Reset();
	//cScene::Reset();
	mTimer.Reset();
	//--------------------------------------------
	tVector target_pos = GetDefaultCamFocus();

	eCamTrackMode mode = eCamTrackModeXZ; //GetCamTrackMode();
	if (mode == eCamTrackModeXZ
		|| mode == eCamTrackModeY)
	{
		target_pos = GetCamTrackPos();
	}
	 

	tVector cam_pos = GetDefaultCamFocus();
	cam_pos[0] = target_pos[0];
	cam_pos[1] = target_pos[1];

	mCamera.TranslateFocus(cam_pos);
	//--------------------------------------------
	mScene->Reset();
}

void cDrawSceneKinChar::Clear()
{
	//cDrawScene::Clear();//KKdebug
	//KKM mScene->Clear();
}

void cDrawSceneKinChar::ParseDec(const std::shared_ptr<cArgParser>& parser)
{
	mArgParser = parser;

	std::string timer_type_str = "";
	mArgParser->ParseString("timer_type", timer_type_str);
	mTimerParams.mType = cTimer::ParseTypeStr(timer_type_str);
	mArgParser->ParseDouble("time_lim_min", mTimerParams.mTimeMin);
	mArgParser->ParseDouble("time_lim_max", mTimerParams.mTimeMax);
	mArgParser->ParseDouble("time_lim_exp", mTimerParams.mTimeExp);
}

void cDrawSceneKinChar::Update(double time_elapsed)
{
	 mTimer.Update(time_elapsed);

	UpdateScene(time_elapsed);
	//UpdateCamera();
	eCamTrackMode mode = eCamTrackModeXZ; //GetCamTrackMode();
	if (mode == eCamTrackModeXZ
		|| mode == eCamTrackModeY
		|| mode == eCamTrackModeXYZ)
	{
		 //------------------------------------------------------------
		eCamTrackMode mode = eCamTrackModeXZ; //GetCamTrackMode();
		  if (mode == eCamTrackModeXZ
			|| mode == eCamTrackModeY)
		{
			tVector track_pos = GetCamTrackPos();
			tVector cam_focus = mCamera.GetFocus();

			double cam_w = mCamera.GetWidth();
			double cam_h = mCamera.GetHeight();
			const double y_pad = std::min(0.5, 0.8 * 0.5 * cam_h);
			const double x_pad = std::min(0.5, 0.8 * 0.5 * cam_w);

			if (mode == eCamTrackModeXZ)
			{
				cam_focus[0] = track_pos[0];
				cam_focus[2] = track_pos[2];

			}
			mCamera.TranslateFocus(cam_focus);
		}
		//-------------------------------------------------------------
	}
	 
}

std::string cDrawSceneKinChar::GetName() const
{
	return mScene->GetName();
}

 

void cDrawSceneKinChar::SetupScene(std::shared_ptr<cSceneKinChar>& out_scene)
{
	out_scene->Init();
}

void cDrawSceneKinChar::UpdateScene(double time_elapsed)
{
	mScene->Update(time_elapsed);
}

tVector cDrawSceneKinChar::GetCamTrackPos() const
{
	return mScene->GetCharPos();
}

 

tVector cDrawSceneKinChar::GetDefaultCamFocus() const
{
	return gCamFocus0;
}

void cDrawSceneKinChar::DrawGround() const
{
	tVector ground_col = tVector::Ones(); ///GetGroundColor();
	cDrawUtil::SetColor(ground_col);
	DrawGround3D();
}

void cDrawSceneKinChar::DrawGround3D() const
{
	 
}


void cDrawSceneKinChar::DrawCharacters() const
{
	const auto& character = mScene->GetCharacter();
	cDrawCharacter::Draw(*character, gLinkWidth, gFilLColor, gLineColor);
}