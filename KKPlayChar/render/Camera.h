#pragma once

#include "util/MathUtil.h"

class cCamera
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	enum eProj
	{
		eProjPerspective,
		eProjOrtho,
		eProjMax
	};

	cCamera();
	cCamera(eProj proj, const tVector& pos, const tVector& focus, const tVector& up,
			double w, double h, double near_z, double far_z);
	virtual ~cCamera();

	virtual const tVector& GetPosition() const;
	virtual tVector GetFocus() const;
	virtual const tVector& GetUp() const;
	virtual tVector GetViewDir() const;
	virtual double GetWidth() const;
	virtual double GetHeight() const;
	 

	 virtual void SetFocus(const tVector& focus);
	 virtual void Resize(double w, double h);
	 virtual void TranslateFocus(const tVector& focus);
	  
	 virtual tMatrix BuildViewWorldMatrix() const;
	virtual tMatrix BuildWorldViewMatrix() const;
	virtual tMatrix BuildProjMatrix() const;

	 
	virtual void SetupGLView() const;
	virtual void SetupGLProj() const;

	 
	virtual void MouseClick(int button, int state, double x, double y);
	virtual void MouseMove(double x, double y);

protected:
	eProj mProj;

	tVector mPosition;
	tVector mFocusDelta;
	tVector mUp;

	double mWidth;
	double mAspectRatio;
	double mNearZ;
	double mFarZ;

	bool mMouseDown;
	tVector mMousePos;

	virtual double CalcFocalLen() const;
	 virtual tMatrix BuildProjMatrixProj() const;

	virtual void Zoom(double zoom);
};