#include "DrawSceneKinChar.h"
#include "render/DrawUtil.h"
#include "render/DrawCharacter.h"
#include "util/FileUtil.h"

const double gLinkWidth = 0.025f;
const tVector gLineColor = tVector(0, 0, 0, 1);
const tVector gFilLColor = tVector(0.6f, 0.65f, 0.675f, 1);
const tVector gCamFocus0 = tVector(0, 0.75, 0, 0);
const double gGroundHeight = 0;

//=============================================================
// camera attributes
const tVector gCameraPosition = tVector(0, 0, 30, 0);
const tVector gCameraFocus = tVector(gCameraPosition[0], gCameraPosition[1], 0.0, 0.0);
const tVector gCameraUp = tVector(0, 1, 0, 0);

const double gViewWidth = 4.5;
const double gViewHeight = gViewWidth * 9.0 / 16.0;
const double gViewNearZ = 2;
const double gViewFarZ = 500;

//const tVector gLineColor = tVector(0, 0, 0, 1);
const tVector gVisOffset = tVector(0, 0, 1, 0); // offset for visualization elements

cDrawSceneKinChar::cDrawSceneKinChar() //: cDrawScene()
{
	mCamTrackMode = eCamTrackModeXZ;
	mDrawInfo = true;
}

cDrawSceneKinChar::~cDrawSceneKinChar()
{

}

void cDrawSceneKinChar::Draw()
{
	SetupView();
	ClearFrame();
	DrawScene();
	RestoreView();
}

void cDrawSceneKinChar::SetupView()
{
	cDrawUtil::PushMatrixProj();
	mCamera.SetupGLProj();

	cDrawUtil::PushMatrixView();
	mCamera.SetupGLView();
}


void cDrawSceneKinChar::ClearFrame()
{
	double depth = 1;
	tVector col = tVector(0.97, 0.97, 1, 0); //GetBkgColor();
	cDrawUtil::ClearDepth(depth);
	cDrawUtil::ClearColor(col);
}

void cDrawSceneKinChar::RestoreView()
{
	cDrawUtil::PopMatrixProj();
	cDrawUtil::PopMatrixView();
}

void cDrawSceneKinChar::DrawObjsMainScene()
{
	const double roughness = 0.4;
	const double enable_tex = 0;
	//KHAdd mShaderMesh->SetUniform4(mMeshMaterialDataHandle, tVector(roughness, enable_tex, 0, 0));
	//KKNULL DrawObjs();
}

void cDrawSceneKinChar::DrawMiscMainScene()
{
	const double roughness = 0.4;
	const double enable_tex = 0;
	//KHAdd mShaderMesh->SetUniform4(mMeshMaterialDataHandle, tVector(roughness, enable_tex, 0, 0));
	//KKNULL DrawMisc();
}

void cDrawSceneKinChar::MouseClick(int button, int state, double x, double y)
{
	cScene::MouseClick(button, state, x, y);
	mCamera.MouseClick(button, state, x, y);
}

void cDrawSceneKinChar::MouseMove(double x, double y)
{
	cScene::MouseMove(x, y);
	mCamera.MouseMove(x, y);
}




void cDrawSceneKinChar::DrawScene()
{
	//DoShadowPass();
	//KHAdd mShaderMesh->Bind();
	//SetupMeshShader();

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	//DrawGroundMainScene();
	DrawCharacterMainScene();
	DrawObjsMainScene();
	DrawMiscMainScene();

	//KHAdd mShaderMesh->Unbind();

	if (mDrawInfo)
	{
		// info is drawn in screen space
		cDrawUtil::PushMatrixProj();
		cDrawUtil::LoadIdentityProj();

		cDrawUtil::PushMatrixView();
		cDrawUtil::LoadIdentityView();

		//DrawInfo();

		cDrawUtil::PopMatrixProj();
		cDrawUtil::PopMatrixView();
	}
}

void cDrawSceneKinChar::DrawCharacterMainScene()
{
	const double roughness = 0.4;
	const double enable_tex = 0;
	//KHAdd  mShaderMesh->SetUniform4(mMeshMaterialDataHandle, tVector(roughness, enable_tex, 0, 0));
	DrawCharacters();
}

void cDrawSceneKinChar::Init(char*motionfile)//)//;
{
	//mScene->mCharParams =new 
	BuildScene(mScene);
	mScene->mCharParams.mCharFile   = "data/characters/humanoid3d.txt";
	mScene->mCharParams.mMotionFile = "data/motions/TestAA.json";
	mScene->mCharParams.mStateFile = "";// state_file;
	mScene->mCharParams.mOrigin[0] = 0;// init_pos_xs;
	
	SetupScene(mScene);
	//cDrawScene::Init();
	cScene::Init();
	//InitCamera();
	//-------------------------------------------------
	cCamera::eProj proj = cCamera::eProjPerspective;
	mCamera = cCamera(proj, gCameraPosition, gCameraFocus, gCameraUp,
		gViewWidth, gViewHeight, gViewNearZ, gViewFarZ);
	//-------------------------------------------------
	//ResetCamera();
	tVector target_pos = GetDefaultCamFocus();

	eCamTrackMode mode = eCamTrackModeXZ; //GetCamTrackMode();
	if (mode == eCamTrackModeXZ
		|| mode == eCamTrackModeY)
	{
		target_pos = GetCamTrackPos();
	}
	else if (mode == eCamTrackModeStill)
	{
		target_pos = GetCamStillPos();
	}

	tVector cam_pos = GetDefaultCamFocus();
	cam_pos[0] = target_pos[0];
	cam_pos[1] = target_pos[1];

	mCamera.TranslateFocus(cam_pos);
	//-------------------------------------------------
	//InitRenderResources();
	float shadow_size = 40.f;
	float shadow_near_z = 1.f;
	float shadow_far_z = 60.f;
	int shadow_res = 2048;
}

void cDrawSceneKinChar::Reset()
{
	//cDrawScene::Reset();
	cScene::Reset();
	//--------------------------------------------
	tVector target_pos = GetDefaultCamFocus();

	eCamTrackMode mode = eCamTrackModeXZ; //GetCamTrackMode();
	if (mode == eCamTrackModeXZ
		|| mode == eCamTrackModeY)
	{
		target_pos = GetCamTrackPos();
	}
	else if (mode == eCamTrackModeStill)
	{
		target_pos = GetCamStillPos();
	}

	tVector cam_pos = GetDefaultCamFocus();
	cam_pos[0] = target_pos[0];
	cam_pos[1] = target_pos[1];

	mCamera.TranslateFocus(cam_pos);
	//--------------------------------------------
	mScene->Reset();
}

void cDrawSceneKinChar::Clear()
{
	//cDrawScene::Clear();//KKdebug
	mScene->Clear();
}

void cDrawSceneKinChar::Update(double time_elapsed)
{
	//cDrawScene::Update(time_elapsed);
	cScene::Update(time_elapsed);

	UpdateScene(time_elapsed);
	//UpdateCamera();
	eCamTrackMode mode = eCamTrackModeXZ; //GetCamTrackMode();
	if (mode == eCamTrackModeXZ
		|| mode == eCamTrackModeY
		|| mode == eCamTrackModeXYZ)
	{
		//UpdateCameraTracking();
		//------------------------------------------------------------
		eCamTrackMode mode = eCamTrackModeXZ; //GetCamTrackMode();
		if (mode == eCamTrackModeXYZ)
		{
			tVector track_pos = GetCamTrackPos();
			tVector focus_pos = mCamera.GetFocus();
			tVector cam_pos = mCamera.GetPosition();
			mCamera.TranslateFocus(track_pos);
		}
		else if (mode == eCamTrackModeXZ
			|| mode == eCamTrackModeY)
		{
			tVector track_pos = GetCamTrackPos();
			tVector cam_focus = mCamera.GetFocus();

			double cam_w = mCamera.GetWidth();
			double cam_h = mCamera.GetHeight();
			const double y_pad = std::min(0.5, 0.8 * 0.5 * cam_h);
			const double x_pad = std::min(0.5, 0.8 * 0.5 * cam_w);

			if (mode == eCamTrackModeXZ)
			{
				cam_focus[0] = track_pos[0];
				cam_focus[2] = track_pos[2];

				if (std::abs(track_pos[1] - cam_focus[1]) > ((0.5 * cam_h) - y_pad))
				{
					const double blend = 0.5;
					double tar_y = track_pos[1] + ((0.5 * cam_h) - y_pad);
					cam_focus[1] = (1 - blend) * cam_focus[1] + blend * tar_y;
				}
			}
			else
			{
				cam_focus[1] = track_pos[1];

				const double blend = 0.5;
				double tar_delta = track_pos[0] - cam_focus[0];
				if (std::abs(tar_delta) > ((0.5 * cam_w) - x_pad))
				{
					double tar_x = track_pos[0] + cMathUtil::Sign(tar_delta) * ((0.95 * cam_w) - x_pad);
					cam_focus[0] = (1 - blend) * cam_focus[0] + blend * tar_x;
				}
			}

			mCamera.TranslateFocus(cam_focus);
		}
		//-------------------------------------------------------------
	}
	else if (mode == eCamTrackModeStill)
	{
		//UpdateCameraStill();
		tVector track_pos = GetCamTrackPos();
		tVector cam_focus = mCamera.GetFocus();

		double cam_w = mCamera.GetWidth();
		double cam_h = mCamera.GetHeight();
		//==============================================================
		const tVector axis = tVector(1, 0, 0, 0);
		double dist = 0.5 * mCamera.GetWidth();

		tVector view_delta = mCamera.GetFocus() - mCamera.GetPosition();
		tVector view_dir = view_delta.normalized();
		double len = view_delta.norm() - mCamera.GetNearZ();
		view_delta = view_dir * len;

		view_delta[1] = 0;
		view_delta[3] = 0;
		len = view_delta.norm();

		if (len > 0)
		{
			view_delta /= len;
			double dot = view_delta.dot(axis);
			double lerp = std::abs(dot);
			lerp = std::pow(lerp, 4);
			lerp = 1 - lerp;
			dist = lerp * dist + (1 - lerp) * 0.5 * len;
		}
		
		double cam_still_snap_dist = dist;// GetCamStillSnapDistX();
		//==============================================================

		const double pad_x = std::min(0.5, 0.4 * cam_still_snap_dist);
		const double pad_y = std::min(0.0, 0.2 * cam_h);

		double avg_h = 0;
		bool snap_x = std::abs(track_pos[0] - cam_focus[0]) > cam_still_snap_dist - pad_x;
		bool snap_y = (track_pos[1] - cam_focus[1]) > 0.5 * cam_h - pad_y
			|| (track_pos[1] - cam_focus[1]) < -(0.5 * cam_h - pad_y);

		if (snap_x || snap_y)
		{
			tVector snap_pos = GetCamStillPos();
			cam_focus[0] = snap_pos[0];
			cam_focus[1] = snap_pos[1];

			tVector pos_delta = track_pos - snap_pos;
			if (std::abs(pos_delta[0]) > cam_still_snap_dist - pad_x)
			{
				cam_focus[0] += pos_delta[0];
			}

			if ((pos_delta[1]) > 0.5 * cam_h - pad_y
				|| (pos_delta[1]) < -(0.5 * cam_h - pad_y))
			{
				cam_focus[1] += pos_delta[1];
			}

			if (snap_x)
			{
				cam_focus[0] += cam_still_snap_dist - pad_x;
			}
		}

		mCamera.TranslateFocus(cam_focus);
	}
}

std::string cDrawSceneKinChar::GetName() const
{
	return mScene->GetName();
}

void cDrawSceneKinChar::BuildScene(std::shared_ptr<cSceneKinChar>& out_scene) const
{
	out_scene = std::shared_ptr<cSceneKinChar>(new cSceneKinChar());
}

void cDrawSceneKinChar::SetupScene(std::shared_ptr<cSceneKinChar>& out_scene)
{
	out_scene->Init();
}

void cDrawSceneKinChar::UpdateScene(double time_elapsed)
{
	mScene->Update(time_elapsed);
}

tVector cDrawSceneKinChar::GetCamTrackPos() const
{
	return mScene->GetCharPos();
}

tVector cDrawSceneKinChar::GetCamStillPos() const
{
	return mScene->GetCharPos();
}

tVector cDrawSceneKinChar::GetDefaultCamFocus() const
{
	return gCamFocus0;
}

void cDrawSceneKinChar::DrawGround() const
{
	tVector ground_col = tVector::Ones(); ///GetGroundColor();
	cDrawUtil::SetColor(ground_col);
	DrawGround3D();
}

void cDrawSceneKinChar::DrawGround3D() const
{
	const double w = 200;
	const tVector ground_origin = tVector(0, gGroundHeight, 0, 0);
	const tVector tex_size = tVector(0.5, 0.5, 0, 0);

	const auto& character = mScene->GetCharacter();
	tVector char_pos = character->GetRootPos();
	char_pos[1] = gGroundHeight;
	tVector a = char_pos - tVector(-0.5 * w, 0, -0.5 * w, 0);
	tVector b = char_pos - tVector(-0.5 * w, 0, 0.5 * w, 0);
	tVector c = char_pos - tVector(0.5 * w, 0, 0.5 * w, 0);
	tVector d = char_pos - tVector(0.5 * w, 0, -0.5 * w, 0);

	tVector min_coord = a - ground_origin;
	tVector max_coord = c - ground_origin;
	min_coord[0] /= tex_size[0];
	min_coord[1] = min_coord[2] / tex_size[1];
	max_coord[0] /= tex_size[0];
	max_coord[1] = max_coord[2] / tex_size[1];

	tVector coord_a = tVector(min_coord[0], min_coord[1], 0, 0);
	tVector coord_b = tVector(min_coord[0], max_coord[1], 0, 0);
	tVector coord_c = tVector(max_coord[0], max_coord[1], 0, 0);
	tVector coord_d = tVector(max_coord[0], min_coord[1], 0, 0);

	cDrawUtil::DrawQuad(a, b, c, d, coord_a, coord_b, coord_c, coord_d);
}


void cDrawSceneKinChar::DrawCharacters() const
{
	const auto& character = mScene->GetCharacter();
	cDrawCharacter::Draw(*character, gLinkWidth, gFilLColor, gLineColor);
}