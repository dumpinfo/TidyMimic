#pragma once
#include <memory>

#include "Scene.h"
#include "render/Camera.h"
#include "SceneKinChar.h"

class cDrawSceneKinChar : virtual public cScene //cDrawScene
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	cDrawSceneKinChar();
	virtual ~cDrawSceneKinChar();

	virtual void Init(char* motionfile);
	virtual void Reset();
	virtual void Clear();
	virtual void Update(double time_elapsed);

	std::string GetName() const;
	void DrawScene();
//------------------------------------------------------------------------
	enum eCamTrackMode
	{
		eCamTrackModeXZ,
		eCamTrackModeY,
		eCamTrackModeXYZ,
		eCamTrackModeStill,
		eCamTrackModeFixed,
		eCamTrackModeMax
	};
	eCamTrackMode mCamTrackMode;
	cCamera mCamera;
	bool mDrawInfo;

	GLuint mMeshLightDirHandle;
	GLuint mMeshLightColourHandle;
	GLuint mMeshAmbientColourHandle;
	GLuint mMeshShadowProjHandle;
	GLuint mMeshMaterialDataHandle;
	GLuint mMeshFogColorHandle;
	GLuint mMeshFogDataHandle;
//-------------------------------------------------------------------------
//protected:
	std::shared_ptr<cSceneKinChar> mScene;

	virtual void BuildScene(std::shared_ptr<cSceneKinChar>& out_scene) const;
	virtual void SetupScene(std::shared_ptr<cSceneKinChar>& out_scene);
	virtual void UpdateScene(double time_elapsed);

	virtual tVector GetCamTrackPos() const;
	virtual tVector GetCamStillPos() const;
	virtual tVector GetDefaultCamFocus() const;

	virtual void DrawGround() const;
	virtual void DrawGround3D() const;
	virtual void DrawCharacters() const;
	//--------------------------------------------------------------------
	void Draw();
	void RestoreView();
	void ClearFrame();
	void SetupView();
	void DrawCharacterMainScene();
	void DrawObjsMainScene();
	void DrawMiscMainScene();
	void  MouseClick(int button, int state, double x, double y);
	void MouseMove(double x, double y);
};