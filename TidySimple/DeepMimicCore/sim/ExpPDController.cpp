#include "ExpPDController.h"
#include <iostream>

#include "sim/SimCharacter.h"

//void cExpPDController::Init(cSimCharacter* character, const Eigen::MatrixXd& pd_params)
//{
//	cController::Init(character);
//	int num_joints = mChar->GetNumJoints();
//	mPDCtrls.resize(num_joints);
//	assert(pd_params.rows() == num_joints);
//
//	int root_id = mChar->GetRootID();
//	for (int j = 0; j < num_joints; ++j)
//	{
//		cSimBodyJoint& joint = mChar->GetJoint(j);
//		if (joint.IsValid() && j != root_id)
//		{
//			const cPDController::tParams& curr_params = pd_params.row(j);
//			cPDController& ctrl = mPDCtrls[j];
//			ctrl.Init(mChar, curr_params);
//		}
//	}
//
//	mValid = true;
//}

void cExpPDController::Init(cSimCharacter* character, const Eigen::MatrixXd& pd_params, const tVector& gravity)
{
	std::shared_ptr<cRBDModel> model = BuildRBDModel(*character, gravity);
	Init(character, model, pd_params, gravity);
	mExternRBDModel = false;
}


void cExpPDController::Init(cSimCharacter* character, const std::shared_ptr<cRBDModel>& model, const Eigen::MatrixXd& pd_params, const tVector& gravity)
{
	//cExpPDController::Init(character, pd_params);
	
	//void cExpPDController::Init(cSimCharacter* character, const Eigen::MatrixXd& pd_params)
  {
	cController::Init(character);
	int num_joints = mChar->GetNumJoints();
	mPDCtrls.resize(num_joints);
	assert(pd_params.rows() == num_joints);

	int root_id = mChar->GetRootID();
	for (int j = 0; j < num_joints; ++j)
	{
		cSimBodyJoint& joint = mChar->GetJoint(j);
		if (joint.IsValid() && j != root_id)
		{
			const cPDController::tParams& curr_params = pd_params.row(j);
			cPDController& ctrl = mPDCtrls[j];
			ctrl.Init(mChar, curr_params);
		}
	}

	mValid = true;
   }
	mGravity = gravity;
	mRBDModel = model;
	InitGains();
}

void cExpPDController::Clear()
{
	//cExpPDController::Clear();
	//void cExpPDController::Clear()
    {
	cController::Clear();
	mPDCtrls.clear();
    }
	mExternRBDModel = true;
	mRBDModel.reset();
}

void cExpPDController::UpdateControlForce(double time_step, Eigen::VectorXd& out_tau)
{
	cController::Update(time_step);

#if defined(IMP_PD_CTRL_PROFILER)
	TIMER_RECORD_BEG(Update_Ctrl_Force)
#endif

		if (time_step > 0)
		{
			if (!mExternRBDModel)
			{
				UpdateRBDModel();
			}

			CalcControlForces(time_step, out_tau);
		}

#if defined(IMP_PD_CTRL_PROFILER)
	TIMER_RECORD_END(Update_Ctrl_Force, mPerfTotalTime, mPerfTotalCount)
#endif

#if defined(IMP_PD_CTRL_PROFILER)
		printf("Solve Time: %.5f\n", mPerfSolveTime);
	printf("Total Time: %.5f\n", mPerfTotalTime);
#endif
}

//void cExpPDController::SetKp(int joint_id, double kp)
//{
//	//cExpPDController::SetKp(joint_id, kp);
//    //void cExpPDController::SetKp(int joint_id, double kp)
//{
//	cPDController& pd_ctrl = GetPDCtrl(joint_id);
//	pd_ctrl.SetKp(kp);
//}
//	int param_offset = mChar->GetParamOffset(joint_id);
//	int param_size = mChar->GetParamSize(joint_id);
//
//	auto curr_kp = mKp.segment(param_offset, param_size);
//	curr_kp.setOnes();
//	curr_kp *= kp;
//}
//
//void cExpPDController::SetKd(int joint_id, double kd)
//{
////	cExpPDController::SetKd(joint_id, kd);
////void cExpPDController::SetKd(int joint_id, double kd)
//{
//	cPDController& pd_ctrl = GetPDCtrl(joint_id);
//	pd_ctrl.SetKd(kd);
//}
//
//	int param_offset = mChar->GetParamOffset(joint_id);
//	int param_size = mChar->GetParamSize(joint_id);
//
//	auto curr_kd = mKd.segment(param_offset, param_size);
//	curr_kd.setOnes();
//	curr_kd *= kd;
//}

void cExpPDController::InitGains()
{
	int num_dof = GetNumDof();
	mKp = Eigen::VectorXd::Zero(num_dof);
	mKd = Eigen::VectorXd::Zero(num_dof);

	for (int j = 0; j < GetNumJoints(); ++j)
	{
		const cPDController& pd_ctrl = GetPDCtrl(j);
		if (pd_ctrl.IsValid())
		{
			int param_offset = mChar->GetParamOffset(j);
			int param_size = mChar->GetParamSize(j);

			double kp = pd_ctrl.GetKp();
			double kd = pd_ctrl.GetKd();

			mKp.segment(param_offset, param_size) = Eigen::VectorXd::Ones(param_size) * kp;
			mKd.segment(param_offset, param_size) = Eigen::VectorXd::Ones(param_size) * kd;
		}
	}
}

std::shared_ptr<cRBDModel> cExpPDController::BuildRBDModel(const cSimCharacter& character, const tVector& gravity) const
{
	std::shared_ptr<cRBDModel> model = std::shared_ptr<cRBDModel>(new cRBDModel());
	model->Init(character.GetJointMat(), character.GetBodyDefs(), gravity);
	return model;
}

void cExpPDController::UpdateRBDModel()
{
	const Eigen::VectorXd& pose = mChar->GetPose();
	const Eigen::VectorXd& vel = mChar->GetVel();
	mRBDModel->Update(pose, vel);
}

void cExpPDController::CalcControlForces(double time_step, Eigen::VectorXd& out_tau)
{
	double t = time_step;

	const Eigen::VectorXd& pose = mChar->GetPose();
	const Eigen::VectorXd& vel = mChar->GetVel();
	Eigen::VectorXd tar_pose;
	Eigen::VectorXd tar_vel;
	BuildTargetPose(tar_pose);
	BuildTargetVel(tar_vel);

	Eigen::DiagonalMatrix<double, Eigen::Dynamic> Kp_mat = mKp.asDiagonal();
	Eigen::DiagonalMatrix<double, Eigen::Dynamic> Kd_mat = mKd.asDiagonal();

	for (int j = 0; j < GetNumJoints(); ++j)
	{
		const cPDController& pd_ctrl = GetPDCtrl(j);
		if (!pd_ctrl.IsValid() || !pd_ctrl.IsActive())
		{
			int param_offset = mChar->GetParamOffset(j);
			int param_size = mChar->GetParamSize(j);
			Kp_mat.diagonal().segment(param_offset, param_size).setZero();
			Kd_mat.diagonal().segment(param_offset, param_size).setZero();
		}
	}

	Eigen::MatrixXd M = mRBDModel->GetMassMat();
	const Eigen::VectorXd& C = mRBDModel->GetBiasForce();
	M.diagonal() += t * mKd;

	Eigen::VectorXd pose_inc;
	const Eigen::MatrixXd& joint_mat = mChar->GetJointMat();
	cKinTree::VelToPoseDiff(joint_mat, pose, vel, pose_inc);

	pose_inc = pose + t * pose_inc;
	cKinTree::PostProcessPose(joint_mat, pose_inc);

	Eigen::VectorXd pose_err;
	cKinTree::CalcVel(joint_mat, pose_inc, tar_pose, 1, pose_err);
	Eigen::VectorXd vel_err = tar_vel - vel;
	Eigen::VectorXd acc = Kp_mat * pose_err + Kd_mat * vel_err - C;

#if defined(IMP_PD_CTRL_PROFILER)
	TIMER_RECORD_BEG(Solve)
#endif

		//int root_size = cKinTree::gRootDim;
		//int num_act_dofs = static_cast<int>(acc.size()) - root_size;
		//auto M_act = M.block(root_size, root_size, num_act_dofs, num_act_dofs);
		//auto acc_act = acc.segment(root_size, num_act_dofs);
		//acc_act = M_act.ldlt().solve(acc_act);

		acc = M.ldlt().solve(acc);

#if defined(IMP_PD_CTRL_PROFILER)
	TIMER_RECORD_END(Solve, mPerfSolveTime, mPerfSolveCount)
#endif

		out_tau += Kp_mat * pose_err + Kd_mat * (vel_err - t * acc);
}

void cExpPDController::BuildTargetPose(Eigen::VectorXd& out_pose) const
{
	out_pose = Eigen::VectorXd::Zero(GetNumDof());

	//const auto& joint_mat = mChar->GetJointMat();
	//tVector root_pos = mChar->GetRootPos();
	//tQuaternion root_rot = mChar->GetRootRotation();
	//cKinTree::SetRootPos(joint_mat, root_pos, out_pose);
	//cKinTree::SetRootRot(joint_mat, root_rot, out_pose);

	for (int j = 0; j < GetNumJoints(); ++j)
	{
		const cPDController& pd_ctrl = GetPDCtrl(j);
		if (pd_ctrl.IsValid())
		{
			Eigen::VectorXd curr_pose;
			pd_ctrl.GetTargetTheta(curr_pose);
			int param_offset = mChar->GetParamOffset(j);
			int param_size = mChar->GetParamSize(j);
			out_pose.segment(param_offset, param_size) = curr_pose;
		}
	}
}

void cExpPDController::BuildTargetVel(Eigen::VectorXd& out_vel) const
{
	out_vel = Eigen::VectorXd::Zero(GetNumDof());

	//const auto& joint_mat = mChar->GetJointMat();
	//tVector root_vel = mChar->GetRootVel();
	//tVector root_ang_vel = mChar->GetRootAngVel();
	//cKinTree::SetRootVel(joint_mat, root_vel, out_vel);
	//cKinTree::SetRootAngVel(joint_mat, root_ang_vel, out_vel);

	for (int j = 0; j < GetNumJoints(); ++j)
	{
		const cPDController& pd_ctrl = GetPDCtrl(j);
		if (pd_ctrl.IsValid())
		{
			Eigen::VectorXd curr_vel;
			pd_ctrl.GetTargetVel(curr_vel);
			int param_offset = mChar->GetParamOffset(j);
			int param_size = mChar->GetParamSize(j);
			out_vel.segment(param_offset, param_size) = curr_vel;
		}
	}
}

///############################################################################################
//#############################################################################################
cExpPDController::cExpPDController()
{
	mExternRBDModel = true;

#if defined(IMP_PD_CTRL_PROFILER)
	mPerfSolveTime = 0;
	mPerfTotalTime = 0;
	mPerfSolveCount = 0;
	mPerfTotalCount = 0;
#endif // IMP_PD_CTRL_PROFILER
}

cExpPDController::~cExpPDController()
{
}



//void cExpPDController::Reset()
//{
//	cController::Reset();
//	for (size_t i = 0; i < mPDCtrls.size(); ++i)
//	{
//		mPDCtrls[i].Reset();
//	}
//}
//
//
//
//void cExpPDController::Update(double time_step)
//{
//	Eigen::VectorXd tau = Eigen::VectorXd::Zero(GetNumDof());
//	UpdateControlForce(time_step, tau);
//	ApplyControlForces(tau);
//}

/*void cExpPDController::UpdateControlForce(double time_step, Eigen::VectorXd& out_tau)
{
	cController::Update(time_step);
	if (time_step > 0)
	{
		CalcControlForces(time_step, out_tau);
	}
}*/

int cExpPDController::GetNumJoints() const
{
	return mChar->GetNumJoints();
}

int cExpPDController::GetNumDof() const
{
	return mChar->GetNumDof();
}
//
//void cExpPDController::GetTargetTheta(int joint_id, Eigen::VectorXd& out_theta) const
//{
//	const cPDController& pd_ctrl = GetPDCtrl(joint_id);
//	pd_ctrl.GetTargetTheta(out_theta);
//}

void cExpPDController::SetTargetTheta(int joint_id, const Eigen::VectorXd& theta)
{
	cPDController& pd_ctrl = GetPDCtrl(joint_id);
	pd_ctrl.SetTargetTheta(theta);
}

//void cExpPDController::GetTargetVel(int joint_id, Eigen::VectorXd& out_vel) const
//{
//	const cPDController& pd_ctrl = GetPDCtrl(joint_id);
//	pd_ctrl.GetTargetVel(out_vel);
//}
//
//void cExpPDController::SetTargetVel(int joint_id, const Eigen::VectorXd& vel)
//{
//	cPDController& pd_ctrl = GetPDCtrl(joint_id);
//	pd_ctrl.SetTargetVel(vel);
//}
//
//bool cExpPDController::UseWorldCoord(int joint_id) const
//{
//	const cPDController& pd_ctrl = GetPDCtrl(joint_id);
//	return pd_ctrl.UseWorldCoord();
//}
//
//void cExpPDController::SetUseWorldCoord(int joint_id, bool use)
//{
//	cPDController& pd_ctrl = GetPDCtrl(joint_id);
//	pd_ctrl.SetUseWorldCoord(use);
//}
//




bool cExpPDController::IsValidPDCtrl(int joint_id) const
{
	const cPDController& pd_ctrl = GetPDCtrl(joint_id);
	return pd_ctrl.IsValid();
}

cPDController& cExpPDController::GetPDCtrl(int joint_id)
{
	assert(joint_id >= 0 && joint_id < GetNumJoints());
	return mPDCtrls[joint_id];
}

const cPDController& cExpPDController::GetPDCtrl(int joint_id) const
{
	assert(joint_id >= 0 && joint_id < GetNumJoints());
	return mPDCtrls[joint_id];
}

/*void cExpPDController::CalcControlForces(double time_step, Eigen::VectorXd& out_tau)
{
	for (int i = 0; i < static_cast<int>(mPDCtrls.size()); ++i)
	{
		cPDController& pd_ctrl = GetPDCtrl(i);
		if (pd_ctrl.IsValid())
		{
			pd_ctrl.UpdateControlForce(time_step, out_tau);
		}
	}
}*/

//void cExpPDController::ApplyControlForces(const Eigen::VectorXd& tau)
//{
//	mChar->ApplyControlForces(tau);
//}