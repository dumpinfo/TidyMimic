#include "DeepMimicCharController.h"
#include "sim/SimCharacter.h"
#include <iostream>
#include <ctime>
#include "util/json/json.h"
#include "util/FileUtil.h"
#include "util/JsonUtil.h"

const int gValLogSize = 50;
const std::string gViewDistMinKey = "ViewDistMin";
const std::string gViewDistMaxKey = "ViewDistMax";
const std::string gPDControllersKey = "PDControllers";


const int cDeepMimicCharController::gNormGroupSingle = 0;
const int cDeepMimicCharController::gNormGroupNone = -1;




int cDeepMimicCharController::GetGoalSize() const
{
	return 0;
}

 

int cDeepMimicCharController::GetNumActions() const
{
	printf(" cDeepMimicCharController::GetNumActions()\n");
	return 0;
}

void cDeepMimicCharController::HandlePoseReset()
{
}

void cDeepMimicCharController::HandleVelReset()
{
}

 
void cDeepMimicCharController::BuildGoalOffsetScale(Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const
{
	int goal_size = GetGoalSize();
	out_offset = Eigen::VectorXd::Zero(goal_size);
	out_scale = Eigen::VectorXd::Ones(goal_size);
}

void cDeepMimicCharController::BuildGoalNormGroups(Eigen::VectorXi& out_groups) const
{
	int goal_size = GetGoalSize();
	out_groups = gNormGroupSingle * Eigen::VectorXi::Ones(goal_size);
}

int cDeepMimicCharController::NumChildren() const
{
	return 0;
}

const std::shared_ptr<cDeepMimicCharController>& cDeepMimicCharController::GetChild(int i) const
{
	return nullptr;
}
//####################################################################
//####################################################################
//####################################################################
void cDeepMimicCharController::SetUpdateRate(double rate)
{
	mUpdateRate = rate;
}

double cDeepMimicCharController::GetUpdateRate() const
{
	return mUpdateRate;
}

void cDeepMimicCharController::UpdateCalcTau(double timestep, Eigen::VectorXd& out_tau)//KK##
{
	// use unnormalized phase
	double prev_phase = 0;
	if (mEnablePhaseAction)
	{
		prev_phase = mTime / mCyclePeriod;
		prev_phase += mPhaseOffset;
	}

	//cDeepMimicCharController::UpdateCalcTau(timestep, out_tau);
	//void cDeepMimicCharController::UpdateCalcTau(double timestep, Eigen::VectorXd& out_tau)
   {
	mTime += timestep;
	if (mNeedNewAction)
	{
		HandleNewAction();
	}
   }

	if (mEnablePhaseAction)
	{
		double phase_rate = GetPhaseRate();
		double tar_phase = prev_phase + timestep * phase_rate;
		mPhaseOffset = tar_phase - mTime / mCyclePeriod;
		mPhaseOffset = std::fmod(mPhaseOffset, 1.0);
	}

	UpdateBuildTau(timestep, out_tau);
}


int cDeepMimicCharController::GetStateSize() const //KK##
{
 	int state_size = 0;
	state_size += GetStatePoseSize();
	state_size += GetStateVelSize();
	state_size += GetStatePhaseSize();
	return state_size;
}

int cDeepMimicCharController::GetActionSize() const
{
	int a_size = 0;
	a_size += GetActionPhaseSize();
	a_size += GetActionCtrlSize();
	return a_size;
}

void cDeepMimicCharController::BuildStateOffsetScale(Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const
{
	//cDeepMimicCharController::BuildStateOffsetScale(out_offset, out_scale);
	int state_size = GetStateSize();
	out_offset = Eigen::VectorXd::Zero(state_size);
	out_scale = Eigen::VectorXd::Ones(state_size);
    //----------------------------------------------------
	
	if (mEnablePhaseInput)
	{
		Eigen::VectorXd phase_offset;
		Eigen::VectorXd phase_scale;
		BuildStatePhaseOffsetScale(phase_offset, phase_scale);

		int phase_idx = GetStatePhaseOffset();
		int phase_size = GetStatePhaseSize();
		out_offset.segment(phase_idx, phase_size) = phase_offset;
		out_scale.segment(phase_idx, phase_size) = phase_scale;
	}
}

void cDeepMimicCharController::BuildActionBounds(Eigen::VectorXd& out_min, Eigen::VectorXd& out_max) const
{
	int action_size = GetActionSize();
	out_min = Eigen::VectorXd::Zero(action_size);
	out_max = Eigen::VectorXd::Zero(action_size);

	int root_id = mChar->GetRootID();
	int root_size = mChar->GetParamSize(root_id);
	int num_joints = mChar->GetNumJoints();
	int ctrl_offset = GetActionCtrlOffset();

	if (mEnablePhaseAction)
	{
		int phase_offset = GetActionPhaseOffset();
		out_min[phase_offset] = -5;
		out_max[phase_offset] = 5;
	}

	for (int j = root_id + 1; j < num_joints; ++j)
	{
		const cSimBodyJoint& joint = mChar->GetJoint(j);
		if (joint.IsValid())
		{
			int param_offset = mChar->GetParamOffset(j);
			int param_size = mChar->GetParamSize(j);

			if (param_size > 0)
			{
				Eigen::VectorXd lim_min;
				Eigen::VectorXd lim_max;
				BuildJointActionBounds(j, lim_min, lim_max);
				assert(lim_min.size() == param_size);
				assert(lim_max.size() == param_size);

				param_offset -= root_size;
				param_offset += ctrl_offset;
				out_min.segment(param_offset, param_size) = lim_min;
				out_max.segment(param_offset, param_size) = lim_max;
			}
		}
	}
}

void cDeepMimicCharController::ResetParams()//KK##
{
	mTime = 0;
	mNeedNewAction = true;
	mTau.setZero();
	mValLog.Clear();

	mPrevActionTime = mTime;
	mPrevActionCOM.setZero();
	
	mPhaseOffset = 0;
	mInitTimeOffset = 0;
}

int cDeepMimicCharController::GetPosFeatureDim() const
{
	int pos_dim = cKinTree::gPosDim;
	return pos_dim;
}

int cDeepMimicCharController::GetRotFeatureDim() const
{
	int rot_dim = cKinTree::gRotDim;
	return rot_dim;
}

double cDeepMimicCharController::GetCyclePeriod() const
{
	return mCyclePeriod;
}

void cDeepMimicCharController::SetCyclePeriod(double period)
{
	mCyclePeriod = period;
}

void cDeepMimicCharController::SetInitTime(double time)
{
	mTime = time;
	mPrevActionTime = time;
	mPhaseOffset = 0;
	mInitTimeOffset = -mTime;
}

double cDeepMimicCharController::GetPhase() const
{
	double phase = mTime / mCyclePeriod;
	phase += mPhaseOffset;
	phase = std::fmod(phase, 1.0);
	phase = (phase < 0) ? (1 + phase) : phase;
	return phase;
}



bool cDeepMimicCharController::CheckNeedNewAction(double timestep) const //KK##
{
	double curr_time = mTime;
	curr_time += mInitTimeOffset;
	bool new_action = cMathUtil::CheckNextInterval(timestep, curr_time, 1 / mUpdateRate);
	return new_action;
}

void cDeepMimicCharController::BuildActionOffsetScale(Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const
{
	int action_size = GetActionSize();
	out_offset = Eigen::VectorXd::Zero(action_size);
	out_scale = Eigen::VectorXd::Ones(action_size);

	int root_id = mChar->GetRootID();
	int root_size = mChar->GetParamSize(mChar->GetRootID());
	int num_joints = mChar->GetNumJoints();
	int ctrl_offset = GetActionCtrlOffset();

	if (mEnablePhaseAction)
	{
		int phase_offset = GetActionPhaseOffset();
		out_offset[phase_offset] = -1 / mCyclePeriod;
		out_scale[phase_offset] = 1;
	}

	for (int j = root_id + 1; j < num_joints; ++j)
	{
		const cSimBodyJoint& joint = mChar->GetJoint(j);
		if (joint.IsValid())
		{
			int param_offset = mChar->GetParamOffset(j);
			int param_size = mChar->GetParamSize(j);

			if (param_size > 0)
			{
				Eigen::VectorXd curr_offset;
				Eigen::VectorXd curr_scale;
				BuildJointActionOffsetScale(j, curr_offset, curr_scale);
				assert(curr_offset.size() == param_size);
				assert(curr_scale.size() == param_size);

				param_offset -= root_size;
				param_offset += ctrl_offset;
				out_offset.segment(param_offset, param_size) = curr_offset;
				out_scale.segment(param_offset, param_size) = curr_scale;
			}
		}
	}
}

void cDeepMimicCharController::BuildStateNormGroups(Eigen::VectorXi& out_groups) const
{
	//cDeepMimicCharController::BuildStateNormGroups(out_groups);
	int state_size = GetStateSize();
	out_groups = gNormGroupSingle * Eigen::VectorXi::Ones(state_size);

	if (mEnablePhaseInput)
	{
		int phase_group = gNormGroupNone;
		int phase_offset = GetStatePhaseOffset();
		int phase_size = GetStatePhaseSize();
		out_groups.segment(phase_offset, phase_size) = phase_group * Eigen::VectorXi::Ones(phase_size);
	}
}

void cDeepMimicCharController::RecordState(Eigen::VectorXd& out_state)//KK##
{
	Eigen::VectorXd phase_state;
	int state_size = GetStateSize();
	// fill with nans to make sure we don't forget to set anything
	out_state = std::numeric_limits<double>::quiet_NaN() * Eigen::VectorXd::Ones(state_size);
	Eigen::VectorXd ground;
	Eigen::VectorXd pose;
	Eigen::VectorXd vel;
	BuildStatePose(pose);
	BuildStateVel(vel);

	int pose_offset = GetStatePoseOffset();
	int pose_size = GetStatePoseSize();
	int vel_offset = GetStateVelOffset();
	int vel_size = GetStateVelSize();

	out_state.segment(pose_offset, pose_size) = pose;
	out_state.segment(vel_offset, vel_size) = vel;
	

	if (mEnablePhaseInput)
	{
		int phase_offset = GetStatePhaseOffset();
		int phase_size = GetStatePhaseSize();
		BuildStatePhase(phase_state);
		out_state.segment(phase_offset, phase_size) = phase_state;
	}
}

 

int cDeepMimicCharController::GetStatePoseSize() const//KK##
{
	int pos_dim = GetPosFeatureDim();
	int rot_dim = GetRotFeatureDim();
	int size = mChar->GetNumBodyParts() * (pos_dim + rot_dim) + 1; // +1 for root y

	return size;
}

/*int cDeepMimicCharController::GetStateVelSize() const
{
	return mChar->GetNumBodyParts() * mPosDim;
}*/

int cDeepMimicCharController::GetStateVelSize() const
{
	int pos_dim = GetPosFeatureDim();
	int rot_dim = GetRotFeatureDim();
	int size = mChar->GetNumBodyParts() * (pos_dim + rot_dim - 1);
	return size;
}

int cDeepMimicCharController::GetStatePoseOffset() const //KK##
{  //cDeepMimicCharController::GetStatePoseOffset()
	return 0 + GetStatePhaseSize();
}

int cDeepMimicCharController::GetStatePhaseSize() const
{
	return (mEnablePhaseInput) ? 1 : 0;
}

int cDeepMimicCharController::GetStatePhaseOffset() const
{
	return 0;
}

int cDeepMimicCharController::GetActionPhaseOffset() const
{
	return 0;
}

int cDeepMimicCharController::GetActionPhaseSize() const
{
	return (mEnablePhaseAction) ? 1 : 0;
}

int cDeepMimicCharController::GetActionCtrlOffset() const
{
	return GetActionPhaseSize();
}

int cDeepMimicCharController::GetActionCtrlSize() const
{
	int ctrl_size = mChar->GetNumDof();
	int root_size = mChar->GetParamSize(mChar->GetRootID());
	ctrl_size -= root_size;
	return ctrl_size;
}

void cDeepMimicCharController::BuildStatePhaseOffsetScale(Eigen::VectorXd& phase_offset, Eigen::VectorXd& phase_scale) const
{
	double offset = -0.5;
	double scale = 2;
	int phase_size = GetStatePhaseSize();
	phase_offset = offset * Eigen::VectorXd::Ones(phase_size);
	phase_scale = scale * Eigen::VectorXd::Ones(phase_size);
}


void cDeepMimicCharController::BuildStatePose(Eigen::VectorXd& out_pose) const//KK##
{
	tMatrix origin_trans = mChar->BuildOriginTrans();
	tQuaternion origin_quat = cMathUtil::RotMatToQuaternion(origin_trans);

	bool flip_stance = FlipStance();
	if (flip_stance)
	{
		origin_trans.row(2) *= -1; // reflect z
	}

	tVector root_pos = mChar->GetRootPos();
	tVector root_pos_rel = root_pos;

	root_pos_rel[3] = 1;
	root_pos_rel = origin_trans * root_pos_rel;
	root_pos_rel[3] = 0;

	out_pose = Eigen::VectorXd::Zero(GetStatePoseSize());
	out_pose[0] = root_pos_rel[1];
	int num_parts = mChar->GetNumBodyParts();
	int root_id = mChar->GetRootID();

	int pos_dim = GetPosFeatureDim();
	int rot_dim = GetRotFeatureDim();

	tQuaternion mirror_inv_origin_quat = origin_quat.conjugate();
	mirror_inv_origin_quat = cMathUtil::MirrorQuaternion(mirror_inv_origin_quat, cMathUtil::eAxisZ);

	int idx = 1;
	for (int i = 0; i < num_parts; ++i)
	{
		int part_id = RetargetJointID(i);
		if (mChar->IsValidBodyPart(part_id))
		{
			const auto& curr_part = mChar->GetBodyPart(part_id);
			tVector curr_pos = curr_part->GetPos();

			if (mRecordWorldRootPos && i == root_id)
			{
				if (flip_stance)
				{
					curr_pos = cMathUtil::QuatRotVec(origin_quat, curr_pos);
					curr_pos[2] = -curr_pos[2];
					curr_pos = cMathUtil::QuatRotVec(mirror_inv_origin_quat, curr_pos);
				}
			}
			else
			{
				curr_pos[3] = 1;
				curr_pos = origin_trans * curr_pos;
				curr_pos -= root_pos_rel;
				curr_pos[3] = 0;
			}

			out_pose.segment(idx, pos_dim) = curr_pos.segment(0, pos_dim);
			idx += pos_dim;

			tQuaternion curr_quat = curr_part->GetRotation();
			if (mRecordWorldRootRot && i == root_id)
			{
				if (flip_stance)
				{
					curr_quat = origin_quat * curr_quat;
					curr_quat = cMathUtil::MirrorQuaternion(curr_quat, cMathUtil::eAxisZ);
					curr_quat = mirror_inv_origin_quat * curr_quat;
				}
			}
			else
			{
				curr_quat = origin_quat * curr_quat;
				if (flip_stance)
				{
					curr_quat = cMathUtil::MirrorQuaternion(curr_quat, cMathUtil::eAxisZ);
				}
			}

			if (curr_quat.w() < 0)
			{
				curr_quat.w() *= -1;
				curr_quat.x() *= -1;
				curr_quat.y() *= -1;
				curr_quat.z() *= -1;
			}
			out_pose.segment(idx, rot_dim) = cMathUtil::QuatToVec(curr_quat).segment(0, rot_dim);
			idx += rot_dim;
		}
	}
}

void cDeepMimicCharController::BuildStateVel(Eigen::VectorXd& out_vel) const//KK##
{
	int num_parts = mChar->GetNumBodyParts();
	tMatrix origin_trans = mChar->BuildOriginTrans();
	tQuaternion origin_quat = cMathUtil::RotMatToQuaternion(origin_trans);

	bool flip_stance = FlipStance();
	if (flip_stance)
	{
		origin_trans.row(2) *= -1; // reflect z
	}

	int pos_dim = GetPosFeatureDim();
	int rot_dim = GetRotFeatureDim();

	out_vel = Eigen::VectorXd::Zero(GetStateVelSize());

	tQuaternion mirror_inv_origin_quat = origin_quat.conjugate();
	mirror_inv_origin_quat = cMathUtil::MirrorQuaternion(mirror_inv_origin_quat, cMathUtil::eAxisZ);

	int idx = 0;
	for (int i = 0; i < num_parts; ++i)
	{
		int part_id = RetargetJointID(i);
		int root_id = mChar->GetRootID();

		const auto& curr_part = mChar->GetBodyPart(part_id);
		tVector curr_vel = curr_part->GetLinearVelocity();

		if (mRecordWorldRootRot && i == root_id)
		{
			if (flip_stance)
			{
				curr_vel = cMathUtil::QuatRotVec(origin_quat, curr_vel);
				curr_vel[2] = -curr_vel[2];
				curr_vel = cMathUtil::QuatRotVec(mirror_inv_origin_quat, curr_vel);
			}
		}
		else
		{
			curr_vel = origin_trans * curr_vel;
		}

		out_vel.segment(idx, pos_dim) = curr_vel.segment(0, pos_dim);
		idx += pos_dim;

		tVector curr_ang_vel = curr_part->GetAngularVelocity();
		if (mRecordWorldRootRot && i == root_id)
		{
			if (flip_stance)
			{
				curr_ang_vel = cMathUtil::QuatRotVec(origin_quat, curr_ang_vel);
				curr_ang_vel[2] = -curr_ang_vel[2];
				curr_ang_vel = -curr_ang_vel;
				curr_ang_vel = cMathUtil::QuatRotVec(mirror_inv_origin_quat, curr_ang_vel);
			}
		}
		else
		{
			curr_ang_vel = origin_trans * curr_ang_vel;
			if (flip_stance)
			{
				curr_ang_vel = -curr_ang_vel;
			}
		}

		out_vel.segment(idx, rot_dim - 1) = curr_ang_vel.segment(0, rot_dim - 1);
		idx += rot_dim - 1;
	}
}

void cDeepMimicCharController::BuildStatePhase(Eigen::VectorXd& out_phase) const
{
	double phase = GetPhase();
	out_phase = Eigen::VectorXd::Zero(GetStatePhaseSize());
	out_phase[0] = phase;
}


bool cDeepMimicCharController::FlipStance() const
{
	return false;
}

int cDeepMimicCharController::RetargetJointID(int joint_id) const
{
	return joint_id;
}

double cDeepMimicCharController::GetPhaseRate() const
{
	assert(mEnablePhaseAction);
	double phase_rate = 0;
	if (mEnablePhaseAction)
	{
		int phase_offset = GetActionPhaseOffset();
		phase_rate = mAction[phase_offset];
	}
	return phase_rate;
}


//====================================================
cDeepMimicCharController::cDeepMimicCharController() : cController()
{
	mTime = 0;
	mPosDim = 0;
	SetViewDistMin(-0.5);
	SetViewDistMax(10);

	mPrevActionTime = mTime;
	mPrevActionCOM.setZero();

	mValLog.Reserve(gValLogSize);
	//--------------------------------------------------------------
	mUpdateRate = 30.0;
	mCyclePeriod = 1;
	mEnablePhaseInput = false;
	mEnablePhaseAction = false;
	mRecordWorldRootPos = false;
	mRecordWorldRootRot = false;
	SetViewDistMax(1);

	mPhaseOffset = 0;
	mInitTimeOffset = 0;
	mGravity = gGravity;
}

 

void cDeepMimicCharController::Reset()//KK##
{
	//cCharController::Reset();
	cController::Reset();
	ResetParams();
	NewActionUpdate();
	mPDCtrl.Reset();
}

void cDeepMimicCharController::Clear()//KK##
{
	//cCharController::Clear();
	cController::Clear();
	ResetParams();
	mPDCtrl.Clear();
}

void cDeepMimicCharController::SetGravity(const tVector& gravity)
{
	mGravity = gravity;
}

std::string cDeepMimicCharController::GetName() const
{
	return "ct_pd";
}

void cDeepMimicCharController::SetupPDControllers(const Json::Value& json, const tVector& gravity)
{
	Eigen::MatrixXd pd_params;
	bool succ = false;
	if (!json[gPDControllersKey].isNull())
	{
		succ = cPDController::LoadParams(json[gPDControllersKey], pd_params);
	}

	if (succ)
	{
		mPDCtrl.Init(mChar, pd_params, gravity);
	}

	mValid = succ;
	if (!mValid)
	{
		printf("Failed to initialize Ct-PD controller\n");
		mValid = false;
	}
}

bool cDeepMimicCharController::ParseParams(const Json::Value& json)//KK##
{
	//bool succ = cCharController::ParseParams(json);
	bool succ = cController::ParseParams(json);
	mViewDistMin = json.get(gViewDistMinKey, mViewDistMin).asDouble();
	mViewDistMax = json.get(gViewDistMaxKey, mViewDistMax).asDouble();
	// = cDeepMimicCharController::ParseParams(json);
	mUpdateRate = json.get("QueryRate", mUpdateRate).asDouble();
	mCyclePeriod = json.get("CyclePeriod", mCyclePeriod).asDouble();
	mEnablePhaseInput = json.get("EnablePhaseInput", mEnablePhaseInput).asBool();
	mEnablePhaseAction = json.get("EnablePhaseAction", mEnablePhaseAction).asBool();
	mRecordWorldRootPos = json.get("RecordWorldRootPos", mRecordWorldRootPos).asBool();
	mRecordWorldRootRot = json.get("RecordWorldRootRot", mRecordWorldRootRot).asBool();

	SetupPDControllers(json, mGravity);
	return succ;
}

void cDeepMimicCharController::UpdateBuildTau(double time_step, Eigen::VectorXd& out_tau)
{
	UpdatePDCtrls(time_step, out_tau);
}/**/

void cDeepMimicCharController::UpdatePDCtrls(double time_step, Eigen::VectorXd& out_tau)
{
	int num_dof = mChar->GetNumDof();
	out_tau = Eigen::VectorXd::Zero(num_dof);
	mPDCtrl.UpdateControlForce(time_step, out_tau);
}

 
void cDeepMimicCharController::ApplyAction(const Eigen::VectorXd& action) //KK##
{
	//cDeepMimicCharController::ApplyAction(action);
	mAction = action;
	PostProcessAction(mAction);
	SetPDTargets(action);
}

void cDeepMimicCharController::BuildJointActionBounds(int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max) const
{
	const Eigen::MatrixXd& joint_mat = mChar->GetJointMat();
	cCtCtrlUtil::BuildBoundsPD(joint_mat, joint_id, out_min, out_max);
}

void cDeepMimicCharController::BuildJointActionOffsetScale(int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const
{
	const Eigen::MatrixXd& joint_mat = mChar->GetJointMat();
	cCtCtrlUtil::BuildOffsetScalePD(joint_mat, joint_id, out_offset, out_scale);
}/**/

void cDeepMimicCharController::ConvertActionToTargetPose(int joint_id, Eigen::VectorXd& out_theta) const
{
#if defined(ENABLE_PD_SPHERE_AXIS)
	cKinTree::eJointType joint_type = GetJointType(joint_id);
	if (joint_type == cKinTree::eJointTypeSpherical)
	{
		double rot_theta = out_theta[0];
		tVector axis = tVector(out_theta[1], out_theta[2], out_theta[3], 0);
		if (axis.squaredNorm() == 0)
		{
			axis[2] = 1;
		}

		axis.normalize();
		tQuaternion quat = cMathUtil::AxisAngleToQuaternion(axis, rot_theta);

		if (FlipStance())
		{
			cKinTree::eJointType joint_type = GetJointType(joint_id);
			if (joint_type == cKinTree::eJointTypeSpherical)
			{
				quat = cMathUtil::MirrorQuaternion(quat, cMathUtil::eAxisZ);
			}
		}
		out_theta = cMathUtil::QuatToVec(quat);
	}
#endif
}

cKinTree::eJointType cDeepMimicCharController::GetJointType(int joint_id) const
{
	const cPDController& ctrl = mPDCtrl.GetPDCtrl(joint_id);
	const cSimBodyJoint& joint = ctrl.GetJoint();
	cKinTree::eJointType joint_type = joint.GetType();
	return joint_type;
}

void cDeepMimicCharController::SetPDTargets(const Eigen::VectorXd& targets)
{
	int root_id = mChar->GetRootID();
	int root_size = mChar->GetParamSize(root_id);
	int num_joints = mChar->GetNumJoints();
	int ctrl_offset = GetActionCtrlOffset();

	for (int j = root_id + 1; j < num_joints; ++j)
	{
		if (mPDCtrl.IsValidPDCtrl(j))
		{
			int retarget_joint = RetargetJointID(j);
			int param_offset = mChar->GetParamOffset(retarget_joint);
			int param_size = mChar->GetParamSize(retarget_joint);

			param_offset -= root_size;
			param_offset += ctrl_offset;
			Eigen::VectorXd theta = targets.segment(param_offset, param_size);
			ConvertActionToTargetPose(j, theta);
			mPDCtrl.SetTargetTheta(j, theta);
		}
	}
}

 

cDeepMimicCharController::~cDeepMimicCharController()
{
}

void cDeepMimicCharController::Init(cSimCharacter* character, const std::string& param_file)
{
	//cCharController::Init(character);
	cController::Init(character);
	LoadParams(param_file);
	ResetParams();

	mPosDim = GetPosDim();
	InitResources();

	mValid = true;
}

 
 
void cDeepMimicCharController::Update(double time_step)
{
	//cCharController::Update(time_step);
	//void cDeepMimicCharController::Update(double time_step)
{
	cController::Update(time_step);
}
	UpdateCalcTau(time_step, mTau);
	UpdateApplyTau(mTau);
}

void cDeepMimicCharController::PostUpdate(double timestep)
{
	mNeedNewAction = CheckNeedNewAction(timestep);
	if (mNeedNewAction)
	{
		NewActionUpdate();
	}
}



void cDeepMimicCharController::UpdateApplyTau(const Eigen::VectorXd& tau)
{
	mTau = tau;
	mChar->ApplyControlForces(tau);
}

void cDeepMimicCharController::SetViewDistMin(double dist)
{
	mViewDistMin = dist;
}

void cDeepMimicCharController::SetViewDistMax(double dist)
{
	mViewDistMax = dist;
}

double cDeepMimicCharController::GetViewDistMin() const
{
	return mViewDistMin;
}

double cDeepMimicCharController::GetViewDistMax() const
{
	return mViewDistMax;
}

void cDeepMimicCharController::GetViewBound(tVector& out_min, tVector& out_max) const
{
	tVector origin = mChar->GetRootPos();
	double max_len = mViewDistMax;
	out_min = origin - tVector(max_len, 0, max_len, 0);
	out_max = origin + tVector(max_len, 0, max_len, 0);
}

double cDeepMimicCharController::GetPrevActionTime() const
{
	return mPrevActionTime;
}

const tVector& cDeepMimicCharController::GetPrevActionCOM() const
{
	return mPrevActionCOM;
}

double cDeepMimicCharController::GetTime() const
{
	return mTime;
}

const Eigen::VectorXd& cDeepMimicCharController::GetTau() const
{
	return mTau;
}

const cCircularBuffer<double>& cDeepMimicCharController::GetValLog() const
{
	return mValLog;
}

void cDeepMimicCharController::LogVal(double val)
{
	mValLog.Add(val);
}

bool cDeepMimicCharController::NeedNewAction() const
{
	return mNeedNewAction;
}



 

void cDeepMimicCharController::RecordGoal(Eigen::VectorXd& out_goal) const
{
	int goal_size = GetGoalSize();
	out_goal = std::numeric_limits<double>::quiet_NaN() * Eigen::VectorXd::Ones(goal_size);
}

eActionSpace cDeepMimicCharController::GetActionSpace() const
{
	return eActionSpaceContinuous;
}

void cDeepMimicCharController::RecordAction(Eigen::VectorXd& out_action) const
{
	out_action = mAction;
}



double cDeepMimicCharController::GetRewardMin() const
{
	return 0;
}

double cDeepMimicCharController::GetRewardMax() const
{
	return 1;
}



 
void cDeepMimicCharController::InitResources()
{
	InitAction();
	InitTau();
}

void cDeepMimicCharController::InitAction()
{
	mAction = Eigen::VectorXd::Zero(GetActionSize());
}

void cDeepMimicCharController::InitTau()
{
	mTau = Eigen::VectorXd::Zero(mChar->GetNumDof());
}

int cDeepMimicCharController::GetPosDim() const
{
	int dim = 3;
	return dim;
}


void cDeepMimicCharController::NewActionUpdate()
{
}

void cDeepMimicCharController::HandleNewAction()
{
	mPrevActionTime = mTime;
	mPrevActionCOM = mChar->CalcCOM();
	mNeedNewAction = false;
}

void cDeepMimicCharController::PostProcessAction(Eigen::VectorXd& out_action) const
{
}


int cDeepMimicCharController::GetStateVelOffset() const
{
	return GetStatePoseOffset() + GetStatePoseSize();
}



