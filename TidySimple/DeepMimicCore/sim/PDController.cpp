#include "PDController.h"
#include <iostream>

#include "sim/SimCharacter.h"
#include "util/FileUtil.h"

const std::string gPDParamKeys[cPDController::eParamMax] =
{
	"JointID",
	"Kp",
	"Kd",
	"TargetTheta0",
	"TargetTheta1",
	"TargetTheta2",
	"TargetTheta3",
	"TargetTheta4",
	"TargetTheta5",
	"TargetTheta6",
	"TargetVel0",
	"TargetVel1",
	"TargetVel2",
	"TargetVel3",
	"TargetVel4",
	"TargetVel5",
	"TargetVel6",
	"UseWorldCoord"
};

 

bool cPDController::LoadParams(const Json::Value& root, Eigen::MatrixXd& out_buffer)
{
	bool succ = true;
	int num_ctrls = root.size();
	out_buffer.resize(num_ctrls, eParamMax);

	for (int i = 0; i < num_ctrls; ++i)
	{
		tParams curr_params;
		const Json::Value& json_pd_ctrl = root.get(i, 0);
		bool succ_def = ParsePDParams(json_pd_ctrl, curr_params);
		if (succ_def)
		{
			int joint_id = i;
			curr_params[eParamJointID] = i;
			out_buffer.row(i) = curr_params;
		}
		else
		{
			succ = false;
			break;
		}
	}
	return succ;
}

bool cPDController::ParsePDParams(const Json::Value& root, tParams& out_params)
{
	bool succ = true;

	out_params.setZero();
	for (int i = 0; i < eParamMax; ++i)
	{
		const std::string& curr_key = gPDParamKeys[i];
		if (!root[curr_key].isNull() && root[curr_key].isNumeric())
		{
			Json::Value json_val = root[curr_key];
			double val = json_val.asDouble();
			out_params[i] = val;
		}
	}

	return succ;
}

cPDController::cPDController()
	: cController()
{
	Clear();
}

cPDController::~cPDController()
{
}

void cPDController::Init(cSimCharacter* character, const tParams& params)
{
	cController::Init(character);
	mParams = params;

	int joint_dim = GetJointDim();
	Eigen::VectorXd target_pose0;
	GetTargetTheta(target_pose0);
	SetTargetTheta(target_pose0);

	mValid = true;
}

void cPDController::Clear()
{
	cController::Clear();

	mParams.setZero();
	mParams[eParamJointID] = static_cast<double>(cKinTree::gInvalidJointID);
}

 

const cSimBodyJoint& cPDController::GetJoint() const
{
	return mChar->GetJoint(GetJointID());
}

 

double cPDController::GetKp() const
{
	return mParams[eParamKp];
}

 

double cPDController::GetKd() const
{
	return mParams[eParamKd];
}

void cPDController::SetTargetTheta(const Eigen::VectorXd& theta)
{
	int theta_size = static_cast<int>(theta.size());
	int joint_dim = GetJointDim();
	assert(theta_size == joint_dim);
	assert(theta_size < 7);
	Eigen::VectorXd theta_proc = theta;
	PostProcessTargetPose(theta_proc);
	mParams.segment(eParamTargetTheta0, theta_size) = theta_proc;
}

 

bool cPDController::UseWorldCoord() const
{
	return mParams[eParamUseWorldCoord] != 0;
}

void cPDController::GetTargetTheta(Eigen::VectorXd& out_theta) const
{
	const cSimBodyJoint& joint = GetJoint();
	cKinTree::eJointType joint_type = joint.GetType();
	out_theta = mParams.segment(eParamTargetTheta0, GetJointDim());

	 
}

void cPDController::GetTargetVel(Eigen::VectorXd& out_vel) const
{
	out_vel = mParams.segment(eParamTargetVel0, GetJointDim());
}


bool cPDController::IsActive() const
{
	bool active = cController::IsActive();
	return active;
}

int cPDController::GetJointDim() const
{
	return mChar->GetParamSize(GetJointID());
}

int cPDController::GetJointID() const
{
	return static_cast<int>(mParams[eParamJointID]);
}

 

void cPDController::PostProcessTargetPose(Eigen::VectorXd& out_pose) const
{
	const cSimBodyJoint& joint = GetJoint();
	int joint_dim = GetJointDim();
	assert(out_pose.size() == joint_dim);

	cKinTree::eJointType joint_type = joint.GetType();
	if (joint_type == cKinTree::eJointTypeSpherical)
	{
		if (out_pose.squaredNorm() == 0)
		{
			out_pose(0) = 1;
		}
		else
		{
			out_pose.normalize();
		}
	}
}

 