#pragma once

#include "util/json/json.h"
#include "sim/Controller.h"
#include "sim/SimBodyJoint.h"

class cPDController : public cController
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	enum eParam
	{
		eParamJointID,
		eParamKp,
		eParamKd,
		eParamTargetTheta0,
		eParamTargetTheta1,
		eParamTargetTheta2,
		eParamTargetTheta3,
		eParamTargetTheta4,
		eParamTargetTheta5,
		eParamTargetTheta6,
		eParamTargetVel0,
		eParamTargetVel1,
		eParamTargetVel2,
		eParamTargetVel3,
		eParamTargetVel4,
		eParamTargetVel5,
		eParamTargetVel6,
		eParamUseWorldCoord,
		eParamMax
	};
	typedef Eigen::Matrix<double, eParamMax, 1> tParams;

	 
	static bool LoadParams(const Json::Value& root, Eigen::MatrixXd& out_buffer);
	static bool ParsePDParams(const Json::Value& root, tParams& out_params);

	cPDController();
	virtual ~cPDController();

	virtual void Init(cSimCharacter* character, const tParams& params);
	virtual void Clear();
 
	virtual const cSimBodyJoint& GetJoint() const;

	virtual double GetKp() const;
	 
	virtual double GetKd() const;
	 
	virtual void SetTargetTheta(const Eigen::VectorXd& theta);
	 
	virtual bool UseWorldCoord() const;

	virtual void GetTargetTheta(Eigen::VectorXd& out_theta) const;
	virtual void GetTargetVel(Eigen::VectorXd& out_vel) const;

	virtual bool IsActive() const;

protected:
	tParams mParams;
	
	virtual int GetJointDim() const;
	virtual int GetJointID() const;
 
	virtual void PostProcessTargetPose(Eigen::VectorXd& out_pose) const;
	 
};
