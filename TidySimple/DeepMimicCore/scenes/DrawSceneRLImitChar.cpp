#include "DrawSceneRLImitChar.h"
#include "SceneRLImitChar.h"
#include "render/DrawCharacter.h"
#include "render/DrawSimCharacter.h"
#include "render/DrawUtil.h"
#include "sim/RBDUtil.h"

#include "render/DrawUtil.h"
#include "render/DrawGround.h"
#include "render/DrawCharacter.h"
#include "render/DrawUtil.h"
#include "util/MathUtil.h"
#include "sim/SimObj.h"


const double gLinkWidth = 0.025f;
const tVector gLineColor = tVector(0, 0, 0, 1);
const tVector gFilLColor = tVector(0.6f, 0.65f, 0.675f, 1);
const tVector gCamFocus0 = tVector(0, 0.75, 0, 0);
const size_t gInitGroundUpdateCount = std::numeric_limits<size_t>::max();

const std::string gOutputCharFile = "output/char_state.txt";


cDrawRLImitateChar::cDrawRLImitateChar()
{
	mEnableTrace = false;
	mTracerBufferSize = 2000;
	mTracerSamplePeriod = 1 / 60.0;
	ResetUI();
	//-----------------------------------
	mDrawKinChar = false;
}

cDrawRLImitateChar::~cDrawRLImitateChar()
{
}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void cDrawRLImitateChar::Update(double time_elapsed)
{
	cDrawScene::Update(time_elapsed);

	UpdateScene(time_elapsed);
	if (mEnableTrace)
	{
		UpdateTracer(time_elapsed);
	}
	
	UpdateGroundDrawMesh();
	UpdateCamera();
}

const std::shared_ptr<cSceneRLImitateChar>& cDrawRLImitateChar::GetScene() const
{
	return mScene;
}

void cDrawRLImitateChar::MouseClick(int button, int state, double x, double y)
{
	const double ray_max_dist = 1000;
	cDrawScene::MouseClick(button, state, x, y);

	if (button == GLUT_LEFT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			mClickScreenPos = tVector(x, y, 0, 0);
			mDragScreenPos = mClickScreenPos;
			tVector start = mCamera.ScreenToWorldPos(mClickScreenPos);
			tVector dir = mCamera.GetRayCastDir(start);
			tVector end = start + dir * ray_max_dist;

			cWorld::tRayTestResult raytest_result;
			RayTest(start, end, raytest_result);
			HandleRayTest(raytest_result);
		}
		else if (state == GLUT_UP)
		{
			ResetUI();
		}
	}
}

void cDrawRLImitateChar::MouseMove(double x, double y)
{
	cDrawScene::MouseMove(x, y);

	if (ObjectSelected())
	{
		mDragScreenPos = tVector(x, y, 0, 0);
	}
}

 

double cDrawRLImitateChar::GetTime() const
{
	return mScene->GetTime();
}

 


void cDrawRLImitateChar::SetupScene(std::shared_ptr<cSceneRLImitateChar>& out_scene)
{
	out_scene->ParseArgs(mArgParser);
	out_scene->Init();
}

void cDrawRLImitateChar::UpdateScene(double time_elapsed)
{
	mScene->Update(time_elapsed);
}

void cDrawRLImitateChar::ResetScene()
{
	cDrawScene::ResetScene();
	mScene->Reset();
	mTracer.Reset();
	BuildGroundDrawMesh();
}

tVector cDrawRLImitateChar::GetCamTrackPos() const
{
	const auto& character = mScene->GetCharacter();
	return character->CalcCOM();
}

tVector cDrawRLImitateChar::GetCamStillPos() const
{
	const auto& character = mScene->GetCharacter();
	tVector char_pos = character->CalcCOM();

	double cam_w = mCamera.GetWidth();
	double cam_h = mCamera.GetHeight();
	const auto& ground = mScene->GetGround();

	const int num_samples = 16;
	double ground_samples[num_samples] = { 0 };
	const double pad = std::min(0.5, 0.5 * cam_w);

	double avg_h = 0;

	double min_x = char_pos[0];
	double max_x = char_pos[0] + cam_w;

	int num_valid_samples = 0;
	for (int i = 0; i < num_samples; ++i)
	{
		tVector pos = char_pos;
		pos[0] = static_cast<double>(i) / (num_samples - 1) * (max_x - min_x) + min_x;

		bool valid_sample = true;
		double ground_h = ground->SampleHeight(pos, valid_sample);
		if (valid_sample)
		{
			ground_samples[i] = ground_h;
			avg_h += ground_h;
			++num_valid_samples;
		}
	}
	avg_h /= num_valid_samples;

	std::sort(ground_samples, &(ground_samples[num_samples - 1]));
	double med_h = ground_samples[num_samples / 2];
	double min_h = ground_samples[0];

	tVector track_pos = char_pos;
	double target_h = avg_h;
	
	double y_pad = -0.4;
	track_pos[1] = target_h + y_pad + 0.5 * cam_h;

	return track_pos;
}

tVector cDrawRLImitateChar::GetDefaultCamFocus() const
{
	return gCamFocus0;
}

void cDrawRLImitateChar::ResetParams()
{
	cDrawScene::ResetParams();
	ResetUI();
}

void cDrawRLImitateChar::ToggleTrace()
{
	mTracer.Reset();
	mEnableTrace = !mEnableTrace;
	if (mEnableTrace)
	{
		printf("Enable character tracer\n");
	}
	else
	{
		printf("Disable character tracer\n");
	}
}

void cDrawRLImitateChar::InitTracer()
{
	mTraceHandles.clear();
	mTracer.Init(mTracerSamplePeriod);
	AddTraces();
}

void cDrawRLImitateChar::AddTraces()
{
	tVectorArr tracer_cols;
	tracer_cols.push_back(tVector(0, 0, 1, 0.5));
	tracer_cols.push_back(tVector(1, 0, 0, 0.5));
	tracer_cols.push_back(tVector(0, 0.5, 0, 0.5));
	tracer_cols.push_back(tVector(0.75, 0, 0.75, 0.5));
	tracer_cols.push_back(tVector(0, 0.5, 0.5, 0.5));
	tracer_cols.push_back(tVector(0, 0, 0, 0.5));
	
	for (int i = 0; i < mScene->GetNumChars(); ++i)
	{
		AddCharTrace(mScene->GetCharacter(i), tracer_cols);
	}
}

void cDrawRLImitateChar::AddCharTrace(const std::shared_ptr<cSimCharacter>& character,
									const tVectorArr& cols)
{
	cObjTracer::tParams com_params;
	com_params.mObj = character;
	com_params.mColors = cols;
	com_params.mType = cObjTracer::eTraceCOM;
	com_params.mBufferSize = mTracerBufferSize;

	int com_handle = mTracer.AddTrace(com_params);
	mTraceHandles.push_back(com_handle);

	int end_eff_idx = 0;
	for (int i = 0; i < character->GetNumBodyParts(); ++i)
	{
		if (character->IsValidBodyPart(i)
			&& character->IsEndEffector(i))
		{
			cObjTracer::tParams contact_params;
			contact_params.mObj = character->GetBodyPart(i);
			contact_params.mColors = cols;
			contact_params.mType = cObjTracer::eTraceContact;
			contact_params.mShapeIdx = end_eff_idx;
			contact_params.mBufferSize = mTracerBufferSize / 10;

			int contact_handle = mTracer.AddTrace(contact_params);
			mTraceHandles.push_back(contact_handle);
			++end_eff_idx;
		}
	}
}

void cDrawRLImitateChar::UpdateTracer(double time_elapsed)
{
	mTracer.Update(time_elapsed);
}

 
void cDrawRLImitateChar::OutputCharState(const std::string& out_file) const
{
	mScene->OutputCharState(out_file);
}

std::string cDrawRLImitateChar::GetOutputCharFile() const
{
	return gOutputCharFile;
}


void cDrawRLImitateChar::ResetUI()
{
	mClickScreenPos.setZero();
	mDragScreenPos.setZero();
	mSelectObjLocalPos.setZero();
	mSelectedObj = nullptr;
}

void cDrawRLImitateChar::RayTest(const tVector& start, const tVector& end, cWorld::tRayTestResult& out_result)
{
	return GetScene()->RayTest(start, end, out_result);
}

bool cDrawRLImitateChar::ObjectSelected() const
{
	return mSelectedObj != nullptr;
}

void cDrawRLImitateChar::HandleRayTest(const cWorld::tRayTestResult& result)
{
	if (result.mObj != nullptr)
	{
		cSimObj::eType obj_type = result.mObj->GetType();
		if (obj_type == cSimObj::eTypeDynamic)
		{
			mSelectedObj = result.mObj;
			if (ObjectSelected())
			{
				mSelectObjLocalPos = mSelectedObj->WorldToLocalPos(result.mHitPos);
			}
		}
	}
}

 

void cDrawRLImitateChar::DrawObjs() const
{
	 
}

void cDrawRLImitateChar::DrawMisc() const
{
	if (mEnableTrace)
	{
		DrawTrace();
	}
}

 
 

void cDrawRLImitateChar::DrawBodyVel() const
{
	const double lin_vel_scale = 0.1;
	const double ang_vel_scale = 1 / (2 * M_PI);
	for (int i = 0; i < mScene->GetNumChars(); ++i)
	{
		const auto& character = mScene->GetCharacter(i);
		cDrawSimCharacter::DrawBodyVel(*(character.get()), lin_vel_scale, ang_vel_scale, GetVisOffset());
	}
}

void cDrawRLImitateChar::DrawHeading() const
{
	for (int i = 0; i < mScene->GetNumChars(); ++i)
	{
		const auto& character = mScene->GetCharacter(i);
		double arrow_size = 0.2;
		tVector arrow_col = tVector(0, 0.8, 0, 0.5);
		cDrawCharacter::DrawHeading(*(character.get()), arrow_size, arrow_col, GetVisOffset());
	}
}

void cDrawRLImitateChar::DrawTrace() const
{
	cDrawUtil::PushMatrixView();
	cDrawUtil::Translate(GetVisOffset());
	mTracer.Draw();
	cDrawUtil::PopMatrixView();
}


 

void cDrawRLImitateChar::DrawGround() const
{
	const auto& ground = mScene->GetGround();

	tVector focus = mCamera.GetFocus();
	double cam_w = mCamera.GetWidth();
	double cam_h = mCamera.GetHeight();

	tVector ground_col = GetGroundColor();
	cDrawUtil::SetColor(ground_col);
	mGroundDrawMesh->Draw();
}

void cDrawRLImitateChar::DrawCharacters() const
{
	if (mDrawKinChar)
	{
		DrawKinCharacters();
	}
	int num_chars = mScene->GetNumChars();
	for (int i = 0; i < num_chars; ++i)
	{
		const auto& curr_char = mScene->GetCharacter(i);
		DrawCharacter(curr_char);
	}
}

void cDrawRLImitateChar::DrawCharacter(const std::shared_ptr<cSimCharacter>& character) const
{
	const tVector fill_tint = tVector(1, 1, 1, 1);
	bool enable_draw_shape = true;
	cDrawSimCharacter::Draw(*(character.get()), fill_tint, GetLineColor(), enable_draw_shape);
}

void cDrawRLImitateChar::UpdateGroundDrawMesh()
{
	const auto& ground = mScene->GetGround();
	size_t update_count = ground->GetUpdateCount();
	if (update_count != mPrevGroundUpdateCount)
	{
		const auto& ground = mScene->GetGround();
		cDrawGround::BuildMesh(ground.get(), mGroundDrawMesh.get());
		mPrevGroundUpdateCount = ground->GetUpdateCount();
	}
}

void cDrawRLImitateChar::BuildGroundDrawMesh()
{
	/*mGroundDrawMesh = std::unique_ptr<cDrawMesh>(new cDrawMesh());
	mGroundDrawMesh->Init(1);

	const auto& ground = mScene->GetGround();
	cDrawGround::BuildMesh(ground.get(), mGroundDrawMesh.get());
	mPrevGroundUpdateCount = ground->GetUpdateCount();*/
}

void cDrawRLImitateChar::DrawInfo() const
{
	DrawPoliInfo();
}

void cDrawRLImitateChar::DrawPoliInfo() const
{
	const auto& character = mScene->GetCharacter();
	const cDeepMimicCharController* trl_ctrl = dynamic_cast<cDeepMimicCharController*>(character->GetController().get());
	if (trl_ctrl != nullptr)
	{
		cDrawSimCharacter::DrawInfoValLog(trl_ctrl->GetValLog(), mCamera);
	}
}
//###########################################################
int cDrawRLImitateChar::GetNumAgents() const
{
	return GetRLScene()->GetNumAgents();
}

bool cDrawRLImitateChar::NeedNewAction(int agent_id) const
{
	return GetRLScene()->NeedNewAction(agent_id);
}

void cDrawRLImitateChar::RecordState(int agent_id, Eigen::VectorXd& out_state) const
{
	GetRLScene()->RecordState(agent_id, out_state);
}

void cDrawRLImitateChar::RecordGoal(int agent_id, Eigen::VectorXd& out_goal) const
{
	GetRLScene()->RecordGoal(agent_id, out_goal);
}

void cDrawRLImitateChar::SetAction(int agent_id, const Eigen::VectorXd& action)
{
	GetRLScene()->SetAction(agent_id, action);
}

eActionSpace cDrawRLImitateChar::GetActionSpace(int agent_id) const
{
	return GetRLScene()->GetActionSpace(agent_id);
}

int cDrawRLImitateChar::GetStateSize(int agent_id) const
{
	return GetRLScene()->GetStateSize(agent_id);
}

int cDrawRLImitateChar::GetGoalSize(int agent_id) const
{
	return GetRLScene()->GetGoalSize(agent_id);
}

int cDrawRLImitateChar::GetActionSize(int agent_id) const
{
	return GetRLScene()->GetActionSize(agent_id);
}

int cDrawRLImitateChar::GetNumActions(int agent_id) const
{
	return GetRLScene()->GetNumActions(agent_id);
}

void cDrawRLImitateChar::BuildStateOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const
{
	GetRLScene()->BuildStateOffsetScale(agent_id, out_offset, out_scale);
}

void cDrawRLImitateChar::BuildGoalOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const
{
	GetRLScene()->BuildGoalOffsetScale(agent_id, out_offset, out_scale);
}

void cDrawRLImitateChar::BuildActionOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const
{
	GetRLScene()->BuildActionOffsetScale(agent_id, out_offset, out_scale);
}

void cDrawRLImitateChar::BuildActionBounds(int agent_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max) const
{
	GetRLScene()->BuildActionBounds(agent_id, out_min, out_max);
}

void cDrawRLImitateChar::BuildStateNormGroups(int agent_id, Eigen::VectorXi& out_groups) const
{
	GetRLScene()->BuildStateNormGroups(agent_id, out_groups);
}

void cDrawRLImitateChar::BuildGoalNormGroups(int agent_id, Eigen::VectorXi& out_groups) const
{
	GetRLScene()->BuildGoalNormGroups(agent_id, out_groups);
}

double cDrawRLImitateChar::CalcReward(int agent_id) const
{
	return GetRLScene()->CalcReward(agent_id);
}

double cDrawRLImitateChar::GetRewardMin(int agent_id) const
{
	return GetRLScene()->GetRewardMin(agent_id);
}

double cDrawRLImitateChar::GetRewardMax(int agent_id) const
{
	return GetRLScene()->GetRewardMax(agent_id);
}

double cDrawRLImitateChar::GetRewardFail(int agent_id)
{
	return GetRLScene()->GetRewardFail(agent_id);
}

double cDrawRLImitateChar::GetRewardSucc(int agent_id)
{
	return GetRLScene()->GetRewardSucc(agent_id);
}

 
cDrawRLImitateChar::eTerminate cDrawRLImitateChar::CheckTerminate(int agent_id) const
{
	return GetRLScene()->CheckTerminate(agent_id);
}

 
void cDrawRLImitateChar::SetMode(eMode mode)
{
	return GetRLScene()->SetMode(mode);
}

void cDrawRLImitateChar::SetSampleCount(int count)
{
	return GetRLScene()->SetSampleCount(count);
}

void cDrawRLImitateChar::LogVal(int agent_id, double val)
{
	GetRLScene()->LogVal(agent_id, val);
}

/*std::string cDrawRLScene::GetName() const
{
	return GetRLScene()->GetName();
}*/
//###########################################################

void cDrawRLImitateChar::Init()
{
	BuildScene(mScene);
	SetupScene(mScene);

	cDrawScene::Init();

	InitTracer();
	BuildGroundDrawMesh();
	//---------------------------------------------------
	//cDrawSceneSimChar::Init();
	//cRLScene::Init(); 
	cScene::Init();
	cDrawScene::Init();
}

void cDrawRLImitateChar::Clear()
{
	//cDrawSceneSimChar::Clear();
	//cDrawRLScene::Clear();
	//cDrawSceneSimChar::Clear();
	cDrawScene::Clear();
	mScene->Clear();
	mTracer.Clear();
	mPrevGroundUpdateCount = gInitGroundUpdateCount;
	
	cScene::Clear();
}

bool cDrawRLImitateChar::IsEpisodeEnd() const
{
	return GetRLScene()->IsEpisodeEnd(); 
}

bool cDrawRLImitateChar::CheckValidEpisode() const
{
	return GetRLScene()->CheckValidEpisode();
}

void cDrawRLImitateChar::Keyboard(unsigned char key, double device_x, double device_y)
{
 
    cDrawScene::Keyboard(key, device_x, device_y);

	switch (key)
	{
	case 's':
		OutputCharState(GetOutputCharFile());
		break;
	case 'y':
		ToggleTrace();
		break;
	default:
		break;
	}
	switch (key)
	{
	case 'k':
		DrawKinChar(!mDrawKinChar);
		break;
	default:
		break;
	}
}

void cDrawRLImitateChar::DrawKinChar(bool enable)
{
	mDrawKinChar = enable;
	if (mDrawKinChar)
	{
		printf("Enabled draw kinematic character\n");
	}
	else
	{
		printf("Disabled draw kinematic character\n");
	}
}

std::string cDrawRLImitateChar::GetName() const
{
	//return cDrawRLScene::GetName();
	return GetRLScene()->GetName();
}/**/

cScene* cDrawRLImitateChar::GetRLScene() const
{
	return dynamic_cast<cScene*>(mScene.get());
}

void cDrawRLImitateChar::BuildScene(std::shared_ptr<cSceneRLImitateChar>& out_scene) const
{
	out_scene = std::shared_ptr<cSceneRLImitateChar>(new cSceneRLImitateChar());
}

 

void cDrawRLImitateChar::DrawKinCharacters() const
{
	const auto& kin_char = GetKinChar();
	DrawKinCharacter(kin_char);
}
void cDrawRLImitateChar::DrawKinCharacter(const std::shared_ptr<cKinCharacter>& kin_char) const
{
	cDrawCharacter::Draw(*kin_char, gLinkWidth, gFilLColor, gLineColor);
}

const std::shared_ptr<cKinCharacter>& cDrawRLImitateChar::GetKinChar() const
{
	const cSceneRLImitateChar* scene = dynamic_cast<const cSceneRLImitateChar*>(mScene.get());
	return scene->GetKinChar();
}
