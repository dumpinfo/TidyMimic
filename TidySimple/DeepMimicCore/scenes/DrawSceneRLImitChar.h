#pragma once

#include "Scene.h"
#include "DrawScene.h"
 
#include "anim/KinCharacter.h"
#include "scenes/DrawScene.h"
#include "scenes/SceneRLImitChar.h"
#include "sim/ObjTracer.h"
 
 
class cDrawRLImitateChar :virtual public cDrawScene 
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	virtual const std::shared_ptr<cSceneRLImitateChar>& GetScene() const;
	
	virtual void MouseClick(int button, int state, double x, double y);
	virtual void MouseMove(double x, double y);
	virtual void Keyboard(unsigned char key, double device_x, double device_y);
	
	virtual double GetTime() const;
	

	virtual std::string GetName() const;
    void  Update(double time_elapsed);
 

	std::shared_ptr<cSceneRLImitateChar> mScene;

	std::unique_ptr<cDrawMesh> mGroundDrawMesh;
	size_t mPrevGroundUpdateCount;

	bool mEnableTrace;
	cObjTracer mTracer;
	std::vector<int> mTraceHandles;

	// UI stuff
	tVector mClickScreenPos;
	tVector mDragScreenPos;
	tVector mSelectObjLocalPos;
	cSimObj* mSelectedObj;

	int mTracerBufferSize;
	double mTracerSamplePeriod;

 
	virtual void SetupScene(std::shared_ptr<cSceneRLImitateChar>& out_scene);
	virtual void UpdateScene(double time_elapsed);
	virtual void ResetScene();

	virtual tVector GetCamTrackPos() const;
	virtual tVector GetCamStillPos() const;
	virtual tVector GetDefaultCamFocus() const;
	
	virtual void ResetParams();

	virtual void ToggleTrace();
	virtual void InitTracer();
	virtual void AddTraces();
	virtual void AddCharTrace(const std::shared_ptr<cSimCharacter>& character,
								const tVectorArr& cols);
	virtual void UpdateTracer(double time_elapsed);

 

	virtual void OutputCharState(const std::string& out_file) const;
	virtual std::string GetOutputCharFile() const;

	virtual void ResetUI();
	virtual void RayTest(const tVector& start, const tVector& end, cWorld::tRayTestResult& out_result);
	virtual bool ObjectSelected() const;
	virtual void HandleRayTest(const cWorld::tRayTestResult& result);
	 

	virtual void DrawObjs() const;
	virtual void DrawMisc() const;

 
	virtual void DrawBodyVel() const;
	virtual void DrawHeading() const;
	virtual void DrawTrace() const;

	 

	virtual void DrawGround() const;
	virtual void DrawCharacters() const;
	virtual void DrawCharacter(const std::shared_ptr<cSimCharacter>& character) const;
	virtual void UpdateGroundDrawMesh();
	virtual void BuildGroundDrawMesh();

	virtual void DrawInfo() const;
	virtual void DrawPoliInfo() const;
//===================================================================
	cDrawRLImitateChar();
	virtual ~cDrawRLImitateChar();

	virtual void Init();
	virtual void Clear();
	virtual bool IsEpisodeEnd() const;
	virtual bool CheckValidEpisode() const;
	virtual void DrawKinChar(bool enable);
//==========================================================
 
	virtual int GetNumAgents() const;
	virtual bool NeedNewAction(int agent_id) const;
	virtual void RecordState(int agent_id, Eigen::VectorXd& out_state) const;
	virtual void RecordGoal(int agent_id, Eigen::VectorXd& out_goal) const;
	virtual void SetAction(int agent_id, const Eigen::VectorXd& action);

	virtual eActionSpace GetActionSpace(int agent_id) const;
	virtual int GetStateSize(int agent_id) const;
	virtual int GetGoalSize(int agent_id) const;
	virtual int GetActionSize(int agent_id) const;
	virtual int GetNumActions(int agent_id) const;

	virtual void BuildStateOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const;
	virtual void BuildGoalOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const;
	virtual void BuildActionOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const;
	virtual void BuildActionBounds(int agent_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max) const;

	virtual void BuildStateNormGroups(int agent_id, Eigen::VectorXi& out_groups) const;
	virtual void BuildGoalNormGroups(int agent_id, Eigen::VectorXi& out_groups) const;

	virtual double CalcReward(int agent_id) const;
	virtual double GetRewardMin(int agent_id) const;
	virtual double GetRewardMax(int agent_id) const;
	virtual double GetRewardFail(int agent_id);
	virtual double GetRewardSucc(int agent_id);

	 
	virtual eTerminate CheckTerminate(int agent_id) const;
 
	virtual void SetMode(eMode mode);

	virtual void SetSampleCount(int count);
	virtual void LogVal(int agent_id, double val);

	 
protected:

	bool mDrawKinChar;

	virtual cScene* GetRLScene() const;

	virtual void BuildScene(std::shared_ptr<cSceneRLImitateChar>& out_scene) const;
	virtual void DrawKinCharacters() const;
	virtual void DrawKinCharacter(const std::shared_ptr<cKinCharacter>& kin_char) const;
	
	virtual const std::shared_ptr<cKinCharacter>& GetKinChar() const;
};
