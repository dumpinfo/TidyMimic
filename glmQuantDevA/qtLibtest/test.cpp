#include <stdio.h>
#include <stdlib.h>
#include "quaternion.h"


int main(int args, char*argv[])
{
	RBquaternionT qtest;// (0.77626, -0.03989, -0.62535, 0.06902);
	
	qtest.w = 0.77626;
	qtest.x = -0.03989;
	qtest.y = -0.62535;
	qtest.z = 0.06902;
	RBquaternionT Qvlg = quaternion_log(qtest);
	printf("%8.5f ,%8.5f ,%8.5f ,%8.5f\n", 
		Qvlg.w,
		Qvlg.x,
		Qvlg.y,
		Qvlg.z);
	return 1;
}