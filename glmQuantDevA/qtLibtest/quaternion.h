#ifndef __QUATERNION_H__
#define __QUATERNION_H__

#define M_PI  3.1415926535897932384626433832795

#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define _QUATERNION_EPS 1e-14

 

  typedef struct {
    double w;
    double x;
    double y;
    double z;
  } RBquaternionT;

  // Constructor-ish
  RBquaternionT quaternion_create_from_spherical_coords(double vartheta, double varphi);
  RBquaternionT quaternion_create_from_euler_angles(double alpha, double beta, double gamma);

  // Unary bool returners
  static inline int quaternion_isnan(RBquaternionT q) {
    return isnan(q.w) || isnan(q.x) || isnan(q.y) || isnan(q.z);
  }
  static inline int quaternion_nonzero(RBquaternionT q) {
    if(quaternion_isnan(q)) { return 1; }
    return ! (q.w == 0 && q.x == 0 && q.y == 0 && q.z == 0);
  }
  static inline int quaternion_isinf(RBquaternionT q) {
    return isinf(q.w) || isinf(q.x) || isinf(q.y) || isinf(q.z);
  }
  static inline int quaternion_isfinite(RBquaternionT q) {
    return isfinite(q.w) && isfinite(q.x) && isfinite(q.y) && isfinite(q.z);
  }

  // Binary bool returners
  static inline int quaternion_equal(RBquaternionT q1, RBquaternionT q2) {
    return
      !quaternion_isnan(q1) &&
      !quaternion_isnan(q2) &&
      q1.w == q2.w &&
      q1.x == q2.x &&
      q1.y == q2.y &&
      q1.z == q2.z;
  }
  static inline int quaternion_not_equal(RBquaternionT q1, RBquaternionT q2) {
    return !quaternion_equal(q1, q2);
  }
  static inline int quaternion_less(RBquaternionT q1, RBquaternionT q2) {
    return
      (!quaternion_isnan(q1) && !quaternion_isnan(q2))
      &&
      (q1.w != q2.w ? q1.w < q2.w :
       q1.x != q2.x ? q1.x < q2.x :
       q1.y != q2.y ? q1.y < q2.y :
       q1.z != q2.z ? q1.z < q2.z : 0);
  }
  static inline int quaternion_greater(RBquaternionT q1, RBquaternionT q2) {
    return
      (!quaternion_isnan(q1) && !quaternion_isnan(q2))
      &&
      (q1.w != q2.w ? q1.w > q2.w :
       q1.x != q2.x ? q1.x > q2.x :
       q1.y != q2.y ? q1.y > q2.y :
       q1.z != q2.z ? q1.z > q2.z : 0);
  }
  static inline int quaternion_less_equal(RBquaternionT q1, RBquaternionT q2) {
    return
      (!quaternion_isnan(q1) && !quaternion_isnan(q2))
      &&
      (q1.w != q2.w ? q1.w < q2.w :
       q1.x != q2.x ? q1.x < q2.x :
       q1.y != q2.y ? q1.y < q2.y :
       q1.z != q2.z ? q1.z < q2.z : 1);
    // Note that the final possibility is 1, whereas in
    // `quaternion_less` it was 0.  This distinction correctly
    // accounts for equality.
  }
  static inline int quaternion_greater_equal(RBquaternionT q1, RBquaternionT q2) {
    return
      (!quaternion_isnan(q1) && !quaternion_isnan(q2))
      &&
      (q1.w != q2.w ? q1.w > q2.w :
       q1.x != q2.x ? q1.x > q2.x :
       q1.y != q2.y ? q1.y > q2.y :
       q1.z != q2.z ? q1.z > q2.z : 1);
    // Note that the final possibility is 1, whereas in
    // `quaternion_greater` it was 0.  This distinction correctly
    // accounts for equality.
  }

  // Unary float returners
  RBquaternionT quaternion_log(RBquaternionT q); // Pre-declare; declared again below, in its rightful place
  static inline double quaternion_norm(RBquaternionT q) {
    return q.w*q.w + q.x*q.x + q.y*q.y + q.z*q.z;
  }
  static inline double quaternion_absolute(RBquaternionT q) {
    return sqrt(q.w*q.w + q.x*q.x + q.y*q.y + q.z*q.z);
  }
  static inline double quaternion_angle(RBquaternionT q) {
    return 2 * quaternion_absolute( quaternion_log( q ) );
  }

  // Unary quaternion returners
  RBquaternionT quaternion_sqrt(RBquaternionT q);
  RBquaternionT quaternion_log(RBquaternionT q);
  RBquaternionT quaternion_exp(RBquaternionT q);
  static inline RBquaternionT quaternion_normalized(RBquaternionT q) {
    double q_abs = quaternion_absolute(q);
    RBquaternionT r = {q.w/q_abs, q.x/q_abs, q.y/q_abs, q.z/q_abs};
    return r;
  }
  static inline RBquaternionT quaternion_x_parity_conjugate(RBquaternionT q) {
    RBquaternionT r = {q.w, q.x, -q.y, -q.z};
    return r;
  }
  static inline RBquaternionT quaternion_x_parity_symmetric_part(RBquaternionT q) {
    RBquaternionT r = {q.w, q.x, 0.0, 0.0};
    return r;
  }
  static inline RBquaternionT quaternion_x_parity_antisymmetric_part(RBquaternionT q) {
    RBquaternionT r = {0.0, 0.0, q.y, q.z};
    return r;
  }
  static inline RBquaternionT quaternion_y_parity_conjugate(RBquaternionT q) {
    RBquaternionT r = {q.w, -q.x, q.y, -q.z};
    return r;
  }
  static inline RBquaternionT quaternion_y_parity_symmetric_part(RBquaternionT q) {
    RBquaternionT r = {q.w, 0.0, q.y, 0.0};
    return r;
  }
  static inline RBquaternionT quaternion_y_parity_antisymmetric_part(RBquaternionT q) {
    RBquaternionT r = {0.0, q.x, 0.0, q.z};
    return r;
  }
  static inline RBquaternionT quaternion_z_parity_conjugate(RBquaternionT q) {
    RBquaternionT r = {q.w, -q.x, -q.y, q.z};
    return r;
  }
  static inline RBquaternionT quaternion_z_parity_symmetric_part(RBquaternionT q) {
    RBquaternionT r = {q.w, 0.0, 0.0, q.z};
    return r;
  }
  static inline RBquaternionT quaternion_z_parity_antisymmetric_part(RBquaternionT q) {
    RBquaternionT r = {0.0, q.x, q.y, 0.0};
    return r;
  }
  static inline RBquaternionT quaternion_parity_conjugate(RBquaternionT q) {
    RBquaternionT r = {q.w, q.x, q.y, q.z};
    return r;
  }
  static inline RBquaternionT quaternion_parity_symmetric_part(RBquaternionT q) {
    RBquaternionT r = {q.w, q.x, q.y, q.z};
    return r;
  }
  static inline RBquaternionT quaternion_parity_antisymmetric_part(RBquaternionT q) {
    RBquaternionT r = {0.0, 0.0, 0.0, 0.0};
    (void) q; // This q parameter is unused, but here for consistency with similar functions
    return r;
  }
  static inline RBquaternionT quaternion_negative(RBquaternionT q) {
    RBquaternionT r = {-q.w, -q.x, -q.y, -q.z};
    return r;
  }
  static inline RBquaternionT quaternion_conjugate(RBquaternionT q) {
    RBquaternionT r = {q.w, -q.x, -q.y, -q.z};
    return r;
  }
  static inline RBquaternionT quaternion_inverse(RBquaternionT q) {
    double norm = quaternion_norm(q);
    RBquaternionT r = {q.w/norm, -q.x/norm, -q.y/norm, -q.z/norm};
    return r;
  }

  // Quaternion-quaternion binary quaternion returners
  static inline RBquaternionT quaternion_copysign(RBquaternionT q1, RBquaternionT q2) {
    RBquaternionT r = {
      copysign(q1.w, q2.w),
      copysign(q1.x, q2.x),
      copysign(q1.y, q2.y),
      copysign(q1.z, q2.z)
    };
    return r;
  }

  // Quaternion-vector binary void returner
//  static inline void _cross(double a[], double b[], double c[]) {
//    c[0] = a[1]*b[2] - a[2]*b[1];
//    c[1] = a[2]*b[0] - a[0]*b[2];
//    c[2] = a[0]*b[1] - a[1]*b[0];
//    return;
//  }
//  static inline void _cross_times_scalar(double s, double a[], double b[], double c[]) {
//    c[0] = s*(a[1]*b[2] - a[2]*b[1]);
//    c[1] = s*(a[2]*b[0] - a[0]*b[2]);
//    c[2] = s*(a[0]*b[1] - a[1]*b[0]);
//    return;
//  }
  static inline void _sv_plus_rxv(RBquaternionT q, double v[], double w[]) {
    w[0] = q.w * v[0] + q.y*v[2] - q.z*v[1];
    w[1] = q.w * v[1] + q.z*v[0] - q.x*v[2];
    w[2] = q.w * v[2] + q.x*v[1] - q.y*v[0];
    return;
  }
  static inline void _v_plus_2rxvprime_over_m(RBquaternionT q, double v[], double w[], double two_over_m, double vprime[]) {
    vprime[0] = v[0] + two_over_m * (q.y*w[2] - q.z*w[1]);
    vprime[1] = v[1] + two_over_m * (q.z*w[0] - q.x*w[2]);
    vprime[2] = v[2] + two_over_m * (q.x*w[1] - q.y*w[0]);
    return;
  }
  static inline void quaternion_rotate_vector(RBquaternionT q, double v[], double vprime[]) {
    // The most efficient formula I know of for rotating a vector by a quaternion is
    //     v' = v + 2 * r x (s * v + r x v) / m
    // where x represents the cross product, s and r are the scalar and vector parts of the quaternion, respectively,
    // and m is the sum of the squares of the components of the quaternion.  This requires 22 multiplications and
    // 14 additions, as opposed to 32 and 24 for naive application of `q*v*q.conj()`.  In this function, I will further
    // reduce the operation count to 18 and 12 by skipping the normalization by `m`.  The full version will be
    // implemented in another function.
    double w[3];
    _sv_plus_rxv(q, v, w);
    _v_plus_2rxvprime_over_m(q, v, w, 2, vprime);
    return;
  }
  static inline void quaternion_rotate_vector_and_normalize(RBquaternionT q, double v[], double vprime[]) {
    // This applies the algorithm described above, but also includes normalization of the quaternion.
    double w[3];
    double m = q.x*q.x+q.y*q.y+q.z*q.z;
    _sv_plus_rxv(q, v, w);
    _v_plus_2rxvprime_over_m(q, v, w, 2/m, vprime);
    return;
  }

  // Quaternion-quaternion/quaternion-scalar binary quaternion returners
  static inline RBquaternionT quaternion_add(RBquaternionT q1, RBquaternionT q2) {
    RBquaternionT r = {
      q1.w+q2.w,
      q1.x+q2.x,
      q1.y+q2.y,
      q1.z+q2.z,
    };
    return r;
  }
  static inline void quaternion_inplace_add(RBquaternionT* q1, RBquaternionT q2) {
    q1->w += q2.w;
    q1->x += q2.x;
    q1->y += q2.y;
    q1->z += q2.z;
    return;
  }
  static inline RBquaternionT quaternion_scalar_add(double s, RBquaternionT q) {
    RBquaternionT r = {s+q.w, q.x, q.y, q.z};
    return r;
  }
  static inline void quaternion_inplace_scalar_add(double s, RBquaternionT* q) {
    q->w += s;
    return;
  }
  static inline RBquaternionT quaternion_add_scalar(RBquaternionT q, double s) {
    RBquaternionT r = {s+q.w, q.x, q.y, q.z};
    return r;
  }
  static inline void quaternion_inplace_add_scalar(RBquaternionT* q, double s) {
    q->w += s;
    return;
  }
  static inline RBquaternionT quaternion_subtract(RBquaternionT q1, RBquaternionT q2) {
    RBquaternionT r = {
      q1.w-q2.w,
      q1.x-q2.x,
      q1.y-q2.y,
      q1.z-q2.z,
    };
    return r;
  }
  static inline void quaternion_inplace_subtract(RBquaternionT* q1, RBquaternionT q2) {
    q1->w -= q2.w;
    q1->x -= q2.x;
    q1->y -= q2.y;
    q1->z -= q2.z;
    return;
  }
  static inline RBquaternionT quaternion_scalar_subtract(double s, RBquaternionT q) {
    RBquaternionT r = {s-q.w, -q.x, -q.y, -q.z};
    return r;
  }
  static inline RBquaternionT quaternion_subtract_scalar(RBquaternionT q, double s) {
    RBquaternionT r = {q.w-s, q.x, q.y, q.z};
    return r;
  }
  static inline void quaternion_inplace_subtract_scalar(RBquaternionT* q, double s) {
    q->w -= s;
    return;
  }
  static inline RBquaternionT quaternion_multiply(RBquaternionT q1, RBquaternionT q2) {
    RBquaternionT r = {
      q1.w*q2.w - q1.x*q2.x - q1.y*q2.y - q1.z*q2.z,
      q1.w*q2.x + q1.x*q2.w + q1.y*q2.z - q1.z*q2.y,
      q1.w*q2.y - q1.x*q2.z + q1.y*q2.w + q1.z*q2.x,
      q1.w*q2.z + q1.x*q2.y - q1.y*q2.x + q1.z*q2.w,
    };
    return r;
  }
  static inline RBquaternionT quaternion_square(RBquaternionT q) {
    return quaternion_multiply(q, q);
  }
  static inline void quaternion_inplace_multiply(RBquaternionT* q1a, RBquaternionT q2) {
    RBquaternionT q1 = {q1a->w, q1a->x, q1a->y, q1a->z};
    q1a->w = q1.w*q2.w - q1.x*q2.x - q1.y*q2.y - q1.z*q2.z;
    q1a->x = q1.w*q2.x + q1.x*q2.w + q1.y*q2.z - q1.z*q2.y;
    q1a->y = q1.w*q2.y - q1.x*q2.z + q1.y*q2.w + q1.z*q2.x;
    q1a->z = q1.w*q2.z + q1.x*q2.y - q1.y*q2.x + q1.z*q2.w;
    return;
  }
  static inline RBquaternionT quaternion_scalar_multiply(double s, RBquaternionT q) {
    RBquaternionT r = {s*q.w, s*q.x, s*q.y, s*q.z};
    return r;
  }
  static inline void quaternion_inplace_scalar_multiply(double s, RBquaternionT* q) {
    q->w *= s;
    q->x *= s;
    q->y *= s;
    q->z *= s;
    return;
  }
  static inline RBquaternionT quaternion_multiply_scalar(RBquaternionT q, double s) {
    RBquaternionT r = {s*q.w, s*q.x, s*q.y, s*q.z};
    return r;
  }
  static inline void quaternion_inplace_multiply_scalar(RBquaternionT* q, double s) {
    q->w *= s;
    q->x *= s;
    q->y *= s;
    q->z *= s;
    return;
  }
  static inline RBquaternionT quaternion_divide(RBquaternionT q1, RBquaternionT q2) {
    double q2norm = q2.w*q2.w + q2.x*q2.x + q2.y*q2.y + q2.z*q2.z;
    RBquaternionT r = {
      (  q1.w*q2.w + q1.x*q2.x + q1.y*q2.y + q1.z*q2.z) / q2norm,
      (- q1.w*q2.x + q1.x*q2.w - q1.y*q2.z + q1.z*q2.y) / q2norm,
      (- q1.w*q2.y + q1.x*q2.z + q1.y*q2.w - q1.z*q2.x) / q2norm,
      (- q1.w*q2.z - q1.x*q2.y + q1.y*q2.x + q1.z*q2.w) / q2norm
    };
    return r;
  }
  static inline void quaternion_inplace_divide(RBquaternionT* q1a, RBquaternionT q2) {
    double q2norm;
    RBquaternionT q1 = *q1a;
    q2norm = q2.w*q2.w + q2.x*q2.x + q2.y*q2.y + q2.z*q2.z;
    q1a->w = ( q1.w*q2.w + q1.x*q2.x + q1.y*q2.y + q1.z*q2.z)/q2norm;
    q1a->x = (-q1.w*q2.x + q1.x*q2.w - q1.y*q2.z + q1.z*q2.y)/q2norm;
    q1a->y = (-q1.w*q2.y + q1.x*q2.z + q1.y*q2.w - q1.z*q2.x)/q2norm;
    q1a->z = (-q1.w*q2.z - q1.x*q2.y + q1.y*q2.x + q1.z*q2.w)/q2norm;
    return;
  }
  static inline RBquaternionT quaternion_scalar_divide(double s, RBquaternionT q) {
    double qnorm = q.w*q.w + q.x*q.x + q.y*q.y + q.z*q.z;
    RBquaternionT r = {
      ( s*q.w) / qnorm,
      (-s*q.x) / qnorm,
      (-s*q.y) / qnorm,
      (-s*q.z) / qnorm
    };
    return r;
  }
  /* The following function is impossible, but listed for completeness: */
  /* static inline void quaternion_inplace_scalar_divide(double* sa, quaternion q2) { } */
  static inline RBquaternionT quaternion_divide_scalar(RBquaternionT q, double s) {
    RBquaternionT r = {q.w/s, q.x/s, q.y/s, q.z/s};
    return r;
  }
  static inline void quaternion_inplace_divide_scalar(RBquaternionT* q, double s) {
    q->w /= s;
    q->x /= s;
    q->y /= s;
    q->z /= s;
    return;
  }
  static inline RBquaternionT quaternion_power(RBquaternionT q, RBquaternionT p) {
    /* Note that the following is just my chosen definition of the power. */
    /* Other definitions may disagree due to non-commutativity. */
    if(! quaternion_nonzero(q)) { /* log(q)=-inf */
      if(! quaternion_nonzero(p)) {
        RBquaternionT r = {1.0, 0.0, 0.0, 0.0}; /* consistent with python */
        return r;
      } else {
        RBquaternionT r = {0.0, 0.0, 0.0, 0.0}; /* consistent with python */
        return r;
      }
    }
    return quaternion_exp(quaternion_multiply(quaternion_log(q), p));
  }
  static inline void quaternion_inplace_power(RBquaternionT* q1, RBquaternionT q2) {
    /* Not overly useful as an in-place operator, but here for completeness. */
    RBquaternionT q3 = quaternion_power(*q1,q2);
    *q1 = q3;
    return;
  }
  RBquaternionT quaternion_scalar_power(double s, RBquaternionT q);
  static inline void quaternion_inplace_scalar_power(double s, RBquaternionT* q) {
    /* Not overly useful as an in-place operator, but here for completeness. */
    RBquaternionT q2 = quaternion_scalar_power(s, *q);
    *q = q2;
    return;
  }
  static inline RBquaternionT quaternion_power_scalar(RBquaternionT q, double s) {
    /* Unlike the quaternion^quaternion power, this is unambiguous. */
    if(! quaternion_nonzero(q)) { /* log(q)=-inf */
      if(s==0) {
        RBquaternionT r = {1.0, 0.0, 0.0, 0.0}; /* consistent with python */
        return r;
      } else {
        RBquaternionT r = {0.0, 0.0, 0.0, 0.0}; /* consistent with python */
        return r;
      }
    }
    return quaternion_exp(quaternion_multiply_scalar(quaternion_log(q), s));
  }
  static inline void quaternion_inplace_power_scalar(RBquaternionT* q, double s) {
    /* Not overly useful as an in-place operator, but here for completeness. */
    RBquaternionT q2 = quaternion_power_scalar(*q, s);
    *q = q2;
    return;
  }

  // Associated functions
  static inline double quaternion_rotor_intrinsic_distance(RBquaternionT q1, RBquaternionT q2) {
    return 2*quaternion_absolute(quaternion_log(quaternion_divide(q1,q2)));
  }
  static inline double quaternion_rotor_chordal_distance(RBquaternionT q1, RBquaternionT q2) {
    return quaternion_absolute(quaternion_subtract(q1,q2));
  }
  static inline double quaternion_rotation_intrinsic_distance(RBquaternionT q1, RBquaternionT q2) {
    if(quaternion_rotor_chordal_distance(q1,q2)<=1.414213562373096) {
      return 2*quaternion_absolute(quaternion_log(quaternion_divide(q1,q2)));
    } else {
      return 2*quaternion_absolute(quaternion_log(quaternion_divide(q1,quaternion_negative(q2))));
    }
  }
  static inline double quaternion_rotation_chordal_distance(RBquaternionT q1, RBquaternionT q2) {
    if(quaternion_rotor_chordal_distance(q1,q2)<=1.414213562373096) {
      return quaternion_absolute(quaternion_subtract(q1,q2));
    } else {
      return quaternion_absolute(quaternion_add(q1,q2));
    }
  }
  static inline RBquaternionT slerp(RBquaternionT q1, RBquaternionT q2, double tau) {
    if(quaternion_rotor_chordal_distance(q1,q2)<=1.414213562373096) {
      return quaternion_multiply( quaternion_power_scalar(quaternion_divide(q2,q1), tau), q1);
    } else {
      return quaternion_multiply( quaternion_power_scalar(quaternion_divide(quaternion_negative(q2),q1), tau), q1);
    }
  }
  static inline RBquaternionT squad_evaluate(double tau_i, RBquaternionT q_i, RBquaternionT a_i, RBquaternionT b_ip1, RBquaternionT q_ip1) {
    return slerp(slerp(q_i, q_ip1, tau_i),
                 slerp(a_i, b_ip1, tau_i),
                 2*tau_i*(1-tau_i));
  }


  RBquaternionT
	  inline quaternion_create_from_spherical_coords(double vartheta, double varphi) {
	  double ct = cos(vartheta / 2.);
	  double cp = cos(varphi / 2.);
	  double st = sin(vartheta / 2.);
	  double sp = sin(varphi / 2.);
	  RBquaternionT r = { cp*ct, -sp * st, st*cp, sp*ct };
	  return r;
  }

  RBquaternionT
	  inline quaternion_create_from_euler_angles(double alpha, double beta, double gamma) {
	  double ca = cos(alpha / 2.);
	  double cb = cos(beta / 2.);
	  double cc = cos(gamma / 2.);
	  double sa = sin(alpha / 2.);
	  double sb = sin(beta / 2.);
	  double sc = sin(gamma / 2.);
	  RBquaternionT r = { ca*cb*cc - sa * cb*sc, ca*sb*sc - sa * sb*cc, ca*sb*cc + sa * sb*sc, sa*cb*cc + ca * cb*sc };
	  return r;
  }

  RBquaternionT
	  inline quaternion_sqrt(RBquaternionT q)
  {
	  double absolute = quaternion_norm(q);  // pre-square-root
	  if (absolute <= DBL_MIN) {
		  RBquaternionT r = { 0.0, 0.0, 0.0, 0.0 };
		  return r;
	  }
	  absolute = sqrt(absolute);
	  if (fabs(absolute + q.w) < _QUATERNION_EPS*absolute) {
		  RBquaternionT r = { 0.0, sqrt(absolute), 0.0, 0.0 };
		  return r;
	  }
	  else {
		  double c = sqrt(0.5 / (absolute + q.w));
		  RBquaternionT r = { (absolute + q.w)*c, q.x*c, q.y*c, q.z*c };
		  return r;
	  }
  }

  RBquaternionT
	  inline quaternion_log(RBquaternionT q)
  {
	  double b = sqrt(q.x*q.x + q.y*q.y + q.z*q.z);
	  if (fabs(b) <= _QUATERNION_EPS * fabs(q.w)) {
		  if (q.w < 0.0) {
			  // fprintf(stderr, "Input quaternion(%.15g, %.15g, %.15g, %.15g) has no unique logarithm; returning one arbitrarily.", q.w, q.x, q.y, q.z);
			  if (fabs(q.w + 1) > _QUATERNION_EPS) {
				  RBquaternionT r = { log(-q.w), M_PI, 0., 0. };
				  return r;
			  }
			  else {
				  RBquaternionT r = { 0., M_PI, 0., 0. };
				  return r;
			  }
		  }
		  else {
			  RBquaternionT r = { log(q.w), 0., 0., 0. };
			  return r;
		  }
	  }
	  else {
		  double v = atan2(b, q.w);
		  double f = v / b;
		  RBquaternionT r = { log(q.w*q.w + b * b) / 2.0, f*q.x, f*q.y, f*q.z };
		  return r;
	  }
  }

  double
	  inline _quaternion_scalar_log(double s) { return log(s); }

  RBquaternionT
	  inline quaternion_scalar_power(double s, RBquaternionT q)
  {
	  /* Unlike the quaternion^quaternion power, this is unambiguous. */
	  if (s == 0.0) { /* log(s)=-inf */
		  if (!quaternion_nonzero(q)) {
			  RBquaternionT r = { 1.0, 0.0, 0.0, 0.0 }; /* consistent with python */
			  return r;
		  }
		  else {
			  RBquaternionT r = { 0.0, 0.0, 0.0, 0.0 }; /* consistent with python */
			  return r;
		  }
	  }
	  else if (s < 0.0) { /* log(s)=nan */
	 // fprintf(stderr, "Input scalar (%.15g) has no unique logarithm; returning one arbitrarily.", s);
		  RBquaternionT t = { log(-s), M_PI, 0, 0 };
		  return quaternion_exp(quaternion_multiply(q, t));
	  }
	  return quaternion_exp(quaternion_multiply_scalar(q, log(s)));
  }

  RBquaternionT
	  inline quaternion_exp(RBquaternionT q)
  {
	  double vnorm = sqrt(q.x*q.x + q.y*q.y + q.z*q.z);
	  if (vnorm > _QUATERNION_EPS) {
		  double s = sin(vnorm) / vnorm;
		  double e = exp(q.w);
		  RBquaternionT r = { e*cos(vnorm), e*s*q.x, e*s*q.y, e*s*q.z };
		  return r;
	  }
	  else {
		  RBquaternionT r = { exp(q.w), 0, 0, 0 };
		  return r;
	  }
  }

#endif // __QUATERNION_H__
