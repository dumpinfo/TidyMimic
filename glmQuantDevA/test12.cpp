#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "rbquat.h"
#include "zmath.h"
#include "BVH.h"

#include <vector>

using namespace std;

int parent_arr[17][2] =
{ 0 , -1 ,
1 , 0  ,
2 , 1  ,
3 ,1   ,
4 , 3  ,
5 , 4  ,
6 ,1   ,
7 , 6  ,
8 , 7  ,
9 , 0  ,
10 , 9  ,
11 , 10 ,
12 , 11 ,
13 , 0  ,
14 , 13 ,
15 , 14 ,
16 , 15 };

void quset(float*pt, vector<float> vec)
{
	pt[0] = vec[0]; pt[1] = vec[1];
	pt[2] = vec[2]; pt[3] = vec[3];
}

vector<float> query_quant(Frame&frame, int joint_no)
{
	float   qw, qx, qy, qz;
	int i = joint_no;

	M4 RotMat = GetRotateMatZYX(frame.joint_rotations[i].euler_rotation[0],
		                        frame.joint_rotations[i].euler_rotation[1],
		                        frame.joint_rotations[i].euler_rotation[2]);
	 
	V4 quant = mRot2Quat(RotMat);

	vector<float> ret_vec;
	ret_vec.push_back(quant.v[0]);
	ret_vec.push_back(quant.v[1]);
	ret_vec.push_back(quant.v[2]);
	ret_vec.push_back(quant.v[3]);
	return ret_vec;
}

M4 query_quantMat(Frame&frame, int joint_no)
{
	int i = joint_no;
	M4 RotMat = GetRotateMatZYX(frame.joint_rotations[i].euler_rotation[0],
		frame.joint_rotations[i].euler_rotation[1],
		frame.joint_rotations[i].euler_rotation[2]);
	return RotMat;
}

float query_angle(Frame&frame, int joint_no)
{
	return frame.joint_rotations[joint_no].euler_rotation[0] * 3.1415926 / 180.0;
}

void transMimicAction(Frame&frame, vector<float> &skl_action)
{
	float scale_factor = 0.1;
	skl_action.resize(44);
	skl_action[0] = 1 / 60.0;
	skl_action[1] = frame.rootpos[0] * scale_factor;
	skl_action[2] = frame.rootpos[1] * scale_factor;
	skl_action[3] = frame.rootpos[2] * scale_factor;
	quset(&skl_action[4], query_quant(frame, 0));//  # hip rotation4D                            //1                         
	quset(&skl_action[8], query_quant(frame, 1));//  # chest rotation4D                         //2           
	quset(&skl_action[12], query_quant(frame, 2));//  # neck rotation4D                         //3
	quset(&skl_action[16], query_quant(frame, 9));//  # right hip rotation4D                    //4
	skl_action[20] = query_angle(frame, 10); //        //5
	quset(&skl_action[21], query_quant(frame, 11));//  # right ankle rotation4D                 //6
	quset(&skl_action[25], query_quant(frame, 3));//  # right shoulder rotation4D               //7
	skl_action[29] = query_angle(frame, 4); //         //8
	quset(&skl_action[30], query_quant(frame, 13));//  # left hip rotation4D                    //9
	skl_action[34] = query_angle(frame, 14); //        //10
	quset(&skl_action[35], query_quant(frame, 15));//  # left ankle rotation4D                  //11
	quset(&skl_action[39], query_quant(frame, 6));//  # left shoulder rotation4D                //12
	skl_action[43] = query_angle(frame, 7); //         //13
}

void GetQuantRotangle(RBquaternionT&quant, float&Rt_xx, float&Rt_yy, float&Rt_zz)
{

	RBquaternionT rotlog = quaternion_log(quant);
	Rt_xx = rotlog.x;
	Rt_yy = rotlog.y;
	Rt_zz = rotlog.z;
}

void GetJointsRotationMat(Frame&frame, vector<M4>&globalmat_vec)
{
	int i;
	vector<M4> jointsmat_vec;
	vector<M4> restore_m_vec;
	
	jointsmat_vec.resize(17);
	globalmat_vec.resize(17);
	restore_m_vec.resize(17);

	for (i = 0; i < 17; i++)
	{
		jointsmat_vec[i] = query_quantMat(frame, i);
	}
	globalmat_vec[0] = jointsmat_vec[0];
	for (i = 1; i < 17; i++)
	{
		int Idx = parent_arr[i][1];
		M4 parentMat = globalmat_vec[Idx];//query_quantMat(frame, Idx);
		globalmat_vec[i] = parentMat * jointsmat_vec[i];
	}

	for (i = 1; i < 17; i++)
	{
		float Rt_xx, Rt_yy, Rt_zz;
		printf("===============%02i==================\n",i);
		globalmat_vec[i].printMat();
		V4 vq =  mRot2Quat(globalmat_vec[i]);
		RBquaternionT quant, q_log,q_restore;
		quant.w = vq.v[0];
		quant.x = vq.v[1];
		quant.y = vq.v[2];
		quant.z = vq.v[3];
		GetQuantRotangle(quant, Rt_xx, Rt_yy, Rt_zz);
		q_log.w = 0;
		q_log.x = Rt_xx;
		q_log.y = Rt_yy;
		q_log.z = Rt_zz;
		q_restore = quaternion_exp(q_log);
		int aa(0);
		aa++;
	}
    //==================test rotation matrix ====================
	for (i = 1; i < 17; i++)
	{
		int Idx = parent_arr[i][1];
		M4 parentMat = globalmat_vec[Idx];//query_quantMat(frame, Idx);
		restore_m_vec[i] =InvM4(parentMat) * globalmat_vec[i]; //=   jointsmat_vec[i];
		printf("#==============%02i==================\n", i);
		restore_m_vec[i].printMat();
	}
	//M4 mrot = Quaternion2RotMat(float qw, float qx, float qy, float qz)
}



int main(int argc, char*argv[])
{
 
	BVH bvhloader;
	bvhloader.Load(argv[1]);
	Frame frame;

	//bvhloader.QueryOneFrame(0, frame);
	//convert_mat(float xx, float yy, float zz)
	//query_quant_rotmat
	int i, j;
	//"Loop": "wrap",
	FILE* file = fopen("dumpActions.json", "wt+");
	fprintf(file, "{ \"Loop\": \"wrap\", \"Frames\": [");
	for (i = 0; i < 500; i++)
	{
		fprintf(file, "[");

		bvhloader.QueryOneFrame(i, frame);
		vector<float> skl_action;
		vector<M4> globalmat_vec;
		transMimicAction(frame, skl_action);
		GetJointsRotationMat(frame, globalmat_vec);

		for (j = 0; j < 44; j++)
		{
			if (j != 43)
				fprintf(file, "%8.4f,", skl_action[j]);
			else
				fprintf(file, "%8.4f ", skl_action[j]);
		}

		if (i != 499)
			fprintf(file, "],");
		else
			fprintf(file, "]");
		fprintf(file, "\n");
	}

	fprintf(file, "]}");
	fclose(file);
	
	printf("hello world!\n");
	return 1;
} 