#pragma once

#include "DeepMimicCharController.h"
#include "util/IndexManager.h"

class cAgentRegistry
{
public:
	cAgentRegistry();
	virtual ~cAgentRegistry();

	virtual void Clear();
	virtual int GetNumAgents() const;
	virtual int AddAgent(const std::shared_ptr<cDeepMimicCharController>& agent, cSimCharacter* character);

	virtual const std::shared_ptr<cDeepMimicCharController>& GetAgent(int id) const;
	virtual cSimCharacter* GetChar(int id) const;
	virtual void PrintInfo() const;

protected:

	cIndexManager mIDManager;
	std::vector<std::shared_ptr<cDeepMimicCharController>> mAgents;
	std::vector<cSimCharacter*> mChars;
};