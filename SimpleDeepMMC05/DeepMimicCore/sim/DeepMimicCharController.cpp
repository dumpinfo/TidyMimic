#include "DeepMimicCharController.h"
#include "sim/SimCharacter.h"
#include <iostream>
#include <ctime>
#include "util/json/json.h"
#include "util/FileUtil.h"
#include "util/JsonUtil.h"

const int gValLogSize = 50;
const std::string gViewDistMinKey = "ViewDistMin";
const std::string gViewDistMaxKey = "ViewDistMax";
const std::string gPDControllersKey = "PDControllers";


const int cDeepMimicCharController::gNormGroupSingle = 0;
const int cDeepMimicCharController::gNormGroupNone = -1;

#include "util/MathUtil.h"
#define ENABLE_PD_SPHERE_AXIS
static void BuildBoundsTorque(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max);
static void BuildBoundsVel(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max);
static void BuildBoundsPD(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max);

static void BuildOffsetScaleTorque(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale);
static void BuildOffsetScaleVel(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale);
static void BuildOffsetScalePD(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale);


static void BuildBoundsPDRevolute(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max);
static void BuildBoundsPDPrismatic(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max);
static void BuildBoundsPDPlanar(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max);
static void BuildBoundsPDFixed(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max);
static void BuildBoundsPDSpherical(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max);

static void BuildOffsetScalePDRevolute(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale);
static void BuildOffsetScalePDPrismatic(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale);
static void BuildOffsetScalePDPlanar(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale);
static void BuildOffsetScalePDFixed(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale);
static void BuildOffsetScalePDSpherical(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale);

#include <limits>
#include "anim/KinTree.h"

const double gDefaultOffsetPDBound = 10;
const double gDefaultRotatePDBound = M_PI;

void inline BuildBoundsTorque(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max)
{
	int joint_dim = cKinTree::GetParamSize(joint_mat, joint_id);
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	out_min = std::numeric_limits<double>::quiet_NaN() * Eigen::VectorXd::Ones(joint_dim);
	out_max = std::numeric_limits<double>::quiet_NaN() * Eigen::VectorXd::Ones(joint_dim);

	double torque_lim = cKinTree::GetTorqueLimit(joint_mat, joint_id);
	double force_lim = cKinTree::GetForceLimit(joint_mat, joint_id);

	switch (joint_type)
	{
	case cKinTree::eJointTypeRevolute:
		out_min.fill(-torque_lim);
		out_max.fill(torque_lim);
		break;
	case cKinTree::eJointTypePrismatic:
		out_min.fill(-force_lim);
		out_max.fill(force_lim);
		break;
	case cKinTree::eJointTypePlanar:
		out_min.fill(-force_lim);
		out_max.fill(force_lim);
		out_min[joint_dim - 1] = -torque_lim;
		out_max[joint_dim - 1] = torque_lim;
		break;
	case cKinTree::eJointTypeFixed:
		break;
	case cKinTree::eJointTypeSpherical:
		out_min.segment(0, joint_dim - 1).fill(-torque_lim);
		out_max.segment(0, joint_dim - 1).fill(torque_lim);
		out_min[joint_dim - 1] = 0;
		out_max[joint_dim - 1] = 0;
		break;
	default:
		assert(false); // unsupported joint type
		break;
	}
}

void inline BuildBoundsVel(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max)
{
	const double max_ang_vel = 20 * M_PI;
	const double max_lin_vel = 5;

	int joint_dim = cKinTree::GetParamSize(joint_mat, joint_id);
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);

	out_min = std::numeric_limits<double>::quiet_NaN() * Eigen::VectorXd::Ones(joint_dim);
	out_max = std::numeric_limits<double>::quiet_NaN() * Eigen::VectorXd::Ones(joint_dim);

	switch (joint_type)
	{
	case cKinTree::eJointTypeRevolute:
		out_min.fill(-max_ang_vel);
		out_max.fill(max_ang_vel);
		break;
	case cKinTree::eJointTypePrismatic:
		out_min.fill(-max_lin_vel);
		out_max.fill(max_lin_vel);
		break;
	case cKinTree::eJointTypePlanar:
		out_min.fill(-max_lin_vel);
		out_max.fill(max_lin_vel);
		out_min[joint_dim - 1] = -max_ang_vel;
		out_max[joint_dim - 1] = max_ang_vel;
		break;
	case cKinTree::eJointTypeFixed:
		break;
	case cKinTree::eJointTypeSpherical:
		out_min.segment(0, joint_dim - 1).fill(-max_ang_vel);
		out_max.segment(0, joint_dim - 1).fill(max_ang_vel);
		out_min[joint_dim - 1] = 0;
		out_max[joint_dim - 1] = 0;
		break;
	default:
		assert(false); // unsupported joint type
		break;
	}
}

void inline BuildBoundsPD(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max)
{
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	switch (joint_type)
	{
	case cKinTree::eJointTypeRevolute:
		BuildBoundsPDRevolute(joint_mat, joint_id, out_min, out_max);
		break;
	case cKinTree::eJointTypePrismatic:
		BuildBoundsPDPrismatic(joint_mat, joint_id, out_min, out_max);
		break;
	case cKinTree::eJointTypePlanar:
		BuildBoundsPDPlanar(joint_mat, joint_id, out_min, out_max);
		break;
	case cKinTree::eJointTypeFixed:
		BuildBoundsPDFixed(joint_mat, joint_id, out_min, out_max);
		break;
	case cKinTree::eJointTypeSpherical:
		BuildBoundsPDSpherical(joint_mat, joint_id, out_min, out_max);
		break;
	default:
		assert(false); // unsupported joint type
		break;
	}
}


void inline BuildOffsetScaleTorque(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale)
{
	const double default_torque_lim = 300;
	const double default_force_lim = 3000;

	int joint_dim = cKinTree::GetParamSize(joint_mat, joint_id);
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	out_offset = Eigen::VectorXd::Zero(joint_dim);
	out_scale = Eigen::VectorXd::Ones(joint_dim);

	double torque_lim = cKinTree::GetTorqueLimit(joint_mat, joint_id);
	double force_lim = cKinTree::GetForceLimit(joint_mat, joint_id);

	if (!std::isfinite(torque_lim))
	{
		torque_lim = default_torque_lim;
	}

	if (!std::isfinite(force_lim))
	{
		force_lim = default_force_lim;
	}

	switch (joint_type)
	{
	case cKinTree::eJointTypeRevolute:
		out_scale.fill(1 / torque_lim);
		break;
	case cKinTree::eJointTypePrismatic:
		out_scale.fill(1 / force_lim);
		break;
	case cKinTree::eJointTypePlanar:
		out_scale.fill(1 / force_lim);
		out_scale[joint_dim - 1] = 1 / torque_lim;
		break;
	case cKinTree::eJointTypeFixed:
		break;
	case cKinTree::eJointTypeSpherical:
		out_scale.fill(1 / torque_lim);
		out_scale[joint_dim - 1] = 0;
		break;
	default:
		assert(false); // unsupported joint type
		break;
	}
}

void inline BuildOffsetScaleVel(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale)
{
	int joint_dim = cKinTree::GetParamSize(joint_mat, joint_id);
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);

	const double ang_vel_scale = 1 / (10 * M_PI);
	const double lin_vel_scale = 1 / 2.5;

	out_offset = Eigen::VectorXd::Zero(joint_dim);
	out_scale = Eigen::VectorXd::Ones(joint_dim);

	switch (joint_type)
	{
	case cKinTree::eJointTypeRevolute:
		out_scale.fill(ang_vel_scale);
		break;
	case cKinTree::eJointTypePrismatic:
		out_scale.fill(lin_vel_scale);
		break;
	case cKinTree::eJointTypePlanar:
		out_scale.fill(lin_vel_scale);
		out_scale[joint_dim - 1] = ang_vel_scale;
		break;
	case cKinTree::eJointTypeFixed:
		break;
	case cKinTree::eJointTypeSpherical:
		out_scale.segment(0, joint_dim - 1).fill(ang_vel_scale);
		out_scale[joint_dim - 1] = 1;
		break;
	default:
		assert(false); // unsupported joint type
		break;
	}
}

void inline BuildOffsetScalePD(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale)
{
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	switch (joint_type)
	{
	case cKinTree::eJointTypeRevolute:
		BuildOffsetScalePDRevolute(joint_mat, joint_id, out_offset, out_scale);
		break;
	case cKinTree::eJointTypePrismatic:
		BuildOffsetScalePDPrismatic(joint_mat, joint_id, out_offset, out_scale);
		break;
	case cKinTree::eJointTypePlanar:
		BuildOffsetScalePDPlanar(joint_mat, joint_id, out_offset, out_scale);
		break;
	case cKinTree::eJointTypeFixed:
		BuildOffsetScalePDFixed(joint_mat, joint_id, out_offset, out_scale);
		break;
	case cKinTree::eJointTypeSpherical:
		BuildOffsetScalePDSpherical(joint_mat, joint_id, out_offset, out_scale);
		break;
	default:
		assert(false); // unsupported joint type
		break;
	}
}



void inline BuildBoundsPDRevolute(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max)
{
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	assert(joint_type == cKinTree::eJointTypeRevolute);

	int joint_dim = cKinTree::GetParamSize(joint_mat, joint_id);
	out_min = Eigen::VectorXd::Zero(joint_dim);
	out_max = Eigen::VectorXd::Zero(joint_dim);

	tVector lim_low = cKinTree::GetJointLimLow(joint_mat, joint_id);
	tVector lim_high = cKinTree::GetJointLimHigh(joint_mat, joint_id);

	for (int i = 0; i < joint_dim; ++i)
	{
		double val_low = lim_low[i];
		double val_high = lim_high[i];
		bool valid_lim = val_high >= val_low;
		if (!valid_lim)
		{
			val_low = -gDefaultRotatePDBound;
			val_high = gDefaultRotatePDBound;
		}

		double mean_val = 0.5 * (val_high + val_low);
		double delta = val_high - val_low;
		val_low = mean_val - 2 * delta;
		val_high = mean_val + 2 * delta;

		out_min[i] = val_low;
		out_max[i] = val_high;
	}
}

void inline BuildBoundsPDPrismatic(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max)
{
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	assert(joint_type == cKinTree::eJointTypePrismatic);

	int joint_dim = cKinTree::GetParamSize(joint_mat, joint_id);
	out_min = Eigen::VectorXd::Zero(joint_dim);
	out_max = Eigen::VectorXd::Zero(joint_dim);

	tVector lim_low = cKinTree::GetJointLimLow(joint_mat, joint_id);
	tVector lim_high = cKinTree::GetJointLimHigh(joint_mat, joint_id);

	for (int i = 0; i < joint_dim; ++i)
	{
		double val_low = lim_low[i];
		double val_high = lim_high[i];
		bool valid_lim = val_high >= val_low;
		if (!valid_lim)
		{
			val_low = -gDefaultOffsetPDBound;
			val_high = gDefaultOffsetPDBound;
		}

		double mean_val = 0.5 * (val_high + val_low);
		double delta = val_high - val_low;
		val_low = mean_val - delta;
		val_high = mean_val + delta;
		out_min[i] = val_low;
		out_max[i] = val_high;
	}
}

void inline BuildBoundsPDPlanar(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max)
{
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	assert(joint_type == cKinTree::eJointTypePlanar);

	int joint_dim = cKinTree::GetParamSize(joint_mat, joint_id);
	out_min = Eigen::VectorXd::Zero(joint_dim);
	out_max = Eigen::VectorXd::Zero(joint_dim);

	tVector lim_low = cKinTree::GetJointLimLow(joint_mat, joint_id);
	tVector lim_high = cKinTree::GetJointLimHigh(joint_mat, joint_id);

	for (int i = 0; i < joint_dim; ++i)
	{
		double val_low = lim_low[i];
		double val_high = lim_high[i];
		bool valid_lim = val_high >= val_low;
		if (!valid_lim)
		{
			val_low = -gDefaultOffsetPDBound;
			val_high = gDefaultOffsetPDBound;
		}
		out_min[i] = val_low;
		out_max[i] = val_high;
	}
}

void inline BuildBoundsPDFixed(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max)
{
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	assert(joint_type == cKinTree::eJointTypeFixed);

	int joint_dim = cKinTree::GetParamSize(joint_mat, joint_id);
	out_min = Eigen::VectorXd::Zero(joint_dim);
	out_max = Eigen::VectorXd::Zero(joint_dim);
}

void inline BuildBoundsPDSpherical(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max)
{
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	assert(joint_type == cKinTree::eJointTypeSpherical);

	int joint_dim = cKinTree::GetParamSize(joint_mat, joint_id);
	out_min = -Eigen::VectorXd::Ones(joint_dim);
	out_max = Eigen::VectorXd::Ones(joint_dim);

#if defined(ENABLE_PD_SPHERE_AXIS)
	tVector lim_low = cKinTree::GetJointLimLow(joint_mat, joint_id);
	tVector lim_high = cKinTree::GetJointLimHigh(joint_mat, joint_id);

	double val_low = lim_low.minCoeff();
	double val_high = lim_high.maxCoeff();
	bool valid_lim = val_high >= val_low;
	if (!valid_lim)
	{
		val_low = -gDefaultRotatePDBound;
		val_high = gDefaultRotatePDBound;
	}

	double mean_val = 0.5 * (val_high + val_low);
	double delta = val_high - val_low;
	val_low = mean_val - 2 * delta;
	val_high = mean_val + 2 * delta;

	out_min[0] = val_low;
	out_max[0] = val_high;
#endif
}


void inline BuildOffsetScalePDRevolute(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale)
{
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	assert(joint_type == cKinTree::eJointTypeRevolute);

	int joint_dim = cKinTree::GetParamSize(joint_mat, joint_id);
	out_offset = Eigen::VectorXd::Zero(joint_dim);
	out_scale = Eigen::VectorXd::Ones(joint_dim);

	tVector lim_low = cKinTree::GetJointLimLow(joint_mat, joint_id);
	tVector lim_high = cKinTree::GetJointLimHigh(joint_mat, joint_id);

	for (int i = 0; i < joint_dim; ++i)
	{
		double val_low = lim_low[i];
		double val_high = lim_high[i];
		bool valid_lim = val_high >= val_low;
		if (!valid_lim)
		{
			val_low = -gDefaultRotatePDBound;
			val_high = gDefaultRotatePDBound;
		}

		double curr_offset = -0.5 * (val_high + val_low);
		double curr_scale = 1 / (val_high - val_low);
		curr_scale *= 0.5;

		out_offset[i] = curr_offset;
		out_scale[i] = curr_scale;
	}
}

void inline BuildOffsetScalePDPrismatic(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale)
{
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	assert(joint_type == cKinTree::eJointTypePrismatic);

	int joint_dim = cKinTree::GetParamSize(joint_mat, joint_id);
	out_offset = Eigen::VectorXd::Zero(joint_dim);
	out_scale = Eigen::VectorXd::Ones(joint_dim);

	tVector lim_low = cKinTree::GetJointLimLow(joint_mat, joint_id);
	tVector lim_high = cKinTree::GetJointLimHigh(joint_mat, joint_id);

	for (int i = 0; i < joint_dim; ++i)
	{
		double val_low = lim_low[i];
		double val_high = lim_high[i];
		bool valid_lim = val_high >= val_low;
		if (!valid_lim)
		{
			val_low = -gDefaultOffsetPDBound;
			val_high = gDefaultOffsetPDBound;
		}
		out_offset[i] = -0.5 * (val_high + val_low);
		out_scale[i] = 2 / (val_high - val_low);
	}
}

void inline BuildOffsetScalePDPlanar(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale)
{
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	assert(joint_type == cKinTree::eJointTypePlanar);

	int joint_dim = cKinTree::GetParamSize(joint_mat, joint_id);
	out_offset = Eigen::VectorXd::Zero(joint_dim);
	out_scale = Eigen::VectorXd::Ones(joint_dim);

	tVector lim_low = cKinTree::GetJointLimLow(joint_mat, joint_id);
	tVector lim_high = cKinTree::GetJointLimHigh(joint_mat, joint_id);

	for (int i = 0; i < joint_dim; ++i)
	{
		double val_low = lim_low[i];
		double val_high = lim_high[i];
		bool valid_lim = val_high >= val_low;
		if (!valid_lim)
		{
			val_low = -gDefaultOffsetPDBound;
			val_high = gDefaultOffsetPDBound;
		}
		out_offset[i] = -0.5 * (val_high + val_low);
		out_scale[i] = 2 / (val_high - val_low);
	}
}

void inline BuildOffsetScalePDFixed(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale)
{
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	assert(joint_type == cKinTree::eJointTypeFixed);

	int joint_dim = cKinTree::GetParamSize(joint_mat, joint_id);
	out_offset = Eigen::VectorXd::Zero(joint_dim);
	out_scale = Eigen::VectorXd::Ones(joint_dim);
}

void inline BuildOffsetScalePDSpherical(const Eigen::MatrixXd& joint_mat, int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale)
{
	cKinTree::eJointType joint_type = cKinTree::GetJointType(joint_mat, joint_id);
	assert(joint_type == cKinTree::eJointTypeSpherical);

	int joint_dim = cKinTree::GetParamSize(joint_mat, joint_id);
	out_offset = Eigen::VectorXd::Zero(joint_dim);
	out_scale = Eigen::VectorXd::Ones(joint_dim);

#if defined(ENABLE_PD_SPHERE_AXIS)
	tVector lim_low = cKinTree::GetJointLimLow(joint_mat, joint_id);
	tVector lim_high = cKinTree::GetJointLimHigh(joint_mat, joint_id);

	double val_low = lim_low.minCoeff();
	double val_high = lim_high.maxCoeff();
	bool valid_lim = val_high >= val_low;
	if (!valid_lim)
	{
		val_low = -gDefaultRotatePDBound;
		val_high = gDefaultRotatePDBound;
	}

	double curr_offset = 0;
	double curr_scale = 1 / (val_high - val_low);
	curr_scale *= 0.5;

	out_offset(0) = curr_offset;
	out_scale(0) = curr_scale;
	out_offset(3) = -0.2;
#else
	out_offset(0) = -0.2;
#endif
}

//#####################################################################################
bool cDeepMimicCharController::LoadParams(const std::string& param_file)
{
	std::ifstream f_stream(param_file);
	Json::Reader reader;
	Json::Value root;
	bool succ = reader.parse(f_stream, root);
	f_stream.close();

	if (succ)
	{
		succ &= ParseParams(root);
	}

	if (!succ)
	{
		printf("Failed to load params from %s\n", param_file.c_str());
		assert(false);
	}

	return succ;
}

int cDeepMimicCharController::GetGoalSize() const
{
	return 0;
}

 

int cDeepMimicCharController::GetNumActions() const
{
	printf(" cDeepMimicCharController::GetNumActions()\n");
	return 0;
}

void cDeepMimicCharController::HandlePoseReset()
{
}

void cDeepMimicCharController::HandleVelReset()
{
}

 
void cDeepMimicCharController::BuildGoalOffsetScale(Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const
{
	int goal_size = GetGoalSize();
	out_offset = Eigen::VectorXd::Zero(goal_size);
	out_scale = Eigen::VectorXd::Ones(goal_size);
}

void cDeepMimicCharController::BuildGoalNormGroups(Eigen::VectorXi& out_groups) const
{
	int goal_size = GetGoalSize();
	out_groups = gNormGroupSingle * Eigen::VectorXi::Ones(goal_size);
}

int cDeepMimicCharController::NumChildren() const
{
	return 0;
}

const std::shared_ptr<cDeepMimicCharController>& cDeepMimicCharController::GetChild(int i) const
{
	return nullptr;
}
//####################################################################
//####################################################################
//####################################################################
void cDeepMimicCharController::SetUpdateRate(double rate)
{
	mUpdateRate = rate;
}

double cDeepMimicCharController::GetUpdateRate() const
{
	return mUpdateRate;
}

void cDeepMimicCharController::UpdateCalcTau(double timestep, Eigen::VectorXd& out_tau)//KK##
{
	// use unnormalized phase
	double prev_phase = 0;
	if (mEnablePhaseAction)
	{
		prev_phase = mTime / mCyclePeriod;
		prev_phase += mPhaseOffset;
	}

	//cDeepMimicCharController::UpdateCalcTau(timestep, out_tau);
	//void cDeepMimicCharController::UpdateCalcTau(double timestep, Eigen::VectorXd& out_tau)
   {
	mTime += timestep;
	if (mNeedNewAction)
	{
		HandleNewAction();
	}
   }

	if (mEnablePhaseAction)
	{
		double phase_rate = GetPhaseRate();
		double tar_phase = prev_phase + timestep * phase_rate;
		mPhaseOffset = tar_phase - mTime / mCyclePeriod;
		mPhaseOffset = std::fmod(mPhaseOffset, 1.0);
	}

	UpdateBuildTau(timestep, out_tau);
}


int cDeepMimicCharController::GetStateSize() const //KK##
{
 	int state_size = 0;
	state_size += GetStatePoseSize();
	state_size += GetStateVelSize();
	state_size += GetStatePhaseSize();
	return state_size;
}

int cDeepMimicCharController::GetActionSize() const
{
	int a_size = 0;
	a_size += GetActionPhaseSize();
	a_size += GetActionCtrlSize();
	return a_size;
}

void cDeepMimicCharController::BuildStateOffsetScale(Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const
{
	//cDeepMimicCharController::BuildStateOffsetScale(out_offset, out_scale);
	int state_size = GetStateSize();
	out_offset = Eigen::VectorXd::Zero(state_size);
	out_scale = Eigen::VectorXd::Ones(state_size);
    //----------------------------------------------------
	
	if (mEnablePhaseInput)
	{
		Eigen::VectorXd phase_offset;
		Eigen::VectorXd phase_scale;
		BuildStatePhaseOffsetScale(phase_offset, phase_scale);

		int phase_idx = GetStatePhaseOffset();
		int phase_size = GetStatePhaseSize();
		out_offset.segment(phase_idx, phase_size) = phase_offset;
		out_scale.segment(phase_idx, phase_size) = phase_scale;
	}
}

void cDeepMimicCharController::BuildActionBounds(Eigen::VectorXd& out_min, Eigen::VectorXd& out_max) const
{
	int action_size = GetActionSize();
	out_min = Eigen::VectorXd::Zero(action_size);
	out_max = Eigen::VectorXd::Zero(action_size);

	int root_id = mChar->GetRootID();
	int root_size = mChar->GetParamSize(root_id);
	int num_joints = mChar->GetNumJoints();
	int ctrl_offset = GetActionCtrlOffset();

	if (mEnablePhaseAction)
	{
		int phase_offset = GetActionPhaseOffset();
		out_min[phase_offset] = -5;
		out_max[phase_offset] = 5;
	}

	for (int j = root_id + 1; j < num_joints; ++j)
	{
		const cSimBodyJoint& joint = mChar->GetJoint(j);
		if (joint.IsValid())
		{
			int param_offset = mChar->GetParamOffset(j);
			int param_size = mChar->GetParamSize(j);

			if (param_size > 0)
			{
				Eigen::VectorXd lim_min;
				Eigen::VectorXd lim_max;
				BuildJointActionBounds(j, lim_min, lim_max);
				assert(lim_min.size() == param_size);
				assert(lim_max.size() == param_size);

				param_offset -= root_size;
				param_offset += ctrl_offset;
				out_min.segment(param_offset, param_size) = lim_min;
				out_max.segment(param_offset, param_size) = lim_max;
			}
		}
	}
}

void cDeepMimicCharController::ResetParams()//KK##
{
	mTime = 0;
	mNeedNewAction = true;
	mTau.setZero();
	mValLog.Clear();

	mPrevActionTime = mTime;
	mPrevActionCOM.setZero();
	
	mPhaseOffset = 0;
	mInitTimeOffset = 0;
}

int cDeepMimicCharController::GetPosFeatureDim() const
{
	int pos_dim = cKinTree::gPosDim;
	return pos_dim;
}

int cDeepMimicCharController::GetRotFeatureDim() const
{
	int rot_dim = cKinTree::gRotDim;
	return rot_dim;
}

double cDeepMimicCharController::GetCyclePeriod() const
{
	return mCyclePeriod;
}

void cDeepMimicCharController::SetCyclePeriod(double period)
{
	mCyclePeriod = period;
}

void cDeepMimicCharController::SetInitTime(double time)
{
	mTime = time;
	mPrevActionTime = time;
	mPhaseOffset = 0;
	mInitTimeOffset = -mTime;
}

double cDeepMimicCharController::GetPhase() const
{
	double phase = mTime / mCyclePeriod;
	phase += mPhaseOffset;
	phase = std::fmod(phase, 1.0);
	phase = (phase < 0) ? (1 + phase) : phase;
	return phase;
}



bool cDeepMimicCharController::CheckNeedNewAction(double timestep) const //KK##
{
	double curr_time = mTime;
	curr_time += mInitTimeOffset;
	bool new_action = cMathUtil::CheckNextInterval(timestep, curr_time, 1 / mUpdateRate);
	return new_action;
}

void cDeepMimicCharController::BuildActionOffsetScale(Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const
{
	int action_size = GetActionSize();
	out_offset = Eigen::VectorXd::Zero(action_size);
	out_scale = Eigen::VectorXd::Ones(action_size);

	int root_id = mChar->GetRootID();
	int root_size = mChar->GetParamSize(mChar->GetRootID());
	int num_joints = mChar->GetNumJoints();
	int ctrl_offset = GetActionCtrlOffset();

	if (mEnablePhaseAction)
	{
		int phase_offset = GetActionPhaseOffset();
		out_offset[phase_offset] = -1 / mCyclePeriod;
		out_scale[phase_offset] = 1;
	}

	for (int j = root_id + 1; j < num_joints; ++j)
	{
		const cSimBodyJoint& joint = mChar->GetJoint(j);
		if (joint.IsValid())
		{
			int param_offset = mChar->GetParamOffset(j);
			int param_size = mChar->GetParamSize(j);

			if (param_size > 0)
			{
				Eigen::VectorXd curr_offset;
				Eigen::VectorXd curr_scale;
				BuildJointActionOffsetScale(j, curr_offset, curr_scale);
				assert(curr_offset.size() == param_size);
				assert(curr_scale.size() == param_size);

				param_offset -= root_size;
				param_offset += ctrl_offset;
				out_offset.segment(param_offset, param_size) = curr_offset;
				out_scale.segment(param_offset, param_size) = curr_scale;
			}
		}
	}
}

void cDeepMimicCharController::BuildStateNormGroups(Eigen::VectorXi& out_groups) const
{
	//cDeepMimicCharController::BuildStateNormGroups(out_groups);
	int state_size = GetStateSize();
	out_groups = gNormGroupSingle * Eigen::VectorXi::Ones(state_size);

	if (mEnablePhaseInput)
	{
		int phase_group = gNormGroupNone;
		int phase_offset = GetStatePhaseOffset();
		int phase_size = GetStatePhaseSize();
		out_groups.segment(phase_offset, phase_size) = phase_group * Eigen::VectorXi::Ones(phase_size);
	}
}

void cDeepMimicCharController::RecordState(Eigen::VectorXd& out_state)//KK##
{
	Eigen::VectorXd phase_state;
	int state_size = GetStateSize();
	// fill with nans to make sure we don't forget to set anything
	out_state = std::numeric_limits<double>::quiet_NaN() * Eigen::VectorXd::Ones(state_size);
	Eigen::VectorXd ground;
	Eigen::VectorXd pose;
	Eigen::VectorXd vel;
	BuildStatePose(pose);
	BuildStateVel(vel);

	int pose_offset = GetStatePoseOffset();
	int pose_size = GetStatePoseSize();
	int vel_offset = GetStateVelOffset();
	int vel_size = GetStateVelSize();

	out_state.segment(pose_offset, pose_size) = pose;
	out_state.segment(vel_offset, vel_size) = vel;
	

	if (mEnablePhaseInput)
	{
		int phase_offset = GetStatePhaseOffset();
		int phase_size = GetStatePhaseSize();
		BuildStatePhase(phase_state);
		out_state.segment(phase_offset, phase_size) = phase_state;
	}
}

 

int cDeepMimicCharController::GetStatePoseSize() const//KK##
{
	int pos_dim = GetPosFeatureDim();
	int rot_dim = GetRotFeatureDim();
	int size = mChar->GetNumBodyParts() * (pos_dim + rot_dim) + 1; // +1 for root y

	return size;
}

/*int cDeepMimicCharController::GetStateVelSize() const
{
	return mChar->GetNumBodyParts() * mPosDim;
}*/

int cDeepMimicCharController::GetStateVelSize() const
{
	int pos_dim = GetPosFeatureDim();
	int rot_dim = GetRotFeatureDim();
	int size = mChar->GetNumBodyParts() * (pos_dim + rot_dim - 1);
	return size;
}

int cDeepMimicCharController::GetStatePoseOffset() const //KK##
{  //cDeepMimicCharController::GetStatePoseOffset()
	return 0 + GetStatePhaseSize();
}

int cDeepMimicCharController::GetStatePhaseSize() const
{
	return (mEnablePhaseInput) ? 1 : 0;
}

int cDeepMimicCharController::GetStatePhaseOffset() const
{
	return 0;
}

int cDeepMimicCharController::GetActionPhaseOffset() const
{
	return 0;
}

int cDeepMimicCharController::GetActionPhaseSize() const
{
	return (mEnablePhaseAction) ? 1 : 0;
}

int cDeepMimicCharController::GetActionCtrlOffset() const
{
	return GetActionPhaseSize();
}

int cDeepMimicCharController::GetActionCtrlSize() const
{
	int ctrl_size = mChar->GetNumDof();
	int root_size = mChar->GetParamSize(mChar->GetRootID());
	ctrl_size -= root_size;
	return ctrl_size;
}

void cDeepMimicCharController::BuildStatePhaseOffsetScale(Eigen::VectorXd& phase_offset, Eigen::VectorXd& phase_scale) const
{
	double offset = -0.5;
	double scale = 2;
	int phase_size = GetStatePhaseSize();
	phase_offset = offset * Eigen::VectorXd::Ones(phase_size);
	phase_scale = scale * Eigen::VectorXd::Ones(phase_size);
}


void cDeepMimicCharController::BuildStatePose(Eigen::VectorXd& out_pose) const//KK##
{
	tMatrix origin_trans = mChar->BuildOriginTrans();
	tQuaternion origin_quat = cMathUtil::RotMatToQuaternion(origin_trans);

	bool flip_stance = FlipStance();
	if (flip_stance)
	{
		origin_trans.row(2) *= -1; // reflect z
	}

	tVector root_pos = mChar->GetRootPos();
	tVector root_pos_rel = root_pos;

	root_pos_rel[3] = 1;
	root_pos_rel = origin_trans * root_pos_rel;
	root_pos_rel[3] = 0;

	out_pose = Eigen::VectorXd::Zero(GetStatePoseSize());
	out_pose[0] = root_pos_rel[1];
	int num_parts = mChar->GetNumBodyParts();
	int root_id = mChar->GetRootID();

	int pos_dim = GetPosFeatureDim();
	int rot_dim = GetRotFeatureDim();

	tQuaternion mirror_inv_origin_quat = origin_quat.conjugate();
	mirror_inv_origin_quat = cMathUtil::MirrorQuaternion(mirror_inv_origin_quat, cMathUtil::eAxisZ);

	int idx = 1;
	for (int i = 0; i < num_parts; ++i)
	{
		int part_id = RetargetJointID(i);
		if (mChar->IsValidBodyPart(part_id))
		{
			const auto& curr_part = mChar->GetBodyPart(part_id);
			tVector curr_pos = curr_part->GetPos();

			if (mRecordWorldRootPos && i == root_id)
			{
				if (flip_stance)
				{
					curr_pos = cMathUtil::QuatRotVec(origin_quat, curr_pos);
					curr_pos[2] = -curr_pos[2];
					curr_pos = cMathUtil::QuatRotVec(mirror_inv_origin_quat, curr_pos);
				}
			}
			else
			{
				curr_pos[3] = 1;
				curr_pos = origin_trans * curr_pos;
				curr_pos -= root_pos_rel;
				curr_pos[3] = 0;
			}

			out_pose.segment(idx, pos_dim) = curr_pos.segment(0, pos_dim);
			idx += pos_dim;

			tQuaternion curr_quat = curr_part->GetRotation();
			if (mRecordWorldRootRot && i == root_id)
			{
				if (flip_stance)
				{
					curr_quat = origin_quat * curr_quat;
					curr_quat = cMathUtil::MirrorQuaternion(curr_quat, cMathUtil::eAxisZ);
					curr_quat = mirror_inv_origin_quat * curr_quat;
				}
			}
			else
			{
				curr_quat = origin_quat * curr_quat;
				if (flip_stance)
				{
					curr_quat = cMathUtil::MirrorQuaternion(curr_quat, cMathUtil::eAxisZ);
				}
			}

			if (curr_quat.w() < 0)
			{
				curr_quat.w() *= -1;
				curr_quat.x() *= -1;
				curr_quat.y() *= -1;
				curr_quat.z() *= -1;
			}
			out_pose.segment(idx, rot_dim) = cMathUtil::QuatToVec(curr_quat).segment(0, rot_dim);
			idx += rot_dim;
		}
	}
}

void cDeepMimicCharController::BuildStateVel(Eigen::VectorXd& out_vel) const//KK##
{
	int num_parts = mChar->GetNumBodyParts();
	tMatrix origin_trans = mChar->BuildOriginTrans();
	tQuaternion origin_quat = cMathUtil::RotMatToQuaternion(origin_trans);

	bool flip_stance = FlipStance();
	if (flip_stance)
	{
		origin_trans.row(2) *= -1; // reflect z
	}

	int pos_dim = GetPosFeatureDim();
	int rot_dim = GetRotFeatureDim();

	out_vel = Eigen::VectorXd::Zero(GetStateVelSize());

	tQuaternion mirror_inv_origin_quat = origin_quat.conjugate();
	mirror_inv_origin_quat = cMathUtil::MirrorQuaternion(mirror_inv_origin_quat, cMathUtil::eAxisZ);

	int idx = 0;
	for (int i = 0; i < num_parts; ++i)
	{
		int part_id = RetargetJointID(i);
		int root_id = mChar->GetRootID();

		const auto& curr_part = mChar->GetBodyPart(part_id);
		tVector curr_vel = curr_part->GetLinearVelocity();

		if (mRecordWorldRootRot && i == root_id)
		{
			if (flip_stance)
			{
				curr_vel = cMathUtil::QuatRotVec(origin_quat, curr_vel);
				curr_vel[2] = -curr_vel[2];
				curr_vel = cMathUtil::QuatRotVec(mirror_inv_origin_quat, curr_vel);
			}
		}
		else
		{
			curr_vel = origin_trans * curr_vel;
		}

		out_vel.segment(idx, pos_dim) = curr_vel.segment(0, pos_dim);
		idx += pos_dim;

		tVector curr_ang_vel = curr_part->GetAngularVelocity();
		if (mRecordWorldRootRot && i == root_id)
		{
			if (flip_stance)
			{
				curr_ang_vel = cMathUtil::QuatRotVec(origin_quat, curr_ang_vel);
				curr_ang_vel[2] = -curr_ang_vel[2];
				curr_ang_vel = -curr_ang_vel;
				curr_ang_vel = cMathUtil::QuatRotVec(mirror_inv_origin_quat, curr_ang_vel);
			}
		}
		else
		{
			curr_ang_vel = origin_trans * curr_ang_vel;
			if (flip_stance)
			{
				curr_ang_vel = -curr_ang_vel;
			}
		}

		out_vel.segment(idx, rot_dim - 1) = curr_ang_vel.segment(0, rot_dim - 1);
		idx += rot_dim - 1;
	}
}

void cDeepMimicCharController::BuildStatePhase(Eigen::VectorXd& out_phase) const
{
	double phase = GetPhase();
	out_phase = Eigen::VectorXd::Zero(GetStatePhaseSize());
	out_phase[0] = phase;
}


bool cDeepMimicCharController::FlipStance() const
{
	return false;
}

int cDeepMimicCharController::RetargetJointID(int joint_id) const
{
	return joint_id;
}

double cDeepMimicCharController::GetPhaseRate() const
{
	assert(mEnablePhaseAction);
	double phase_rate = 0;
	if (mEnablePhaseAction)
	{
		int phase_offset = GetActionPhaseOffset();
		phase_rate = mAction[phase_offset];
	}
	return phase_rate;
}


//====================================================
cDeepMimicCharController::cDeepMimicCharController() //: cController()
{
	mTime = 0;
	mPosDim = 0;
	SetViewDistMin(-0.5);
	SetViewDistMax(10);

	mPrevActionTime = mTime;
	mPrevActionCOM.setZero();

	mValLog.Reserve(gValLogSize);
	//--------------------------------------------------------------
	mUpdateRate = 30.0;
	mCyclePeriod = 1;
	mEnablePhaseInput = false;
	mEnablePhaseAction = false;
	mRecordWorldRootPos = false;
	mRecordWorldRootRot = false;
	SetViewDistMax(1);

	mPhaseOffset = 0;
	mInitTimeOffset = 0;
	mGravity = gGravity;
}

 

void cDeepMimicCharController::Reset()//KK##
{
	//cCharController::Reset();
	mMode = eModeActive;//cController::Reset();
	
	ResetParams();
	NewActionUpdate();
	mPDCtrl.Reset();
}

void cDeepMimicCharController::Clear()//KK##
{
	//cCharController::Clear();
	//cController::Clear();
	mValid = false;
	mChar = nullptr;
	mMode = eModeActive;
	//---------------------------
	ResetParams();
	mPDCtrl.Clear();
}

void cDeepMimicCharController::SetGravity(const tVector& gravity)
{
	mGravity = gravity;
}

std::string cDeepMimicCharController::GetName() const
{
	return "ct_pd";
}

void cDeepMimicCharController::SetupPDControllers(const Json::Value& json, const tVector& gravity)
{
	Eigen::MatrixXd pd_params;
	bool succ = false;
	if (!json[gPDControllersKey].isNull())
	{
		succ = cPDController::LoadParams(json[gPDControllersKey], pd_params);
	}

	if (succ)
	{
		mPDCtrl.Init(mChar, pd_params, gravity);
	}

	mValid = succ;
	if (!mValid)
	{
		printf("Failed to initialize Ct-PD controller\n");
		mValid = false;
	}
}

bool cDeepMimicCharController::ParseParams(const Json::Value& json)//KK##
{
	//bool succ = cCharController::ParseParams(json);
	bool succ = true;//cController::ParseParams(json);
	mViewDistMin = json.get(gViewDistMinKey, mViewDistMin).asDouble();
	mViewDistMax = json.get(gViewDistMaxKey, mViewDistMax).asDouble();
	// = cDeepMimicCharController::ParseParams(json);
	mUpdateRate = json.get("QueryRate", mUpdateRate).asDouble();
	mCyclePeriod = json.get("CyclePeriod", mCyclePeriod).asDouble();
	mEnablePhaseInput = json.get("EnablePhaseInput", mEnablePhaseInput).asBool();
	mEnablePhaseAction = json.get("EnablePhaseAction", mEnablePhaseAction).asBool();
	mRecordWorldRootPos = json.get("RecordWorldRootPos", mRecordWorldRootPos).asBool();
	mRecordWorldRootRot = json.get("RecordWorldRootRot", mRecordWorldRootRot).asBool();

	SetupPDControllers(json, mGravity);
	return succ;
}

void cDeepMimicCharController::UpdateBuildTau(double time_step, Eigen::VectorXd& out_tau)
{
	UpdatePDCtrls(time_step, out_tau);
}/**/

void cDeepMimicCharController::UpdatePDCtrls(double time_step, Eigen::VectorXd& out_tau)
{
	int num_dof = mChar->GetNumDof();
	out_tau = Eigen::VectorXd::Zero(num_dof);
	mPDCtrl.UpdateControlForce(time_step, out_tau);
}

 
void cDeepMimicCharController::ApplyAction(const Eigen::VectorXd& action) //KK##
{
	//cDeepMimicCharController::ApplyAction(action);
	mAction = action;
	PostProcessAction(mAction);
	SetPDTargets(action);
}

void cDeepMimicCharController::BuildJointActionBounds(int joint_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max) const
{
	const Eigen::MatrixXd& joint_mat = mChar->GetJointMat();
	BuildBoundsPD(joint_mat, joint_id, out_min, out_max);//cCtCtrlUtil::
}

void cDeepMimicCharController::BuildJointActionOffsetScale(int joint_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const
{
	const Eigen::MatrixXd& joint_mat = mChar->GetJointMat();
	BuildOffsetScalePD(joint_mat, joint_id, out_offset, out_scale);//cCtCtrlUtil::
}/**/

void cDeepMimicCharController::ConvertActionToTargetPose(int joint_id, Eigen::VectorXd& out_theta) const
{
#if defined(ENABLE_PD_SPHERE_AXIS)
	cKinTree::eJointType joint_type = GetJointType(joint_id);
	if (joint_type == cKinTree::eJointTypeSpherical)
	{
		double rot_theta = out_theta[0];
		tVector axis = tVector(out_theta[1], out_theta[2], out_theta[3], 0);
		if (axis.squaredNorm() == 0)
		{
			axis[2] = 1;
		}

		axis.normalize();
		tQuaternion quat = cMathUtil::AxisAngleToQuaternion(axis, rot_theta);

		if (FlipStance())
		{
			cKinTree::eJointType joint_type = GetJointType(joint_id);
			if (joint_type == cKinTree::eJointTypeSpherical)
			{
				quat = cMathUtil::MirrorQuaternion(quat, cMathUtil::eAxisZ);
			}
		}
		out_theta = cMathUtil::QuatToVec(quat);
	}
#endif
}

cKinTree::eJointType cDeepMimicCharController::GetJointType(int joint_id) const
{
	const cPDController& ctrl = mPDCtrl.GetPDCtrl(joint_id);
	const cSimBodyJoint& joint = ctrl.GetJoint();
	cKinTree::eJointType joint_type = joint.GetType();
	return joint_type;
}

void cDeepMimicCharController::SetPDTargets(const Eigen::VectorXd& targets)
{
	int root_id = mChar->GetRootID();
	int root_size = mChar->GetParamSize(root_id);
	int num_joints = mChar->GetNumJoints();
	int ctrl_offset = GetActionCtrlOffset();

	for (int j = root_id + 1; j < num_joints; ++j)
	{
		if (mPDCtrl.IsValidPDCtrl(j))
		{
			int retarget_joint = RetargetJointID(j);
			int param_offset = mChar->GetParamOffset(retarget_joint);
			int param_size = mChar->GetParamSize(retarget_joint);

			param_offset -= root_size;
			param_offset += ctrl_offset;
			Eigen::VectorXd theta = targets.segment(param_offset, param_size);
			ConvertActionToTargetPose(j, theta);
			mPDCtrl.SetTargetTheta(j, theta);
		}
	}
}

 

cDeepMimicCharController::~cDeepMimicCharController()
{
}

void cDeepMimicCharController::Init(cSimCharacter* character, const std::string& param_file)
{
	//cCharController::Init(character);
	//cController::Init(character);
	mChar = character;
	mMode = eModeActive;
	//--------------------------------
	LoadParams(param_file);
	ResetParams();

	mPosDim = GetPosDim();
	InitResources();

	mValid = true;
}

 
 
void cDeepMimicCharController::Update(double time_step)
{
	//cCharController::Update(time_step);
	//void cDeepMimicCharController::Update(double time_step)
{
	//cController::Update(time_step);
}
	UpdateCalcTau(time_step, mTau);
	UpdateApplyTau(mTau);
}

void cDeepMimicCharController::PostUpdate(double timestep)
{
	mNeedNewAction = CheckNeedNewAction(timestep);
	if (mNeedNewAction)
	{
		NewActionUpdate();
	}
}



void cDeepMimicCharController::UpdateApplyTau(const Eigen::VectorXd& tau)
{
	mTau = tau;
	mChar->ApplyControlForces(tau);
}

void cDeepMimicCharController::SetViewDistMin(double dist)
{
	mViewDistMin = dist;
}

void cDeepMimicCharController::SetViewDistMax(double dist)
{
	mViewDistMax = dist;
}

double cDeepMimicCharController::GetViewDistMin() const
{
	return mViewDistMin;
}

double cDeepMimicCharController::GetViewDistMax() const
{
	return mViewDistMax;
}

void cDeepMimicCharController::GetViewBound(tVector& out_min, tVector& out_max) const
{
	tVector origin = mChar->GetRootPos();
	double max_len = mViewDistMax;
	out_min = origin - tVector(max_len, 0, max_len, 0);
	out_max = origin + tVector(max_len, 0, max_len, 0);
}

double cDeepMimicCharController::GetPrevActionTime() const
{
	return mPrevActionTime;
}

const tVector& cDeepMimicCharController::GetPrevActionCOM() const
{
	return mPrevActionCOM;
}

double cDeepMimicCharController::GetTime() const
{
	return mTime;
}

const Eigen::VectorXd& cDeepMimicCharController::GetTau() const
{
	return mTau;
}

const cCircularBuffer<double>& cDeepMimicCharController::GetValLog() const
{
	return mValLog;
}

void cDeepMimicCharController::LogVal(double val)
{
	mValLog.Add(val);
}

bool cDeepMimicCharController::NeedNewAction() const
{
	return mNeedNewAction;
}



 

void cDeepMimicCharController::RecordGoal(Eigen::VectorXd& out_goal) const
{
	int goal_size = GetGoalSize();
	out_goal = std::numeric_limits<double>::quiet_NaN() * Eigen::VectorXd::Ones(goal_size);
}

eActionSpace cDeepMimicCharController::GetActionSpace() const
{
	return eActionSpaceContinuous;
}

void cDeepMimicCharController::RecordAction(Eigen::VectorXd& out_action) const
{
	out_action = mAction;
}



double cDeepMimicCharController::GetRewardMin() const
{
	return 0;
}

double cDeepMimicCharController::GetRewardMax() const
{
	return 1;
}



 
void cDeepMimicCharController::InitResources()
{
	InitAction();
	InitTau();
}

void cDeepMimicCharController::InitAction()
{
	mAction = Eigen::VectorXd::Zero(GetActionSize());
}

void cDeepMimicCharController::InitTau()
{
	mTau = Eigen::VectorXd::Zero(mChar->GetNumDof());
}

int cDeepMimicCharController::GetPosDim() const
{
	int dim = 3;
	return dim;
}


void cDeepMimicCharController::NewActionUpdate()
{
}

void cDeepMimicCharController::HandleNewAction()
{
	mPrevActionTime = mTime;
	mPrevActionCOM = mChar->CalcCOM();
	mNeedNewAction = false;
}

void cDeepMimicCharController::PostProcessAction(Eigen::VectorXd& out_action) const
{
}


int cDeepMimicCharController::GetStateVelOffset() const
{
	return GetStatePoseOffset() + GetStatePoseSize();
}

//####################################################################################
//####################################################################################
int cDeepMimicCharController::GetNumOptParams() const
{
	return 0;
}

void cDeepMimicCharController::BuildOptParams(Eigen::VectorXd& out_params) const
{
}

void cDeepMimicCharController::SetOptParams(const Eigen::VectorXd& params)
{
}

void cDeepMimicCharController::SetOptParams(const Eigen::VectorXd& params, Eigen::VectorXd& out_params) const
{
	out_params = params;
}

void cDeepMimicCharController::FetchOptParamScale(Eigen::VectorXd& out_scale) const
{
}

void cDeepMimicCharController::OutputOptParams(const std::string& file, const Eigen::VectorXd& params) const
{
	FILE* f = cFileUtil::OpenFile(file, "w");
	if (f != nullptr)
	{
		OutputOptParams(f, params);
		cFileUtil::CloseFile(f);
	}
}

void cDeepMimicCharController::OutputOptParams(FILE* f, const Eigen::VectorXd& params) const
{
}

void cDeepMimicCharController::SetActive(bool active)
{
	SetMode((active) ? eModeActive : eModeInactive);
}

bool cDeepMimicCharController::IsActive() const
{
	return mMode != eModeInactive;
}

void cDeepMimicCharController::SetMode(eMode mode)
{
	mMode = mode;
}

const cSimCharacter* cDeepMimicCharController::GetChar() const
{
	return mChar;
}

