#pragma once

#include <vector>
#include <string>
#include <memory>
#include <functional>

#include "util/MathUtil.h"
#include "util/ArgParser.h"
#include "util/Timer.h"
#include "sim/ActionSpace.h"

class cScene
{
public:
	enum eMode
	{
		eModeTrain,
		eModeTest,
		eModeMax
	};

	enum eTerminate
	{
		eTerminateNull,
		eTerminateFail,
		eTerminateSucc,
		eTerminateMax
	};
	
	virtual int GetNumAgents() const {return 1;};
	virtual bool NeedNewAction(int agent_id) const {return 1;};
	virtual void RecordState(int agent_id, Eigen::VectorXd& out_state) const {};
	virtual void RecordGoal(int agent_id, Eigen::VectorXd& out_goal) const {};
	virtual void SetAction(int agent_id, const Eigen::VectorXd& action) {};

	virtual eActionSpace GetActionSpace(int agent_id) const {return eActionSpaceNull;};
	virtual int GetStateSize(int agent_id) const {return 1;};
	virtual int GetGoalSize(int agent_id) const {return 1;};
	virtual int GetActionSize(int agent_id) const {return 1;};
	virtual int GetNumActions(int agent_id) const {return 1;};

	virtual void BuildStateOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const {};
	virtual void BuildGoalOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const {};
	virtual void BuildActionOffsetScale(int agent_id, Eigen::VectorXd& out_offset, Eigen::VectorXd& out_scale) const {};
	virtual void BuildActionBounds(int agent_id, Eigen::VectorXd& out_min, Eigen::VectorXd& out_max) const {};

	virtual void BuildStateNormGroups(int agent_id, Eigen::VectorXi& out_groups) const {};
	virtual void BuildGoalNormGroups(int agent_id, Eigen::VectorXi& out_groups) const {};

	virtual double CalcReward(int agent_id) const {return 1;};
	virtual double GetRewardMin(int agent_id) const {return 1;};
	virtual double GetRewardMax(int agent_id) const {return 1;};
	//-------------------------------------------------------------------------
	virtual ~cScene();
	
	virtual void ParseArgs(const std::shared_ptr<cArgParser>& parser);
	virtual void Init();
	virtual void Clear();
	virtual void Reset();
	virtual void Update(double timestep);

	virtual void Draw();
	virtual void Keyboard(unsigned char key, double device_x, double device_y);
	virtual void MouseClick(int button, int state, double device_x, double device_y);
	virtual void MouseMove(double device_x, double device_y);
	virtual void Reshape(int w, int h);

	virtual void Shutdown();
	virtual bool IsDone() const;
	virtual double GetTime() const;

	virtual bool HasRandSeed() const;
	virtual void SetRandSeed(unsigned long seed);
	virtual unsigned long GetRandSeed() const;

	//virtual bool IsEpisodeEnd() const;
	virtual bool CheckValidEpisode() const;

	virtual std::string GetName() const = 0;
	//------------------------------------------------------------------------
	virtual double GetRewardFail(int agent_id);
	virtual double GetRewardSucc(int agent_id);

	virtual bool IsEpisodeEnd() const;
	virtual eTerminate CheckTerminate(int agent_id) const;
	virtual void SetSampleCount(int count);
	virtual void SetMode(eMode mode);
	virtual void LogVal(int agent_id, double val) {};
protected:

    eMode mMode;
	int mSampleCount;
	cRand mRand;
	bool mHasRandSeed;
	unsigned long mRandSeed;

	std::shared_ptr<cArgParser> mArgParser;
	cTimer::tParams mTimerParams;
	cTimer mTimer;

	cScene();

	virtual void ResetParams();
	virtual void ResetScene();

	virtual void InitTimers();
	virtual void ResetTimers();
	virtual void UpdateTimers(double timestep);
};